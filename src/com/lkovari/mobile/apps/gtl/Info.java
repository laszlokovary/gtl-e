package com.lkovari.mobile.apps.gtl;



import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.os.BatteryManager;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.lkovari.mobile.apps.gtl.settings.GPSSettings;
import com.lkovari.mobile.apps.gtl.utils.EmailUtils;


/**
 * 
 * @author lkovari
 *
 */
public class Info extends Activity {
	private TextView batteryLevelTitleTextView;
	private TextView batteryLevelValueTextView;
	private TextView deviceIDTitleTextView;
	private TextView deviceIDTitleValueTextView;
	private boolean isBatteryChangeReceiversRegistered = false;
	
	/**
	 * 18/06/2014 #9 v1.02.6
	 * @author lkovari
	 *
	 */
	private class SendEmailAsyncTask extends AsyncTask<String, Void, Boolean> {

		@Override
		protected Boolean doInBackground(String... params) {
			Boolean isOK = null;
			String emailAddress = (String)params[0];
			if (emailAddress != null) {
				try {
		    		EmailUtils.sendTrackingPageInEmail(Info.this, emailAddress);
					isOK = true;
				}
				catch (Exception e) {
					isOK = false;
				}
			}
			return isOK;
		}
		
		@Override
		protected void onPostExecute(Boolean isOk) {
			if (!isOk) {
				Toast.makeText(Info.this, R.string.followme_error_send_failed, Toast.LENGTH_LONG).show();
			}
		}
	}
	
	/**
	 * 
	 */
	private final BroadcastReceiver batteryChangedIntentReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
	        if (Intent.ACTION_BATTERY_CHANGED.equals(intent.getAction())) {
	            int batteryLevel = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, 0);
	            double batteryVoltage = intent.getIntExtra(BatteryManager.EXTRA_VOLTAGE, 0) / 1000;
	            double batteryTemperature = intent.getIntExtra(BatteryManager.EXTRA_TEMPERATURE, 0) / 10;
	            
	             int status = intent.getIntExtra(BatteryManager.EXTRA_STATUS, BatteryManager.BATTERY_STATUS_UNKNOWN);
	             String batteryStatus = null;
	             if (status == BatteryManager.BATTERY_STATUS_CHARGING){
	            	 //# 123 09/08/2013
	                 batteryStatus = context.getResources().getString(R.string.about_battery_status_text_charging); 
	             } else if (status == BatteryManager.BATTERY_STATUS_DISCHARGING){
	                 batteryStatus = context.getResources().getString(R.string.about_battery_status_text_discharging);
	             } else if (status == BatteryManager.BATTERY_STATUS_NOT_CHARGING){
	                 batteryStatus = context.getResources().getString(R.string.about_battery_status_text_notcharging);
	             } else if (status == BatteryManager.BATTERY_STATUS_FULL){
	                 batteryStatus = context.getResources().getString(R.string.about_battery_status_text_full);
	             } else {
	                 batteryStatus = "N/A";
	             }
	            
	             int health = intent.getIntExtra(BatteryManager.EXTRA_HEALTH, BatteryManager.BATTERY_HEALTH_UNKNOWN);
	             String batteryHealth = null;
	             if (health == BatteryManager.BATTERY_HEALTH_GOOD){
	            	 //# 123 09/08/2013
	            	 batteryHealth = context.getResources().getString(R.string.about_battery_health_text_good);
	             } else if (health == BatteryManager.BATTERY_HEALTH_OVERHEAT){
	            	 batteryHealth = context.getResources().getString(R.string.about_battery_health_text_overheat);
	             } else if (health == BatteryManager.BATTERY_HEALTH_DEAD){
	            	 batteryHealth = context.getResources().getString(R.string.about_battery_health_text_dead);
	             } else if (health == BatteryManager.BATTERY_HEALTH_OVER_VOLTAGE){
	            	 batteryHealth = context.getResources().getString(R.string.about_battery_health_text_overvoltage);
	             } else if (health == BatteryManager.BATTERY_HEALTH_UNSPECIFIED_FAILURE){
	            	 batteryHealth = context.getResources().getString(R.string.about_battery_health_text_unspecified);
	             } else{
	            	 batteryHealth = "N/A";
	             }
	             
	             if ((batteryStatus != null) || (batteryHealth != null)) {
	            	 //# 123 09/08/2013
	            	 batteryLevelTitleTextView.setText(context.getResources().getString(R.string.about_batterylevel_text_title) + ": " + batteryHealth + ", " + batteryStatus);
	             }
	            // always greater then -1
	            if (batteryLevel > -1) { 
	            	batteryLevelValueTextView.setText(" " + batteryLevel + "% " + String.format("%.02fV", batteryVoltage) + " " + String.format("%.02fC", batteryTemperature));
	            }	
	        }
		}
		
	};
	
	
	private void displayVersionName() {
		/*
	    String versionName = "";
	    String versionCode = "";
	    PackageInfo packageInfo;
	    try {
	        packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
	        versionName = "v " + packageInfo.versionName;
	        versionCode = " #" + packageInfo.versionCode;
	        
	    } catch (NameNotFoundException e) {
	    	versionName = "-";
	    	versionCode = "-";
	        e.printStackTrace();
	    }
	    */
	    TextView tv = (TextView) findViewById(R.id.aboutVersionLabelValueTextView);
	    tv.setText(GPSSettings.VERSION);
	}	
	
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.info);
		// fix orientation
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);		

		batteryLevelTitleTextView = (TextView)findViewById(R.id.aboutBatteryLabelTextView);
		batteryLevelValueTextView = (TextView)findViewById(R.id.aboutBatteryLabelValueTextView);
		//23/03/2014 #9 v1.02.6
		deviceIDTitleTextView = (TextView)findViewById(R.id.aboutdeviceidtitleTextView);
		deviceIDTitleValueTextView = (TextView)findViewById(R.id.aboutdeviceidvalueTextView);
		deviceIDTitleValueTextView.setText(GPSSettings.DEVICE_ID);
		
		deviceIDTitleValueTextView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
		    	// 17/08/2014 #9 v1.02.6
		    	if (GPSSettings.IS_ENABLE_REALTIME_POSITION_SENDING) {
					AlertDialog.Builder builder = new AlertDialog.Builder(Info.this);
					builder.setTitle(Info.this.getResources().getString(R.string.followme_title_ask_emailaddress));
					final EditText input = new EditText(Info.this);
					input.setInputType(InputType.TYPE_CLASS_TEXT);
					builder.setView(input);

					// Set up the buttons
					builder.setPositiveButton(Info.this.getResources().getString(R.string.followme_positive) , new DialogInterface.OnClickListener() { 
					    @Override
					    public void onClick(DialogInterface dialog, int which) {
				    		String recipientAddress = input.getText().toString();
				    		if (recipientAddress != "") {
				    			//EmailUtils.sendTrackingPageInEmail(Info.this, recipientAddress);
				    			SendEmailAsyncTask sendEmailAsyncTask = new SendEmailAsyncTask();
				    			sendEmailAsyncTask.execute(recipientAddress);
				    		}
					    }
					});
					builder.setNegativeButton(Info.this.getResources().getString(R.string.followme_negative) , new DialogInterface.OnClickListener() {
					    @Override
					    public void onClick(DialogInterface dialog, int which) {
					        dialog.cancel();
					    }
					});
					builder.show();
		    	}
			}
		});
		
		// 01/08/2013
		displayVersionName();
		
        /*
        ImageView imageView = (ImageView) findViewById(R.id.logoImageView);
        imageView.setImageResource(R.drawable.icon);

        TextView appnameTextView = (TextView) findViewById(R.id.aboutApplicationLabelValueTextView);
        appnameTextView.setText(R.string.about_application_label_value);

        TextView descriptionTextView = (TextView) findViewById(R.id.aboutDescriptionLabelValueTextView);
        descriptionTextView.setText(R.string.about_description_label_value);

        TextView authorTextView = (TextView) findViewById(R.id.aboutAuthorLabelValueTextView);
        authorTextView.setText(R.string.about_author_label_value);
        */
    }
    
    
    @Override
    protected void onResume() {
		  IntentFilter intentToReceiveFilter = new IntentFilter();
		  intentToReceiveFilter.addAction(Intent.ACTION_BATTERY_CHANGED);
//		  this.registerReceiver(batteryChangedIntentReceiver, intentToReceiveFilter, null, handler);
		  this.registerReceiver(batteryChangedIntentReceiver, intentToReceiveFilter);
		  isBatteryChangeReceiversRegistered = true;
    	
    	super.onResume();
    }
    
    @Override
    protected void onPause() {
		if(isBatteryChangeReceiversRegistered) {
			unregisterReceiver(batteryChangedIntentReceiver);
			isBatteryChangeReceiversRegistered = false;
		}
    	super.onPause();
    }
    
}
