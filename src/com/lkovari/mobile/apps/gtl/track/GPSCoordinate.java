package com.lkovari.mobile.apps.gtl.track;

import android.location.Location;

import com.lkovari.mobile.apps.gtl.map.filter.LocPts;
import com.lkovari.mobile.apps.gtl.utils.Utils;


/**
 * 
 * @author lkovari
 *
 */
public class GPSCoordinate implements LocPts {
	Location location = null;
	private float avgSpeed;
	private double odometer;
	private long elapsedTime;
	// X is the force from left to right of phone or back
	private double accelerationx;
	// Y is the force from bottom to top of phone or back
	private double accelerationy;
	// Z is the force from back to screen of phone or back
	private double accelerationz;
	private String accelerationTo = null;
	private double leftAngle;
	private double rightAngle;
	// rotate left down right up
	private double pitch;
	// rotate left or right
	private double azimuth;
	// rotate bottom down and top up
	private double roll;
	private String address;
	private int satellites = 0;
	private double averageStrength = 0.0;
	private boolean isPlacemark = false;
	// #5 10/12/2013
	private long elapsedTimeInMove = 0;
	private long elapsedTimeInWait = 0;
	// #13 1.02.10 2015.11.29.
	private float ambientTemperature = 0.0f;
	
	public static class Builder {
		Location location = null;
		private float avgSpeed;
		private double odometer;
		private long elapsedTime;
		// X is the force from left to right of phone or back
		private double accelerationx;
		// Y is the force from bottom to top of phone or back
		private double accelerationy;
		// Z is the force from back to screen of phone or back
		private double accelerationz;
		private String accelerationTo = null;
		private double leftAngle;
		private double rightAngle;
		// rotate left down right up
		private double pitch;
		// rotate left or right
		private double azimuth;
		// rotate bottom down and top up
		private double roll;
		private int satellites = 0;
		private double averageStrength = 0.0;
		private boolean isPlacemark = false;
		// #13 1.02.10 2015.11.29.
		private float ambientTemperature = 0.0f;

		public Builder(Location loc, boolean isPlaceMark) {
			this.location = loc;
			this.isPlacemark = isPlaceMark;
		}
		
		public Builder averageSpeed(float value) {
			this.avgSpeed = value;
			return this;
		}

		public Builder odometer(double value) {
			this.odometer = value;
			return this;
		}

		public Builder accelerationX(float value) {
			this.accelerationx = Utils.roundTo(value, 2);
			return this;
		}

		public Builder accelerationY(float value) {
			this.accelerationy = Utils.roundTo(value, 2);
			return this;
		}
		
		public Builder accelerationZ(float value) {
			this.accelerationz = Utils.roundTo(value, 2);
			return this;
		}
		
		public Builder accelerationTo(String value) {
			this.accelerationTo = value == null ? null : new String(value);
			return this;
		}
		
		public Builder leftAngle(double value) {
			this.leftAngle = Utils.roundTo(value, 1);
			return this;
		}
		
		public Builder rightAngle(double value) {
			this.rightAngle = Utils.roundTo(value, 1);
			return this;
		}
	
		public Builder pitch(double value) {
			this.pitch = Utils.roundTo(value, 2);
			return this;
		}

		public Builder azimuth(double value) {
			this.azimuth = Utils.roundTo(value, 2);
			return this;
		}
		
		public Builder roll(double value) {
			this.roll = Utils.roundTo(value, 2);
			return this;
		}

		public Builder satellites(int value) {
			this.satellites = value;
			return this;
		}
		
		public Builder elapsedTime(long value) {
			this.elapsedTime = value;
			return this;
		}
		
		public Builder averageStrength(double value) {
			this.averageStrength = value;
			return this;
		}
		
		public Builder ambientTemperature(float value) {
			this.ambientTemperature = value;
			return this;
		}
		
		public Builder placemark(boolean value) {
			this.isPlacemark = value;
			return this;
		}
		
		public GPSCoordinate build() {
			return new GPSCoordinate(this);
		}
	}

	public GPSCoordinate(Builder builder) {
		this.location = builder.location;
		this.avgSpeed = builder.avgSpeed;
		this.odometer = builder.odometer;
		this.accelerationx = builder.accelerationx;
		this.accelerationy = builder.accelerationy;
		this.accelerationz = builder.accelerationz;
		this.accelerationTo = builder.accelerationTo;
		this.leftAngle = builder.leftAngle;
		this.rightAngle = builder.rightAngle;
		this.pitch = builder.pitch;
		this.azimuth = builder.azimuth;
		this.roll = builder.roll;
		this.isPlacemark = builder.isPlacemark;
		this.satellites = builder.satellites;
		this.elapsedTime = builder.elapsedTime;
		this.averageStrength = builder.averageStrength;
		this.ambientTemperature = builder.ambientTemperature;
	}
	
	public GPSCoordinate(Location loc, float aspd, double odo, double accx, double accy, double accz, String accto, double lang, double rang, double pit, double azi, double ro, int sats, long elapsed, double strength, float ambientTemp, boolean isPlaceMark) {
		this.location = loc;
		this.avgSpeed = aspd;
		this.odometer = odo;
		this.accelerationx = Utils.roundTo(accx, 2);
		this.accelerationy = Utils.roundTo(accy, 2);
		this.accelerationz = Utils.roundTo(accz, 2);
		if (accto != null)
			this.accelerationTo = new String(accto);
		else
			this.accelerationTo = null;
		this.leftAngle = Utils.roundTo(lang, 1);
		this.rightAngle = Utils.roundTo(rang, 1);
		this.pitch = Utils.roundTo(pit, 2);
		this.azimuth = Utils.roundTo(azi, 2);
		this.roll = Utils.roundTo(ro, 2);
		this.isPlacemark = isPlaceMark;
		this.satellites = sats;
		this.elapsedTime = elapsed;
		this.averageStrength = strength;
		// #13 1.02.10 2015.11.29.
		this.ambientTemperature = ambientTemp;
	}
	
	public float getAvgSpeed() {
		return avgSpeed;
	}
	
	public double getOdometer() {
		return odometer;
	}
	
	public double getAccelerationx() {
		return Utils.roundTo(accelerationx, 3);
	}

	public double getAccelerationy() {
		return Utils.roundTo(accelerationy, 3);
	}

	public double getAccelerationz() {
		return Utils.roundTo(accelerationz, 3);
	}
	
	public String getAccelerationTo() {
		return accelerationTo;
	}
	
	public double getLeftAngle() {
		return leftAngle;
	}
	
	public double getRightAngle() {
		return rightAngle;
	}
	
	public double getPitch() {
		return Utils.roundTo(pitch, 2);
	}
	
	public double getAzimuth() {
		return Utils.roundTo(azimuth, 2);
	}

	public double getRoll() {
		return Utils.roundTo(roll, 2);
	}
	
	public double getGForce() {
		return Utils.roundTo(Math.sqrt(accelerationx * accelerationx + accelerationy * accelerationy + accelerationz * accelerationz), 2);
	}
	
	public boolean isPlacemark() {
		return isPlacemark;
	}

	
	public String getAddress() {
		return address;
	}
	
	public void setAddress(String address) {
		this.address = address;
	}
	
	
	public GPSCoordinate(Location loc) {
		this.location = loc;
	}

	
	public long getElapsedTime() {
		return elapsedTimeInMove + elapsedTimeInWait;
	}
	
	public Location getLocation() {
		return location;
	}

	public int getSatellites() {
		return satellites;
	}

	public double getAverageStrength() {
		return averageStrength;
	}
	
	public double getLatitudeE6() {
		return this.location.getLatitude() * 1E6;
	}

	public double getLongitudeE6() {
		return this.location.getLongitude() * 1E6;
	}
	
	@Override
	public float distanceSquared(LocPts otherLocPts) {
        return Math.round(((this.location.getLatitude() - ((GPSCoordinate)otherLocPts).getLocation().getLatitude()) * (this.location.getLatitude() - ((GPSCoordinate)otherLocPts).getLocation().getLatitude()) + (this.location.getLongitude() - ((GPSCoordinate)otherLocPts).getLocation().getLongitude()) * (this.location.getLongitude() - ((GPSCoordinate)otherLocPts).getLocation().getLongitude())));        
	}

	@Override
	public float length() {
        return Math.round(Math.sqrt(this.location.getLatitude() * this.location.getLatitude() + this.location.getLongitude() * this.location.getLongitude()));        
	}

	@Override
	public LocPts minus(LocPts locPts) {
		this.location.setLatitude(this.location.getLatitude() - ((GPSCoordinate)locPts).getLocation().getLatitude());
		this.location.setLongitude(this.location.getLongitude() - ((GPSCoordinate)locPts).getLocation().getLongitude());
    	return duplicateGPSCoordinate(this);
	}

	@Override
	public LocPts plus(LocPts locPts) {
		this.location.setLatitude(this.location.getLatitude() + ((GPSCoordinate)locPts).getLocation().getLatitude());
		this.location.setLongitude(this.location.getLongitude() + ((GPSCoordinate)locPts).getLocation().getLongitude());
    	return duplicateGPSCoordinate(this);
	}

	@Override
	public float dot(LocPts locPts) {
    	return Math.round(((this.location.getLatitude() * ((GPSCoordinate)locPts).getLocation().getLatitude()) + (this.location.getLongitude() * ((GPSCoordinate)locPts).getLocation().getLongitude())));  
	}

	@Override
	public LocPts times(float a) {
    	location.setLatitude(this.location.getLatitude() * a);
    	location.setLongitude(this.location.getLongitude() * a);
    	return duplicateGPSCoordinate(this);
	}

	//  int sats, boolean isPlaceMark
	public GPSCoordinate duplicateGPSCoordinate(GPSCoordinate source) {
		GPSCoordinate target = new GPSCoordinate(source.location, source.getAvgSpeed(), source.getOdometer(), source.getAccelerationx(), source.getAccelerationy(), source.getAccelerationz(), source.getAccelerationTo(), source.getLeftAngle(), source.getRightAngle(), source.getPitch(), source.getAzimuth(), source.getRoll(), source.getSatellites(), source.getElapsedTime(), source.getAverageStrength(), source.getAmbientTemperature(), source.isPlacemark());
		// #5 10/20/2013
		target.setElapsedTimeInMove(source.getElapsedTimeInMove());
		target.setElapsedTimeInWait(source.getElapsedTimeInWait());
		return target;
	}
	
	/**
	 * #5 10/12/2013
	 * @return
	 */
	public long getElapsedTimeInMove() {
		return elapsedTimeInMove;
	}

	/**
	 * #5 10/12/2013
	 * @return
	 */
	public long getElapsedTimeInWait() {
		return elapsedTimeInWait;
	}
	
	/**
	 * #5 10/12/2013
	 * @param milis
	 */
	public void setElapsedTimeInWait(long elapsedTimeInWait) {
		this.elapsedTimeInWait = elapsedTimeInWait;
	}
	
	/**
	 * #5 10/12/2013
	 * @param milis
	 */
	public void setElapsedTimeInMove(long elapsedTimeInMove) {
		this.elapsedTimeInMove = elapsedTimeInMove;
	}

	public float getAmbientTemperature() {
		return ambientTemperature;
	}
	
	public void setAmbientTemperature(float ambientTemperature) {
		this.ambientTemperature = ambientTemperature;
	}
}

