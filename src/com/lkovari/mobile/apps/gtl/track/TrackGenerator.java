package com.lkovari.mobile.apps.gtl.track;

import java.util.Date;
import java.util.List;

import android.content.Context;

import com.lkovari.mobile.apps.gtl.R;
import com.lkovari.mobile.apps.gtl.maxvalues.MaxAccelerationValue;
import com.lkovari.mobile.apps.gtl.maxvalues.MaxAltitudeValue;
import com.lkovari.mobile.apps.gtl.maxvalues.MaxOrientationValue;
import com.lkovari.mobile.apps.gtl.maxvalues.MaxSpeedValue;
import com.lkovari.mobile.apps.gtl.maxvalues.MinMaxTemperatureValue;
import com.lkovari.mobile.apps.gtl.settings.GPSSettings;
import com.lkovari.mobile.apps.gtl.settings.UsageType;
import com.lkovari.mobile.apps.gtl.utils.MeasurementKind;
import com.lkovari.mobile.apps.gtl.utils.MeasurementManager;
import com.lkovari.mobile.apps.gtl.utils.Utils;
import com.lkovari.mobile.apps.gtl.utils.error.ErrorHandler;


/**
 * 
 * @author lkovari
 *
 */
public class TrackGenerator {
	private TrackFormat trackFormat = TrackFormat.KML;
	// 1000 feet
	private int range = 304;
	private GPSCoordinate lookAtCoordinate;
	private int lineWidth = 7;
	private String lineColor = "ff0000ff";
	private Context context = null;
	
	//max values
	private MaxSpeedValue maxSpeedValue = null;
	private MaxAltitudeValue maxAltitudeValue = null;
	private MaxAccelerationValue maxAccelerationValue = null;
	private MaxOrientationValue maxOrientationValue = null;
	// #13 1.02.10 2015.11.26 
	private MinMaxTemperatureValue minMaxTemperatureValue = null;
	
	/**
	 * 
	 * @param lookAt
	 * @param trackFormat
	 * @param lineColor
	 * @param lineWidth
	 * @param range
	 */
	public TrackGenerator(Context context, GPSCoordinate lookAt, TrackFormat trackFormat, String lineColor, Integer lineWidth, Integer range) {
		this.lookAtCoordinate = lookAt;
		this.trackFormat = trackFormat;
		if (lineColor != null)
			this.lineColor = lineColor;
		if (lineWidth != null)
			this.lineWidth = lineWidth;
		if (range != null)
			this.range = range;
		this.context = context;
	}

	public void setupMaxValues(MaxSpeedValue maxSpeed, MaxAltitudeValue maxAltitude, MaxAccelerationValue maxAcceleration, MaxOrientationValue maxOrientation, MinMaxTemperatureValue minMaxTemp) {
		this.maxSpeedValue = maxSpeed;
		this.maxAltitudeValue = maxAltitude;
		this.maxAccelerationValue = maxAcceleration;
		this.maxOrientationValue = maxOrientation;
		this.minMaxTemperatureValue = minMaxTemperatureValue;
	}
	
	public GPSCoordinate getLookAtCoordinate() {
		return lookAtCoordinate;
	}
	
	public TrackFormat getTrackFormat() {
		return trackFormat;
	}
	
	public int getRange() {
		return range;
	}
	
	/**
	 * 
	 * @param docName
	 * @return
	 */
	public String createKMLHeaderOpen(String docName) {
		String kmlHeader = "";
		kmlHeader = KMLTags.XML_NS + KMLTags.CRLF;
		kmlHeader += KMLTags.KML_OPEN + KMLTags.CRLF;
		kmlHeader += KMLTags.DOCUMENT_OPEN + KMLTags.CRLF;
		kmlHeader += KMLTags.NAME_OPEN + docName + KMLTags.NAME_CLOSE + KMLTags.CRLF;
		kmlHeader += KMLTags.VISIBILITY_OPEN + "1" + KMLTags.VISIBILITY_CLOSE + KMLTags.CRLF;
		return kmlHeader;
	}

	/**
	 * 
	 * @param optimizationPercent
	 * @param optimizedPts
	 * @param totalPts
	 * @return
	 */
	public String createKMLHeaderClose(int optimizedPts, int totalPts) {
		String kmlBuffer = KMLTags.DOCUMENT_CLOSE + KMLTags.CRLF;
		kmlBuffer += KMLTags.KML_CLOSE + KMLTags.CRLF;
		kmlBuffer += KMLTags.REMARK_OPEN + "T#" + GPSSettings.DOUGLAS_PEUCKER_TOLARANCE + " " + optimizedPts + "/" + totalPts  + KMLTags.REMARK_CLOSE; 
		kmlBuffer += KMLTags.REMARK_OPEN + KMLTags.DATA_AUTHOR_COPYRIGHT + " " + KMLTags.DATA_AUTHOR_NAME + " " + KMLTags.DATA_AUTHOR_EMAIL +  " " + KMLTags.DATA_DEVELOPER_VERSION + KMLTags.REMARK_CLOSE; 
		return kmlBuffer;
	}
	
	/**
	 * 
	 * @param kmlBuffer
	 * @param coordinate
	 * @param range
	 * @return
	 */
	public String createView(GPSCoordinate coordinate, int range) {
		String kmlBuffer = KMLTags.LOOKAT_OPEN + KMLTags.LONGITUDE_OPEN + coordinate.getLocation().getLongitude() + KMLTags.LONGITUDE_CLOSE + KMLTags.LATTITUDE_OPEN + coordinate.getLocation().getLatitude() + KMLTags.LATTITUDE_CLOSE + KMLTags.CRLF;
		kmlBuffer += KMLTags.RANGE_OPEN + range + KMLTags.RANGE_CLOSE + KMLTags.CRLF;
		kmlBuffer += KMLTags.LOOKAT_CLOSE + KMLTags.CRLF;
		return kmlBuffer;
	}

	/**
	 * 
	 * @param kmlBuffer
	 * @param lineColor
	 * @param lineWidth
	 * @return
	 */
	public String createLineStyle(String lineColor, int lineWidth) {
		String kmlBuffer = KMLTags.STYLE_OPEN + KMLTags.CRLF;
		kmlBuffer += KMLTags.LINESTYLE_OPEN + KMLTags.CRLF;
		kmlBuffer += KMLTags.COLOR_OPEN + lineColor + KMLTags.COLOR_CLOSE + KMLTags.CRLF;
		kmlBuffer += KMLTags.WIDTH_OPEN + lineWidth + KMLTags.WIDTH_CLOSE + KMLTags.CRLF;
		kmlBuffer += KMLTags.LINESTYLE_CLOSE + KMLTags.CRLF;
		kmlBuffer += KMLTags.STYLE_CLOSE + KMLTags.CRLF;
		return kmlBuffer;
	}
	
	/**
	 * 
	 * @param kmlBuffer
	 * @param logName
	 * @param placemarkName
	 * @return
	 */
	public String createTracklogOpen(String logName) {
		String kmlBuffer = KMLTags.FOLDER_OPEN + KMLTags.NAME_OPEN + logName + KMLTags.NAME_CLOSE + KMLTags.CRLF;
		kmlBuffer += KMLTags.VISIBILITY_OPEN + "1" + KMLTags.VISIBILITY_CLOSE + KMLTags.CRLF;
		return kmlBuffer;
	}

	public String createPlacemarkOpen(String placemarkName) {
		return  KMLTags.PLACEMARK_OPEN + KMLTags.NAME_OPEN + placemarkName + KMLTags.NAME_CLOSE + KMLTags.CRLF;
	}

	public String createPlacemarkClose() {
		return  KMLTags.PLACEMARK_CLOSE + KMLTags.CRLF;
	}
	
	/**
	 * 
	 * @param kmlBuffer
	 * @return
	 */
	public String createTracklogClose() {
		return KMLTags.FOLDER_CLOSE + KMLTags.CRLF;
	}

	/**
	 * 
	 * @param kmlBuffer
	 * @return
	 */
	public String createLineStringOpen() {
		String kmlBuffer = KMLTags.LINESTRING_OPEN + KMLTags.CRLF;
		kmlBuffer += KMLTags.COORDINATES_OPEN + KMLTags.CRLF;
		return kmlBuffer;
	}

	/**
	 * 
	 * @param kmlBuffer
	 * @return
	 */
	public String createLineStringClose() {
		String kmlBuffer = KMLTags.COORDINATES_CLOSE + KMLTags.CRLF;
		kmlBuffer += KMLTags.LINESTRING_CLOSE + KMLTags.CRLF;
		return kmlBuffer;
	}

	/**
	 * 
	 * @param kmlBuffer
	 * @param coordinate
	 * @return
	 */
	public String createCoordinate(GPSCoordinate coordinate) {
		String lonlatalt = "	" + coordinate.getLocation().getLongitude() + "," + coordinate.getLocation().getLatitude() + "," + coordinate.getLocation().getAltitude() + KMLTags.CRLF;
		return lonlatalt;
	}

	
	/**
	 * 
	 * @return
	 */
	public String createBallon() {
		String kmlBuffer = KMLTags.STYLE_OPEN_WITH_ID + KMLTags.CRLF; 
		kmlBuffer += KMLTags.BALLON_OPEN + KMLTags.TEXT_OPEN + KMLTags.CDATA_OPEN + KMLTags.CRLF;
		kmlBuffer += KMLTags.CDATA_TIME + KMLTags.CRLF;
		kmlBuffer += KMLTags.CDATA_ELAPSEDTIME + KMLTags.CRLF;
		// #5 10/20/2013
		kmlBuffer += KMLTags.CDATA_ELAPSEDTIME_INMOVE + KMLTags.CRLF;
		kmlBuffer += KMLTags.CDATA_ELAPSEDTIME_INWAIT + KMLTags.CRLF;
		kmlBuffer += KMLTags.CDATA_BEARING + KMLTags.CRLF;
		// 12/25/2012 if not pedestrian and water craft include Altitude
		if (!GPSSettings.USAGE_TYPE.equals(UsageType.PEDESTRIAN) && !GPSSettings.USAGE_TYPE.equals(UsageType.WATERCRAFTS))
			kmlBuffer += KMLTags.CDATA_ALTITUDE + KMLTags.CRLF;
		kmlBuffer += KMLTags.CDATA_SPEED + KMLTags.CRLF;
		kmlBuffer += KMLTags.CDATA_ODOMETER + KMLTags.CRLF;
		//12/15/2012
		switch (GPSSettings.USAGE_TYPE) {
		case AIRCRAFT : {
			kmlBuffer += KMLTags.CDATA_ACCELERATIONX + KMLTags.CRLF;
			kmlBuffer += KMLTags.CDATA_ACCELERATIONY + KMLTags.CRLF;
			kmlBuffer += KMLTags.CDATA_ACCELERATIONZ + KMLTags.CRLF;
//			kmlBuffer += KMLTags.CDATA_ACCELERATIONTO + KMLTags.CRLF;
			kmlBuffer += KMLTags.CDATA_PITCH + KMLTags.CRLF;
			kmlBuffer += KMLTags.CDATA_AZIMUTH + KMLTags.CRLF;
			kmlBuffer += KMLTags.CDATA_ROLL + KMLTags.CRLF;
			break;
		}
		case WATERCRAFTS : {
			kmlBuffer += KMLTags.CDATA_ACCELERATIONX + KMLTags.CRLF;
			kmlBuffer += KMLTags.CDATA_ACCELERATIONY + KMLTags.CRLF;
			kmlBuffer += KMLTags.CDATA_ACCELERATIONZ + KMLTags.CRLF;
//			kmlBuffer += KMLTags.CDATA_ACCELERATIONTO + KMLTags.CRLF;
			kmlBuffer += KMLTags.CDATA_PITCH + KMLTags.CRLF;
			kmlBuffer += KMLTags.CDATA_AZIMUTH + KMLTags.CRLF;
			kmlBuffer += KMLTags.CDATA_ROLL + KMLTags.CRLF;
			break;
		}
		case FOUR_WHEELERS : {
			kmlBuffer += KMLTags.CDATA_ACCELERATIONX + KMLTags.CRLF;
			kmlBuffer += KMLTags.CDATA_ACCELERATIONY + KMLTags.CRLF;
			kmlBuffer += KMLTags.CDATA_ACCELERATIONZ + KMLTags.CRLF;
//			kmlBuffer += KMLTags.CDATA_ACCELERATIONTO + KMLTags.CRLF;
			kmlBuffer += KMLTags.CDATA_PITCH + KMLTags.CRLF;
			kmlBuffer += KMLTags.CDATA_AZIMUTH + KMLTags.CRLF;
			break;
		}
		case TWO_WHEELERS : {
			kmlBuffer += KMLTags.CDATA_ACCELERATIONX + KMLTags.CRLF;
			kmlBuffer += KMLTags.CDATA_ACCELERATIONY + KMLTags.CRLF;
			kmlBuffer += KMLTags.CDATA_ACCELERATIONZ + KMLTags.CRLF;
//			kmlBuffer += KMLTags.CDATA_ACCELERATIONTO + KMLTags.CRLF;
			kmlBuffer += KMLTags.CDATA_LEFTANGLE + KMLTags.CRLF;
			kmlBuffer += KMLTags.CDATA_RIGHTANGLE + KMLTags.CRLF;
			kmlBuffer += KMLTags.CDATA_PITCH + KMLTags.CRLF;
			kmlBuffer += KMLTags.CDATA_AZIMUTH + KMLTags.CRLF;
			break;
		}
		case WALKING_HIKE : {
			break;
		}
		case PEDESTRIAN : {
			break;
		}
		}

		//#13 1.02.10 2015.11.29.
		if (android.os.Build.VERSION.SDK_INT >= 14) {
			kmlBuffer += KMLTags.CDATA_TEMPERATURE + KMLTags.CRLF;
		}
		
		kmlBuffer += KMLTags.CDATA_SATELLITES + KMLTags.CRLF;
		kmlBuffer += KMLTags.CDATA_ACCURACY + KMLTags.CRLF;
		kmlBuffer += KMLTags.CDATA_ADDRESS + KMLTags.CRLF;
		kmlBuffer += KMLTags.CDATA_CLOSE + KMLTags.TEXT_CLOSE + KMLTags.BALLON_CLOSE + KMLTags.CRLF;
		kmlBuffer += KMLTags.STYLE_CLOSE + KMLTags.CRLF;
		return kmlBuffer;
	}

	/**
	 * 
	 */
	public String createIconDefinitionsForPlacemarks() {
		String kmlBuffer = "";
		kmlBuffer += KMLTags.PLACEMARK_STYLE_START + KMLTags.CRLF; 
		kmlBuffer += KMLTags.PLACEMARK_ICONSTYLE_START + KMLTags.CRLF;
		kmlBuffer += KMLTags.PLACEMARK_HREF_START + KMLTags.CRLF;
		kmlBuffer += KMLTags.ICONSTYLE_CLOSE + KMLTags.CRLF;
		kmlBuffer += KMLTags.STYLE_CLOSE + KMLTags.CRLF;

		kmlBuffer += KMLTags.PLACEMARK_STYLE_STANDARD + KMLTags.CRLF; 
		kmlBuffer += KMLTags.PLACEMARK_ICONSTYLE_STANDARD + KMLTags.CRLF;
		kmlBuffer += KMLTags.PLACEMARK_HREF_STANDARD + KMLTags.CRLF;
		kmlBuffer += KMLTags.ICONSTYLE_CLOSE + KMLTags.CRLF;
		kmlBuffer += KMLTags.STYLE_CLOSE + KMLTags.CRLF;
		
		kmlBuffer += KMLTags.PLACEMARK_STYLE_PAUSE + KMLTags.CRLF; 
		kmlBuffer += KMLTags.PLACEMARK_ICONSTYLE_PAUSE + KMLTags.CRLF;
		kmlBuffer += KMLTags.PLACEMARK_HREF_PAUSE + KMLTags.CRLF;
		kmlBuffer += KMLTags.ICONSTYLE_CLOSE + KMLTags.CRLF;
		kmlBuffer += KMLTags.STYLE_CLOSE + KMLTags.CRLF;
		
		kmlBuffer += KMLTags.PLACEMARK_STYLE_STOP + KMLTags.CRLF; 
		kmlBuffer += KMLTags.PLACEMARK_ICONSTYLE_STOP + KMLTags.CRLF;
		kmlBuffer += KMLTags.PLACEMARK_HREF_STOP + KMLTags.CRLF;
		kmlBuffer += KMLTags.ICONSTYLE_CLOSE + KMLTags.CRLF;
		kmlBuffer += KMLTags.STYLE_CLOSE + KMLTags.CRLF;
		return kmlBuffer;
	}

	/**
	 * 
	 * @param name
	 * @param placemarkIconStyleKind
	 * @return
	 */
	public String createExtraDataOpen(String name, PlacemarkIconStyleKind placemarkIconStyleKind) {
		String kmlBuffer = KMLTags.PLACEMARK_OPEN + KMLTags.CRLF;
		kmlBuffer += KMLTags.NAME_OPEN + name + KMLTags.NAME_CLOSE + KMLTags.CRLF;
		String styleURLContent = "#placemark_standard";
		switch (placemarkIconStyleKind) {
		case START : {
			styleURLContent = "#placemark_start";
			break;
		}
		case PAUSE : {
			styleURLContent = "#placemark_pause";
			break;
		}
		case MOVING : {
			styleURLContent = "#placemark_standard";
			break;
		}
		case END : {
			styleURLContent = "#placemark_stop";
			break;
		}
		}
		kmlBuffer += KMLTags.STYLEURL_OPEN + "#telemetry" + KMLTags.STYLEURL_CLOSE + KMLTags.CRLF;
		kmlBuffer += KMLTags.STYLEURL_OPEN + styleURLContent + KMLTags.STYLEURL_CLOSE + KMLTags.CRLF;
		kmlBuffer += KMLTags.EXTENDEDDATA_OPEN + KMLTags.CRLF;
		return kmlBuffer;
	}
	
	public String createExtraData(GPSCoordinate gpsCoordinate, boolean isLast) {
		String maxLAngle = "";
		String maxRAngle = "";
		String maxSpeed = "";
		String maxAltitude = "";
		String maxAccelX = "";
		String maxAccelY = "";
		String maxAccelZ = "";
		String minMaxTemperature = "";
		Date time = new Date(gpsCoordinate.getLocation().getTime());
		String kmlBuffer = null;
		try {
			if (isLast) {
//				double maxkmph = (maxSpeedValue.getMaxSpeed() * (60 * 60)) / 1000;
//				maxSpeed = " Max " + Utils.roundTo(maxkmph, 1) + " km/h";
				// 12/14/2012
				String maxSpeedAsText = MeasurementManager.convertvalueByMeasurementSystemAndKindToText(GPSSettings.MEASUREMENT_SYSTEM, MeasurementKind.SPEED, maxSpeedValue.getMaxSpeed(), false);
				maxSpeedAsText += " " + MeasurementManager.selectUnitNameByMeasurementSystemAndKind(GPSSettings.MEASUREMENT_SYSTEM, MeasurementKind.SPEED, maxSpeedValue.getMaxSpeed(), false);
				maxSpeed = " Max " + maxSpeedAsText; 

//				maxAltitude = " Max Altitude " + Utils.roundTo(maxAltitudeValue.getMaxAltitude(), 1) + " m";
				// 12/14/2012
				String maxAltitudeAsText = MeasurementManager.convertvalueByMeasurementSystemAndKindToText(GPSSettings.MEASUREMENT_SYSTEM, MeasurementKind.LENGTH, maxAltitudeValue.getMaxAltitude(), true);
				maxAltitudeAsText += " " + MeasurementManager.selectUnitNameByMeasurementSystemAndKind(GPSSettings.MEASUREMENT_SYSTEM, MeasurementKind.LENGTH, maxAltitudeValue.getMaxAltitude(), true);
				maxAltitude = " Max Altitude " +  maxAltitudeAsText;
//FIXME 12/14/2012 extend to other units the MeasurementKind and manager				
				maxAccelX = " Max " + maxAccelerationValue.getMaxAccelerationX();
				maxAccelY = " Max " + maxAccelerationValue.getMaxAccelerationY();
				maxAccelZ = " Max " + maxAccelerationValue.getMaxAccelerationZ();
				maxLAngle = " Max " + Utils.roundTo(90 - maxOrientationValue.getMaxLeftAngle(), 1);
				maxRAngle = " Max " + Utils.roundTo(90 - maxOrientationValue.getMaxRightAngle(), 1);
				minMaxTemperature = minMaxTemperatureValue != null ? " Min " + minMaxTemperatureValue.getMinTemperature() + " Max " + minMaxTemperatureValue.getMaxTemperature() : ""; 
			}
			kmlBuffer = KMLTags.DATA_TIME +  KMLTags.VALUE_OPEN + time.toString() + KMLTags.VALUE_CLOSE + KMLTags.DATA_CLOSE + KMLTags.CRLF;
			String elapsedText = Utils.milis2TimeString(gpsCoordinate.getElapsedTime());
			kmlBuffer += KMLTags.DATA_ELAPSEDTIME +  KMLTags.VALUE_OPEN + elapsedText + KMLTags.VALUE_CLOSE + KMLTags.DATA_CLOSE + KMLTags.CRLF;
			//#5 10/20/2013
			String elapsedInMoveText = Utils.milis2TimeString(gpsCoordinate.getElapsedTimeInMove());
			kmlBuffer += KMLTags.DATA_ELAPSEDTIMEINMOVE +  KMLTags.VALUE_OPEN + elapsedInMoveText + KMLTags.VALUE_CLOSE + KMLTags.DATA_CLOSE + KMLTags.CRLF;
			String elapsedInWaitText = Utils.milis2TimeString(gpsCoordinate.getElapsedTimeInWait());
			kmlBuffer += KMLTags.DATA_ELAPSEDTIMEINWAIT +  KMLTags.VALUE_OPEN + elapsedInWaitText + KMLTags.VALUE_CLOSE + KMLTags.DATA_CLOSE + KMLTags.CRLF;
			kmlBuffer += KMLTags.DATA_BEARING +  KMLTags.VALUE_OPEN + gpsCoordinate.getLocation().getBearing() + KMLTags.VALUE_CLOSE + KMLTags.DATA_CLOSE + KMLTags.CRLF;

//			kmlBuffer += KMLTags.DATA_ALTITUDE +  KMLTags.VALUE_OPEN + Utils.roundTo(gpsCoordinate.getLocation().getAltitude(),1) +  " m" + maxAltitude + KMLTags.VALUE_CLOSE + KMLTags.DATA_CLOSE + KMLTags.CRLF;
			// 12/14/2012
			String altitudeAsText = MeasurementManager.convertvalueByMeasurementSystemAndKindToText(GPSSettings.MEASUREMENT_SYSTEM, MeasurementKind.LENGTH, gpsCoordinate.getLocation().getAltitude(), true);
			altitudeAsText += " " + MeasurementManager.selectUnitNameByMeasurementSystemAndKind(GPSSettings.MEASUREMENT_SYSTEM, MeasurementKind.LENGTH, gpsCoordinate.getLocation().getAltitude(), true);
			kmlBuffer += KMLTags.DATA_ALTITUDE +  KMLTags.VALUE_OPEN + altitudeAsText + maxAltitude + KMLTags.VALUE_CLOSE + KMLTags.DATA_CLOSE + KMLTags.CRLF;
			
			/*
			double kmph = (gpsCoordinate.getLocation().getSpeed() * (60 * 60)) / 1000;
			double akmph = (gpsCoordinate.getAvgSpeed() * (60 * 60)) / 1000;
			*/
			
//			kmlBuffer += KMLTags.DATA_SPEED +  KMLTags.VALUE_OPEN + Utils.roundTo(kmph, 2) + " km/h " + maxSpeed + KMLTags.VALUE_CLOSE + KMLTags.DATA_CLOSE + KMLTags.CRLF;
			// 12/14/2012
			String speedAsText = MeasurementManager.convertvalueByMeasurementSystemAndKindToText(GPSSettings.MEASUREMENT_SYSTEM, MeasurementKind.SPEED, gpsCoordinate.getLocation().getSpeed(), false);
			speedAsText += " " + MeasurementManager.selectUnitNameByMeasurementSystemAndKind(GPSSettings.MEASUREMENT_SYSTEM, MeasurementKind.SPEED, gpsCoordinate.getLocation().getSpeed(), false);
			kmlBuffer += KMLTags.DATA_SPEED +  KMLTags.VALUE_OPEN + speedAsText + maxSpeed + KMLTags.VALUE_CLOSE + KMLTags.DATA_CLOSE + KMLTags.CRLF;
			
//			kmlBuffer += KMLTags.DATA_AVGSPEED +  KMLTags.VALUE_OPEN + Utils.roundTo(akmph, 2) + " km/h " + KMLTags.VALUE_CLOSE + KMLTags.DATA_CLOSE + KMLTags.CRLF;
			// 12/14/2012
			String avgSpeedAsText = MeasurementManager.convertvalueByMeasurementSystemAndKindToText(GPSSettings.MEASUREMENT_SYSTEM, MeasurementKind.SPEED, gpsCoordinate.getAvgSpeed(), false);
			avgSpeedAsText += " " + MeasurementManager.selectUnitNameByMeasurementSystemAndKind(GPSSettings.MEASUREMENT_SYSTEM, MeasurementKind.SPEED, gpsCoordinate.getAvgSpeed(), false);
			kmlBuffer += KMLTags.DATA_AVGSPEED +  KMLTags.VALUE_OPEN + avgSpeedAsText + KMLTags.VALUE_CLOSE + KMLTags.DATA_CLOSE + KMLTags.CRLF;
			
			/*
			String odo = "";
			double odometer = gpsCoordinate.getOdometer();
			if (odometer > 1000)
				odo = "" + Utils.roundTo((odometer / 1000), 3) + " km";
			else
				odo = "" + Utils.roundTo(odometer, 3) + " m";
			*/
			
//			kmlBuffer += KMLTags.DATA_ODOMETER +  KMLTags.VALUE_OPEN + odo + KMLTags.VALUE_CLOSE + KMLTags.DATA_CLOSE + KMLTags.CRLF;
			// 12/14/2012
			String odometerAsText = MeasurementManager.convertvalueByMeasurementSystemAndKindToText(GPSSettings.MEASUREMENT_SYSTEM, MeasurementKind.LENGTH, gpsCoordinate.getOdometer(), false);
			odometerAsText += " " + MeasurementManager.selectUnitNameByMeasurementSystemAndKind(GPSSettings.MEASUREMENT_SYSTEM, MeasurementKind.LENGTH, gpsCoordinate.getOdometer(), false);
			kmlBuffer += KMLTags.DATA_ODOMETER +  KMLTags.VALUE_OPEN + odometerAsText + KMLTags.VALUE_CLOSE + KMLTags.DATA_CLOSE + KMLTags.CRLF;
			
//			if (gpsCoordEx.getAccelerationTo() != null)
//				kmlBuffer += KMLTags.DATA_ACCELERATIONTO +  KMLTags.VALUE_OPEN + gpsCoordEx.getAccelerationTo() + KMLTags.VALUE_CLOSE + KMLTags.DATA_CLOSE + KMLTags.CRLF;
//			else
//				kmlBuffer += KMLTags.DATA_ACCELERATIONTO +  KMLTags.VALUE_OPEN + "N/A" + KMLTags.VALUE_CLOSE + KMLTags.DATA_CLOSE + KMLTags.CRLF;
			
			switch (GPSSettings.USAGE_TYPE) {
			case AIRCRAFT : {
				kmlBuffer += KMLTags.DATA_ACCELERATIONX +  KMLTags.VALUE_OPEN + gpsCoordinate.getAccelerationx() + maxAccelX + KMLTags.VALUE_CLOSE + KMLTags.DATA_CLOSE + KMLTags.CRLF;
				kmlBuffer += KMLTags.DATA_ACCELERATIONY +  KMLTags.VALUE_OPEN + gpsCoordinate.getAccelerationy() + maxAccelY + " G:" + gpsCoordinate.getGForce() + KMLTags.VALUE_CLOSE + KMLTags.DATA_CLOSE + KMLTags.CRLF;
				kmlBuffer += KMLTags.DATA_ACCELERATIONZ +  KMLTags.VALUE_OPEN + gpsCoordinate.getAccelerationz() + maxAccelZ + KMLTags.VALUE_CLOSE + KMLTags.DATA_CLOSE + KMLTags.CRLF;

				kmlBuffer += KMLTags.DATA_PITCH +  KMLTags.VALUE_OPEN + Utils.roundTo(gpsCoordinate.getPitch(), 1) + KMLTags.VALUE_CLOSE + KMLTags.DATA_CLOSE + KMLTags.CRLF;
				kmlBuffer += KMLTags.DATA_AZIMUTH +  KMLTags.VALUE_OPEN + (90 - Utils.roundTo(gpsCoordinate.getAzimuth(), 1)) + KMLTags.VALUE_CLOSE + KMLTags.DATA_CLOSE + KMLTags.CRLF;
				kmlBuffer += KMLTags.DATA_ROLL +  KMLTags.VALUE_OPEN + Utils.roundTo(gpsCoordinate.getRoll(), 1) + KMLTags.VALUE_CLOSE + KMLTags.DATA_CLOSE + KMLTags.CRLF;
				break;
			}
			case WATERCRAFTS : {
				kmlBuffer += KMLTags.DATA_ACCELERATIONX +  KMLTags.VALUE_OPEN + gpsCoordinate.getAccelerationx() + maxAccelX + KMLTags.VALUE_CLOSE + KMLTags.DATA_CLOSE + KMLTags.CRLF;
				kmlBuffer += KMLTags.DATA_ACCELERATIONY +  KMLTags.VALUE_OPEN + gpsCoordinate.getAccelerationy() + maxAccelY + " G:" + gpsCoordinate.getGForce() + KMLTags.VALUE_CLOSE + KMLTags.DATA_CLOSE + KMLTags.CRLF;
				kmlBuffer += KMLTags.DATA_ACCELERATIONZ +  KMLTags.VALUE_OPEN + gpsCoordinate.getAccelerationz() + maxAccelZ + KMLTags.VALUE_CLOSE + KMLTags.DATA_CLOSE + KMLTags.CRLF;

				kmlBuffer += KMLTags.DATA_PITCH +  KMLTags.VALUE_OPEN + Utils.roundTo(gpsCoordinate.getPitch(), 1) + KMLTags.VALUE_CLOSE + KMLTags.DATA_CLOSE + KMLTags.CRLF;
				kmlBuffer += KMLTags.DATA_AZIMUTH +  KMLTags.VALUE_OPEN + (90 - Utils.roundTo(gpsCoordinate.getAzimuth(), 1)) + KMLTags.VALUE_CLOSE + KMLTags.DATA_CLOSE + KMLTags.CRLF;
				kmlBuffer += KMLTags.DATA_ROLL +  KMLTags.VALUE_OPEN + Utils.roundTo(gpsCoordinate.getRoll(), 1) + KMLTags.VALUE_CLOSE + KMLTags.DATA_CLOSE + KMLTags.CRLF;
				break;
			}
			case FOUR_WHEELERS : {
				kmlBuffer += KMLTags.DATA_ACCELERATIONX +  KMLTags.VALUE_OPEN + gpsCoordinate.getAccelerationx() + maxAccelX + KMLTags.VALUE_CLOSE + KMLTags.DATA_CLOSE + KMLTags.CRLF;
				kmlBuffer += KMLTags.DATA_ACCELERATIONY +  KMLTags.VALUE_OPEN + gpsCoordinate.getAccelerationy() + maxAccelY + " G:" + gpsCoordinate.getGForce() + KMLTags.VALUE_CLOSE + KMLTags.DATA_CLOSE + KMLTags.CRLF;
				kmlBuffer += KMLTags.DATA_ACCELERATIONZ +  KMLTags.VALUE_OPEN + gpsCoordinate.getAccelerationz() + maxAccelZ + KMLTags.VALUE_CLOSE + KMLTags.DATA_CLOSE + KMLTags.CRLF;

				kmlBuffer += KMLTags.DATA_PITCH +  KMLTags.VALUE_OPEN + Utils.roundTo(gpsCoordinate.getPitch(), 1) + KMLTags.VALUE_CLOSE + KMLTags.DATA_CLOSE + KMLTags.CRLF;
				kmlBuffer += KMLTags.DATA_AZIMUTH +  KMLTags.VALUE_OPEN + (90 - Utils.roundTo(gpsCoordinate.getAzimuth(), 1)) + KMLTags.VALUE_CLOSE + KMLTags.DATA_CLOSE + KMLTags.CRLF;
				break;
			}
			case TWO_WHEELERS : {
				kmlBuffer += KMLTags.DATA_ACCELERATIONX +  KMLTags.VALUE_OPEN + gpsCoordinate.getAccelerationx() + maxAccelX + KMLTags.VALUE_CLOSE + KMLTags.DATA_CLOSE + KMLTags.CRLF;
				kmlBuffer += KMLTags.DATA_ACCELERATIONY +  KMLTags.VALUE_OPEN + gpsCoordinate.getAccelerationy() + maxAccelY + " G:" + gpsCoordinate.getGForce() + KMLTags.VALUE_CLOSE + KMLTags.DATA_CLOSE + KMLTags.CRLF;
				kmlBuffer += KMLTags.DATA_ACCELERATIONZ +  KMLTags.VALUE_OPEN + gpsCoordinate.getAccelerationz() + maxAccelZ + KMLTags.VALUE_CLOSE + KMLTags.DATA_CLOSE + KMLTags.CRLF;

				kmlBuffer += KMLTags.DATA_LEFTANGLE +  KMLTags.VALUE_OPEN + (90 - Utils.roundTo(gpsCoordinate.getLeftAngle(), 1)) + maxLAngle + KMLTags.VALUE_CLOSE + KMLTags.DATA_CLOSE + KMLTags.CRLF;
				kmlBuffer += KMLTags.DATA_RIGHTANGLE +  KMLTags.VALUE_OPEN + (90 - Utils.roundTo(gpsCoordinate.getRightAngle(), 1)) + maxRAngle + KMLTags.VALUE_CLOSE + KMLTags.DATA_CLOSE + KMLTags.CRLF;
				
				kmlBuffer += KMLTags.DATA_PITCH +  KMLTags.VALUE_OPEN + Utils.roundTo(gpsCoordinate.getPitch(), 1) + KMLTags.VALUE_CLOSE + KMLTags.DATA_CLOSE + KMLTags.CRLF;
				kmlBuffer += KMLTags.DATA_AZIMUTH +  KMLTags.VALUE_OPEN + (90 - Utils.roundTo(gpsCoordinate.getAzimuth(), 1)) + KMLTags.VALUE_CLOSE + KMLTags.DATA_CLOSE + KMLTags.CRLF;
				
				break;
			}
			case WALKING_HIKE : {
				break;
			}
			case PEDESTRIAN : {
				break;
			}
			}

			//#13 1.02.10 2015.11.29.
			if (android.os.Build.VERSION.SDK_INT >= 14) {
				kmlBuffer += KMLTags.DATA_TEMPERATURE +  KMLTags.VALUE_OPEN + Utils.roundTo(gpsCoordinate.getAmbientTemperature(), 1) + minMaxTemperature + KMLTags.VALUE_CLOSE + KMLTags.DATA_CLOSE + KMLTags.CRLF;
			}

			kmlBuffer += KMLTags.DATA_SATELLITES +  KMLTags.VALUE_OPEN + gpsCoordinate.getSatellites() + KMLTags.VALUE_CLOSE + KMLTags.DATA_CLOSE + KMLTags.CRLF;
			
//			kmlBuffer += KMLTags.DATA_ACCURACY +  KMLTags.VALUE_OPEN + gpsCoordinate.getLocation().getAccuracy() +  " m"+ KMLTags.VALUE_CLOSE + KMLTags.DATA_CLOSE + KMLTags.CRLF;
			// 12/14/2012
			String accuracyAsText = MeasurementManager.convertvalueByMeasurementSystemAndKindToText(GPSSettings.MEASUREMENT_SYSTEM, MeasurementKind.LENGTH, gpsCoordinate.getLocation().getAccuracy(), true);
			accuracyAsText += " " + MeasurementManager.selectUnitNameByMeasurementSystemAndKind(GPSSettings.MEASUREMENT_SYSTEM, MeasurementKind.LENGTH, gpsCoordinate.getLocation().getAccuracy(), true);
			kmlBuffer += KMLTags.DATA_ACCURACY +  KMLTags.VALUE_OPEN + accuracyAsText + KMLTags.VALUE_CLOSE + KMLTags.DATA_CLOSE + KMLTags.CRLF;
			
			String avgSNR = String.format("%.02f", gpsCoordinate.getAverageStrength());
			kmlBuffer += KMLTags.DATA_STRENGTH +  KMLTags.VALUE_OPEN + avgSNR + KMLTags.VALUE_CLOSE + KMLTags.DATA_CLOSE + KMLTags.CRLF;
			
			String address = (gpsCoordinate.getAddress() != null) ? gpsCoordinate.getAddress() : "-";
			kmlBuffer += KMLTags.DATA_ADDRESS +  KMLTags.VALUE_OPEN + address + KMLTags.VALUE_CLOSE + KMLTags.DATA_CLOSE + KMLTags.CRLF;
			time = null;
		}
		catch (Exception e) {
			ErrorHandler.createErrorLog(context, e);
		}
		return kmlBuffer;
	}

	
	
	public String createExtraDataClose(double lon, double lat, double alt) {
		String kmlBuffer = KMLTags.EXTENDEDDATA_CLOSE + KMLTags.CRLF;
		kmlBuffer += KMLTags.POINT_OPEN + KMLTags.CRLF;
		kmlBuffer += KMLTags.COORDINATES_OPEN + lon + "," + lat + "," + alt + KMLTags.COORDINATES_CLOSE + KMLTags.CRLF;
		kmlBuffer += KMLTags.POINT_CLOSE + KMLTags.CRLF;
		kmlBuffer += KMLTags.PLACEMARK_CLOSE + KMLTags.CRLF;
		return kmlBuffer;
	}

	
	/**
	 * 
	 * @param trackFormat
	 * @param trackLog
	 * @param viewCoordinate
	 * @param logName
	 * @param markName
	 * @return
	 */
	public StringBuffer generateTrackOpen(TrackFormat trackFormat, StringBuffer trackLog, GPSCoordinate viewCoordinate, String logName, String markName) {
		if (this.trackFormat == null)
			this.trackFormat = trackFormat;
		if (this.trackFormat != null) {
			switch (trackFormat) {
			case KML : {
//				try {
					trackLog.append(createKMLHeaderOpen(logName));
					trackLog.append(createView(viewCoordinate, range));
					trackLog.append(createTracklogOpen(logName));
					trackLog.append(createPlacemarkOpen(markName));
					trackLog.append(createIconDefinitionsForPlacemarks());
					trackLog.append(createLineStyle(lineColor, lineWidth));
					trackLog.append(createLineStringOpen());
//				} catch (Exception e) {
//		    		e.printStackTrace();
//                    CommonLogger.log(Level.SEVERE, e.getMessage());
//					Toast.makeText(context, "ERROR in TrackGenerator.generateTrackOpen " + e.getMessage(), Toast.LENGTH_LONG).show();
//				}		
				break;
			}
			case GPX : {
				break;
			}
			case GRM : {
				break;
			}
			}
		}
		else {
			throw new RuntimeException("TrackFormat is null!");
		}
		return trackLog;
	}
	
	/**
	 * 
	 * @param trackFormat
	 * @param trackLog
	 * @return
	 */
	public StringBuffer generateTrackClose(TrackFormat trackFormat, StringBuffer trackLog, List<GPSCoordinate> coordinates, int totalPts) {
		if (coordinates != null) {
			switch (trackFormat) {
			case KML : {
				trackLog.append(createLineStringClose());
				trackLog.append(createPlacemarkClose());
				//telemetry
				trackLog.append(createBallon());
				int cnt = 0;
				int maxCnt = coordinates.size();
//				trackLog.append("<!-- DEBUG: maxCnt = " + maxCnt + "  -->");
				double distance = 0.0;
				GPSCoordinate prevGPSCoordinate = null;
				String telemetryTittle = "";
				for (GPSCoordinate gpsCoordinate : coordinates) {
//					trackLog.append("<!-- DEBUG: gpsCoordinate.isPlacemark() "+cnt+" -->");
//					telemetryTittle = "Route Pt-" + cnt;
					// 11/21/2012
					// 11/23/2012
					if (GPSSettings.IS_STORE_PLACEMARKS_AT_START_PAUSE_STOP_ONLY)
						telemetryTittle = context.getResources().getString(R.string.route_point_move);
					else
						telemetryTittle = context.getResources().getString(R.string.route_point_move) + cnt;
						
					// default i sNONE
					PlacemarkIconStyleKind placemarkIconStyleKind = PlacemarkIconStyleKind.NONE;
					//11/19/2012
					if (!GPSSettings.IS_STORE_PLACEMARKS_AT_START_PAUSE_STOP_ONLY) {
						placemarkIconStyleKind = PlacemarkIconStyleKind.MOVING;
					}
					// is stay is one place?
					if (!gpsCoordinate.getLocation().hasSpeed() || gpsCoordinate.getLocation().getSpeed() == 0) {
						// stay
						placemarkIconStyleKind = PlacemarkIconStyleKind.PAUSE;
						telemetryTittle = context.getResources().getString(R.string.route_point_pause) + cnt;
					}
					else if (gpsCoordinate.getLocation().hasSpeed() && gpsCoordinate.getLocation().getSpeed() > 0){
						// in moving
						//11/19/2012
						if (!GPSSettings.IS_STORE_PLACEMARKS_AT_START_PAUSE_STOP_ONLY) {
							placemarkIconStyleKind = PlacemarkIconStyleKind.MOVING;
						}
					}
					// is first position?
					if (cnt == 0) {
						// first position
						placemarkIconStyleKind = PlacemarkIconStyleKind.START;
						telemetryTittle = context.getResources().getString(R.string.route_point_start) + cnt;
					}
					else if (cnt + 1 == maxCnt) {
						// last position
						placemarkIconStyleKind = PlacemarkIconStyleKind.END;
						telemetryTittle = context.getResources().getString(R.string.route_point_end) + cnt;
					}
//					trackLog.append("<!-- DEBUG: PlacemarkIconStyleKind.MOVING "+cnt+" " + telemetryTittle + " -->");
					// 11/21/2012
					cnt++;
					// 11/19/2012 - is store only start pause and stop? 
					if (GPSSettings.IS_STORE_PLACEMARKS_AT_START_PAUSE_STOP_ONLY) {
						// is start pause and stop?
						if ((!placemarkIconStyleKind.equals(PlacemarkIconStyleKind.MOVING)) && (!placemarkIconStyleKind.equals(PlacemarkIconStyleKind.NONE))) {
//							trackLog.append("<!-- DEBUG: START or PAUSE or STOP "+cnt+" -->");
							trackLog.append(createExtraDataOpen(telemetryTittle, placemarkIconStyleKind));
							trackLog.append(createExtraData(gpsCoordinate, (cnt == maxCnt)));
							prevGPSCoordinate = gpsCoordinate;
							trackLog.append(createExtraDataClose(gpsCoordinate.getLocation().getLongitude(), gpsCoordinate.getLocation().getLatitude(), gpsCoordinate.getLocation().getAltitude()));
						}
					}
					else {
						trackLog.append(createExtraDataOpen(telemetryTittle, placemarkIconStyleKind));
						trackLog.append(createExtraData(gpsCoordinate, (cnt == maxCnt)));
						prevGPSCoordinate = gpsCoordinate;
						trackLog.append(createExtraDataClose(gpsCoordinate.getLocation().getLongitude(), gpsCoordinate.getLocation().getLatitude(), gpsCoordinate.getLocation().getAltitude()));
					}
				}
				
				trackLog.append(createTracklogClose());
				trackLog.append(createKMLHeaderClose(coordinates.size(), totalPts));
				break;
			}
			case GPX : {
				break;
			}
			case GRM : {
				break;
			}
			}
		}
		return trackLog;
	}
	
}

