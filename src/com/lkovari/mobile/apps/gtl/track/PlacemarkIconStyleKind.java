package com.lkovari.mobile.apps.gtl.track;

/**
 * 
 * @author lkovari
 *
 */
public enum PlacemarkIconStyleKind {
	START,
	PAUSE,
	MOVING,
	NONE,
	END;
}
