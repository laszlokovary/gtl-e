package com.lkovari.mobile.apps.gtl.track;

import com.lkovari.mobile.apps.gtl.settings.GPSSettings;


/**
 * 
 * @author lkovari
 *
 */
public class KMLTags {
	public static final String CRLF = "\n";
	public static final String TAB = "	";
	public static final String XML_NS = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>";
	public static final String KML_OPEN = "<kml xmlns=\"http://earth.google.com/kml/2.0\">";
	public static final String KML_CLOSE = "</kml>";
	public static final String DOCUMENT_OPEN = "<Document>";
	public static final String DOCUMENT_CLOSE = "</Document>";
	public static final String NAME_OPEN = "<name>";
	public static final String NAME_CLOSE = "</name>";
	public static final String VISIBILITY_OPEN = "<visibility>";
	public static final String VISIBILITY_CLOSE = "</visibility>";
	public static final String LOOKAT_OPEN = "<LookAt>";
	public static final String LOOKAT_CLOSE = "</LookAt>";
	public static final String LONGITUDE_OPEN = "<longitude>";
	public static final String LONGITUDE_CLOSE = "</longitude>";
	public static final String LATTITUDE_OPEN = "<latitude>";
	public static final String LATTITUDE_CLOSE = "</latitude>";
	public static final String RANGE_OPEN = "<range>";
	public static final String RANGE_CLOSE = "</range>";
	public static final String FOLDER_OPEN = "<Folder>";
	public static final String FOLDER_CLOSE = "</Folder>";
	public static final String PLACEMARK_OPEN = "<Placemark>";
	public static final String PLACEMARK_CLOSE = "</Placemark>";
	public static final String ICONSTYLE_OPEN = "<IconStyle>";
	public static final String PLACEMARK_STYLE_START = "<Style id=\"placemark_start\">";
	public static final String PLACEMARK_STYLE_STANDARD = "<Style id=\"placemark_standard\">";
	public static final String PLACEMARK_STYLE_PAUSE = "<Style id=\"placemark_pause\">";
	public static final String PLACEMARK_STYLE_STOP = "<Style id=\"placemark_stop\">";
	public static final String PLACEMARK_ICONSTYLE_START = "<IconStyle id=\"start\">";
	public static final String PLACEMARK_ICONSTYLE_STANDARD = "<IconStyle id=\"standard\">";
	public static final String PLACEMARK_ICONSTYLE_PAUSE = "<IconStyle id=\"pause\">";
	public static final String PLACEMARK_ICONSTYLE_STOP = "<IconStyle id=\"stop\">";

	public static final String PLACEMARK_HREF_START = "<Icon><href>http://www.eklsofttrade.com/lk/images/android/placemark_icon_start_rect_16x16.png</href></Icon>";
	public static final String PLACEMARK_HREF_STANDARD = "<Icon><href>http://www.eklsofttrade.com/lk/images/android/placemark_icon_blue_flag_16x16.png</href></Icon>";
	public static final String PLACEMARK_HREF_PAUSE = "<Icon><href>http://www.eklsofttrade.com/lk/images/android/placemark_icon_pause_rect_16x16.png</href></Icon>";
	public static final String PLACEMARK_HREF_STOP = "<Icon><href>http://www.eklsofttrade.com/lk/images/android/placemark_icon_stop_rect_16x16.png</href></Icon>";
	public static final String PLACEMARK_ICONSTYLE_CLOSE = "</IconStyle>";
	public static final String ICONSTYLE_CLOSE = "</IconStyle>";
	public static final String ICON_OPEN = "<Icon>";
	public static final String ICON_CLOSE = "</Icon>";
	public static final String STYLE_OPEN = "<Style>";
	public static final String STYLE_CLOSE = "</Style>";
	public static final String LINESTYLE_OPEN = "<LineStyle>";
	public static final String LINESTYLE_CLOSE = "</LineStyle>";
	public static final String COLOR_OPEN = "<color>";
	public static final String COLOR_CLOSE = "</color>";
	public static final String WIDTH_OPEN = "<width>";
	public static final String WIDTH_CLOSE = "</width>";
	public static final String LINESTRING_OPEN = "<LineString>";
	public static final String LINESTRING_CLOSE = "</LineString>";
	public static final String COORDINATES_OPEN = "<coordinates>";
	public static final String COORDINATES_CLOSE = "</coordinates>";
	public static final String DATA_OPEN = "<Data>";
	public static final String DATA_CLOSE = "</Data>";
	public static final String VALUE_OPEN = "<value>";
	public static final String VALUE_CLOSE = "</value>";
	public static final String STYLE_OPEN_WITH_ID = "<Style id=\"telemetry\">";
	public static final String BALLON_OPEN = "<BallonStyle>";
	public static final String BALLON_CLOSE = "</BallonStyle>";
	public static final String TEXT_OPEN = "<text>";
	public static final String TEXT_CLOSE = "</text>";
	
	public static final String CDATA_OPEN = "<![CDATA[";
	public static final String CDATA_TIME = "Time $[time]";
	public static final String CDATA_ELAPSEDTIME = "Elapsed $[elapsed]";
	// #5 10/2062013
	public static final String CDATA_ELAPSEDTIME_INMOVE = "Move $[inmove]";
	public static final String CDATA_ELAPSEDTIME_INWAIT = "Wait $[inwait]";
	public static final String CDATA_BEARING = "Bearing $[bearing]";
	public static final String CDATA_ODOMETER = "Odometer $[odometer]";
	public static final String CDATA_SPEED = "Speed $[speed]";
	public static final String CDATA_AVGSPEED = "AvgSpeed $[avgspeed]";
	public static final String CDATA_ALTITUDE = "Altitude $[altitude]";
	public static final String CDATA_ACCELERATIONX = "Acceleration $[accelerationx]";
	public static final String CDATA_ACCELERATIONY = "Acceleration $[accelerationy]";
	public static final String CDATA_ACCELERATIONZ = "Acceleration $[accelerationz]";
//	public static final String CDATA_ACCELERATIONTO = "Acceleration to $[accelerationto]";
	public static final String CDATA_LEFTANGLE = "Left angle $[leftangle]";
	public static final String CDATA_RIGHTANGLE = "Right angle $[rightangle]";
	// put phone to the table, bottom part nearest me
	// phone top up bottom down or reverse
	public static final String CDATA_PITCH = "Pitch $[pitch]";
	// phone rotate reverse clockvise or clockwise
	public static final String CDATA_AZIMUTH = "Azimuth $[azimuth]";
	// phone left down right up or reverse
	public static final String CDATA_ROLL = "Roll $[roll]";
	// #13 1.02.10 2015.11.29.
	public static final String CDATA_TEMPERATURE = "Temperature $[temperature]";
	public static final String CDATA_SATELLITES = "Satellites $[satellites]";
	public static final String CDATA_ADDRESS = "Address $[address]";
	public static final String CDATA_ACCURACY = "Accuracy $[accuracy]";
	public static final String CDATA_STRENGTH = "SNR $[snr]";
	public static final String CDATA_CLOSE = "]]>";
	
	public static final String STYLEURL_OPEN = "<styleUrl>";
	public static final String STYLEURL_CLOSE = "</styleUrl>";
	
	public static final String EXTENDEDDATA_OPEN = "<ExtendedData>";
	public static final String EXTENDEDDATA_CLOSE = "</ExtendedData>";

	public static final String POINT_OPEN = "<Point>";
	public static final String POINT_CLOSE = "</Point>";
	public static final String REMARK_OPEN = "<!--";
	public static final String REMARK_CLOSE = "-->";
	
	public static final String DATA_TIME = "<Data name=\"Time\">";
	public static final String DATA_ELAPSEDTIME = "<Data name=\"Elapsed\">";
	//#5 10/2062013
	public static final String DATA_ELAPSEDTIMEINMOVE = "<Data name=\"Move\">";
	public static final String DATA_ELAPSEDTIMEINWAIT = "<Data name=\"Wait\">";
	
	public static final String DATA_BEARING = "<Data name=\"Bearing\">";
	public static final String DATA_ALTITUDE = "<Data name=\"Altitude\">";
	public static final String DATA_SPEED = "<Data name=\"Speed\">";
	public static final String DATA_AVGSPEED = "<Data name=\"Avgspeed\">";
	public static final String DATA_ODOMETER = "<Data name=\"Odometer\">";
	public static final String DATA_ACCELERATIONX = "<Data name=\"AccelerationX\">";
	public static final String DATA_ACCELERATIONY = "<Data name=\"AccelerationY\">";
	public static final String DATA_ACCELERATIONZ = "<Data name=\"AccelerationZ\">";
//	public static final String DATA_ACCELERATIONTO = "<Data name=\"accelerationto\">";
	public static final String DATA_LEFTANGLE = "<Data name=\"LeftAngle\">";
	public static final String DATA_RIGHTANGLE = "<Data name=\"RightAngle\">";
	public static final String DATA_PITCH = "<Data name=\"Pitch\">";
	public static final String DATA_AZIMUTH = "<Data name=\"Azimuth\">";
	public static final String DATA_ROLL = "<Data name=\"Roll\">";
	//#13 1.02.10 2015.11.29.									
	public static final String DATA_TEMPERATURE = "<Data name=\"Temperature\">";
	public static final String DATA_ADDRESS = "<Data name=\"Address\">";
	public static final String DATA_SATELLITES = "<Data name=\"Satellites\">";
	public static final String DATA_ACCURACY = "<Data name=\"Accuracy\">";
	public static final String DATA_STRENGTH = "<Data name=\"SNR\">";
	public static final String DATA_AUTHOR_NAME = "Laszlo Kovary";
//	public static final String DATA_AUTHOR_PHONE = "+36703214433";
	public static final String DATA_AUTHOR_EMAIL = "info@eklsofttrade.com";
	public static final String DATA_AUTHOR_COPYRIGHT = "Copyright (c) 2013�2014 by EKLSoft Trade LLC.";
	public static final String DATA_DEVELOPER_VERSION = "Ver. " + GPSSettings.VERSION;
	
}

