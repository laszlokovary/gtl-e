package com.lkovari.mobile.apps.gtl.track;


/**
 * 
 * @author lkovari
 *
 */
public enum TrackFormat {
	KML,
	GPX,
	GRM;
}

