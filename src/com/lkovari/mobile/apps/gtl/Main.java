package com.lkovari.mobile.apps.gtl;


import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.Thread.UncaughtExceptionHandler;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.TabActivity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.res.Resources;
import android.hardware.GeomagneticField;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.GpsSatellite;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Looper;
import android.os.PowerManager;
import android.os.StatFs;
import android.os.StrictMode;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager.LayoutParams;
import android.widget.EditText;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TabHost.TabSpec;
import android.widget.Toast;

import com.google.android.maps.GeoPoint;
import com.lkovari.mobile.apps.gtl.SimpleGesture.SimpleGestureListener;
import com.lkovari.mobile.apps.gtl.map.LogSelector;
import com.lkovari.mobile.apps.gtl.map.MapTab;
import com.lkovari.mobile.apps.gtl.map.filter.DouglasPeuckerOptimizer;
import com.lkovari.mobile.apps.gtl.map.mapsforge.MapTabMapForge;
import com.lkovari.mobile.apps.gtl.maxvalues.ElapsedTimeKind;
import com.lkovari.mobile.apps.gtl.maxvalues.MaxAccelerationValue;
import com.lkovari.mobile.apps.gtl.maxvalues.MaxAltitudeValue;
import com.lkovari.mobile.apps.gtl.maxvalues.MaxOrientationValue;
import com.lkovari.mobile.apps.gtl.maxvalues.MaxSpeedValue;
import com.lkovari.mobile.apps.gtl.maxvalues.MinMaxTemperatureValue;
import com.lkovari.mobile.apps.gtl.preferences.GTLSettings;
import com.lkovari.mobile.apps.gtl.settings.DataProviderKind;
import com.lkovari.mobile.apps.gtl.settings.GPSSettings;
import com.lkovari.mobile.apps.gtl.settings.UsageType;
import com.lkovari.mobile.apps.gtl.shutdownreceiver.GTLRestartReceiver;
import com.lkovari.mobile.apps.gtl.shutdownreceiver.GTLShutdownReceiver;
import com.lkovari.mobile.apps.gtl.track.GPSCoordinate;
import com.lkovari.mobile.apps.gtl.track.TrackGenerator;
import com.lkovari.mobile.apps.gtl.utils.ConnectionUtils;
import com.lkovari.mobile.apps.gtl.utils.DeviceID;
import com.lkovari.mobile.apps.gtl.utils.GUIDataIdentifiers;
import com.lkovari.mobile.apps.gtl.utils.Utils;
import com.lkovari.mobile.apps.gtl.utils.ValuesAverageManager;
import com.lkovari.mobile.apps.gtl.utils.error.ErrorHandler;
import com.lkovari.mobile.apps.gtl.utils.logger.CommonLogger;
import com.lkovari.mobile.apps.gtl.utils.mapsforge.MapSelector;
import com.lkovari.mobile.apps.gtl.utils.sdcard.SDCardManager;

/**
 * 
 * @author lkovari
 *
 */
public class Main extends TabActivity  implements SimpleGestureListener {
//TODO !!!!!!! replace TabActivity
	public static final String BROADCAST_ACTION_GPS_STATUS_DATA_CHANGED = "com.lkovari.mobile.apps.gtl.gpsstatusdatachanged";
	public static final String BROADCAST_ACTION_GPS_LOCATION_DATA_CHANGED = "com.lkovari.mobile.apps.gtl.gpslocationdatachanged";
	public static final String BROADCAST_ACTION_GPS_ELLAPSED_IN_MOVE_OR_IN_WAIT_CHANGED = "com.lkovari.mobile.apps.gtl.gpsellapsedinmoveorinwaithanged";
	
	private SimpleGesture simpleGesture;
	
	// data identifier for pass to another activity trough Bundle and Intent
	public static String BUNDLE_CONTENT_TAG_DATA1 = "DATA1";	
	public static String BUNDLE_CONTENT_TAG_DATA2 = "DATA2";	
	public static String BUNDLE_CONTENT_TAG_DATA3 = "DATA2";

	//coordinate points optimization
	private int points_origin = 0;

	//origin screen off timeout
	int originScreenOffTimeout = 0;
	// count broadcast
	long broadcastLocationCnt = 0;
	long broadcastStatusCnt = 0;
	
	// test data to pass
	
	public Long data1 = Long.valueOf(1);
	public Long data2 = Long.valueOf(1);
	public Long data3 = Long.valueOf(1);
	
	/*
	 * Sensors and managers
	 */
	private Sensor mOrientationSensor = null;
	private Sensor mAccelerometerSensor = null;
	private Sensor mMagneticFieldSensor = null;
	//#13 v1.02.10 2015.11.29
	private Sensor mAmbientTemperatureSensor = null;

	//#13 v1.02.10 2015.11.29
	private SensorManager sensorManager;
	//08/12/2016 #16 v1.02.13
	private boolean isEnableMenuItems = true;
	
	
	/*
	 * GPS related classes
	 */
	private GpsStatus gpsStatus = null;
	private LocationManager locationManager;
	private GPSLocationListener gpsLocationListener;
	private GPSStatusListener gpsStatusListener;
	private SensorListener sensorEventListener;

	//GPS Status text
	private String gpsStatusText = null;
	private int gpsStatusEvent = -1;
	// the average of strength of available satellites
	private double averageSNR = 0.0;
	
	
	/*
	 * GUI elements
     */
	TabHost tabHost = null;
	
	/*
	 * Track related classes
	 */
	TrackGenerator trackGenerator = null;
	private StringBuffer trackLog = new StringBuffer();
	private StringBuffer logEvents = new StringBuffer();

	// listeners enable disable
	private boolean isNeedToRegisterGPSStatusListener = true;
	
	private boolean isLocationListenerRegistered = false;
	
	private boolean isProviderEnabled = false;
	private boolean isServiceAvailable = false;

	
	// current
	private Location currentLoc = null;
	// previous
	private Location prevLoc = null;
	
	private int gravityChangedTo = -1;
	private int maxSatellites = 0;
	private int fixedSatellites = 0;
	private int unFixedSatellites = 0;
	private double odometer = 0.0;
//	private long elapsed = -1;
    private GPSCoordinate lookAtCoordinate = null;
//	private List<GPSCoordinate> gpsCoordinates = new ArrayList<GPSCoordinate>();
	private float sumAccelerometerX = Float.valueOf(0.0f);
	private float sumAccelerometerY = Float.valueOf(0.0f);
	private float sumAccelerometerZ = Float.valueOf(0.0f);
	private float lastAccelerometerX = Float.valueOf(0.0f);
	private float lastAccelerometerY = Float.valueOf(0.0f);
	private float lastAccelerometerZ = Float.valueOf(0.0f);
	private float sumOrientationAzimuth = Float.valueOf(0.0f);
	private float sumOrientationPitch = Float.valueOf(0.0f);
	private float sumOrientationRoll = Float.valueOf(0.0f);
	private float lastOrientationAzimuth = Float.valueOf(0.0f);
	private float lastOrientationPitch = Float.valueOf(0.0f);
	private float lastOrientationRoll = Float.valueOf(0.0f);
	private float[] accels = new float[3];
	private float[] orient = new float[3];
	// #13 1.02.10 2015.11.29.
	private float ambientTemperature = 0.0f;

	private int cntAccelerometer = 0;
	private int cntOrientation = 0;
	private int acceleroAvgCnt = 5;
	private int orientAvgCnt = 5;
	
	private String bestProvider = null;
	private Criteria criteria = null;
	//15 degree
	private int CURVE_CRITERIA = 15;
	private boolean isInCurve = false;
	
	// speed limits and min time 6 distance
	private final int SPEED_0 = 0;
	private final int SPEED_0_MINTIME = 25000;
	private final int SPEED_0_MINDISTANCE = 2;
	private final int SPEED_5 = 5;
	private final int SPEED_5_MINTIME = 2500;
	private final int SPEED_5_MINDISTANCE = 4;
	private final int SPEED_10 = 10;
	private final int SPEED_10_MINTIME = 5000;
	private final int SPEED_10_MINDISTANCE = 10;
	private final int SPEED_25 = 25;
	private final int SPEED_25_MINTIME = 7500;
	private final int SPEED_25_MINDISTANCE = 20;
	private final int SPEED_50 = 50;
	private final int SPEED_50_MINTIME = 10000;
	private final int SPEED_50_MINDISTANCE = 36;
	private final int SPEED_75 = 75;
	private final int SPEED_75_MINTIME = 15000;
	private final int SPEED_75_MINDISTANCE = 48;
	private final int SPEED_100 = 100;
	private final int SPEED_100_MINTIME = 20000;
	private final int SPEED_100_MINDISTANCE = 70;
	private final int SPEED_150 = 150;
	private final int SPEED_150_MINTIME = 25000;
	private final int SPEED_150_MINDISTANCE = 98;
	private final int SPEED_200 = 200;
	private final int SPEED_200_MINTIME = 25000;
	private final int SPEED_200_MINDISTANCE = 124;
	private final int SPEED_250 = 250;
	private final int SPEED_250_MINTIME = 30000;
	private final int SPEED_250_MINDISTANCE = 152;
	private final int SPEED_300 = 300;
	private final int SPEED_300_MINTIME = 35000;
	private final int SPEED_300_MINDISTANCE = 194;
	private final int SPEED_400 = 400;
	private final int SPEED_400_MINTIME = 40000;
	private final int SPEED_400_MINDISTANCE = 250;
	private final int SPEED_500 = 500;
	private final int SPEED_500_MINTIME = 45000;
	private final int SPEED_500_MINDISTANCE = 348;
	private final int SPEED_750 = 750;
	private final int SPEED_750_MINTIME = 50000;
	private final int SPEED_750_MINDISTANCE = 243;
	private final int SPEED_1000 = 1000;
	private final int SPEED_1000_MINTIME = 55000;
	private final int SPEED_1000_MINDISTANCE = 556;
	private final int SPEED_1500 = 1500;
	private final int SPEED_1500_MINTIME = 60000;
	private final int SPEED_1500_MINDISTANCE = 695;
	private final int SPEED_OVER_1500_MINTIME = 65000;
	private final int SPEED_OVER_1500_MINDISTANCE = 834;

	private float refDistance = 0;
	private float currentDistance = 0;
	
	private long locationEventCnt = 0;
	private long storedLocationCnt = 0;
	
	float speedSum = 0;
	int speedCnt = 0;
	
	private MaxSpeedValue maxSpeedValue = new MaxSpeedValue();
	private MaxAltitudeValue maxAltitudeValue = new MaxAltitudeValue();
	private MaxAccelerationValue maxAccelerationValue = new MaxAccelerationValue();
	private MaxOrientationValue maxOrientationValue = new MaxOrientationValue();
	// #13 1.02.10 2015.11.29.
	private MinMaxTemperatureValue minMaxTemperatureValue = new MinMaxTemperatureValue();
	
	private Double startAccelerationToTime = null;
	/*
	private Double acceleration400 = null;
	private Double acceleration200 = null;
	private Double acceleration100 = null;
	private Double acceleration50 = null;
	*/
	// contains the average of bearing
	private float lastBearing = 0;
	private float currentBearing = 0;
	private ValuesAverageManager averageValuesManager = null;
	// 12/27/2012
	private Menu optionsMenu = null;
	private boolean isFirstFix = false;
    // 12/28/2012
    Intent mapTabIntent = null;
    boolean isNeedToAddMapTabSpec = false;
    TabSpec mapTabSpec = null;
	
	// 12/30/2012
	private boolean isMapTabSpecExists = false;
	// 07/09/2013 #122
	private int usedGlonassSatellites = 0;
	private int usedGlonassSatellitesInFix = 0;
	// 19/07/2014 #9 v1.02.6
	private int usedBeiDouSatellites = 0;
	private int usedBeiDouSatellitesInFix = 0;
	private static final int GLONASS_LOW_LIMIT = 65;
	private static final int GLONASS_HIGH_LIMIT = 88;
	private static final int BEIDOU_LOW_LIMIT = 200;
	private static final int BEIDOU_HIGH_LIMIT = 235;
	// 08/23/2013 tab tags
	private static final String TAB_TAG_GPS = "GPS";
	private static final String TAB_TAG_ROUTE = "Route";
	private static final String TAB_TAG_COMPASS = "Compass";
	private static final String TAB_TAG_MAP = "Map";
	// #5 10/06/2013
	private boolean isFirstStart = true;
	// #5 10/12/2013
	private String targetAddressAsText;
	private Address targetAddressAsAddress = null;
	private MapTab mapTab = null;
	private long elapsedInMoveStart = -1;
	private long elapsedInWaitStart = -1;
	private long currentInMove = 0;
	private long currentInWait = 0;
	private long elapsedInMove = 0;
	private long elapsedInWait = 0;
	private ElapsedTimeKind elapsedTimeKind = ElapsedTimeKind.IN_MOVE;
	// #5 10/19/2013
	Timer timer = new Timer(false);
    private boolean isTimerStarted = false;

	//#6 11/27/2013 true if stopLogging is call
	private boolean isStopLoggingNow = false;
	
	//#8 v1.02.5 02/15/2014 
	private boolean IS_SAVE_IMEI_TO_FILE = false;
	//#8 v1.02.5 03/08/2014 
	private DownloadPhoneIDAsyncTask downloadPhoneIDAsyncTask = null;
	//#9 v1.02.6 03/22/2014 
	private StoreLocationAsyncTask storeLocationAsyncTask = null;
	//#9 v1.02.6 04/02/2014 add period
	//#9 v1.02.6 06/07/2014 add postime
	private String STORE_LOC = "http://www.eklsofttrade.com/gtl/ins.php?phoneid=%s&lat=%s&lng=%s&period=%s&postime=%s";
	private long lastStore = Long.MIN_VALUE;
	// 03/12/2016 #15 v1.02.12
	private boolean isForceSaveGPSCoordinatesAndStopLogging = false;

	// 03/12/2016 #15 v1.02.12 
	private GTLShutdownReceiver shutdownReceiver = new GTLShutdownReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			if (GPSSettings.IS_LOGGING_STARTED) {
				stopLogging();
			}
		}	
	};	

	// 8/12/2016 #16 v1.02.13
	private GTLRestartReceiver restartReceiver = new GTLRestartReceiver() {
		public void onReceive(Context context, Intent intent) {
			if (GPSSettings.IS_LOGGING_STARTED) {
// TODO restore logging if phone crassed				
			}
			
		};
	};
	
	/**
	 * 08/12/2016 #16 v1.02.13
	 * @author lkovari
	 *
	 */
	class RestartReceiver  extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
// TODO Implemented lated for handling crashes
			
		}
		
	}
	
	
	/**
	 * 22/03/2014 #9 v1.02.6
	 * @author lkovari
	 *
	http://www.eklsofttrade.com/gtl/ins.php?phoneid=t20130428&lat=47.5863785482943&lng=19.0499249193818
	http://www.eklsofttrade.com/gtl/ins.php?phoneid=t20130428&lat=47.5922592077405&lng=19.0515030641109
	http://www.eklsofttrade.com/gtl/ins.php?phoneid=t20130428&lat=47.5948767922819&lng=19.0531765948981
	http://www.eklsofttrade.com/gtl/ins.php?phoneid=t20130428&lat=47.5975241325796&lng=19.0549572464079

	http://www.eklsofttrade.com/gtl/ins.php?phoneid=TEST36205583720&lat=47.5863785482943&lng=19.0499249193818&period=30&postime=1402155102184

	http://www.eklsofttrade.com/gtl/map.php?phoneid=t20130428		

	Test page http://www.eklsofttrade.com/gtl/gtlwebviewtestpage.html
	
	Phone id URL http://www.eklsofttrade.com/gtl/gtlfind.php?phoneid=52EC21D0B5E5E835A3941A3FE57AA395BB9E2E3C
	
	
	 *
	 */
	private class StoreLocationAsyncTask extends AsyncTask<String, Void, Boolean> {

		@SuppressLint("NewApi") 
		@Override
		protected Boolean doInBackground(String... params) {
			Boolean isOK = null;
			String url = (String)params[0];
			if (url != null) {
				try {
					if (ConnectionUtils.isInternetAvailable(getCurrentActivity())) {
					    if (android.os.Build.VERSION.SDK_INT > 9) {
					        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
					        StrictMode.setThreadPolicy(policy);
					    }						
						DefaultHttpClient httpclient = new DefaultHttpClient();
						Calendar posTime = Calendar.getInstance();
						long timeInLong = posTime.getTimeInMillis();
						String posUrl = String.format(url, GPSSettings.DEVICE_ID, ""+currentLoc.getLatitude(), ""+currentLoc.getLongitude(), ""+GPSSettings.STORELOCATION_EVERY_N_MINUTES, ""+ timeInLong);
						posTime = null;
						HttpGet httpget = new HttpGet(posUrl);
						HttpResponse response = httpclient.execute(httpget);
						BufferedReader in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
						StringBuffer sb = new StringBuffer("");
						String line = "";
						String NL = System.getProperty("line.separator");
						while ((line = in.readLine()) != null) {                    
							sb.append(line + NL);
						}
						in.close();
						String result = sb.toString();				
					}
					isOK = true;
				}
				catch (Exception e) {
					isOK = false;
					ErrorHandler.createErrorLog(Main.this, e);
				}
			}
			return isOK;
		}
		
		@Override
		protected void onPostExecute(Boolean isOk) {
			if (!isOk) {
				Toast.makeText(Main.this, R.string.error_position_not_stored_on_web, Toast.LENGTH_LONG).show();
			}
		}
	}
	
	/**
	 * #8 v1.02.5 02/15/2014
	 * @author lkovari
	 *
	 */
	@SuppressLint("NewApi") 
	private class DownloadPhoneIDAsyncTask extends AsyncTask<String, Void, String> {
		private String imei = null;

		@Override
		protected void onPostExecute(String result) {
			String deviceids = result;
			if ((this.imei != null) && (deviceids != null)) {
				String imeiMd5 = null;
				try {
					// create MD5 fingerprint
					imeiMd5 = ConnectionUtils.md5((String)imei);
					// is device id file content contains current device id? AND not using off-line map?
					GPSSettings.IS_EXTRA_FUNCTIONS_ENABLED = deviceids.contains(imeiMd5) && !GPSSettings.IS_USE_OFFLINE_MAP;
					if (GPSSettings.IS_EXTRA_FUNCTIONS_ENABLED) {
						// #8 v1.02.5 03/08/2014
					    if (android.os.Build.VERSION.SDK_INT > 9) {
					        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
					        StrictMode.setThreadPolicy(policy);
					    }						
					}
				} catch (Exception e) {
					GPSSettings.IS_EXTRA_FUNCTIONS_ENABLED = false;
				}
			}
			super.onPostExecute(result);
		}
		
		@Override
		protected String doInBackground(String... params) {
			GPSSettings.IS_EXTRA_FUNCTIONS_ENABLED = false;
			// current device IMEI
			this.imei = (String)params[0];
			String phoneids = null;
			try {
				// download phone Ids (IMEI which has extra functionality)
				phoneids = ConnectionUtils.downloadIMEINumbers();
			} catch (Exception e) {
				phoneids = null;
			}
			return phoneids;
		}
	}

	private Runnable timerTick = new Runnable() {
		public void run() {
        	if (isTimerStarted) {
        		//#5 10/1962013
        		switch (elapsedTimeKind) {
        		case IN_MOVE : {
    				currentInMove = (System.currentTimeMillis() - elapsedInMoveStart);
        			break;
        		}
        		case IN_WAIT : {
    				currentInWait = (System.currentTimeMillis() - elapsedInWaitStart);
        			break;
        		}
        		}
        		long elapsedm = elapsedInMove + currentInMove;
        		long elapsedw = elapsedInWait + currentInWait;
        		long elapsed = elapsedm + elapsedw;
        		sendBroadcastElapsedInMoveInWait(elapsed, elapsedm, elapsedw);
        	}	
		}		
	};
	
	private void timerMethod() {
		this.runOnUiThread(timerTick);
	}
	
	TimerTask timerTask = new TimerTask() {
	    @Override
	    public void run() {
           	timerMethod();
	    }
	};
	
	
	// 01/15/2013
	private BroadcastReceiver batInfoReceiverBroadcastReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context c, Intent intent) {
	        String action = intent.getAction(); 
	        if (Intent.ACTION_BATTERY_CHANGED.equals(action)) {
				int batteryLevel = intent.getIntExtra("level", 0);
				int batteryScale = intent.getIntExtra("scale", 100);
				int batteryPlugged = intent.getIntExtra("plugged", 0);		
				if (batteryPlugged == 0) {
					if ((batteryLevel * 100 / batteryScale) <= GPSSettings.BATTERY_LEVEL_WHEN_SAVE_TRACKLOG_AND_FINISH_TRACKING) {
						if (GPSSettings.IS_LOGGING_STARTED) {
							stopLogging();				
						}	
					}
				}
	        }
		}

	};
	
	
	class ErrorhandlerThread extends Thread {
		private Context context;
		private Exception exception;
		
		public ErrorhandlerThread() {
			super();
		}
		
		public ErrorhandlerThread(Context c, Exception e) {
			this();
			this.context = c;
			this.exception = e;
		}

	}
	
	
	class GTLUncaughtExceptionHandler implements UncaughtExceptionHandler {
		private Context context;
		private UncaughtExceptionHandler defaultUEH;
		
		public GTLUncaughtExceptionHandler(Context context) {
			this.context = context;
	        this.defaultUEH = Thread.getDefaultUncaughtExceptionHandler();
		}
		
		
		public Context getContext() {
			return context;
		}

		
		public void uncaughtException(Thread t, Throwable e) {
			// 10/29/2012
			final Writer result = new StringWriter();
			final PrintWriter printWriter = new PrintWriter(result);
			e.printStackTrace(printWriter);

			StringBuffer list = new StringBuffer();
			// device name and how many sensors are available
			list.append("GTL on " + ErrorHandler.extractDeviceName(true) + "\n\r ");
			// android version
			String androidVer = ErrorHandler.extractAndroidVersion();
			if (!androidVer.equals(""))
				list.append(ErrorHandler.extractAndroidVersion() + " \n\r ");
			list.append(" \n\r ");
			list.append("Stack Trace\n\r ");
			String stacktrace = result.toString();
			list.append(stacktrace);
			printWriter.close();
			// #5 10/06/2013
			Toast.makeText(context, stacktrace, Toast.LENGTH_LONG).show();
			String timeStamp = CommonLogger.calculateFileNameStamp();
			String fileName = CommonLogger.BASE_FILE_NAME + "E-" + timeStamp + ".log";

			SDCardManager.createFileForString(context, fileName, list.toString());

			defaultUEH.uncaughtException(t, e);
		}
	
	}
	
	private Activity sendLocationInfoToMapTab(double longitude, double latitude, float speedv) {
		Activity mapActivity = null;
		Activity activity = getLocalActivityManager().getActivity(TAB_TAG_MAP);
		if (activity == null)
			return null;
		if (!GPSSettings.IS_USE_OFFLINE_MAP) {
			// Google Map
			if (activity instanceof MapTab) {
				MapTab mapTab = (MapTab)activity;
				mapActivity = mapTab;
				mapTab.showLocationOnMap(longitude, latitude, speedv);
			}
		}
		else {
			// Offline Map
			if (activity instanceof MapTabMapForge) {
				MapTabMapForge mapTabMapForge = (MapTabMapForge)activity;
				mapActivity = mapTabMapForge;
				mapTabMapForge.showLocationOnMap(longitude, latitude, speedv);
			}
		}
		return mapActivity;
	}
	
	
	/*
	 * GPS related classes
	 */
	
	
	/**
	 * 
	 * @author lkovari
	 *
	 */
	class SensorListener implements SensorEventListener {
		//ramp-speed - play with this value until satisfied
		private final float kalmanFilteringFactor = 0.1f;
		float[] lastAccel; 
		float[] gravity = {0,0,0};
	    float[] mR = new float[16];
	    float[] mI = new float[16];
	    private int lastAccuracyAccelero = 0;
	    private int lastAccuracyOrientation = 0;
	    private int lastAccuracyMagneticField = 0;
	    
        private String getSensorAccuracyString(int accuracy) {
            String accuracyString = "Unknown";
            switch(accuracy) {
            	case SensorManager.SENSOR_STATUS_ACCURACY_HIGH: accuracyString = "High"; break;
                case SensorManager.SENSOR_STATUS_ACCURACY_LOW: accuracyString = "Low"; break;
                case SensorManager.SENSOR_STATUS_ACCURACY_MEDIUM: accuracyString = "Medium"; break;
                case SensorManager.SENSOR_STATUS_UNRELIABLE: accuracyString = "Unreliable"; break;
                default: accuracyString = "Unknown"; break;
            }
            return accuracyString;
        }
	    
		@Override
		public void onSensorChanged(SensorEvent event) {
			float[] gravityValues = null;
			float[] compassValues = null;
			
			int type = event.sensor.getType();
			if (type == Sensor.TYPE_ACCELEROMETER) {
				lastAccuracyAccelero = event.accuracy;
				
				gravityValues = event.values.clone();

				accels = event.values;
				//high-pass filter to eleminate gravity
				gravity[0] = kalmanFilteringFactor * gravity[0] + (1 - kalmanFilteringFactor) * accels[0];
			    gravity[1] = kalmanFilteringFactor * gravity[1] + (1 - kalmanFilteringFactor) * accels[1];
			    gravity[2] = kalmanFilteringFactor * gravity[2] + (1 - kalmanFilteringFactor) * accels[2];

			    accels[0] = accels[0] - gravity[0];
			    accels[1] = accels[1] - gravity[1];
			    accels[2] = accels[2] - gravity[2];
				
				cntAccelerometer++;
				sumAccelerometerX += accels[0];
				sumAccelerometerY += accels[1];
				sumAccelerometerZ += accels[2];
				if (cntAccelerometer > acceleroAvgCnt) {
					lastAccelerometerX = sumAccelerometerX / acceleroAvgCnt;
					lastAccelerometerY = sumAccelerometerY / acceleroAvgCnt;
					lastAccelerometerZ = sumAccelerometerZ / acceleroAvgCnt;
	                sumAccelerometerX = 0;
	                sumAccelerometerY = 0;
	                sumAccelerometerZ = 0;
					cntAccelerometer = 0;
				}
			}
			else if (type == Sensor.TYPE_GYROSCOPE) {
			}
			else if ( type == Sensor.TYPE_MAGNETIC_FIELD) {
				lastAccuracyMagneticField = event.accuracy;
				compassValues = event.values.clone();
			}
			else if (type == Sensor.TYPE_ORIENTATION) {
				/*
				 Azimuth 0-359 0=North, 90=East, 180=South, 270=West
				 Pitch -180 - +180 rotation around x 
				 Roll -90 - +90 rotation around y  
				*/
				lastAccuracyOrientation = event.accuracy;
				orient = event.values;
				cntOrientation++;
				sumOrientationAzimuth += orient[0];
				sumOrientationPitch += orient[1];
				//correction
				sumOrientationRoll += (orient[2] + 3.55);
				if (cntOrientation > orientAvgCnt) {
					lastOrientationAzimuth = sumOrientationAzimuth / orientAvgCnt;
					lastOrientationPitch = sumOrientationPitch / orientAvgCnt;
					lastOrientationRoll = sumOrientationRoll / orientAvgCnt;
					cntOrientation = 0;
					sumOrientationAzimuth = 0;
					sumOrientationPitch = 0;
					sumOrientationRoll = 0;
				}
				
			}
			else if (type == Sensor.TYPE_PRESSURE) {
			}
			else if (type == Sensor.TYPE_PROXIMITY) {
			}
			else if (android.os.Build.VERSION.SDK_INT >= 14) {
		         // #13 1.02.10. Ambient Temperature
				if (type == Sensor.TYPE_AMBIENT_TEMPERATURE) {
					ambientTemperature = event.values[0];
				}
			}	

			// If gravity and compass have values then find rotation matrix
		    if (gravityValues != null && compassValues != null) {
		        // checks that the rotation matrix is found
		        boolean success = SensorManager.getRotationMatrix(mR, mI, gravityValues, compassValues);
		        if (success) {
		        	float[] orientVals = {0,0,0};
		            SensorManager.getOrientation(mR, orientVals);
		            float azimuth = (float) Math.toDegrees(orientVals[0]);
		            float pitch = (float) Math.toDegrees(orientVals[1]);
		            float roll = (float) Math.toDegrees(orientVals[2]);
		            
		            if (CommonLogger.IS_LOGGER_ENABLED) {
		            	String bearingText = "";
		            	if (currentLoc != null)
		            		bearingText = " Bearing " + currentLoc.getBearing();
		            	String azimuthText = "> Calculated azimuth " + Utils.roundTo(azimuth, 1) + " azimuth + " + lastOrientationAzimuth + bearingText +  " "; 
		            	String pitchText = "> Calculated pitch " + Utils.roundTo(pitch, 1) + " azimuth + " + lastOrientationPitch + " "; 
		            	String rollText = "> Calculated roll " + Utils.roundTo(roll, 1) + " azimuth + " + lastOrientationRoll + " ";
		            	CommonLogger.log(Level.INFO, "SENSOR::" + azimuthText + pitchText + rollText);
		            }
		            
		        }
		    }
			
        }

		@Override
		public void onAccuracyChanged(Sensor sensor, int accuracy) {
//            if (isLogEventsActive) {
//                String sensorName = sensor.getName();
//            	logEvents.append("SENSOR " + sensorName + " Accuracy changed " + getSensorAccuracyString(accuracy) + "\r\n");
//            }
		}
	};
		
	/**
	 * 
	 * @author lkovari
	 *
	 */
	class GPSStatusListener implements GpsStatus.Listener {

		@Override
		public void onGpsStatusChanged(int event) {
			gpsStatusEvent = event;
            switch (event) {
            case GpsStatus.GPS_EVENT_FIRST_FIX: {
            	gpsStatusText = "1St.Fix";
            	// first fix
            	isFirstFix = true;
            	// 06/11/2013 #122
            	GPSSettings.IS_FIRST_FIX = isFirstFix;
            	// getMenuItem 
            	if (optionsMenu != null) {
    				MenuItem menuItem = optionsMenu.findItem(R.id.gps_itemStartLogging);
    				menuItem.setVisible(true);
            	}
				sendBroadcastStatus(gpsStatusText, maxSatellites, fixedSatellites, null, usedGlonassSatellites, usedGlonassSatellitesInFix, usedBeiDouSatellites, usedBeiDouSatellitesInFix);
            	break;
            }
            case GpsStatus.GPS_EVENT_STARTED: {
            	gpsStatusText = "Started";
				sendBroadcastStatus(gpsStatusText, maxSatellites, fixedSatellites, null, usedGlonassSatellites, usedGlonassSatellitesInFix, usedBeiDouSatellites, usedBeiDouSatellitesInFix);
            	break;
            }
            case GpsStatus.GPS_EVENT_STOPPED: {
            	gpsStatusText = "Stopped";
				sendBroadcastStatus(gpsStatusText, maxSatellites, fixedSatellites, null, usedGlonassSatellites, usedGlonassSatellitesInFix, usedBeiDouSatellites, usedBeiDouSatellitesInFix);
            	// #5 10/20/2013 restart application when no first fix and stopped
            	if (!isFirstFix) {
                	Intent intent = getBaseContext().getPackageManager().getLaunchIntentForPackage( getBaseContext().getPackageName());
                	intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                	startActivity(intent);
                	finish();
            	}
            	break;
            }
            case GpsStatus.GPS_EVENT_SATELLITE_STATUS: {
                if (gpsStatus == null) {
                	//08/12/2016 #16 v1.02.13 fix NPE
                	if (locationManager != null)
                		gpsStatus = locationManager.getGpsStatus(null);
                } else {
                	//08/12/2016 #16 v1.02.13 fix NPE
                	if (locationManager != null)
                		locationManager.getGpsStatus(gpsStatus);
                }    	        
                if (gpsStatus != null) {
                	// max satellites
                	int maxSatellitesCnt = gpsStatus.getMaxSatellites();
    	            // all satellites
    	            Iterable<GpsSatellite> gpsSatellites = gpsStatus.getSatellites();
    	            Iterator<GpsSatellite> iterator = gpsSatellites.iterator();
    	            // reset counter
    	            int usedGlonassCnt = 0;
    	            int usedGlonassInFixCnt = 0;
    	            int usedBeiDouCnt = 0;
    	            int usedBeiDouInFixCnt = 0;
    	            int fixedSatellitesCnt = 0;
    	            int unfixedSatellitesCnt = 0;
    	            averageSNR = 0.0;
    	            double sumSNR = 0.0;
    	            int cntSNR = 1;
    	            while (iterator.hasNext() && fixedSatellitesCnt <= maxSatellitesCnt) {
    	            	GpsSatellite gpsSat = iterator.next();
    	            	// is satellite valid?
    	            	if (gpsSat != null) {
    	            		// 07/09/2013 #122 is GLONASS satellite?
    	            		if ((gpsSat.getPrn() >= GLONASS_LOW_LIMIT) && (gpsSat.getPrn() <= GLONASS_HIGH_LIMIT)) {
    	            			usedGlonassCnt++;
    	            			if (gpsSat.usedInFix()) {
    	            				usedGlonassInFixCnt++;
    	            			}
    	            		}
    	            		else if ((gpsSat.getPrn() >= BEIDOU_LOW_LIMIT) && (gpsSat.getPrn() <= BEIDOU_HIGH_LIMIT)) {
    	            			// https://gergely.imreh.net/blog/
    	            			usedBeiDouCnt++;
    	            			if (gpsSat.usedInFix()) {
    	            				usedBeiDouInFixCnt++;
    	            			}
    	            		}
    	            		if (gpsSat.usedInFix()) {
    	            			fixedSatellitesCnt++;
    	            		}	
    	            		else {
    	            			unfixedSatellitesCnt++;
    	            		}
    	            		sumSNR += gpsSat.getSnr();
    	            		averageSNR = sumSNR / cntSNR;
    	            		cntSNR++;
    	            	}
    	            }
    	            // 07/09/2013 #122
            		usedGlonassSatellites = usedGlonassCnt;
            		usedGlonassSatellitesInFix = usedGlonassInFixCnt;
            		// 19/07/2014 #9 v1.02.6
            		usedBeiDouSatellites = usedBeiDouCnt;
            		usedBeiDouSatellitesInFix = usedBeiDouInFixCnt;
            		
//    				if (satsTextView != null) {
//    					satsTextView.setText("Satellites fixed " + fixedSats);
//    				}
    	            
    	            // possible calculation of max satellites
    	            if (unFixedSatellites != unfixedSatellitesCnt) {
    	            	unFixedSatellites = unfixedSatellitesCnt;
    	            	maxSatellitesCnt = fixedSatellitesCnt + unfixedSatellitesCnt;
    	            }
    				if (fixedSatellites != fixedSatellitesCnt) {
    					fixedSatellites = fixedSatellitesCnt;
    				}
    				if (maxSatellites != maxSatellitesCnt) {
    					maxSatellites = maxSatellitesCnt;
    				}
    				sendBroadcastStatus(gpsStatusText, maxSatellites, fixedSatellites, averageSNR, usedGlonassSatellites, usedGlonassSatellitesInFix, usedBeiDouSatellites, usedBeiDouSatellitesInFix);
    	        }
            	break;
            }
            }
		}
		
	}
	

	
	
    /**
     * 
     * @author lkovari
     *
     */
	class GPSLocationListener implements LocationListener {
		private static final int TWO_MINUTES = 1000 * 60 * 2;

		/** Determines whether one Location reading is better than the current Location fix
		  * @param location  The new Location that you want to evaluate
		  * @param currentBestLocation  The current Location fix, to which you want to compare the new one
		  */
		protected boolean isBetterLocation(Location location, Location currentBestLocation) {
		    if (currentBestLocation == null) {
		        // A new location is always better than no location
		        return true;
		    }

		    // Check whether the new location fix is newer or older
		    long timeDelta = location.getTime() - currentBestLocation.getTime();
		    boolean isSignificantlyNewer = timeDelta > TWO_MINUTES;
		    boolean isSignificantlyOlder = timeDelta < -TWO_MINUTES;
		    boolean isNewer = timeDelta > 0;

		    // If it's been more than two minutes since the current location, use the new location
		    // because the user has likely moved
		    if (isSignificantlyNewer) {
		        return true;
		    // If the new location is more than two minutes older, it must be worse
		    } else if (isSignificantlyOlder) {
		        return false;
		    }

		    // Check whether the new location fix is more or less accurate
		    int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
		    boolean isLessAccurate = accuracyDelta > 0;
		    boolean isMoreAccurate = accuracyDelta < 0;
		    boolean isSignificantlyLessAccurate = accuracyDelta > 200;

		    // Check if the old and new location are from the same provider
		    boolean isFromSameProvider = isSameProvider(location.getProvider(),
		            currentBestLocation.getProvider());

		    // Determine location quality using a combination of timeliness and accuracy
		    if (isMoreAccurate) {
		        return true;
		    } else if (isNewer && !isLessAccurate) {
		        return true;
		    } else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
		        return true;
		    }
		    return false;
		}

		/** Checks whether two providers are the same */
		private boolean isSameProvider(String provider1, String provider2) {
		    if (provider1 == null) {
		      return provider2 == null;
		    }
		    return provider1.equals(provider2);
		}		
		
		private void storedTimeInLongAndDate(double time, String postfix) {
			Date timeInDate = new Date();
			timeInDate.setTime((long) time);
			logEvents.append("Main.storedTimeInLongAndDate::ZERO Time  Date " + postfix + time + " " + timeInDate.toString() + "\r\n");
			timeInDate = null;
		}

		private void currentTimeInLongAndDate(double time, Float speed, String postfix) {
			Date timeInDate = new Date();
			timeInDate.setTime((long) time);
			if (speed != null)
				logEvents.append("Main.storedTimeInLongAndDate::CURRENT Time  Date Speed " + postfix + Utils.roundTo(time, 3) + " " + timeInDate.toString() + " " + speed + "\r\n");
			else	
				logEvents.append("Main.storedTimeInLongAndDate::CURRENT Time  Date " + postfix + Utils.roundTo(time, 3) + " " + timeInDate.toString() + "\r\n");
			timeInDate = null;
		}
		
		private void resetAccelerationCounters(boolean isResetStart) {
			if (isResetStart) {
				startAccelerationToTime = (double) currentLoc.getTime();
				storedTimeInLongAndDate(startAccelerationToTime, "");
			}
			/*
			acceleration400 = null;
			acceleration200 = null;
			acceleration100 = null;
			acceleration50 = null;
			*/
		}
		
		/**
		 * 2012.06.04
		 * @param str
		 * @return
		 */
		private String replaceSpecialCharacters(String str) {
			str = str.replace("&", "and");
			return str;
		}
		
		
		@Override
		public void onLocationChanged(Location currLoc) {
			try {
				if (currLoc != null) {
					// 26/1/2014 #8 v1.02.5
					if (GPSSettings.DATA_PROVIDER_KIND.equals(DataProviderKind.NETWORK_PROVIDER)) {
						// if not use Best provider or GPS provider first fix event (GpsStatus.GPS_EVENT_FIRST_FIX) can't be occur
						isFirstFix = true; 
					}

					
					// 26/1/2014 #8 v1.02.5 if 
					if (GPSSettings.DATA_PROVIDER_KIND.equals(DataProviderKind.BEST_PROVIDER) || GPSSettings.DATA_PROVIDER_KIND.equals(DataProviderKind.GPS_PROVIDER)) {
						// skip inaccurate values
						if ((currLoc.getAccuracy() > GPSSettings.MIN_ACCURACY) && (fixedSatellites < GPSSettings.MIN_SATELLITES)) {
							return;
						}
					}	
					
					// 26/1/2014 #8 v1.02.5
					if (GPSSettings.DATA_PROVIDER_KIND.equals(DataProviderKind.BEST_PROVIDER)) {
						// check speed by criteria
						if ((criteria == null) || ((criteria != null) && criteria.isSpeedRequired() && !currLoc.hasSpeed())) {
							return;
						}	
					}
					
					locationEventCnt++;
					// reset odometer at the first
					if (currentLoc == null) {
						odometer = 0;
					}
					currentLoc = currLoc;
					// add bearing
					averageValuesManager.addValueFloat(currentLoc.getBearing());
					
					// check speed
					checkSpeedAndSetLocationParameters(currentLoc);
                	
					// 03/17/2015 #11 v1.02.8
	        		long elapsedTimeInMove = elapsedInMove + currentInMove;
	        		long elapsedTimeInWait = elapsedInWait + currentInWait;
	        		long elapsedTimeTotal = elapsedTimeInMove + elapsedTimeInWait;					
					
					// #5 10/12/2013
					if (currentLoc != null) {
						
						// is speed zero?
						if (currentLoc.getSpeed() == 0.0) {
							// speed is zero
							switch (elapsedTimeKind) {
							case IN_MOVE : {
						        // calculate elapsed in move and add
								long elm = (System.currentTimeMillis() - elapsedInMoveStart);
								// initialize elapsedInWaitStart
						        elapsedInWaitStart = System.currentTimeMillis();
						        // collect it
					        	elapsedInMove += elm;
					        	currentInMove = 0;
								// Switch to IN_WAIT
								elapsedTimeKind = ElapsedTimeKind.IN_WAIT;
								break;
							}
							case IN_WAIT : {
								currentInWait = (System.currentTimeMillis() - elapsedInWaitStart);
								break;
							}
							}
						}
						else {
							// speed is non zero
							switch (elapsedTimeKind) {
							case IN_MOVE : {
								currentInMove = (System.currentTimeMillis() - elapsedInMoveStart);
								break;
							}
							case IN_WAIT : {
						        // calculate elapsed in wait
								long elw = (System.currentTimeMillis() - elapsedInWaitStart);
								// initialize elapsed in move start
						        elapsedInMoveStart = System.currentTimeMillis();
						        // collect it
					        	elapsedInWait += elw;
					        	currentInWait = 0;
								// not in IN_MOVE mode switch to IN_MOVE
								elapsedTimeKind = ElapsedTimeKind.IN_MOVE;
								break;
							}
							}
						}
						
						
					}
					
					speedSum += currentLoc.getSpeed();
					speedCnt++;

                	// check is in curve
                	if (prevLoc != null) {
                		// 12/25/2012
                		float distance = (prevLoc.distanceTo(currentLoc));

    					// 26/1/2014 #8 v1.02.5
                		if (GPSSettings.DATA_PROVIDER_KIND.equals(DataProviderKind.BEST_PROVIDER) || GPSSettings.DATA_PROVIDER_KIND.equals(DataProviderKind.GPS_PROVIDER)) {
                			// if speed is greater than zero
                			if (currentLoc.getSpeed() > 0) {
                				odometer += distance;
                			}	
                			currentDistance += distance;
                		}
                		else {
            				odometer += distance;
            				currentDistance += distance;
                		}
                	}
                	
               		lastBearing = currentBearing;
            		currentBearing = averageValuesManager.getAverageFloat(); 

                	// check curve
            		if ((lastBearing != 0.0) && (currentBearing != 0.0)) {
    					float diff = (float) 0.0;
    					if (currentBearing > lastBearing) {
    						diff = currentBearing - lastBearing;
    						if (diff > CURVE_CRITERIA) 
    							isInCurve = true;
    						else
    							isInCurve = false;
    					}
    					else if (currentBearing < lastBearing) {
    						diff = lastBearing - currentBearing;
    						if (diff > CURVE_CRITERIA) 
    							isInCurve = true;
    						else
    							isInCurve = false;
    					}
    					else if (currentBearing == lastBearing) {
    						isInCurve = false;
    					}
            		}
                	
					// 26/1/2014 #8 v1.02.5 if
            		boolean isNeedToStoreLocation = false;
					if (GPSSettings.DATA_PROVIDER_KIND.equals(DataProviderKind.BEST_PROVIDER) || GPSSettings.DATA_PROVIDER_KIND.equals(DataProviderKind.GPS_PROVIDER)) {
						isNeedToStoreLocation = isNeedToStoreLocation(refDistance, currentDistance);
					}
					else {
						// if the provider is not GPS or Best all location will stored
						isNeedToStoreLocation = true;
					}

    				// prevent store more location if speed is zero
    				boolean isPrevLocSpeedisZero = (prevLoc != null) && (prevLoc.getSpeed() == 0) && (prevLoc.getBearing() == 0);
    				boolean isCurrentLocSpeedisZero = (currentLoc != null) && (currentLoc.getSpeed() == 0) && (currentLoc.getBearing() == 0);
    				// NO need to store if it is zero again
    				boolean isSpeedAndBearingWasZeroAgain = (isPrevLocSpeedisZero && isCurrentLocSpeedisZero);
    				
					GPSCoordinate gpsCoordinate = null;

					double leftAngle = 0.0;
					double rightAngle = 0.0;
					if (lastOrientationRoll < 0) {
						rightAngle = Math.abs(lastOrientationRoll);
					}	
					else {
						leftAngle = Math.abs(lastOrientationRoll);
					}	
					float avgSpeed = (speedSum / speedCnt);

//					long elapsedTime = System.currentTimeMillis() - elapsed;
					//03/17/2015 #11 v1.02.8 long elapsedTime = elapsedInMove + elapsedInWait;
					//12/15/2012 added roll
//					gpsCoordinate = new GPSCoordinate(currentLoc, avgSpeed, Double.valueOf(odometer), lastAccelerometerX, lastAccelerometerY, lastAccelerometerZ, null, leftAngle, rightAngle, lastOrientationPitch, lastOrientationAzimuth, lastOrientationRoll, fixedSatellites, elapsedTimeTotal, averageSNR, ambientTemperature,true);
					// #13 1.02.10. 2015.11.29. builder pattern
					gpsCoordinate = new GPSCoordinate.Builder(currentLoc, true)
							.averageSpeed(avgSpeed)
							.odometer( Double.valueOf(odometer))
							.accelerationX(lastAccelerometerX)
							.accelerationY(lastAccelerometerY)
							.accelerationZ(lastAccelerometerZ)
							.accelerationTo(null).leftAngle(leftAngle)
							.rightAngle(rightAngle).pitch(lastOrientationPitch)
							.azimuth(lastOrientationAzimuth)
							.roll(lastOrientationRoll)
							.satellites(fixedSatellites)
							.elapsedTime(elapsedTimeTotal)
							.ambientTemperature(ambientTemperature)
							.averageStrength(averageSNR).build();
					
					gpsCoordinate.setElapsedTimeInMove(elapsedTimeInMove);
					gpsCoordinate.setElapsedTimeInWait(elapsedTimeInWait);
					
					// 17/08/2014 #9 v1.02.6
			    	if (GPSSettings.IS_ENABLE_REALTIME_POSITION_SENDING) {
			    		// 22/3/2014 #9 v1.02.6
			    		if (GPSSettings.IS_STORELOCATION_ON_WEB_FUNCTIONS_ENABLED) {
			    			if (currLoc != null) {
			    				long time = currentLoc.getTime();
			    				long timeInMinutes = time / (60000);
			    				// in every five minutes
			    				if ((timeInMinutes % GPSSettings.STORELOCATION_EVERY_N_MINUTES) == 0) {
			    					// once in time
			    					if (timeInMinutes != lastStore) {
			    						try {
			    							storeLocationAsyncTask = new StoreLocationAsyncTask();
			    							storeLocationAsyncTask.execute(STORE_LOC);
			    						}
			    						finally {
			    							lastStore = timeInMinutes;
			    						}
			    					}
			    				}
			    			}
			    		}
			    	}

                	// 03/12/2016 #15 v1.02.11 
			    	isForceSaveGPSCoordinatesAndStopLogging = false;
					
    				// is switched on storing tracklog?
					if (GPSSettings.IS_LOGGING_STARTED) {
	                	// just runners
	                	if (GPSSettings.USAGE_TYPE.equals(UsageType.RUNNER)) {
                			// just the distance option is switched on
                			if (GPSSettings.RUNNER_DISTNACE > 0) {
                				// current distance in meters over the target of runner distance?
                				if (gpsCoordinate.getOdometer() >= GPSSettings.RUNNER_DISTNACE) {
                					isForceSaveGPSCoordinatesAndStopLogging = true;
                				}
                			}
                			// just the elapsed time option is switched on
                			if (GPSSettings.RUNNER_ELAPSED_TIME > 0) {
                				// elapsed time (in mils) is over the target of runner elapsed time?
                				if (gpsCoordinate.getElapsedTime() >= (GPSSettings.RUNNER_ELAPSED_TIME * 60000)) {
                					isForceSaveGPSCoordinatesAndStopLogging = true;
                				}
                			}
	                	}

						// 03/17/2015 #11 v1.02.8
						gpsCoordinate.setElapsedTimeInMove(elapsedTimeInMove);
						gpsCoordinate.setElapsedTimeInWait(elapsedTimeInWait);
	                	
						// is need to store location? current distance is over reference distance and was not zero the speed
	    				if (isNeedToStoreLocation || isForceSaveGPSCoordinatesAndStopLogging) {
	    					if (gpsCoordinate.getLocation().getSpeed() == 0) {
	    						String address = location2Address(gpsCoordinate.getLocation(), "");
	    						if (address != null) {
		    						address = replaceSpecialCharacters(address);
	    							gpsCoordinate.setAddress(address);
	    						}	
	    					}

	    					// storing GPS location to tracklog
    						GPSSettings.gpsCoordinates.add(gpsCoordinate);
    						storedLocationCnt++;
            			}

	    				
						// keep max angles
	    				if ((maxSpeedValue != null) && (gpsCoordinate != null))
	    					maxSpeedValue.captureMaxSpeedValues(gpsCoordinate.getLocation().getSpeed(), gpsCoordinate.getLocation().getLongitude(), gpsCoordinate.getLocation().getLatitude());
	    				if ((maxAltitudeValue != null) && (gpsCoordinate != null))
	    					maxAltitudeValue.captureMaxAltitude(gpsCoordinate.getLocation().getAltitude(), gpsCoordinate.getLocation().getLongitude(), gpsCoordinate.getLocation().getLatitude());
	    				if ((maxAccelerationValue != null) && (gpsCoordinate != null))
	    					maxAccelerationValue.captureMaxAccelerationValues(gpsCoordinate.getAccelerationx(), gpsCoordinate.getAccelerationy(), gpsCoordinate.getAccelerationz(), gpsCoordinate.getLocation().getLongitude(), gpsCoordinate.getLocation().getLatitude());
	    				if ((maxOrientationValue != null) && (gpsCoordinate != null))
	    					maxOrientationValue.captureMaxOrientationValues(gpsCoordinate.getLeftAngle(), gpsCoordinate.getRightAngle(), gpsCoordinate.getLocation().getLongitude(), gpsCoordinate.getLocation().getLatitude());
	    				// #13 1.02.10 2015.11.29.
	    				if ((minMaxTemperatureValue != null) && (gpsCoordinate != null))
	    					minMaxTemperatureValue.captureMinMaxTemperature(ambientTemperature, gpsCoordinate.getLocation().getLongitude(), gpsCoordinate.getLocation().getLatitude());
	    				
					}
					else {
						// tracklog storing switch off
						
            			if (isNeedToStoreLocation) {
	    					// Not storing GPS location to tracklog because track log is off
            				// reset current distance
            				currentDistance = 0;
            			}
					}
					
					// refresh GUI data
					// #5 10/2062013 update
					// 03/12/2016 #15 v1.02.11 removed odometer
					sendBroadcastLocation(gpsCoordinate);

					if (isNeedToStoreLocation) {
    					// location stored
        				currentDistance = 0;
					}	
					
					// 03/12/2016 #15 v1.02.11 
					if (isForceSaveGPSCoordinatesAndStopLogging) {
    					// alert, distance reached
    					stopLogging();
    					allertSignal();
    					// hide start show stop
    					GPSSettings.IS_LOGGING_STARTED = false;
    					GPSSettings.RUNNER_DISTNACE = 0;
    					GPSSettings.RUNNER_ELAPSED_TIME = 0;
					}
					
					gpsCoordinate = null;

					prevLoc = currentLoc;
				}
			}
			catch (Exception e) {
				StackTraceElement[] trace = e.getStackTrace();
				String callStack = e.getMessage();
				if (trace != null) {
					for (int ix = 0; ix < trace.length; ix++) {
						StackTraceElement ste = trace[ix];
						String line = ste.getFileName() + " " + ste.getClassName() + " " + ste.getMethodName() + " " + ste.getLineNumber()+ "\r\n";
						callStack += line;
					}
				}
				ErrorHandler.createErrorLog(getApplicationContext(), e);
//    			Toast.makeText(getApplicationContext(), "ERROR in LocationListener " + e.getMessage(), Toast.LENGTH_LONG).show();
			}
		}

		@Override
		public void onProviderDisabled(String arg0) {
			isProviderEnabled = false;
			CommonLogger.log(Level.INFO, "PROVIDER " + arg0 + " disabled");
		}

		@Override
		public void onProviderEnabled(String arg0) {
			isProviderEnabled = true;
			CommonLogger.log(Level.INFO, "PROVIDER " + arg0 + " enabled");
		}

		@Override
		public void onStatusChanged(String arg0, int arg1, Bundle arg2) {
			switch (arg1) {
			case LocationProvider.OUT_OF_SERVICE : {
				// out of service
            	gpsStatusText = "OutOfSvc";
				isServiceAvailable = false;
				sendBroadcastStatus(gpsStatusText, maxSatellites, fixedSatellites, null, usedGlonassSatellites, usedGlonassSatellitesInFix, usedBeiDouSatellites, usedBeiDouSatellitesInFix);
				CommonLogger.log(Level.INFO, "PROVIDER " + arg0 + " out of service");
				break;
			}
			case LocationProvider.TEMPORARILY_UNAVAILABLE : {
				// temporarily unavailable
            	gpsStatusText = "Unavail.";
				isServiceAvailable = false;
				sendBroadcastStatus(gpsStatusText, maxSatellites, fixedSatellites, null, usedGlonassSatellites, usedGlonassSatellitesInFix, usedBeiDouSatellites, usedBeiDouSatellitesInFix);
				CommonLogger.log(Level.INFO, "PROVIDER " + arg0 + " temporarily unavailable");
				break;
			}
			case LocationProvider.AVAILABLE : {
				// available
            	gpsStatusText = "Avail.";
				isServiceAvailable = true;
				sendBroadcastStatus(gpsStatusText, maxSatellites, fixedSatellites, null, usedGlonassSatellites, usedGlonassSatellitesInFix, usedBeiDouSatellites, usedBeiDouSatellitesInFix);
				CommonLogger.log(Level.INFO, "PROVIDER " + arg0 + " available");
				break;
			}
			}
		}

	}	


	/**
	 * 
	 * @param maxSatellites
	 * @param satellitesUsedInFix
	 */
    public void sendBroadcastStatus(String status, int maxSatellites, int satellitesUsedInFix, Double satAvgSNR, int gnss, int gnssinfix, int bds, int bdsinfix){
        Intent broadcast = new Intent();
        broadcast.setAction(BROADCAST_ACTION_GPS_STATUS_DATA_CHANGED);
        broadcastStatusCnt++;
        broadcast.putExtra(GUIDataIdentifiers.GPS_DATA_BROADCAST_STATUS_IX, broadcastStatusCnt);
        broadcast.putExtra(GUIDataIdentifiers.GPS_DATA_STATUS, status);
//        broadcast.putExtra(GUIDataIdentifiers.GPS_DATA_SATELLITES, maxSatellites);
        broadcast.putExtra(GUIDataIdentifiers.GPS_DATA_SATELLITESINFIX, satellitesUsedInFix);
        broadcast.putExtra(GUIDataIdentifiers.GPS_DATA_SATELLITESAVGSNR, satAvgSNR);
        // 07/09/2013
        broadcast.putExtra(GUIDataIdentifiers.GPS_DATA_GLONASS_SATELLITES, gnss);
        broadcast.putExtra(GUIDataIdentifiers.GPS_DATA_GLONASS_SATELLITES_INFIX, gnssinfix);
        // 19/07/2014 #9 v1.02.6
        broadcast.putExtra(GUIDataIdentifiers.GPS_DATA_BEIDOU_SATELLITES, bds);
        broadcast.putExtra(GUIDataIdentifiers.GPS_DATA_BEIDOU_SATELLITES_INFIX, bdsinfix);
        sendBroadcast(broadcast);
    }

    /**
     * 
     * @param gpscoordinateex
     */
    public void sendBroadcastLocation(GPSCoordinate gpscoordinate){
    	if (gpscoordinate == null || gpscoordinate.getLocation() == null)
    		return;
        Intent broadcast = new Intent();
        broadcast.setAction(BROADCAST_ACTION_GPS_LOCATION_DATA_CHANGED);
        broadcastLocationCnt++;
        broadcast.putExtra(GUIDataIdentifiers.GPS_DATA_BROADCAST_LOCATION_IX, broadcastLocationCnt);
        // 03/12/2016 #15 v1.02.11 removed odometer, using the gpsCoordinate odometer
        broadcast.putExtra(GUIDataIdentifiers.ROUTE_DATA_ODOMETER, gpscoordinate.getOdometer());
        broadcast.putExtra(GUIDataIdentifiers.GPS_DATA_PROVIDER, gpscoordinate.getLocation().getProvider());
        broadcast.putExtra(GUIDataIdentifiers.GPS_DATA_ACCURACY, gpscoordinate.getLocation().getAccuracy());
        broadcast.putExtra(GUIDataIdentifiers.GPS_DATA_LONGITUDE, gpscoordinate.getLocation().getLongitude());
        broadcast.putExtra(GUIDataIdentifiers.GPS_DATA_LATITUDE, gpscoordinate.getLocation().getLatitude());
        broadcast.putExtra(GUIDataIdentifiers.ROUTE_DATA_ALTITUDE, gpscoordinate.getLocation().getAltitude());
//        if (gpscoordinate.getLocation().hasSpeed())
        	broadcast.putExtra(GUIDataIdentifiers.ROUTE_DATA_SPEED, gpscoordinate.getLocation().getSpeed());
//        else
//        	broadcast.putExtra(GUIDataIdentifiers.ROUTE_DATA_SPEED, new Float(1));
        // 01/02/2013
    	broadcast.putExtra(GUIDataIdentifiers.ROUTE_DATA_AVGSPEED, new Float(gpscoordinate.getAvgSpeed()));
        broadcast.putExtra(GUIDataIdentifiers.ROUTE_DATA_BEARING, gpscoordinate.getLocation().getBearing());
        sendBroadcast(broadcast);
        String tabTag = tabHost.getCurrentTabTag();
        //#5 10/25/2013
        if (!tabTag.equals(TAB_TAG_MAP)) {
        	sendLocationInfoToMapTab(gpscoordinate.getLocation().getLongitude(), gpscoordinate.getLocation().getLatitude(), gpscoordinate.getLocation().getSpeed());
        }
    }

    /**
     * {@link #acceleration50} 10/19/2013
     * @param elapsedKind
     * @param elapsedInMove
     * @param elapsedInWait
     */
    public void sendBroadcastElapsedInMoveInWait(long elapsed, long elapsedInMove, long elapsedInWait){
        Intent broadcast = new Intent();
        broadcast.setAction(BROADCAST_ACTION_GPS_ELLAPSED_IN_MOVE_OR_IN_WAIT_CHANGED);
        // #5 10/12/2013
        broadcast.putExtra(GUIDataIdentifiers.ROUTE_DATA_ELAPSED, elapsed);
        broadcast.putExtra(GUIDataIdentifiers.ROUTE_DATA_ELAPSED_IN_MOVE, elapsedInMove);
        broadcast.putExtra(GUIDataIdentifiers.ROUTE_DATA_ELAPSED_IN_WAIT, elapsedInWait);
        sendBroadcast(broadcast);
    }
    
	/**
	 * 
	 * @param reference - reference distance
	 * @param distance - current distance
	 * @return true if current distance is greater than reference
	 */
	private boolean isNeedToStoreLocation(float reference, float distance) {
		return (distance >= reference); 
	}
	
	/**
	 * 
	 * @param loc
	 * @return
	 */
	private float calculateRealNorth(Location loc) {
		GeomagneticField eomagneticField = new GeomagneticField(Double.valueOf(loc.getLatitude()).floatValue(),
				 Double.valueOf(loc.getLongitude()).floatValue(),
				 Double.valueOf(loc.getAltitude()).floatValue(),
				 System.currentTimeMillis());
		float realNorth = eomagneticField.getDeclination();
		eomagneticField = null;
		return realNorth;
	}
	
	
	/**
	 * SaveLogEvents
	 */
//	private void saveLogEvents() {
//		String logEventsFileName = Logger.BASE_FILENAME + "E-" + currentFileStamp + ".txt";  
//		logEvents.append("LocationEvent call " + locationEventCnt + "\r\n");
//		logEvents.append("Stored locations " + storedLocationCnt + "\r\n");
//		SDCardManager.createFile(getApplicationContext(), logEventsFileName, logEvents);
//	}
	
	
	/**
	 * 
	 * @param loc
	 * @return
	 */
    private String location2Address(Location loc, String prefix) {
    	String addr = null;
    	List<Address> addresses;
    	try {
    		Geocoder geocoder = new Geocoder(this, Locale.ENGLISH);
    		addresses = geocoder.getFromLocation(loc.getLatitude(), loc.getLongitude(), 1);
    		if ((addresses != null) && (addresses.size() > 0)) {
    			Address currentAddr = addresses.get(0);
    			StringBuilder sb = new StringBuilder("Address:\n");
    			for(int i=0; i<currentAddr.getMaxAddressLineIndex(); i++) {
    				sb.append(currentAddr.getAddressLine(i)).append("\n");
    			}
    			addr = sb.toString();
    		}
    	} 
    	catch (IOException e) {
    		addr = null;
    		CommonLogger.log(Level.WARNING, "Address can't evaluate " + prefix);
    	}    	
    	return addr;
    }

	
    

    private void checkSpeedAndSetLocationParameters(Location loc) {
    	if (loc == null)
    		return;
    	double speedInKm = Utils.meterpersec2kilometer(loc.getSpeed());
    	float prevRefDistance = refDistance;
    	if (speedInKm == SPEED_0) {
    		if (refDistance != SPEED_0_MINDISTANCE) {
    			refDistance = SPEED_0_MINDISTANCE;
    		}
    	}
    	else if ((speedInKm > SPEED_0) && (speedInKm <= SPEED_5)) {
    		if (refDistance != SPEED_5_MINDISTANCE) {
    			refDistance = SPEED_5_MINDISTANCE;
    		}
    	}
    	else if ((speedInKm > SPEED_5) && (speedInKm <= SPEED_10)) {
    		if (refDistance != SPEED_10_MINDISTANCE) {
    			refDistance = SPEED_10_MINDISTANCE;
    		}
    	}
    	else if ((speedInKm > SPEED_10) && (speedInKm <= SPEED_25)) {
    		if (refDistance != SPEED_25_MINDISTANCE) {
    			refDistance = SPEED_25_MINDISTANCE;
    		}
    	}
    	else if ((speedInKm > SPEED_25) && (speedInKm <= SPEED_50)) {
    		if (refDistance != SPEED_50_MINDISTANCE) {
    			refDistance = SPEED_50_MINDISTANCE;
    		}
    	}
    	else if ((speedInKm > SPEED_50) && (speedInKm <= SPEED_75)) {
    		if (refDistance != SPEED_75_MINDISTANCE) {
    			refDistance = SPEED_75_MINDISTANCE;
    		}
    	}
    	else if ((speedInKm > SPEED_75) && (speedInKm <= SPEED_100)) {
    		if (refDistance != SPEED_100_MINDISTANCE) {
    			refDistance = SPEED_100_MINDISTANCE;
    		}
    	}
    	else if ((speedInKm > SPEED_100) && (speedInKm <= SPEED_150)) {
    		if (refDistance != SPEED_150_MINDISTANCE) {
    			refDistance = SPEED_150_MINDISTANCE;
    		}
    	}
    	else if ((speedInKm > SPEED_150) && (speedInKm <= SPEED_200)) {
    		if (refDistance != SPEED_200_MINDISTANCE) {
    			refDistance = SPEED_200_MINDISTANCE;
    		}
    	}
    	else if ((speedInKm > SPEED_200) && (speedInKm <= SPEED_250)) {
    		if (refDistance != SPEED_250_MINDISTANCE) {
    			refDistance = SPEED_250_MINDISTANCE;
    		}
    	}
    	else if ((speedInKm > SPEED_250) && (speedInKm <= SPEED_300)) {
    		if (refDistance != SPEED_300_MINDISTANCE) {
    			refDistance = SPEED_300_MINDISTANCE;
    		}
    	}
    	else if ((speedInKm > SPEED_300) && (speedInKm <= SPEED_400)) {
    		if (refDistance != SPEED_400_MINDISTANCE) {
    			refDistance = SPEED_400_MINDISTANCE;
    		}
    	}
    	else if ((speedInKm > SPEED_400) && (speedInKm <= SPEED_500)) {
    		if (refDistance != SPEED_500_MINDISTANCE) {
    			refDistance = SPEED_500_MINDISTANCE;
    		}
    	}
    	else if ((speedInKm > SPEED_500) && (speedInKm <= SPEED_750)) {
    		if (refDistance != SPEED_750_MINDISTANCE) {
    			refDistance = SPEED_750_MINDISTANCE;
    		}
    	}
    	else if ((speedInKm > SPEED_750) && (speedInKm <= SPEED_1000)) {
    		if (refDistance != SPEED_1000_MINDISTANCE) {
    			refDistance = SPEED_1000_MINDISTANCE;
    		}
    	}
    	else if ((speedInKm > SPEED_1000) && (speedInKm <= SPEED_1500)) {
    		if (refDistance != SPEED_1500_MINDISTANCE) {
    			refDistance = SPEED_1500_MINDISTANCE;
    		}
    	}
    	else if ((speedInKm > SPEED_1500)) {
    		if (refDistance != SPEED_OVER_1500_MINDISTANCE) {
    			refDistance = SPEED_OVER_1500_MINDISTANCE;
    		}
    	}
    	if (isInCurve) {
//    		refDistance = refDistance / 2;
//			currentDistance = 0;
    	}
    }
	
    
    /**
     * 
     */
    private void resetToStart() {
    	// 12/25/2012
//    	elapsed = 0;
		currentDistance = 0;
    	points_origin = 0;    	
    	GPSSettings.gpsCoordinates.clear();
    	isInCurve = false;
    	currentLoc = null;
    	prevLoc = null;
    	trackLog = null;
    	trackLog = new StringBuffer();
		// #5 10/13/2013
        this.elapsedInWaitStart = -1;
        this.elapsedInMoveStart = -1;
        odometer = 0.0;
    }

    /**
     * 
     */
    private void registerLocationListener() {
    	try {
        	if (gpsLocationListener == null) {
        		gpsLocationListener = new GPSLocationListener();
        		CommonLogger.log(Level.INFO, "gpsLocationListener is null!");
        	}	
            //12/21/2012
            if (bestProvider != null) {
            	if (locationManager.isProviderEnabled(bestProvider)) {
                	locationManager.requestLocationUpdates(bestProvider, GPSSettings.MIN_TIME, GPSSettings.MIN_DISTANCE, gpsLocationListener);
                	isLocationListenerRegistered = true;
            	}
            	else {
            		// show message
            		String title = getApplicationContext().getString(R.string.message_provider_is_not_enabled_title);
            		String message = bestProvider + " " + getApplicationContext().getString(R.string.message_provider_is_not_enabled);
            		showMessageDialog(title, message);
            		setupLocationmanagerAndCriteria(DataProviderKind.BEST_PROVIDER);
                	locationManager.requestLocationUpdates(bestProvider, GPSSettings.MIN_TIME, GPSSettings.MIN_DISTANCE, gpsLocationListener);
                	isLocationListenerRegistered = true;
            	}
            }
            else {
        		setupLocationmanagerAndCriteria(DataProviderKind.BEST_PROVIDER);
           		locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, GPSSettings.MIN_TIME, GPSSettings.MIN_DISTANCE, gpsLocationListener);
           		isLocationListenerRegistered = true;
            }
    	}
    	finally {
   			if ((locationManager != null) && (gpsLocationListener != null))
   				CommonLogger.log(Level.INFO, "registerLocationListener T" + GPSSettings.MIN_TIME + " D" + GPSSettings.MIN_DISTANCE);
    	}
    }

    /**
     * 
     */
    private void unregisterLocationListener() {
    	try {
        	locationManager.removeUpdates(gpsLocationListener);
        	isLocationListenerRegistered = false;
    	}
    	finally {
    		CommonLogger.log(Level.INFO, "unregisterLocationListener");
    	}
    }
    
    /**
     * 
     */
    private void setupLocationmanagerAndCriteria(DataProviderKind provider) {
    	// 25/1/2014 #8 v1.02.5
    	DataProviderKind usedProvider = DataProviderKind.BEST_PROVIDER;
    	if (provider != null) 
    		usedProvider = provider;
    	else
    		usedProvider = GPSSettings.DATA_PROVIDER_KIND;
    		
    	if (locationManager == null)
    		locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        
        switch (usedProvider) {
        case BEST_PROVIDER : {
            criteria = new Criteria();
            criteria.setAccuracy(GPSSettings.CRITERIA_ACCURACY_LEVEL);
            criteria.setPowerRequirement(GPSSettings.CRITERIA_POWER_LEVEL);
            criteria.setSpeedRequired(GPSSettings.IS_CRITERIA_SPEED_REQUIRED);
            criteria.setAltitudeRequired(GPSSettings.IS_CRITERIA_ALTITUDE_REQUIRED);
            criteria.setBearingRequired(GPSSettings.IS_CRITERIA_BEARING_REQUIRED);
            criteria.setCostAllowed(false);
            
            bestProvider = locationManager.getBestProvider(criteria, true);

            // 27/1/2014 #8 v1.02.5
            // if not found find enabled
            int accuracy = Integer.MAX_VALUE;
            LocationProvider bestLocProv = null;
            // best provider is null or empty?
            if ((bestProvider == null) || ((bestProvider != null) && (bestProvider.trim() == ""))) {
                List<String> providersForCriteria = locationManager.getProviders(criteria, true);
               	for (String prov : providersForCriteria) {
               		// is enabled?
               		if (locationManager.isProviderEnabled(prov)) {
               			LocationProvider locProv = locationManager.getProvider(prov);
               			// is accuracy better than current stored?
               			if (locProv != null) {
               				if (accuracy > locProv.getAccuracy()) {
               					bestLocProv = locProv;
               					accuracy = locProv.getAccuracy();
               				}
               			}
               		}
               	}
               	if (bestLocProv != null)
               		bestProvider = bestLocProv.getName();
               	else {
                	checkGPSProvidersOption(true);
               	}
            }
            
        	break;
        }
        case GPS_PROVIDER : {
        	/* 20ft ~ 6.1m
        	 * This provider determines location using satellites. Depending on conditions, this provider may take a while to return a location fix
			Power usage High 	Technology: Autonomous GPS, Provider: gps
    			uses GPS chip on the device
    			line of sight to the satellites
    			need about 7 to get a fix
    			takes a long time to get a fix
    			doesn�t work around tall buildings
        	 
        	 */
        	bestProvider = LocationManager.GPS_PROVIDER;
        	// 27/1/2014 #8 v1.02.5
        	checkGPSProvidersOption(true);
        	break;
        }
        case NETWORK_PROVIDER : {
        	/* 200Ft ~ 61m
        	 * This provider determines location based on availability of cell tower and WiFi access points
       	  	Power usage Medium � Low 	Technology: Assisted GPS (AGPS), Provider: network
    			uses GPS chip on device, as well as assistance from the network (cellular network) to provide a fast initial fix
    			very low power consumption
    			very accurate
    			works without any line of sight to the sky
    			depends on carrier and phone supporting this (even if phone supports it, and network does not then this does not work)
        	 */
        	bestProvider = LocationManager.NETWORK_PROVIDER;
        	// #8 23/02/2014 
        	checkMobileDataConnection(false);
        	break;
        }
        default : {
        	bestProvider = LocationManager.GPS_PROVIDER;
        	// 27/1/2014 #8 v1.02.5
        	checkGPSProvidersOption(true);
        }
        }
        /*
        LocationProvider bestLocationProvider = null;
        if (bestProvider != null) {
        	bestLocationProvider = locationManager.getProvider(bestProvider);
        }
        */
    }
	
	
	/**
	 * 
	 * @param currentContent
	 * @param moduleIntent
	 * @param data1
	 * @param data2
	 * @param data3
	 */
	private void passCommonInfo(Activity currentContent, Intent moduleIntent, Long data1, Long data2, Long data3) {
		Bundle b = new Bundle();
		b.putLong(BUNDLE_CONTENT_TAG_DATA1, data1);
		b.putLong(BUNDLE_CONTENT_TAG_DATA2, data2);
		b.putLong(BUNDLE_CONTENT_TAG_DATA3, data3);
		moduleIntent.putExtras(b);
	}
	
	/**
	 * 
	 * @param min
	 */
	private void setupScreenOff(int min) {
		PowerManager manager = (PowerManager)getSystemService(Context.POWER_SERVICE);
		//manager.goToSleep(min);
	}
	
	/**
	 * 
	 */
	private void isScreenOn() {
		PowerManager manager = (PowerManager)getSystemService(Context.POWER_SERVICE);
		boolean screen = manager.isScreenOn();
	}
	
	/**
	 * 
	 * @param gpsCoordinates
	 */
	private void setupAddresses(List<GPSCoordinate> gpsCoordinates) {
		GPSCoordinate firstLoc = (GPSCoordinate) gpsCoordinates.get(0);
		String startA = location2Address(firstLoc.getLocation(), "start");
		if (startA != null) {
			((GPSCoordinate) gpsCoordinates.get(0)).setAddress(startA);            			
		}
		GPSCoordinate endtLoc = (GPSCoordinate) gpsCoordinates.get(gpsCoordinates.size() - 1);
		String endA = location2Address(endtLoc.getLocation(), "end");
		if (endA != null) {
			((GPSCoordinate) gpsCoordinates.get(gpsCoordinates.size()-1)).setAddress(endA);            			
		}
	}

	/**
	 * 
	 * @param locations
	 */
	private List<GPSCoordinate> removeLocationsWhereSpeedIsZero(List<GPSCoordinate> locations) {
		List<GPSCoordinate> locs = new ArrayList<GPSCoordinate>();
        boolean wasValueZero = false;
        for (GPSCoordinate gpsc : locations) {
            boolean isValueZero = gpsc.getLocation().getSpeed() < 1;
            if (wasValueZero) {
                if (isValueZero) {
                	// nothing to do
                }
                else {
                	locs.add(gpsc);
                }
            }
            else {
            	locs.add(gpsc);
            }
            wasValueZero = isValueZero;
        }
        return locs;
	}


	/**
	 * 
	 */
	public void checkGPSorWirelessNetworkProvidersOption() {
		// check in settings the option Use GPS or Wireless Network
		Context context = getApplicationContext();
		boolean isNeedToShow = !ConnectionUtils.isWirelessNetworkEnabled(context) || !ConnectionUtils.isUseGPSSatellites(context); 
		if (isNeedToShow) {
			String mess = getApplicationContext().getString(R.string.error_nolocationsourceenabled_message);
			String title = getApplicationContext().getString(R.string.error_nolocationsourceenabled_title);
			String yes_btn = getApplicationContext().getString(R.string.button_yes_title);
			String no_btn = getApplicationContext().getString(R.string.button_no_title);
			Toast.makeText(getApplicationContext(), mess, Toast.LENGTH_LONG).show();
			//AlertDialog alertDialog = new AlertDialog.Builder(Main.this).create();
			AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
			alertDialog.setTitle(title);
			alertDialog.setMessage(mess);
			alertDialog.setPositiveButton(yes_btn, new DialogInterface.OnClickListener() {
	            public void onClick(DialogInterface dialog,int which) {
					Intent settingsIntent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
					settingsIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_HISTORY);
					startActivity(settingsIntent);
	            }
	        });
	 
	        // Setting Negative "NO" Button
	        alertDialog.setNegativeButton(no_btn, new DialogInterface.OnClickListener() {
	            public void onClick(DialogInterface dialog,    int which) {
	            dialog.cancel();
	            // 25/1/2014 #8 v1.02.5
				//exit();
	            }
	        });
	 			
			alertDialog.show();
		}	
	}
	
	/**
	 * 25/1/2014 #8 v1.02.5
	 * @param title
	 * @param message
	 */
	public void showMessageDialog(String title, String message) {
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
		alertDialog.setTitle(title);
		alertDialog.setMessage(message);
		alertDialog.setNeutralButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {
            }
        });
		alertDialog.show();
	}
	
	/**
	 * 12/24/2012
	 */
	public void checkGPSProvidersOption(final boolean isExitWhenPressOk) {
		// check in settings the option Use GPS or Wireless Network
		Context context = getApplicationContext();
		boolean isUseGPSSatellitesEnabled = ConnectionUtils.isUseGPSSatellites(context); 
		if (!isUseGPSSatellitesEnabled) {
			String mess = getApplicationContext().getString(R.string.error_nolocationsourceenabledexit_title);
			String title = getApplicationContext().getString(R.string.error_nolocationsourceenabled_title);
			//AlertDialog alertDialog = new AlertDialog.Builder(Main.this).create();
			AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
			alertDialog.setTitle(title);
			alertDialog.setMessage(mess);
			alertDialog.setNeutralButton("OK", new DialogInterface.OnClickListener() {
	            public void onClick(DialogInterface dialog,int which) {
	            	if (isExitWhenPressOk) {
	            		dialog.cancel();
	            		exit();
	            	}
	            }
	        });
			alertDialog.show();
		}	
	}

	
	/**
	 * #8 23/02/2014 
	 * @param isExitWhenPressOk
	 */
	public void checkMobileDataConnection(final boolean isExitWhenPressOk) {
		// check in settings the option Use GPS or Wireless Network
		Context context = getApplicationContext();
		boolean isInternetEnabled = ConnectionUtils.isInternetAvailable(Main.this); 
		if (!isInternetEnabled) {
			String mess = getApplicationContext().getString(R.string.error_nointernetconnectionavailable);
			String title = getApplicationContext().getString(R.string.error_nointernetconnectionavailable_title);
			//AlertDialog alertDialog = new AlertDialog.Builder(Main.this).create();
			AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
			alertDialog.setTitle(title);
			alertDialog.setMessage(mess);
			alertDialog.setNeutralButton("OK", new DialogInterface.OnClickListener() {
	            public void onClick(DialogInterface dialog,int which) {
	            	if (isExitWhenPressOk) {
	            		dialog.cancel();
	            		exit();
	            	}
	            }
	        });
			alertDialog.show();
		}	
	}
	
	/**
	 * 
	 * @param trackLog
	 * @param trackGenerator
	 * @param gpsCoordinates
	 */
	private void storeLocationsToTracklog(StringBuffer trackLog, TrackGenerator trackGenerator, List<GPSCoordinate> gpsCoordinates) {
        // store coordinates
		if ((trackLog != null) && (trackGenerator != null) && (gpsCoordinates != null)) {
	        for (GPSCoordinate gpsCoordinate : gpsCoordinates) {
	        	trackLog.append(trackGenerator.createCoordinate(gpsCoordinate));
	        }
		}
	}
	
	/**
	 * 12/15/2012
	 */
	public void storeRouteToFile() {
   		List<GPSCoordinate> gpsCoordinatesZeroRemoved = null;
   		List<GPSCoordinate> gpsCoordinatesOptimized = null;
		//stop
		if (GPSSettings.gpsCoordinates.size() > 0) {
			try {
				if (GPSSettings.IS_OPTIMIZATION_ACTIVE) {
					if (GPSSettings.IS_REMOVE_ZERO_SPEED_LOCATIONS_ACTIVE) {
            			// remove more zero speed when stand in one place
        				gpsCoordinatesZeroRemoved = removeLocationsWhereSpeedIsZero(GPSSettings.gpsCoordinates);
					}
					else {
						gpsCoordinatesZeroRemoved = GPSSettings.gpsCoordinates;
					}
               		// optimization
    				gpsCoordinatesOptimized =  DouglasPeuckerOptimizer.reductionGPSCoordinate(gpsCoordinatesZeroRemoved, GPSSettings.DOUGLAS_PEUCKER_TOLARANCE);
    				points_origin = gpsCoordinatesZeroRemoved.size();
				}
				else {
					gpsCoordinatesOptimized = gpsCoordinatesZeroRemoved;
				}

                // setup addresses
        		if (gpsCoordinatesOptimized != null) {
        			// has less element then gpsCoordinatesZeroRemoved 
            		if ((gpsCoordinatesOptimized.size() > 0) && (gpsCoordinatesOptimized.size() < gpsCoordinatesZeroRemoved.size())) {
            			// is Internet available?
            			boolean isInternetAvailable = ConnectionUtils.isInternetAvailable(getCurrentActivity());
            			if (isInternetAvailable) {
            				setupAddresses(gpsCoordinatesOptimized);
            			}
            			else {
            				// internet NOT available
            			}	
            		}
        		}
        		else {
            		if (GPSSettings.gpsCoordinates.size() > 0) {
            			// is Internet available?
            			boolean isInternetAvailable = ConnectionUtils.isInternetAvailable(getCurrentActivity());
            			if (isInternetAvailable) {
            				setupAddresses(GPSSettings.gpsCoordinates);
            			}
            			else {
            				// internet NOT available
            			}	
            		}
        		}
			} catch (Exception e) {
	    		e.printStackTrace();
				ErrorHandler.createErrorLog(getApplicationContext(), e);
				Toast.makeText(getApplicationContext(), "ERROR in Main.removeLocationsWhereSpeedIsZero " + e.getMessage(), Toast.LENGTH_LONG).show();
			}		
		}
		// 11/28/2015 #13 v1.02.10
		if ((lookAtCoordinate == null) || (lookAtCoordinate.getLocation() == null)) {
			Location loc = new Location("");
			loc.setLatitude(47.44721885);
			loc.setLongitude(19.19528895);
			lookAtCoordinate = new GPSCoordinate(loc);
		}
		
		try {
			// generate tracks
			trackGenerator = new TrackGenerator(getApplicationContext(), lookAtCoordinate, GPSSettings.trackFormat, GPSSettings.KML_TRACK_COLOR, null, null);
			// #13 1.02.10 2015.11.29.
	        trackGenerator.setupMaxValues(maxSpeedValue, maxAltitudeValue, maxAccelerationValue, maxOrientationValue, minMaxTemperatureValue);
	        trackGenerator.generateTrackOpen(GPSSettings.trackFormat, trackLog, lookAtCoordinate, CommonLogger.BASE_FILE_NAME, "TrackLog");
	        
	        // 05/15/2012 prevent NPE if restart logging and stop immediatelly
	        if (gpsCoordinatesOptimized != null) {
	            // store coordinates
	        	storeLocationsToTracklog(trackLog, trackGenerator, gpsCoordinatesOptimized);
	        }
	        else if (gpsCoordinatesZeroRemoved != null) {
	            // store coordinates
	        	storeLocationsToTracklog(trackLog, trackGenerator, gpsCoordinatesZeroRemoved);
	        }
	        else if (GPSSettings.gpsCoordinates != null) {
	            // store coordinates
	        	storeLocationsToTracklog(trackLog, trackGenerator, GPSSettings.gpsCoordinates);
	        }

	        trackGenerator.generateTrackClose(GPSSettings.trackFormat, trackLog, gpsCoordinatesOptimized, points_origin);
	        String stamp = null;
	        if (CommonLogger.IS_LOGGER_ENABLED) {
	        	stamp = CommonLogger.FILE_STAMP;
	        }
	        else {
	        	stamp = CommonLogger.calculateFileNameStamp();
	        }
			String fileName = CommonLogger.BASE_FILE_NAME + "T-" + stamp;
			
	        //#1/10/2014 #10 v1.02.7
			String folderPath = SDCardManager.composeAbsolutePath(getApplicationContext());
			String filePath = SDCardManager.createFileForStringBufferWithPath(getApplicationContext(), folderPath, fileName  + ".kml", trackLog);
			String storedPath = Utils.readLogFilePath(this.getCurrentActivity());
			if ((storedPath == null) || ((storedPath != null) && (storedPath.trim() == ""))) {
				Utils.writeLogFilePath(this.getCurrentActivity(), folderPath);
			}
			
			/*
			String filePath = SDCardManager.createFileForStringBuffer(getApplicationContext(), fileName  + ".kml", trackLog);
			*/
	    	
			// 12/01/2012
	        Toast.makeText(getApplicationContext(), filePath, Toast.LENGTH_SHORT).show();		
		}
		catch (Exception e) {
	        Toast.makeText(getApplicationContext(), "Error while storing file!\n"+e.getMessage(), Toast.LENGTH_LONG).show();		
		}
		
		
	}
	

	/**
	 * 12/27/2012
	 */
	private void startLogging() {
		GPSSettings.IS_LOGGING_STARTED = true;
		// reset elapsed
//		elapsed = System.currentTimeMillis();
   		
		if (currentLoc == null) {
//			Toast.makeText(getApplicationContext(), "You need wait till to get the first GPS location for so be able to start the logging!", Toast.LENGTH_LONG).show();
			return;
		}
		// start
		odometer = 0;
		currentDistance = 0;
        // GPSCoordinate lookAt, TrackFormat trackFormat, String lineColor, Integer lineWidth, Integer range
        if ((locationEventCnt > 0) && (currentLoc != null)) {
            lookAtCoordinate = new GPSCoordinate(currentLoc);
        }
        else {
        	lookAtCoordinate = new GPSCoordinate(prevLoc);
        }
		// 01/02/2013
		speedSum = 0;
		speedCnt = 0;
        // #5 10/12/2013 initialize
        this.elapsedInWait = 0;
        this.elapsedInMove = 0;
        this.elapsedInWaitStart = System.currentTimeMillis();
        this.elapsedInMoveStart = System.currentTimeMillis();
        elapsedTimeKind = ElapsedTimeKind.IN_MOVE;
	}
	
	/**
	 * 05/05/2013
	 */
	private void switchToMapTab() {
		// switch to current tab
        int tabCount = tabHost.getTabWidget().getTabCount();
        int originTabIx = tabHost.getCurrentTab();
        // Map Tab is the last one
        tabHost.setCurrentTab(tabCount - 1);
        // get tag for check it
        String tabTag = tabHost.getCurrentTabTag();
        if (!tabTag.equals(TAB_TAG_MAP)) {
        	// change back to origin
            tabHost.setCurrentTab(originTabIx);
        }
	}
	
	/**
	 * {@link #acceleration50} 10/2162013
	 * @param isClearRoute
	 */
	private void switchToMapTabAndShowLog(boolean isSwitch, boolean isShowStartAndEdFlag) {
    	if ((GPSSettings.gpsCoordinates != null) && (GPSSettings.gpsCoordinates.size() > 1)) {
    		if (isMapTabSpecExists) {
                // 05/05/2013 
                String originTag = getTabHost().getCurrentTabTag();
                //#5 10/2162013
                if (isSwitch) {
                	// 05/05/2013 
                	switchToMapTab();
                }
                // get tab tag
                String tabTag = getTabHost().getCurrentTabTag();
                // 05/05/2013 if changed do show route 
//#5 10/22/2013 if (!originTag.equals(tabTag)) {
                    // get Activity by tab tag
                    Activity activity = getLocalActivityManager().getActivity(tabTag);
                    if (!GPSSettings.IS_USE_OFFLINE_MAP) {
                        if (activity instanceof MapTab) {
                        	MapTab mapTab = (MapTab)activity;

 //TODO	isClearRoute is true when stop logging only!                         	

                        	// 11/28/2012
                        	// #5 11/23/2013 always clear coordinates, not clear start and stop points
                       		mapTab.clear(true, true, false);
                        		
                        	// #5 10/24/2013 if not show flag then capture max values 
                        	mapTab.redrawRoute(GPSSettings.gpsCoordinates, !isShowStartAndEdFlag);
                        	if (isShowStartAndEdFlag) {
                        		// #6 11/27/2013
                        		if (isStopLoggingNow) {
                        			mapTab.showStartAndEndFlags(true);
                        		}
                        	}	
                        	mapTab.setupZoomForFullRoute();
//                        	mapTab.animateToLastLocation();
                        }
                    }
                    else {
                        if (activity instanceof MapTabMapForge) {
                        	// #5 10/21/2013
                        	MapTabMapForge mapTabMapForge = (MapTabMapForge)activity;
                        	// #5 11/23/2013 added coordinates
                        	mapTabMapForge.redrawRoutePoints(GPSSettings.gpsCoordinates, true);
                        	mapTabMapForge.moveToCurrentLocation();
                        }
                    }
//#5 10/22/2013 }
    		}
    	}
	}
	
	private void allertSignal() throws IllegalArgumentException, SecurityException, IllegalStateException, IOException {
	    Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
	    MediaPlayer mediaPlayer = new MediaPlayer();
	    mediaPlayer.setDataSource(getApplicationContext(), uri);
	    final AudioManager audioManager = (AudioManager) getApplicationContext().getSystemService(Context.AUDIO_SERVICE);
	    if (audioManager.getStreamVolume(AudioManager.STREAM_ALARM) != 0) {
	        mediaPlayer.setAudioStreamType(AudioManager.STREAM_ALARM);
	        mediaPlayer.setLooping(false);
	        mediaPlayer.prepare();
	        mediaPlayer.start();
	    }	
	}
	
	/**
	 * 12/27/2012
	 */
	private void stopLogging() {
		GPSSettings.IS_LOGGING_STARTED = false;
		try {
			isStopLoggingNow = true;
			
       		List<GPSCoordinate> gpsCoordinatesZeroRemoved = null;
       		List<GPSCoordinate> gpsCoordinatesOptimized = null;
    		//stop
    		if (GPSSettings.gpsCoordinates.size() > 0) {
    			try {
    				if (GPSSettings.IS_OPTIMIZATION_ACTIVE) {
    					if (GPSSettings.IS_REMOVE_ZERO_SPEED_LOCATIONS_ACTIVE) {
                			// remove more zero speed when stand in one place
            				gpsCoordinatesZeroRemoved = removeLocationsWhereSpeedIsZero(GPSSettings.gpsCoordinates);
    					}
    					else {
    						gpsCoordinatesZeroRemoved = GPSSettings.gpsCoordinates;
    					}
                   		// optimization
        				gpsCoordinatesOptimized =  DouglasPeuckerOptimizer.reductionGPSCoordinate(gpsCoordinatesZeroRemoved, GPSSettings.DOUGLAS_PEUCKER_TOLARANCE);
        				points_origin = gpsCoordinatesZeroRemoved.size();
    				}
    				else {
    					gpsCoordinatesOptimized = gpsCoordinatesZeroRemoved;
    				}

                    // setup addresses
            		if (gpsCoordinatesOptimized != null) {
            			// has less element then gpsCoordinatesZeroRemoved 
                		if ((gpsCoordinatesOptimized.size() > 0) && (gpsCoordinatesOptimized.size() < gpsCoordinatesZeroRemoved.size())) {
                			// is Internet available?
                			boolean isInternetAvailable = ConnectionUtils.isInternetAvailable(getCurrentActivity());
                			if (isInternetAvailable) {
                				setupAddresses(gpsCoordinatesOptimized);
                			}
                			else {
                				// internet NOT available
                			}	
                		}
            		}
            		else {
                		if (GPSSettings.gpsCoordinates.size() > 0) {
                			// is Internet available?
                			boolean isInternetAvailable = ConnectionUtils.isInternetAvailable(getCurrentActivity());
                			if (isInternetAvailable) {
                				setupAddresses(GPSSettings.gpsCoordinates);
                			}
                			else {
                				// internet NOT available
                			}	
                		}
            		}
				} catch (Exception e) {
		    		e.printStackTrace();
					ErrorHandler.createErrorLog(getApplicationContext(), e);
					Toast.makeText(getApplicationContext(), "ERROR in Main.removeLocationsWhereSpeedIsZero " + e.getMessage(), Toast.LENGTH_LONG).show();
				}		
    		}	
    		
    		// 12/27/2012
    		// 11/28/2015 #13 v1.02.10
    		
    		
    		// 11/28/2015 #13 v1.02.10
    		if ((lookAtCoordinate == null) || (lookAtCoordinate.getLocation() == null)) {
    			Location loc = new Location("");
    			loc.setLatitude(47.44721885);
    			loc.setLongitude(19.19528895);
    			lookAtCoordinate = new GPSCoordinate(loc);
    		}
    		
    		// generate tracks
    		trackGenerator = new TrackGenerator(getApplicationContext(), lookAtCoordinate, GPSSettings.trackFormat, GPSSettings.KML_TRACK_COLOR, null, null);
    		// #13 1.02.10 2015.11.29.
            trackGenerator.setupMaxValues(maxSpeedValue, maxAltitudeValue, maxAccelerationValue, maxOrientationValue, minMaxTemperatureValue);
            trackGenerator.generateTrackOpen(GPSSettings.trackFormat, trackLog, lookAtCoordinate, CommonLogger.BASE_FILE_NAME, "TrackLog");
            
            // 05/15/2012 prevent NPE if restart logging and stop immediatelly
            if (gpsCoordinatesOptimized != null) {
                // store coordinates
            	storeLocationsToTracklog(trackLog, trackGenerator, gpsCoordinatesOptimized);
            }
            else if (gpsCoordinatesZeroRemoved != null) {
                // store coordinates
            	storeLocationsToTracklog(trackLog, trackGenerator, gpsCoordinatesZeroRemoved);
            }
            else if (GPSSettings.gpsCoordinates != null) {
                // store coordinates
            	storeLocationsToTracklog(trackLog, trackGenerator, GPSSettings.gpsCoordinates);
            }

            trackGenerator.generateTrackClose(GPSSettings.trackFormat, trackLog, gpsCoordinatesOptimized, points_origin);
            String stamp = null;
            if (CommonLogger.IS_LOGGER_ENABLED) {
            	stamp = CommonLogger.FILE_STAMP;
            }
            else {
            	stamp = CommonLogger.calculateFileNameStamp();
            }
    		String fileName = CommonLogger.BASE_FILE_NAME + "T-" + stamp;
    		
            //#1/10/2014 #10 v1.02.7
    		String folderPath = SDCardManager.composeAbsolutePath(getApplicationContext());
    		String filePath = SDCardManager.createFileForStringBufferWithPath(getApplicationContext(), folderPath, fileName  + ".kml", trackLog);
    		String storedPath = Utils.readLogFilePath(this.getCurrentActivity());
    		if ((storedPath == null) || ((storedPath != null) && (storedPath.trim() == ""))) {
    			Utils.writeLogFilePath(this.getCurrentActivity(), folderPath);
    		}
    		
    		/*#1/10/2014 #10 v1.02.7
    		String filePath = SDCardManager.createFileForStringBuffer(getApplicationContext(), fileName  + ".kml", trackLog);
    		*/
    		
    		// 12/01/2012
            Toast.makeText(getApplicationContext(), filePath, Toast.LENGTH_SHORT).show();

            // 11/25/2012
            if (GPSSettings.IS_SWITCH_TO_MAP_TAB_WHEN_STOP_TRACKING) {
            	// # 10/21/2013
            	switchToMapTabAndShowLog(true, true);
            }
            // reset
            resetToStart();
    		
    		
		}
		catch (Exception e) {
			e.printStackTrace();
			ErrorHandler.createErrorLog(getApplicationContext(), e);
			Toast.makeText(getApplicationContext(), "ERROR in Main.startStopToggleButton.setOnClickListener STOP " + e.getMessage(), Toast.LENGTH_LONG).show();
		}
		finally {
			isStopLoggingNow = false;
		}
	}

	/**
	 * 25/1/2014 #8 v1.02.5
	 */
	private void setupLastKnownGoodLocation() {
        // last known good location
        //12/21/2012
        if (bestProvider != null)
        	prevLoc =	locationManager.getLastKnownLocation(bestProvider);
        
        if (prevLoc != null) {
			String lon = String.format("Longitude: %.08f", prevLoc.getLongitude());
			String lat = String.format("Latitude: %.08f", prevLoc.getLatitude());
			String text = "Best provider = " + bestProvider + "\r\n";
			text += "Last known good location\r\n";
				text += lon + "\r\n";
				text += lat + "\r\n";
				logEvents.append(text);
        }
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		// 01/26/2013
		// switch off screensaver 
		getWindow().addFlags(LayoutParams.FLAG_KEEP_SCREEN_ON);
		// 01/27/2013
		if (GPSSettings.IS_FULLSCREEN_ON) {
			// hide status bar, turn on full screen
			getWindow().addFlags(LayoutParams.FLAG_FULLSCREEN);
		}
		// fix orientation
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);		

		SDCardManager.debug("onCreate");

	    //10/29/2012
		Thread.setDefaultUncaughtExceptionHandler(new GTLUncaughtExceptionHandler(getApplicationContext()) {
	    	private Exception throwable = null;
	    	
	        @Override
	        public void uncaughtException(Thread thread, Throwable ex) {
	        	throwable = (Exception) ex;
	            new Thread() {
	                @Override
	                public void run() {
	                    Looper.prepare();
	                    if (throwable != null) {
	            	        // #5 10/06/2013
	                    	Context cntxt = getContext();
	                    	if ((throwable != null) && (cntxt != null)) {
		            	        final Writer result = new StringWriter();
		            	        final PrintWriter printWriter = new PrintWriter(result);
		            	        throwable.printStackTrace(printWriter);
		            	        String stacktrace = result.toString();
		            	        printWriter.close();
		            	        Toast.makeText(cntxt, stacktrace, Toast.LENGTH_LONG).show();
		                    	ErrorHandler.createErrorLog(cntxt, throwable);
	                    	}
	                    }
	                    Looper.loop();
	                }
	            }.start();
	        }
	    });
		
		simpleGesture = new SimpleGesture(this,this);
		
		// # 01/01/2015 #10 v1.02.7
		GPSSettings.IS_EXTRA_FUNCTIONS_ENABLED = false;
		
		// is internet available?
		if (ConnectionUtils.isInternetAvailable(this)) {
			GPSSettings.PHONE_IMEI  = ConnectionUtils.extractPhoneIMEI(this);
			if (GPSSettings.PHONE_IMEI != null) {
				//#8 v1.02.5 02/15/2014
				if (IS_SAVE_IMEI_TO_FILE) {
					String path = SDCardManager.saveImeiToFile(getApplicationContext(), GPSSettings.PHONE_IMEI);
				}
				// check extra functions is available
				try {
					if (downloadPhoneIDAsyncTask != null) {
						downloadPhoneIDAsyncTask = null;
					}
					downloadPhoneIDAsyncTask = new DownloadPhoneIDAsyncTask();
					downloadPhoneIDAsyncTask.execute(GPSSettings.PHONE_IMEI);
				} catch (Exception e) {
					GPSSettings.IS_EXTRA_FUNCTIONS_ENABLED = false;
					ErrorHandler.createErrorLog(getApplicationContext(), e);
					Toast.makeText(getApplicationContext(), "ERROR in download phone ids Main.onCreate " + e.getMessage(), Toast.LENGTH_LONG).show();
				}
			}
		}
		else {
			GPSSettings.IS_EXTRA_FUNCTIONS_ENABLED = false;
		}
		
		
        // 11/15/2012
        GPSSettings.loadPreferences(getApplicationContext());
		
        // 01/15/2013
		registerReceiver(batInfoReceiverBroadcastReceiver, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        
	    // 03/12/2016 #15 v1.02.12 
		registerReceiver(shutdownReceiver, new IntentFilter(Intent.ACTION_SHUTDOWN));

		// reset elapsed
//		elapsed = System.currentTimeMillis();

		// check in settings the option Use GPS or Wireless Network
		checkGPSorWirelessNetworkProvidersOption();
		
		// average value managers
		averageValuesManager = new ValuesAverageManager(10);
        
        /* TabHost will have Tabs */
        tabHost = (TabHost)findViewById(android.R.id.tabhost);
        
        /* TabSpec used to create a new tab. 
         * By using TabSpec only we can able to setContent to the tab.
         * By using TabSpec setIndicator() we can set name to tab. */
        
        /* tid1 is firstTabSpec Id. Its used to access outside. */
       	Resources res = getResources();
       	TabSpec gpsTabSpec = tabHost.newTabSpec(TAB_TAG_GPS);
        TabSpec routeTabSpec = tabHost.newTabSpec(TAB_TAG_ROUTE);
//        TabSpec mapTabSpec = tabHost.newTabSpec("Map");
        mapTabSpec = tabHost.newTabSpec(TAB_TAG_MAP);
        // 12/31/2012
        TabSpec compassTabSpec = tabHost.newTabSpec(TAB_TAG_COMPASS);
        
        /* TabSpec setIndicator() is used to set name for the tab. */
        /* TabSpec setContent() is used to set content for a particular tab. */
        Intent gpsTabIntent = null;
       	gpsTabIntent = new Intent(this, GPSTab.class);
       	// dummy call because currently not need to pass data to gps activity/tab/
       	passCommonInfo(this, gpsTabIntent, data1, data2, data3);
       	gpsTabSpec.setIndicator("", res.getDrawable(R.drawable.ic_tab_gps)).setContent(gpsTabIntent);

       	Intent routeTabIntent = new Intent(this, RouteTab.class);
       	// dummy call because currently not need to pass data to gps activity/tab/
       	passCommonInfo(this, routeTabIntent, data1, data2, data3);
        routeTabSpec.setIndicator("", res.getDrawable(R.drawable.ic_tab_route)).setContent(routeTabIntent);

        // 12/2962012
        if (!GPSSettings.IS_USE_OFFLINE_MAP) {
            mapTabIntent = new Intent(this, MapTab.class);
//       	// dummy call because currently not need to pass data to gps activity/tab/
//       	passCommonInfo(this, mapTabIntent, data1, data2, data3);
            mapTabSpec.setIndicator("", res.getDrawable(R.drawable.ic_tab_map)).setContent(mapTabIntent);
            isNeedToAddMapTabSpec = true;
        }
        else {
        }
        
        // 12/31/2012
        this.sensorManager = (SensorManager)getSystemService(Context.SENSOR_SERVICE);
        List<Sensor> sensors = this.sensorManager.getSensorList(Sensor.TYPE_ORIENTATION);
        boolean isOrientationSensorAvailable = false;
        if(sensors.size() > 0){
        	isOrientationSensorAvailable = true;
        	Intent compassTabIntent = new Intent(this, CompassTab.class);
        	// dummy call because currently not need to pass data to gps activity/tab/
        	passCommonInfo(this, compassTabIntent, data1, data2, data3);
        	compassTabSpec.setIndicator("", res.getDrawable(R.drawable.ic_tab_compass)).setContent(compassTabIntent);
        }
        
        /* Add tabSpec to the TabHost to display. */
        tabHost.addTab(gpsTabSpec);
        tabHost.addTab(routeTabSpec);
        // 12/31/2012 02/10/2013
        if (isOrientationSensorAvailable)
        	tabHost.addTab(compassTabSpec);
        if (isNeedToAddMapTabSpec) {
        	tabHost.addTab(mapTabSpec);
        	isMapTabSpecExists = true;
        }	
        
        tabHost.setCurrentTab(0);

        
        //disable on/off on MapTab
        tabHost.setOnTabChangedListener(new OnTabChangeListener() {

            @Override
            public void onTabChanged(String tabId) {
            	String currentTag = getTabHost().getCurrentTabTag();
        		boolean isGPSOkay = (gpsStatusEvent == GpsStatus.GPS_EVENT_FIRST_FIX) || (gpsStatusEvent == GpsStatus.GPS_EVENT_STARTED);
            	if (tabId.equals(TAB_TAG_GPS)) {
           			// 12/07/2012
           			GPSSettings.IS_ENABLE_ROUTEOVERLAY_TO_STORE_START_AND_FINISH_GP = true;
            	}
            	else if (tabId.equals(TAB_TAG_ROUTE)) {
           			// 12/07/2012
           			GPSSettings.IS_ENABLE_ROUTEOVERLAY_TO_STORE_START_AND_FINISH_GP = true;
            	}
            	else if (tabId.equals(TAB_TAG_MAP)) {
            		// #5 10/21/2013
                   	switchToMapTabAndShowLog(false, false);
            	}
            }

        });
        
        /* 11/23/2012 setup default location to Satoraljaujhely HUNGARY
        if (lookAtLocation == null) {
            lookAtLocation = new LookAtLocation(prevLoc);
            lookAtLocation.setLatitude(47.394152);
            lookAtLocation.setLatitude(21.655995);
            lookAtCoordinate = new GPSCoordinate(lookAtLocation);
        }
		*/
        
        
        try {
        	// sensor event listeners
        	
        	if (this.sensorEventListener == null)
        		this.sensorEventListener = new SensorListener();
        	
        	this.sensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);
        	this.mAccelerometerSensor = this.sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
            if (this.mAccelerometerSensor != null) {
            	this.sensorManager.registerListener(this.sensorEventListener, this.mAccelerometerSensor, SensorManager.SENSOR_DELAY_NORMAL);
            }
            
            this.mOrientationSensor = this.sensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION);
            if (this.mOrientationSensor != null) {
            	this.sensorManager.registerListener(this.sensorEventListener, this.mOrientationSensor, SensorManager.SENSOR_DELAY_NORMAL);
            }

            this.mMagneticFieldSensor = this.sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
            if (this.mMagneticFieldSensor != null) {
            	this.sensorManager.registerListener(this.sensorEventListener, this.mMagneticFieldSensor, SensorManager.SENSOR_DELAY_NORMAL);
            }
            
            // #13 1.02.10. Ambient Temperature
			if (android.os.Build.VERSION.SDK_INT >= 14) {
	            mAmbientTemperatureSensor = this.sensorManager.getDefaultSensor(Sensor.TYPE_AMBIENT_TEMPERATURE);
	            if (mAmbientTemperatureSensor != null) {
	            	this.sensorManager.registerListener(this.sensorEventListener, this.mAmbientTemperatureSensor, SensorManager.SENSOR_DELAY_NORMAL);
	            }
			}
            
            setupLocationmanagerAndCriteria(null);
            
            // 25/1/2014 #8 v1.02.5 last known good location
            setupLastKnownGoodLocation();
            
            // GPS status
            if (isNeedToRegisterGPSStatusListener) {
                gpsStatusListener = new GPSStatusListener();
                locationManager.addGpsStatusListener(gpsStatusListener);
            }
            
            // GPS Location
            registerLocationListener();
            // #5 10/19/2013
        	timer.schedule(timerTask, 1000, 500);
        }
        catch (Exception e) {
			ErrorHandler.createErrorLog(getApplicationContext(), e);
			Toast.makeText(getApplicationContext(), "ERROR in Main.onCreate " + e.getMessage(), Toast.LENGTH_LONG).show();
        }
	}	

    @Override
    public boolean dispatchTouchEvent(MotionEvent me){
       this.simpleGesture.onTouchEvent(me);
       return super.dispatchTouchEvent(me);
    }
    
	/**
	 * 08/23/2013
	 * @param tabTag
	 * @return
	 */
	private boolean isCurrentTab(String tabTag) {
        String currentTabTag = getTabHost().getCurrentTabTag();
        return currentTabTag.equals(tabTag);
	}
	

	/**
	 * 08/23/2013
	 * @param menu
	 * @param isEnable
	 */
	private void setupMapRelatedMenuItems(Menu menu, boolean isEnable, boolean isGoogle, boolean isEnableFind) {

		MenuItem map_menuItemclear = menu.findItem(R.id.map_itemClearRoute);
		MenuItem map_menuItemcurrentloc = menu.findItem(R.id.map_itemCurrentLoc);
		MenuItem map_menuItemmark = menu.findItem(R.id.map_itemMarkLoc);
		MenuItem map_menuItemFind = menu.findItem(R.id.map_itemFindAddress);
		MenuItem map_menuItemRecalc = menu.findItem(R.id.map_itemRecalcRoute);
		MenuItem map_menuItemsave = menu.findItem(R.id.map_itemSave);

		map_menuItemclear.setVisible(isEnable);
		map_menuItemcurrentloc.setVisible(isEnable);
		map_menuItemmark.setVisible(isEnable);
		map_menuItemFind.setVisible(isEnable);
		map_menuItemRecalc.setVisible(isEnable);
		map_menuItemsave.setVisible(isEnable);
		
		map_menuItemclear.setEnabled(isEnable);
		map_menuItemcurrentloc.setEnabled(isEnable);
		map_menuItemmark.setEnabled(isEnable);
		map_menuItemFind.setEnabled(isEnable);
		map_menuItemRecalc.setEnabled(isEnable);
		map_menuItemsave.setEnabled(isEnable);


		if (!isGoogle && isEnable) {
			map_menuItemmark.setEnabled(!isEnable);
			map_menuItemFind.setEnabled(!isEnable);
			map_menuItemRecalc.setEnabled(!isEnable);
			map_menuItemsave.setEnabled(!isEnable);
		
			map_menuItemmark.setVisible(!isEnable);
			map_menuItemFind.setVisible(!isEnable);
			map_menuItemRecalc.setVisible(!isEnable);
			map_menuItemsave.setVisible(!isEnable);
		}
		
		/* if find enable...
		map_menuItemFind.setVisible(isEnableFind);
		map_menuItemRecalc.setVisible(isEnableFind);
		map_menuItemFind.setEnabled(isEnableFind);
		map_menuItemRecalc.setEnabled(isEnableFind);
		*/
	}
	
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {

		if (menu != null) {
			// enable related menu items
			MenuItem manuItemAbout = menu.findItem(R.id.gps_itemAbout);
			MenuItem menuItemChangeMap = menu.findItem(R.id.gps_itemChangeMap);
			MenuItem menuItemHelp = menu.findItem(R.id.gps_itemHelp);
			MenuItem menuItemLoadMap = menu.findItem(R.id.gps_itemLoadMap);
			MenuItem menuItemSettings = menu.findItem(R.id.gps_itemSettings);
			MenuItem menuItemStoredLog = menu.findItem(R.id.gps_itemStoredlog);
			MenuItem menuItemShowLog = menu.findItem(R.id.map_itemShowlog);
			
			manuItemAbout.setVisible(true);
			menuItemChangeMap.setVisible(true);
			menuItemHelp.setVisible(true);
			menuItemLoadMap.setVisible(true);
			menuItemSettings.setVisible(true);
			menuItemStoredLog.setVisible(true);
			menuItemShowLog.setVisible(true);
		}
		
		boolean isEnableStart = false;
		boolean isEnableStop = false;
		if (GPSSettings.IS_LOGGING_STARTED) {
			// hide start show stop
			isEnableStart = false;
			isEnableStop = true;

			class GTLDialogClickListener implements DialogInterface.OnClickListener {
				private boolean isSelectYes = false;

				@Override
				public void onClick(DialogInterface dialog, int which) {
			        switch (which){
			        case DialogInterface.BUTTON_POSITIVE:
			            //Yes button clicked
			        	isSelectYes = true;
			            break;

			        case DialogInterface.BUTTON_NEGATIVE:
			            //No button clicked
			        	isSelectYes = false;
			            break;
			        }
				}
				public boolean isSelectYes() {
					return isSelectYes;
				}
			}
		}
		else {
			// show start hide stop
			isEnableStart = true;
			isEnableStop = false;
		}
		if (menu != null) {
			// 01/26/2013 load map or change map
			MenuItem loadMapMenuItem = menu.findItem(R.id.gps_itemLoadMap);
			MenuItem changeMapMenuItem = menu.findItem(R.id.gps_itemChangeMap);
			if (GPSSettings.IS_USE_OFFLINE_MAP) {
				if (ConnectionUtils.isInternetAvailable(getCurrentActivity())) {
					if (loadMapMenuItem != null)
						loadMapMenuItem.setVisible(true);
				}
				//#7 12/08/2013
				if (changeMapMenuItem != null) {
					List<File> foundMapFiles = SDCardManager.findFiles(Environment.getExternalStorageDirectory().getPath() + "/" + GPSSettings.GTL_MAPS_FOLDER + "/" , ".map");
					if (foundMapFiles.size() > 1 ) {
						changeMapMenuItem.setVisible(true);
					}	
					else if (foundMapFiles.size() == 1) {
						// set the map 
						if (GPSSettings.DEFAUL_MAPSFORGE_MAP == null) {
							GPSSettings.DEFAUL_MAPSFORGE_MAP = foundMapFiles.get(0).getName();
						}	
					}
					foundMapFiles.clear();
					foundMapFiles = null;
				}
			}
			else {
				if (loadMapMenuItem != null)
					loadMapMenuItem.setVisible(false);
				if (changeMapMenuItem != null)
					changeMapMenuItem.setVisible(false);
			}
			// stop
			MenuItem stopMenuItem = menu.findItem(R.id.gps_itemStopLogging);
			// start
			MenuItem startMenuItem = menu.findItem(R.id.gps_itemStartLogging);
			if (isFirstFix) {
				if (stopMenuItem != null)
					stopMenuItem.setVisible(isEnableStop);
				if (startMenuItem != null)
					startMenuItem.setVisible(isEnableStart);
			}	
		}
		
		// 08/06/2014 #9 v1.02.6
		MenuItem map_menuItemShowLogs = menu.findItem(R.id.gps_itemStoredlog);
		map_menuItemShowLogs.setVisible(GPSSettings.IS_ENABLE_REALTIME_POSITION_SENDING);
		
		//#8 v1.02.5 03/08/2014
		boolean isLastLocationAvailable = (mapTab != null) && (mapTab.getLastLocation() != null);
		// 08/23/2013
       	setupMapRelatedMenuItems(menu, isCurrentTab(TAB_TAG_MAP), !GPSSettings.IS_USE_OFFLINE_MAP, GPSSettings.IS_EXTRA_FUNCTIONS_ENABLED && isLastLocationAvailable);

		//#1/10/2014 #10 v1.02.7
		MenuItem showLogMenuItem = menu.findItem(R.id.map_itemShowlog);
		showLogMenuItem.setVisible(Utils.isGoogleEarthInstalled(getCurrentActivity()));

		// 08/12/2016 #16 v1.02.13
		if (GPSSettings.IS_LOGGING_STARTED) {
			if (menu != null) {
				// disable menu items
				MenuItem manuItemAbout = menu.findItem(R.id.gps_itemAbout);
				MenuItem menuItemChangeMap = menu.findItem(R.id.gps_itemChangeMap);
				MenuItem menuItemHelp = menu.findItem(R.id.gps_itemHelp);
				MenuItem menuItemLoadMap = menu.findItem(R.id.gps_itemLoadMap);
				MenuItem menuItemSettings = menu.findItem(R.id.gps_itemSettings);
				MenuItem menuItemStoredLog = menu.findItem(R.id.gps_itemStoredlog);
				MenuItem menuItemShowLog = menu.findItem(R.id.map_itemShowlog);
				
				manuItemAbout.setVisible(false);
				menuItemChangeMap.setVisible(false);
				menuItemHelp.setVisible(false);
				menuItemLoadMap.setVisible(false);
				menuItemSettings.setVisible(false);
				menuItemStoredLog.setVisible(false);
				menuItemShowLog.setVisible(false);
			}
		}
		return super.onPrepareOptionsMenu(menu);
	}
	
	/**
	 * 
	 */
	public boolean onCreateOptionsMenu(Menu menu) {
		// 08/23/2013
		boolean res = false;
       	MenuInflater inflater = getMenuInflater();
       	inflater.inflate(R.menu.optionsmenu, menu);
       	optionsMenu = menu;
       	// #8 12/07/2013
       	return super.onCreateOptionsMenu(menu);
	}

	/**
	 * 
	 */
	private void exit() {
        Intent intentData = new Intent();
        intentData.putExtra("IS_EXIT", new Boolean(true));
        setResult(android.app.Activity.RESULT_OK, intentData);				
		finish();
		System.exit(1);
	}
	
	/**
	 * 
	 */
	public boolean onOptionsItemSelected(MenuItem item) {
		boolean isSelectedYes = false;
		
		// 08/23/2013
		DialogInterface.OnClickListener dialogClickListenerWhenExit = new DialogInterface.OnClickListener() {
		    @Override
		    public void onClick(DialogInterface dialog, int which) {
		        switch (which){
		        case DialogInterface.BUTTON_POSITIVE:
		            //Yes button clicked
					exit();
		            break;

		        case DialogInterface.BUTTON_NEGATIVE:
		            //No button clicked
		            break;
		        }
		    }
		};
		
		Intent intent = null;
		switch (item.getItemId()) {
		case R.id.gps_itemStartLogging : {
			startLogging();
			break;
		}
		case R.id.gps_itemStopLogging : {
			stopLogging();
			break;
		}
		case R.id.map_itemShowlog: {
			try {
				if (Utils.isGoogleEarthInstalled(getCurrentActivity())) {
			        intent = new Intent();
			        intent.setClass(getApplicationContext(), LogSelector.class);
			        startActivity(intent);      
				}
			}
			catch (Exception e) {
				SDCardManager.debug(">> Exception " + e.toString());
				e.printStackTrace();
			}
			break;
		}
		case R.id.gps_itemLoadMap:
			if (ConnectionUtils.isInternetAvailable(this)) {
				// start the Map Selector
		        intent = new Intent();
		        intent.setClass(this, MapSelector.class);
		        // start with download map mode
	        	passCommonInfo(this, intent, new Long(1), data2, data3);
		        startActivity(intent);      
			}
			return true;
		case R.id.gps_itemChangeMap:
			// start the Map Selector
	        intent = new Intent();
	        intent.setClass(this, MapSelector.class);
	        // start with download map mode
	        // #6 11/26/2013 not need to read last used offline map
        	passCommonInfo(this, intent, new Long(0), new Long(1), data3);
	        startActivity(intent);      
			return true;
		case R.id.gps_itemAbout:
	        intent = new Intent();
	        intent.setClass(getApplicationContext(), Info.class);
	        startActivity(intent);      
			return true;
		case R.id.gps_itemSettings:
	        intent = new Intent();
//	        intent.setClass(getApplicationContext(), Settings.class);
	        //2012.11.04.
	        intent.setClass(getApplicationContext(), GTLSettings.class);
	        startActivity(intent);      
			return true;
		case R.id.gps_itemHelp:
			// 12/29/2012
			if (ConnectionUtils.isInternetAvailable(getCurrentActivity())) {
		        intent = new Intent();
		        //12/23/2012
		        intent.setClass(getApplicationContext(), GTLHelp.class);
		        startActivity(intent);      
			}
			else {
				Toast.makeText(this, R.string.error_nointernetconnectionavailable, Toast.LENGTH_LONG).show();
			}
			return true;
		case R.id.gps_itemStoredlog:
			// 08/06/2014 #9 v1.02.6
			if (ConnectionUtils.isInternetAvailable(getCurrentActivity())) {
		        intent = new Intent();
		        intent.setClass(getApplicationContext(), GTLStoredLogs.class);
		        startActivity(intent);      
			}
			else {
				Toast.makeText(this, R.string.error_nointernetconnectionavailable, Toast.LENGTH_LONG).show();
			}
			return true;
		case R.id.gps_itemExit:
			if (SDCardManager.isStoreDebugLog) {
				String fileName = CommonLogger.BASE_FILE_NAME + "D-" + CommonLogger.calculateFileNameStamp();				
				SDCardManager.createFileForStringBuffer(this.getApplicationContext(), fileName+".txt", SDCardManager.log);
			}	
			
			if (GPSSettings.IS_LOGGING_STARTED) {
				AlertDialog alertDialog = new AlertDialog.Builder(this).setTitle(R.string.message_in_logging_title).setMessage(R.string.message_in_logging_really_want_to_exit).setNegativeButton(R.string.button_no_title, dialogClickListenerWhenExit).setPositiveButton(R.string.button_yes_title, dialogClickListenerWhenExit).show();
				alertDialog = null;
			}
			else {
				// #5 10/22/2013
				GPSSettings.IS_LOGGING_STARTED = false;
				exit();
			}
				return true;
		}
		
        if (isCurrentTab(TAB_TAG_MAP)) {
            // get Activity by tab tag
            Activity activity = getLocalActivityManager().getActivity(TAB_TAG_MAP);
            if (!GPSSettings.IS_USE_OFFLINE_MAP) {
                if (activity instanceof MapTab) {
                	mapTab = (MapTab)activity;
                	
            		switch (item.getItemId()) {
        			case R.id.map_itemClearRoute: {
        				// 11/28/2012
        				mapTab.clear(true, true, true);
        				break;	
        			}
        			case R.id.map_itemCurrentLoc: {
        				// 11/28/2012
        				mapTab.animateToLastLocation();
        				break;	
        			}	
        			case R.id.map_itemMarkLoc: {
        				mapTab.setMarkPressed(true);
        				break;	
        			}
        			case R.id.map_itemSave: {
        				try {
        					
        				}
        				catch (Exception e) {
        					SDCardManager.debug(">> Exception " + e.toString());
        					e.printStackTrace();
        				}
        				finally {
       						Toast.makeText(this, R.string.message_not_implemented_yet_off, Toast.LENGTH_LONG).show();
        				}
        				break;
        			}
        			case R.id.map_itemRecalcRoute : {
        				// #5 10/12/2013
        				if (targetAddressAsText != null) {
    		                GeoPoint lastLoc = mapTab.getLastLocation();
    		                double startLat = lastLoc.getLatitudeE6() / 1E6;
    		                double startLon = lastLoc.getLongitudeE6() / 1E6;
    		                try {
    							mapTab.drawRouteToTarget(startLat, startLon, targetAddressAsAddress.getLatitude(), targetAddressAsAddress.getLongitude(), GPSSettings.MAP_CALC_ROUTE_MODE, GPSSettings.IS_MAP_CALC_ROUTE_AVOID_HIGHWAYS, GPSSettings.IS_MAP_CALC_ROUTE_AVOID_TOLLS);
    						} catch (Exception e) {
    							ErrorHandler.createErrorLog(getApplicationContext(), e);
        						Toast.makeText(getApplicationContext(), "Error occurred when draw route to " + targetAddressAsText, Toast.LENGTH_LONG).show();
    						}
    		                // 05/19/2013
    		                mapTab.refresh();
        				}
        				break;
        			}
        			case R.id.map_itemFindAddress : {
        				//#5 10/12/2013
        				String title = getApplicationContext().getString(R.string.map_enter_addr_dialog_title_text);
        				String message = getApplicationContext().getString(R.string.map_enter_addr_dialog_message_text);
        				String okText = getApplicationContext().getString(R.string.map_enter_addr_dialog_ok);
        				String cancelText = getApplicationContext().getString(R.string.map_enter_addr_dialog_cancel);

        				AlertDialog.Builder builder = new AlertDialog.Builder(this);
        				builder.setTitle(title);
        				builder.setMessage(message);
        				// Set up the input
        				final EditText input = new EditText(this);
        				
        				if (targetAddressAsText != null) {
        					input.setText(targetAddressAsText);
        				}	
        				
        				// Specify the type of input
        				input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_NORMAL);
        				builder.setView(input);

        				// Set up the buttons
        				builder.setPositiveButton(okText, new DialogInterface.OnClickListener() { 
        				    @Override
        				    public void onClick(DialogInterface dialog, int which) {
        				    	targetAddressAsText = input.getText().toString();
        //TODO ask did you means?				    	
        						Toast.makeText(getApplicationContext(), targetAddressAsText, Toast.LENGTH_LONG).show();
        				        try {
        				        	Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());				        	
        				            List<Address> geoResults = geocoder.getFromLocationName(targetAddressAsText, 1);
        				            while (geoResults.size() == 0) {
        				                geoResults = geocoder.getFromLocationName(targetAddressAsText, 1);
        				            }
        				            if (geoResults.size() > 0) {
        				            	targetAddressAsAddress = geoResults.get(0);
        				            	if (mapTab.getLastLocation() != null) {
            				                GeoPoint lastLoc = mapTab.getLastLocation();
            				                double startLat = lastLoc.getLatitudeE6() / 1E6;
            				                double startLon = lastLoc.getLongitudeE6() / 1E6;
            				                mapTab.drawRouteToTarget(startLat, startLon, targetAddressAsAddress.getLatitude(), targetAddressAsAddress.getLongitude(), GPSSettings.MAP_CALC_ROUTE_MODE, GPSSettings.IS_MAP_CALC_ROUTE_AVOID_HIGHWAYS, GPSSettings.IS_MAP_CALC_ROUTE_AVOID_TOLLS);
            				                // 05/19/2013
            				                mapTab.refresh();
        				            	}
        				            }
        				        } catch (Exception e) {
        							ErrorHandler.createErrorLog(getApplicationContext(), e);
            						Toast.makeText(getApplicationContext(), "Error occurred when draw route to " + targetAddressAsText, Toast.LENGTH_LONG).show();
        				        }				        
        				    }
        				});
        				builder.setNegativeButton(cancelText, new DialogInterface.OnClickListener() {
        				    @Override
        				    public void onClick(DialogInterface dialog, int which) {
        				        dialog.cancel();
        				    }
        				});

        				builder.show();				
        				break;
        			}
            		}
                	
                }	
            }
            else {
            	// offline map
                if (activity instanceof MapTabMapForge) {
                	MapTabMapForge mapTabMF = (MapTabMapForge)activity;
            		switch (item.getItemId()) {
        			case R.id.map_itemClearRoute: {
        				mapTabMF.clearMap();
        				break;	
        			}
        			case R.id.map_itemCurrentLoc: {
        				//# 08/23/2013
        				mapTabMF.moveToCurrentLocation();
        				break;	
        			}	
                	
                }	
    		}
            	
            }
        }
		
		return false;
	}


	@Override
	protected void onRestart() {
		try {
			CommonLogger.log(Level.INFO);
			SDCardManager.debug("onRestart");
		} catch (Exception e) {
			e.printStackTrace();
			ErrorHandler.createErrorLog(getApplicationContext(), e);
			Toast.makeText(getApplicationContext(), "ERROR in Main.onStop " + e.getMessage(), Toast.LENGTH_LONG).show();
		}		
		super.onRestart();
	}
	
	
	@Override
	protected void onStart() {
		try {
			CommonLogger.log(Level.INFO);
			SDCardManager.debug("onStart");
			
			if (GPSSettings.gpsCoordinates == null)
				GPSSettings.gpsCoordinates = new ArrayList<GPSCoordinate>();
			
			// #5 10/06/2013
			if (isFirstStart) {
		        if (GPSSettings.IS_USE_OFFLINE_MAP) {
					try {
				        // #5 10/06/2013 
						String state = SDCardManager.checkExternalStorageAvailable(getApplicationContext(), true);
				        // #5 10/06/2013 
						if (Environment.MEDIA_MOUNTED.equals(state)) {
							SDCardManager.createFolderOnSDCard(GPSSettings.GTL_TRACKLOG_FOLDER);
							SDCardManager.createFolderOnSDCard(GPSSettings.GTL_MAPS_FOLDER);
							// copy map files to GTL_MAPS_FOLDER
							SDCardManager.copyfilesToRightFolder(this, ".map");
							// copy tracklog files
							SDCardManager.copyfilesToRightFolder(this, ".kml");
						}
						
			            // #5 10/06/2013
						if (Environment.MEDIA_MOUNTED.equals(state)) {
				        	// is map file exists?
							if (!SDCardManager.isMapFileExists()) {
								// is internet available?
								if (ConnectionUtils.isInternetAvailable(this)) {
									// start the Map Selector
							        Intent intent = new Intent();
							        intent.setClass(this, MapSelector.class);
							        // start with download map mode
						        	passCommonInfo(this, intent, new Long(1), data2, data3);
							        // 02/10/2013 startActivity(intent);
						        	startActivityForResult(intent, MapSelector.MAP_SELECTOR_ID_NUM);
								}
							}
							else {
								// 01/13/2013	
								List<File> foundMapFiles = SDCardManager.findFiles(Environment.getExternalStorageDirectory().getPath() + "/" + GPSSettings.GTL_MAPS_FOLDER + "/", ".map");
								if (foundMapFiles.size() > 0) {
									if (foundMapFiles.size() > 1) {
										// #6 11/26/2013 check is there stored last used offline map
										boolean isFoundLastUsed = false; 
										// check last used offline map is available
										String lastUsedOfflineMapName = Utils.readLastUsedOfflineMap(this);
										if (lastUsedOfflineMapName != null) {
											// check it is available on media
											for (File file : foundMapFiles) {
												if (file.getAbsolutePath().contains(lastUsedOfflineMapName)) {
													// use selected map
													GPSSettings.DEFAUL_MAPSFORGE_MAP = lastUsedOfflineMapName;
													isFoundLastUsed = true;
										        	mapTabIntent = new Intent(this, MapTabMapForge.class);
										            mapTabSpec.setIndicator("", getResources().getDrawable(R.drawable.ic_tab_map)).setContent(mapTabIntent);
										            isNeedToAddMapTabSpec = true;
										            // #6 11/26/2013
										            if (tabHost.getTabWidget().getTabCount() < 4)
										            	tabHost.addTab(mapTabSpec);
												}
											}
										}
										if (!isFoundLastUsed) {
											// start the Map Selector
									        Intent intent = new Intent();
									        intent.setClass(this, MapSelector.class);
									        // start with view mode
								        	passCommonInfo(this, intent, new Long(0), data2, data3);
									        // 02/10/2013 startActivity(intent);
								        	startActivityForResult(intent, MapSelector.MAP_SELECTOR_ID_NUM);
										}
									}
									else {
										GPSSettings.DEFAUL_MAPSFORGE_MAP = foundMapFiles.get(0).getName();
										// 02/12/2013
							        	mapTabIntent = new Intent(this, MapTabMapForge.class);
							            mapTabSpec.setIndicator("", getResources().getDrawable(R.drawable.ic_tab_map)).setContent(mapTabIntent);
							            isNeedToAddMapTabSpec = true;
							            // #6 11/26/2013
							            if (tabHost.getTabWidget().getTabCount() < 4)
							            	tabHost.addTab(mapTabSpec);
									}
								}
								foundMapFiles.clear();
								foundMapFiles = null;
							}
						}
						
					}
					finally {
						isFirstStart = false;
					}
		        }
			}
			// #5 10/13/2013
			GPSSettings.VERSION = Utils.extractVersionText(getCurrentActivity());
	        // #5 10/19/2013
   			isTimerStarted = true;
   			//#9 v1.02.6 03/23/2014
   			// S4 : 52EC21D0B5E5E835A3941A3FE57AA395BB9E2E3C
   			GPSSettings.DEVICE_ID = DeviceID.getID(Main.this);
		} catch (Exception e) {
			e.printStackTrace();
			ErrorHandler.createErrorLog(getApplicationContext(), e);
			Toast.makeText(getApplicationContext(), "ERROR in Main.onStop " + e.getMessage(), Toast.LENGTH_LONG).show();
		}		
		super.onStart();
	}

    @Override
    protected void onPause() {
    	super.onPause();
    	try {
    		CommonLogger.log(Level.INFO);
			SDCardManager.debug("onpause");
    		//#13 1.02.10 2015.11.29
    		if (this.sensorManager != null && this.sensorEventListener != null) {
    			this.sensorManager.unregisterListener(this.sensorEventListener);
    		}	
        	
        	if (isNeedToRegisterGPSStatusListener)
        		locationManager.removeGpsStatusListener(gpsStatusListener);
        	
        	locationManager.removeUpdates(gpsLocationListener);
        	        	
        	//01/15/2013
        	unregisterReceiver(batInfoReceiverBroadcastReceiver);

        	// 03/12/2016 #15 v1.02.12 
        	unregisterReceiver(shutdownReceiver);
        	
        	gpsStatusListener = null;
        	gpsLocationListener = null;
//        	locationManager = null;
        	gpsStatus = null;
		} catch (Exception e) {
    		e.printStackTrace();
			ErrorHandler.createErrorLog(getApplicationContext(), e);
			Toast.makeText(getApplicationContext(), "ERROR in Main.onPause " + e.getMessage(), Toast.LENGTH_LONG).show();
		}		
    }    
    
    
    @Override
    protected void onResume() {
    	super.onResume();
    	try {
    		CommonLogger.log(Level.INFO);
			SDCardManager.debug("onResume");
    		// 01/15/2013
    		registerReceiver(batInfoReceiverBroadcastReceiver, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
    		
    		// 03/12/2016 #15 v1.02.12 
    		registerReceiver(shutdownReceiver, new IntentFilter(Intent.ACTION_SHUTDOWN));
    		
        	//#13 1.02.10 2015.11.29
        	if (this.sensorManager != null) {
        		this.sensorManager.registerListener(this.sensorEventListener, mOrientationSensor, SensorManager.SENSOR_DELAY_NORMAL);
        		this.sensorManager.registerListener(this.sensorEventListener, this.mAccelerometerSensor, SensorManager.SENSOR_DELAY_NORMAL);
        		this.sensorManager.registerListener(this.sensorEventListener, mMagneticFieldSensor, SensorManager.SENSOR_DELAY_NORMAL);
    			
        		if (android.os.Build.VERSION.SDK_INT >= 14) {
   					this.sensorManager.registerListener(this.sensorEventListener, mAmbientTemperatureSensor, SensorManager.SENSOR_DELAY_NORMAL);
    			}	
        	}
        	
    		if (isNeedToRegisterGPSStatusListener) {
        		if (gpsStatusListener == null) {
        			// GPS status
        			gpsStatusListener = new GPSStatusListener();
        			locationManager.addGpsStatusListener(gpsStatusListener);
        		}
    		}
    		//#13 1.02.10 2015.11.29.
            setupLocationmanagerAndCriteria(null);
            // GPS Location
            registerLocationListener();
            // 25/1/2014 #8 v1.02.5 last known good location
            setupLastKnownGoodLocation();
        	// 03/12/2016 #15 v1.02.11 
            switchToMapTabAndShowLog(true, false);
            
		} catch (Exception e) {
    		e.printStackTrace();
			ErrorHandler.createErrorLog(getApplicationContext(), e);
			Toast.makeText(getApplicationContext(), "ERROR in Main.onResume " + e.getMessage(), Toast.LENGTH_LONG).show();
		}		
    }
    
    
    /**
     * 01/05/2013
     */
    private void isNeedToSaveTracklog(Double freeMem) {
		String mess = getApplicationContext().getString(R.string.gps_save_tracklog);
		// 01/15/2013
    	if (freeMem != null)
    		mess += "("+freeMem+"Mb)";
		String title = getApplicationContext().getString(R.string.gps_application_stop);
		String yes_btn = getApplicationContext().getString(R.string.button_yes_title);
		String no_btn = getApplicationContext().getString(R.string.button_no_title);
//		Toast.makeText(getApplicationContext(), mess, Toast.LENGTH_LONG).show();
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
		alertDialog.setTitle(title);
		alertDialog.setMessage(mess);
		alertDialog.setPositiveButton(yes_btn, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {
            	try {
            		storeRouteToFile();
            		resetToStart();
            	}
            	finally {
            		// #5 10/22/2013
            		GPSSettings.IS_LOGGING_STARTED = false;
            	}	
            }
        });
 
        // Setting Negative "NO" Button
        alertDialog.setNegativeButton(no_btn, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,    int which) {
            	// #5 10/1962013
            	try {
            		resetToStart();
            	}
            	finally {
            		// #5 10/22/2013
            		GPSSettings.IS_LOGGING_STARTED = false;
            	}	
            }
        });
 			
		alertDialog.show();
    	
    }
    
    
    
    /**
     * 01/15/2013
     */
    @Override
    public void onBackPressed() {
    	if (GPSSettings.IS_LOGGING_STARTED) {
    		String mess = getApplicationContext().getString(R.string.gps_save_tracklog);
    		String title = getApplicationContext().getString(R.string.gps_application_stop);
    		String yes_btn = getApplicationContext().getString(R.string.button_yes_title);
    		String no_btn = getApplicationContext().getString(R.string.button_no_title);
//    		Toast.makeText(getApplicationContext(), mess, Toast.LENGTH_LONG).show();
    		AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
    		alertDialog.setTitle(title);
    		alertDialog.setMessage(mess);
    		alertDialog.setPositiveButton(yes_btn, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog,int which) {
                	try {
                		storeRouteToFile();
                		resetToStart();
                		Main.super.onBackPressed();
                	}
                	finally {
                		// #5 10/22/2013
                		GPSSettings.IS_LOGGING_STARTED = false;
                	}
                }
            });
     
            // Setting Negative "NO" Button
            alertDialog.setNegativeButton(no_btn, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog,    int which) {
                	try {
        				Main.super.onBackPressed();
                	}
            		finally {
                		// #5 10/22/2013
                		GPSSettings.IS_LOGGING_STARTED = false;
            		}
                }
            });
     			
    		alertDialog.show();
    	}	
    	else {
    		super.onBackPressed();
    	}
    }
    
    @Override
    protected void onStop() {
    	try {
    		CommonLogger.log(Level.INFO);
			SDCardManager.debug("onStop");
       		String postNote = "LocationEvent call " + locationEventCnt + " ";
       		postNote += "Stored locations " + storedLocationCnt + " ";
       		//#5 10/19/2013
      		isTimerStarted = false;
       		
		} catch (Exception e) {
    		e.printStackTrace();
			ErrorHandler.createErrorLog(getApplicationContext(), e);
			Toast.makeText(getApplicationContext(), "ERROR in Main.onStop " + e.getMessage(), Toast.LENGTH_LONG).show();
		}		
        super.onStop();
    }
    
    private double calculateMemoryAmountInMb(int mem) {
    	return ((mem / 1024) / 1024);
    }
    
    @Override
    public void onLowMemory() {
    	StatFs statFs = new StatFs(Environment.getRootDirectory().getAbsolutePath());
        int freeMem  = (statFs.getAvailableBlocks() * statFs.getBlockSize()) / 1048576;
        // 01/15/2013
        isNeedToSaveTracklog(new Double(calculateMemoryAmountInMb(freeMem)));
    	CommonLogger.log(Level.INFO, "Free memory is " + calculateMemoryAmountInMb(freeMem) + "Mb.");
    	super.onLowMemory();
    }
    
    @Override
    public void onWindowAttributesChanged(LayoutParams params) {
    	gravityChangedTo = params.gravity;
    	super.onWindowAttributesChanged(params);
    }

    
	@Override
	public void finish() {
		super.finish();
	}
    
    @Override
    protected void onDestroy() {
		SDCardManager.debug("onDestroy");
		this.mOrientationSensor = null;
		this.mAccelerometerSensor = null;
		this.mAmbientTemperatureSensor = null;
		this.mMagneticFieldSensor = null;
		this.gpsStatus = null;
		this.locationManager = null;
    	this.sensorManager = null;
    	this.gpsLocationListener = null;
    	this.gpsStatusListener = null;
    	this.sensorEventListener = null;
    	this.trackLog = null; 
    	this.logEvents = null;
    	GPSSettings.gpsCoordinates.clear();
    	GPSSettings.gpsCoordinates = null;
    	this.maxSpeedValue = null;
    	this.maxAltitudeValue = null;
    	this.maxAccelerationValue = null;
    	this.maxOrientationValue = null;
    	this.minMaxTemperatureValue = null;
    	this.averageValuesManager = null;
    	// #5 10/19/2013
    	timer = null;
    	CommonLogger.log(Level.INFO, "Copyright � 2011-2015 by EKLSoft Trade LLC.");
    	CommonLogger.shutDown();
    	super.onDestroy();
    }	

    @Override 
    public void onActivityResult(int requestCode, int resultCode, Intent data) {     
      super.onActivityResult(requestCode, resultCode, data); 
      switch(requestCode) { 
        case MapSelector.MAP_SELECTOR_ID_NUM : { 
          if (resultCode == Activity.RESULT_OK) { 
        	  int isSelected = -1;
        	  isSelected = data.getIntExtra(MapSelector.MAP_SELECTOR_ID, isSelected);
        	  if (isSelected == 1) {
  	        	if (GPSSettings.DEFAUL_MAPSFORGE_MAP != null) {
  	        		// #5 10/06/2013 remove previous map tab
  	        		if (mapTabIntent != null) {
  	        	        int tabCount = tabHost.getTabWidget().getTabCount();
  	        	        int originTabIx = tabHost.getCurrentTab();
  	        	        // Map Tab is the last one
  	        	        tabHost.setCurrentTab(tabCount - 1);
  	        	        // get tag for check it
  	        	        String tabTag = tabHost.getCurrentTabTag();
  	        	        if (tabTag.equals(TAB_TAG_MAP)) {
  	  	        	        tabHost.getCurrentTabView().setVisibility(View.GONE);
  	        	        }
        	        	// change back to origin
        	            tabHost.setCurrentTab(originTabIx);
  	        		}
	        		mapTabIntent = new Intent(this, MapTabMapForge.class);
	                mapTabSpec.setIndicator("", getResources().getDrawable(R.drawable.ic_tab_map)).setContent(mapTabIntent);
	                isNeedToAddMapTabSpec = true;
	            	tabHost.addTab(mapTabSpec);
	            	isMapTabSpecExists = true;
	        	}	
	        	else {
	        		Toast.makeText(this, R.string.error_offline_map_file_not_found, Toast.LENGTH_LONG).show();
	        	}
        		  
        	  }
          } 
          break; 
        } 
      } 
    }

    /**
     * #8 23/02/2014 
     */
	@Override
	public void onSwipe(int direction) {
		int tabCnt = tabHost.getTabWidget().getTabCount();
		int tabIx = getTabHost().getCurrentTab();
		switch (direction) {
		case SimpleGesture.SWIPE_RIGHT: {
			tabIx--;
			if (tabIx < 0) {
				tabIx = tabCnt - 1;
			}
			break;
		}
		case SimpleGesture.SWIPE_LEFT: {
			tabIx++;
			if (tabIx > tabCnt - 1) {
				tabIx = 0;
			}
			break;
		}
		case SimpleGesture.SWIPE_UP: {
			break;
		}
		case SimpleGesture.SWIPE_DOWN: {
			break;
		}
		}
		tabHost.setCurrentTab(tabIx);
	}

	@Override
	public void onDoubleTap() {
		
	}

}
