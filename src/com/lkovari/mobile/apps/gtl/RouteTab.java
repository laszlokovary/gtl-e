package com.lkovari.mobile.apps.gtl;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.widget.TextView;
import android.widget.Toast;

import com.lkovari.mobile.apps.gtl.settings.GPSSettings;
import com.lkovari.mobile.apps.gtl.utils.GUIDataIdentifiers;
import com.lkovari.mobile.apps.gtl.utils.MeasurementKind;
import com.lkovari.mobile.apps.gtl.utils.MeasurementManager;
import com.lkovari.mobile.apps.gtl.utils.Utils;
import com.lkovari.mobile.apps.gtl.utils.error.ErrorHandler;

/**
 * 
 * @author lkovari
 *
 */
public class RouteTab extends Activity {
	
	private boolean isReceiversRegistered = false;

	private TextView elapsedTextView;
	private TextView odometerTextView;
	private TextView altitudeTextView;
	private TextView bearingTextView;
	private TextView speedTextView; 
	// 01/02/2013 in heading text view
	private TextView avgSpeedTextView;
	private Resources resources;
	// #5 10/06/2013
	private TextView elapsedInMoveTextView;
	private TextView elapsedInWaitTextView;
	
	// Define a handler and a broadcast receiver
	private final Handler handler = new Handler();
	
	private final BroadcastReceiver intentReceiverLoc = new BroadcastReceiver() {
		  @Override
		  public void onReceive(Context context, Intent intent) {
		    double altitude = 0.0;
		    float speed = 0;
		    float avgSpeed = 0;
		    float bearing = 0;
		    // Handle reciever
		    String mAction = intent.getAction();
		    if (mAction.equals(Main.BROADCAST_ACTION_GPS_LOCATION_DATA_CHANGED)) {
		    	Bundle bundle = intent.getExtras();
		    	long broadcastLocationCnt = bundle.getLong(GUIDataIdentifiers.GPS_DATA_BROADCAST_LOCATION_IX); 		    	
		    	double odometer = bundle.getDouble(GUIDataIdentifiers.ROUTE_DATA_ODOMETER);
		    	altitude = bundle.getDouble(GUIDataIdentifiers.ROUTE_DATA_ALTITUDE);
			    speed = bundle.getFloat(GUIDataIdentifiers.ROUTE_DATA_SPEED);
			    // 01/02/2013
			    avgSpeed = bundle.getFloat(GUIDataIdentifiers.ROUTE_DATA_AVGSPEED);
			    bearing = bundle.getFloat(GUIDataIdentifiers.ROUTE_DATA_BEARING);
			    
		    	// show values
		    	showValues(odometer, altitude, speed, avgSpeed, bearing);
		    }
		}
		
	};

	private final BroadcastReceiver intentReceiverElapsed = new BroadcastReceiver() {
		  @Override
		  public void onReceive(Context context, Intent intent) {
		    // Handle reciever
		    String mAction = intent.getAction();
		    if (mAction.equals(Main.BROADCAST_ACTION_GPS_ELLAPSED_IN_MOVE_OR_IN_WAIT_CHANGED)) {
		    	// #5 10/19/2013
		    	Bundle bundle = intent.getExtras();
		    	long elapsed = bundle.getLong(GUIDataIdentifiers.ROUTE_DATA_ELAPSED);
		    	long elapsedInMove = bundle.getLong(GUIDataIdentifiers.ROUTE_DATA_ELAPSED_IN_MOVE);
		    	long elapsedInWait = bundle.getLong(GUIDataIdentifiers.ROUTE_DATA_ELAPSED_IN_WAIT);
		    	showInMoveInWait(elapsed, elapsedInMove, elapsedInWait);
		    }
		}
		
	};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.route_tab);
		// fix orientation
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		// 12/13/2012
		resources = getResources();
	}

	/**
	 * 
	 * @param odometer
	 * @param altitude
	 * @param speed
	 */
	private void setupDataFieldTitles(double odometer, double altitude, float speed, float avgSpeed) {
		// odometer
		String odometerTitle = resources.getString(R.string.route_odometer_title_text);
		String unitName = MeasurementManager.selectUnitNameByMeasurementSystemAndKind(GPSSettings.MEASUREMENT_SYSTEM, MeasurementKind.LENGTH, odometer, false);		
		TextView textView = (TextView) findViewById(R.id.textviewOdometer);
		textView.setText(odometerTitle + " ("+unitName+")");
		
		// altitude
		String altitudeTitle = resources.getString(R.string.route_altitude_title_text);
		unitName = MeasurementManager.selectUnitNameByMeasurementSystemAndKind(GPSSettings.MEASUREMENT_SYSTEM, MeasurementKind.LENGTH, altitude, true);		
		textView = (TextView) findViewById(R.id.textviewAltitude);
		textView.setText(altitudeTitle + " ("+unitName+")");

		// speed
		String speedTitle = resources.getString(R.string.route_speed_title_text);
		unitName = MeasurementManager.selectUnitNameByMeasurementSystemAndKind(GPSSettings.MEASUREMENT_SYSTEM, MeasurementKind.SPEED, speed, false);		
		textView = (TextView) findViewById(R.id.textviewSpeed);
		textView.setText(speedTitle + " ("+unitName+")");
		
		// avg.speed
		String avgSpeedTitle = resources.getString(R.string.route_heading_title_text);
		unitName = MeasurementManager.selectUnitNameByMeasurementSystemAndKind(GPSSettings.MEASUREMENT_SYSTEM, MeasurementKind.SPEED, avgSpeed, false);		
		textView = (TextView) findViewById(R.id.textviewHeading);
		textView.setText(avgSpeedTitle + " ("+unitName+")");
		
	}
	
	/**
	 * 
	 * @param satellites
	 * @param provider
	 * @param accuracy
	 * @param longitude
	 * @param latitude
	 */
	private void showValues(double odometer, double altitude, float speed, float avgSpeed, float bearing) {
		if (odometerTextView == null)
			odometerTextView = (TextView) findViewById(R.id.textviewOdometerData);
		if (altitudeTextView == null)
			altitudeTextView = (TextView) findViewById(R.id.textviewAltitudeData);
		if (speedTextView == null)
			speedTextView = (TextView) findViewById(R.id.textviewSpeedData);
		if (bearingTextView == null)
			bearingTextView = (TextView) findViewById(R.id.textviewBearingData); 
		// 01/02/2013 in heading TextView
		if (avgSpeedTextView == null)
			avgSpeedTextView = (TextView) findViewById(R.id.textviewHeadingData); 

		// 12/13/2012 
		// 01/02/2013 added avgSpeed
		setupDataFieldTitles(odometer, altitude, speed, avgSpeed);
		
		odometerTextView.setText(MeasurementManager.convertvalueByMeasurementSystemAndKindToText(GPSSettings.MEASUREMENT_SYSTEM, MeasurementKind.LENGTH, odometer, false));
		
		/*
    	altitudeTextView.setText(String.format("%.02f", altitude));
    	*/
		altitudeTextView.setText(MeasurementManager.convertvalueByMeasurementSystemAndKindToText(GPSSettings.MEASUREMENT_SYSTEM, MeasurementKind.LENGTH, altitude, true));
		
		
//    	if (speed > -1) {
    		speedTextView.setText(MeasurementManager.convertvalueByMeasurementSystemAndKindToText(GPSSettings.MEASUREMENT_SYSTEM, MeasurementKind.SPEED, speed, false));
//    	}
    	// 01/02/2013
    	avgSpeedTextView.setText(MeasurementManager.convertvalueByMeasurementSystemAndKindToText(GPSSettings.MEASUREMENT_SYSTEM, MeasurementKind.SPEED, avgSpeed, false));
    	
    	bearingTextView.setText(String.format("%.02f", bearing));
	}
	  
	/**
	 * #5 10/19/2013
	 * @param inMove
	 * @param inWait
	 */
	private void showInMoveInWait(long elapsed, long inMove, long inWait) {
		// #5 10/06/2013
		if (elapsedTextView == null)
			elapsedTextView = (TextView) findViewById(R.id.textviewElapsedData);
		if (elapsedInMoveTextView == null)
			elapsedInMoveTextView = (TextView) findViewById(R.id.textviewTimeInMoveData);
		if (elapsedInWaitTextView == null)
			elapsedInWaitTextView = (TextView) findViewById(R.id.textviewTimeInWaitData);

//		long elapsedTime = System.currentTimeMillis() - elapsed;
		long elapsedTime = elapsed;		
		String elapsedText = Utils.milis2TimeString(elapsedTime);
		elapsedTextView.setText(elapsedText);
		
		// #5 10/06/2013
		if (inMove > 0) {
			String elapsedInMoveText = Utils.milis2TimeString(inMove);
			elapsedInMoveTextView.setText(elapsedInMoveText);
		}

		if (inWait > 0) {
			String elapsedInWaitText = Utils.milis2TimeString(inWait);
			elapsedInWaitTextView.setText(elapsedInWaitText);
		}
	}
	
	@Override
	protected void onResume() {
	  try {
		  // Register Sync Recievers
		  IntentFilter intentToReceiveFilterLoc = new IntentFilter();
		  intentToReceiveFilterLoc.addAction(Main.BROADCAST_ACTION_GPS_LOCATION_DATA_CHANGED);
		  this.registerReceiver(intentReceiverLoc, intentToReceiveFilterLoc);
		  //#5 10/19/2013
		  IntentFilter intentToReceiveFilterElapsed = new IntentFilter();
		  intentToReceiveFilterElapsed.addAction(Main.BROADCAST_ACTION_GPS_ELLAPSED_IN_MOVE_OR_IN_WAIT_CHANGED);
		  this.registerReceiver(intentReceiverElapsed, intentToReceiveFilterElapsed);
		  
		  isReceiversRegistered = true;
	  } catch (Exception e) {
		  e.printStackTrace();
		  ErrorHandler.createErrorLog(getApplicationContext(), e);
		  Toast.makeText(getApplicationContext(), "ERROR in RouteTab.onResume " + e.getMessage(), Toast.LENGTH_LONG).show();
	  }		
	  super.onResume();
	}
	
	@Override
	public void onPause() {
	  try {
		  // Make sure you unregister your receivers when you pause your activity
		  if(isReceiversRegistered) {
		    unregisterReceiver(intentReceiverLoc);
		    unregisterReceiver(intentReceiverElapsed);
		    isReceiversRegistered = false;
		  }
	  } catch (Exception e) {
		  e.printStackTrace();
		  ErrorHandler.createErrorLog(getApplicationContext(), e);
		  Toast.makeText(getApplicationContext(), "ERROR in RouteTab.onPause " + e.getMessage(), Toast.LENGTH_LONG).show();
	  }		
	  super.onPause();
	}

	/**
	 * 01/15/2013
	 */
    @Override
    public void onBackPressed() {
    	this.getParent().onBackPressed();
    }
	
}
