package com.lkovari.mobile.apps.gtl;

/**
 * 
 * @author lkovari
 *
 */
public class Coordinate {
	private int degree;
	private double min;
	
	public Coordinate() {
	}
	
	public int getDegree() {
		return degree;
	}
	
	public double getMin() {
		return min;
	}
	
	public void setDegree(int degree) {
		this.degree = degree;
	}
	
	public void setMin(double min) {
		this.min = min;
	}

}
