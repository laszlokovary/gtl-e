package com.lkovari.mobile.apps.gtl;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.lkovari.mobile.apps.gtl.compass.CompassView;

/**
 * #12/31/2012 
 * @author lkovari
 *
 */
public class CompassTab extends Activity {
	private SensorManager orientationSensorManager;
	private Sensor orientationSensor = null;
	private boolean isOrientationSensorRegistered = false;
	private CompassView compassView;
	
	
    private SensorEventListener sensorEventListener = new SensorEventListener(){
		public void onAccuracyChanged(Sensor sensor, int accuracy) {
			
		}

		public void onSensorChanged(SensorEvent event) {
			compassView.updateDirection((float)event.values[0]);
		}
    };
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.compass_tab);
		// fix orientation
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		
		LinearLayout compassTabLayout = (LinearLayout)findViewById(R.id.compastab_linearlayout);		

		compassView = new CompassView(this);
        setContentView(compassView);
//        compassView.requestFocus();		
        
       	orientationSensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);
       	orientationSensor = orientationSensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION);
        if (orientationSensor != null) {
          	isOrientationSensorRegistered = orientationSensorManager.registerListener(sensorEventListener, orientationSensor, SensorManager.SENSOR_DELAY_NORMAL);
        }
		
        
        if (!isOrientationSensorRegistered) {
        	Toast.makeText(this, R.string.error_no_orientationSensor, Toast.LENGTH_LONG).show();
        }
		
	}	
	
	@Override
	protected void onPause() {
		if (isOrientationSensorRegistered) {
        	orientationSensorManager.unregisterListener(sensorEventListener);
		}
		super.onPause();
	}
	
	@Override
	protected void onResume() {
    	if (orientationSensorManager != null) {
    		orientationSensorManager.registerListener(sensorEventListener, orientationSensor, Sensor.TYPE_ORIENTATION);
    	}
		super.onResume();
	}
	
	/**
	 * 01/15/2013
	 */
    @Override
    public void onBackPressed() {
    	this.getParent().onBackPressed();
    }
	
}
