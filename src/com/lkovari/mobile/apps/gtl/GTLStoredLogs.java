package com.lkovari.mobile.apps.gtl;


import java.util.Locale;

import com.lkovari.mobile.apps.gtl.settings.GPSSettings;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.webkit.WebView;
import android.widget.Toast;


public class GTLStoredLogs extends Activity {
	// 08/06/2014 #9 v1.02.6 
	private String SHOW_STORED_LOCS = "http://www.eklsofttrade.com/gtl/gtlfind.php?phoneid=%s";

	/**
	 * 15/03/2014 #9 v1.02.6
	 * @author lkovari
	 *
	 */
	private class ShowHelpAsyncTask extends AsyncTask<String, Void, Boolean> {

		@Override
		protected Boolean doInBackground(String... params) {
			Boolean isOK = null;
			String url = (String)params[0];
			if (url != null) {
				try {
					WebView webview = (WebView) findViewById(R.id.webview);
					webview.loadUrl(url);
					isOK = true;
				}
				catch (Exception e) {
					isOK = false;
				}
			}
			return isOK;
		}
		
		@Override
		protected void onPostExecute(Boolean isOk) {
			if (!isOk) {
				Toast.makeText(GTLStoredLogs.this, R.string.error_nonet_visitlogs, Toast.LENGTH_LONG).show();
			}
		}
	}
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.storedlogs);
		// fix orientation
//		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		
		// 12/2462012
		String iso3Lang = Locale.getDefault().getISO3Language();
		String langPostfix = "en";
		if (iso3Lang.toUpperCase().equals("ENG")) {
			langPostfix = "en";
		}
		else if (iso3Lang.toUpperCase().equals("HUN")) {
			langPostfix = "hu";
		}
		// 15/03/2014 #9 v1.02.6
		String url = SHOW_STORED_LOCS;
		String posUrl = String.format(url, GPSSettings.DEVICE_ID);
		ShowHelpAsyncTask showHelpAsyncTask = new ShowHelpAsyncTask();
		showHelpAsyncTask.execute(posUrl);
	}	
}
