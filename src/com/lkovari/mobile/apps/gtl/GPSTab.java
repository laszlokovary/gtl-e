package com.lkovari.mobile.apps.gtl;

import java.util.logging.Level;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.widget.TextView;
import android.widget.Toast;

import com.lkovari.mobile.apps.gtl.settings.GPSSettings;
import com.lkovari.mobile.apps.gtl.utils.GUIDataIdentifiers;
import com.lkovari.mobile.apps.gtl.utils.MeasurementKind;
import com.lkovari.mobile.apps.gtl.utils.MeasurementManager;
import com.lkovari.mobile.apps.gtl.utils.Utils;
import com.lkovari.mobile.apps.gtl.utils.error.ErrorHandler;
import com.lkovari.mobile.apps.gtl.utils.logger.CommonLogger;

/**
 * 
 * @author lkovari
 *
 */
public class GPSTab extends Activity {
	private boolean isReceiversRegistered = false;

	private TextView statusTextView;
	private TextView satellitesInFixTextView;
	private TextView satellitesAvgStrengthTextView;
	private TextView providerTextView;
	private TextView longitudeTextView;
	private TextView latitudeTextView;
	private TextView accuracyTextView; 
	private Resources resources;
	// 07/09/2013
	private TextView glonassNumTextView;
	private TextView glonassInFixTextView;
	// 19/07/2014 #9 v1.02.6
	private TextView beidouNumTextView;
	private TextView beidouInFixTextView;

	// Define a handler and a broadcast receiver
	private final Handler handler = new Handler();
	
	private final BroadcastReceiver intentReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
	    	float accuracy = 0;
	    	double longitude = 0;
	    	double latitude = 0;
		    // Handle reciever
		    String mAction = intent.getAction();
		    if (mAction.equals(Main.BROADCAST_ACTION_GPS_STATUS_DATA_CHANGED)) {
		    	Bundle bundle = intent.getExtras();
		    	long broadcastStatusCnt = bundle.getLong(GUIDataIdentifiers.GPS_DATA_BROADCAST_STATUS_IX); 		    	
		    	String status = bundle.getString(GUIDataIdentifiers.GPS_DATA_STATUS);
//		    	int satellitesfixed = bundle.getInt(GUIDataIdentifiers.GPS_DATA_SATELLITES);
		    	int usedSatellitesInFix = bundle.getInt(GUIDataIdentifiers.GPS_DATA_SATELLITESINFIX);
		    	Double avgSatStrength = bundle.getDouble(GUIDataIdentifiers.GPS_DATA_SATELLITESAVGSNR);
		    	// 07/09/2013
		    	int glonassNum = bundle.getInt(GUIDataIdentifiers.GPS_DATA_GLONASS_SATELLITES);
		    	int glonassInFix = bundle.getInt(GUIDataIdentifiers.GPS_DATA_GLONASS_SATELLITES_INFIX);
		    	// 19/07/2014 #9 v1.02.6
		    	int beidouNum = bundle.getInt(GUIDataIdentifiers.GPS_DATA_BEIDOU_SATELLITES);
		    	int beidouInFix = bundle.getInt(GUIDataIdentifiers.GPS_DATA_BEIDOU_SATELLITES_INFIX);
		    	
		    	// show values
		    	showStatusValues(context, status, usedSatellitesInFix, avgSatStrength, glonassNum, glonassInFix, beidouNum, beidouInFix);
		    }
		    
		    if (mAction.equals(Main.BROADCAST_ACTION_GPS_LOCATION_DATA_CHANGED)) {
		    	Bundle bundle = intent.getExtras();
		    	long broadcastLocationCnt = bundle.getLong(GUIDataIdentifiers.GPS_DATA_BROADCAST_LOCATION_IX); 		    	
		    	String provider = bundle.getString(GUIDataIdentifiers.GPS_DATA_PROVIDER);
		    	accuracy = bundle.getFloat(GUIDataIdentifiers.GPS_DATA_ACCURACY);
		    	longitude = bundle.getDouble(GUIDataIdentifiers.GPS_DATA_LONGITUDE);
		    	latitude = bundle.getDouble(GUIDataIdentifiers.GPS_DATA_LATITUDE);
		    	// show values
		    	showLocationValues(context, accuracy, longitude, latitude, provider);
		    }	
		}
		
	};

	/**
	 * 
	 * @param context
	 * @param status
	 * @param usedSatellitesInFix
	 * @param satsAvgStrength
	 * @param gnss
	 * @param gnssinfix
	 */
	private void showStatusValues(Context context, String status, int usedSatellitesInFix, Double satsAvgStrength, int gnss, int gnssinfix, int bds, int bdsInFix) {
		if (statusTextView == null)
			statusTextView = (TextView) findViewById(R.id.textviewStatusData);
		if (satellitesInFixTextView == null)
			satellitesInFixTextView = (TextView) findViewById(R.id.textviewSatellitesData);

		if (satellitesAvgStrengthTextView == null)
			satellitesAvgStrengthTextView = (TextView) findViewById(R.id.textviewSNRValue);
		
		statusTextView.setText("" + status);
		satellitesInFixTextView.setText("" + usedSatellitesInFix);
		// 07/09/2013
		if (glonassNumTextView == null)
			glonassNumTextView = (TextView) findViewById(R.id.textviewGlonassNumData);

		glonassNumTextView.setText("" + gnss);
		
		if (glonassInFixTextView == null)
			glonassInFixTextView = (TextView) findViewById(R.id.textviewGlonassInFixData);

		glonassInFixTextView.setText("" + gnssinfix);
		
		// 19/07/2014 #9 v1.02.6
		if (beidouNumTextView == null)
			beidouNumTextView = (TextView) findViewById(R.id.textviewBeidouNumData);

		beidouNumTextView.setText("" + bds);
		
		if (beidouInFixTextView == null)
			beidouInFixTextView = (TextView) findViewById(R.id.textviewBeidouInFixData);

		beidouInFixTextView.setText("" + bdsInFix);
		
		
		if (satsAvgStrength != null) {
			String strengthText = String.format("%.02f", satsAvgStrength);
			satellitesAvgStrengthTextView.setText(strengthText);
		}
	}

	/**
	 * 12/13/2012
	 * @param accuracy
	 */
	private void setupDataFieldTitles(double accuracy) {
		// accuracy
		String accuracyTitle = resources.getString(R.string.gps_accuracy_title_text);
		String unitName = MeasurementManager.selectUnitNameByMeasurementSystemAndKind(GPSSettings.MEASUREMENT_SYSTEM, MeasurementKind.LENGTH, accuracy, true);		
		TextView textView = (TextView) findViewById(R.id.textviewAccuracy);
		textView.setText(accuracyTitle + " ("+unitName+")");
	}
	
	
	/**
	 * 
	 * @param satellites
	 * @param provider
	 * @param accuracy
	 * @param longitude
	 * @param latitude
	 */
	private void showLocationValues(Context context, float accuracy, double longitude, double latitude, String provider) {
		if (longitudeTextView == null)
			longitudeTextView = (TextView) findViewById(R.id.textviewLongitudeData);
		if (latitudeTextView == null)
			latitudeTextView = (TextView) findViewById(R.id.textviewLatitudeData);
		if (accuracyTextView == null)
			accuracyTextView = (TextView) findViewById(R.id.textviewAccuracyData); 
		if (providerTextView == null)
			providerTextView = (TextView) findViewById(R.id.textviewProviderData);
		
		// 12/13/2012
		setupDataFieldTitles(accuracy);
		
		String northSign = context.getString(R.string.gps_north_sign_text_value);
		String eastSign = context.getString(R.string.gps_east_sign_text_value);
		
		providerTextView.setText(provider);
		
//		accuracyTextView.setText("" + accuracy + " m");
		// 12/13/2012
		accuracyTextView.setText(MeasurementManager.convertvalueByMeasurementSystemAndKindToText(GPSSettings.MEASUREMENT_SYSTEM, MeasurementKind.LENGTH, accuracy, true));
		
		Coordinate coord = Utils.convertposToCoordinate(longitude);
		String lonText1 = eastSign + String.format(" %.08f", longitude);
		String lonText2 = eastSign + String.format(" %d %.03f", coord.getDegree(), coord.getMin());
		longitudeTextView.setText(lonText2);
		coord = Utils.convertposToCoordinate(latitude);
		String latText1 = northSign + String.format(" %.08f", latitude);
		String latText2 = northSign + String.format(" %d %.03f", coord.getDegree(), coord.getMin());
		latitudeTextView.setText(latText2);
	}
	  
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.gps_tab);
		// fix orientation
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);		
		// 12/13/2012
		resources = getResources();
	}
	
	@Override
	protected void onResume() {
		try {
			CommonLogger.log(Level.INFO);
		  // Register Sync Recievers
		  IntentFilter intentToReceiveFilter = new IntentFilter();
		  intentToReceiveFilter.addAction(Main.BROADCAST_ACTION_GPS_STATUS_DATA_CHANGED);
		  intentToReceiveFilter.addAction(Main.BROADCAST_ACTION_GPS_LOCATION_DATA_CHANGED);
//		  this.registerReceiver(intentReceiver, intentToReceiveFilter, null, handler);
		  this.registerReceiver(intentReceiver, intentToReceiveFilter);
		  isReceiversRegistered = true;
		} catch (Exception e) {
			e.printStackTrace();
			CommonLogger.log(Level.SEVERE, e.getMessage());
			ErrorHandler.createErrorLog(getApplicationContext(), e);
			Toast.makeText(getApplicationContext(), "ERROR in GPSTab.onResume " + e.getMessage(), Toast.LENGTH_LONG).show();
		}		
		super.onResume();
	}
	
	@Override
	public void onPause() {
		try {
			CommonLogger.log(Level.INFO);
			// Make sure you unregister your receivers when you pause your activity
			if(isReceiversRegistered) {
				unregisterReceiver(intentReceiver);
			    isReceiversRegistered = false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			CommonLogger.log(Level.SEVERE, e.getMessage());
			ErrorHandler.createErrorLog(getApplicationContext(), e);
			Toast.makeText(getApplicationContext(), "ERROR in GPSTab.onPause " + e.getMessage(), Toast.LENGTH_LONG).show();
		}		
		super.onPause();
	}

	/**
	 * 01/15/2013
	 */
    @Override
    public void onBackPressed() {
    	this.getParent().onBackPressed();
    }
	

}
