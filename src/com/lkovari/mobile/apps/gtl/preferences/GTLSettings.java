package com.lkovari.mobile.apps.gtl.preferences;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceCategory;
import android.preference.PreferenceGroup;
import android.preference.PreferenceManager;
import android.preference.PreferenceScreen;
import android.widget.Toast;

import com.lkovari.mobile.apps.gtl.R;
import com.lkovari.mobile.apps.gtl.settings.DataProviderKind;
import com.lkovari.mobile.apps.gtl.settings.GPSSettings;
import com.lkovari.mobile.apps.gtl.settings.TrackColorKind;
import com.lkovari.mobile.apps.gtl.settings.TrackThicknessKind;
import com.lkovari.mobile.apps.gtl.settings.UsageType;
import com.lkovari.mobile.apps.gtl.utils.MeasurementSystem;

/**
 * 2012.11.04.
 * @author lkovari
 *
 */
public class GTLSettings extends PreferenceActivity implements OnSharedPreferenceChangeListener {
//TODO !!!!!!! rewrite to use PreferenceFragment	
	private boolean isChanged = false;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		PreferenceManager.setDefaultValues(getApplicationContext(), R.xml.gtl_preferences, false);
		
		SharedPreferences preferencesSettings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		preferencesSettings.registerOnSharedPreferenceChangeListener(this);

		addPreferencesFromResource(R.xml.gtl_preferences);
		
	}
	
	@Override
	protected void onStart() {
		// 25/1/2014 #8 v1.02.5
		if (!GPSSettings.DATA_PROVIDER_KIND.equals(DataProviderKind.BEST_PROVIDER)) {
			// get preference gtlsettings
			PreferenceScreen preferenceScreen = (PreferenceScreen) findPreference("key_gtlpreference");
			// disable category 
			PreferenceGroup preferenceGroup = (PreferenceGroup) findPreference("key_category_criterias");
			if (preferenceScreen != null) {
				preferenceScreen.removePreference(preferenceGroup);
			}
		}
		
		// 01/02/2013
		if (GPSSettings.IS_USE_OFFLINE_MAP) {
			PreferenceGroup preferenceGroup = (PreferenceGroup) findPreference("key_category_map");
			if (preferenceGroup != null) {
				/* #8 25/02/2014
				Preference preferenceAutomaticZoomLevel = findPreference("key_automatic_zoom_level");
				if (preferenceAutomaticZoomLevel != null) {
					preferenceGroup.removePreference(preferenceAutomaticZoomLevel);
				}
				*/
				Preference preferenceShowAddress = findPreference("key_show_address");
				if (preferenceShowAddress != null) {
					preferenceGroup.removePreference(preferenceShowAddress);
				}
				
				Preference switchToMapViewWhenStop = findPreference("key_switch_to_map_view_when_stop");
				if (switchToMapViewWhenStop != null) {
					preferenceGroup.removePreference(switchToMapViewWhenStop);
				}
			}
			// 05/22/2013
			PreferenceScreen preferenceScreen = (PreferenceScreen) findPreference("key_gtlpreference");
			if (preferenceScreen != null) {
				PreferenceCategory preferenceCategory = (PreferenceCategory) findPreference("key_category_calcroute");
				if (preferenceCategory != null) {
					preferenceScreen.removePreference(preferenceCategory);
				}
			}
			
		}
		
		// #5 10/12/2013
		// #8 23/02/2014 
		PreferenceGroup preferenceGroup = (PreferenceGroup) findPreference("key_category_calcroute");
		if (!GPSSettings.IS_EXTRA_FUNCTIONS_ENABLED) {
			if (preferenceGroup != null) {
				preferenceGroup.setEnabled(false);
			}
		}
		else {
			if (preferenceGroup != null) {
				preferenceGroup.setEnabled(true);
			}
		}
		
		// 17/08/2014 #9 v1.02.6
   		Preference markerStoreRealtimePreferenc = findPreference("key_marker_store_on_web");
   		if (markerStoreRealtimePreferenc != null) {
   			markerStoreRealtimePreferenc.setEnabled(GPSSettings.IS_ENABLE_REALTIME_POSITION_SENDING);
   		}
		
   		// 03/12/2016 #15 v1.02.11 
   		Preference runnersPreference = findPreference("key_category_runners");
		runnersPreference.setEnabled(GPSSettings.USAGE_TYPE.equals(UsageType.RUNNER));
   		
		isChanged = false;
		super.onStart();
	}
	
	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
		// 25/1/2014 #8 v1.02.5
		if (key.equals("key_dataprovider")) {
			String dataProvider = sharedPreferences.getString("key_dataprovider", "0");
			if (dataProvider.equals("0")) {
				GPSSettings.DATA_PROVIDER_KIND = DataProviderKind.BEST_PROVIDER;
			}
			else if (dataProvider.equals("1")) {
				GPSSettings.DATA_PROVIDER_KIND = DataProviderKind.GPS_PROVIDER;
			} 
			else if (dataProvider.equals("2")) {
				GPSSettings.DATA_PROVIDER_KIND = DataProviderKind.NETWORK_PROVIDER;
			} 
		}
		else if (key.equals("key_min_accuracy")) {
			String minAccuracy = sharedPreferences.getString("key_min_accuracy", "30");
			GPSSettings.MIN_ACCURACY = Integer.parseInt(minAccuracy);
		}
		else if (key.equals("key_min_satellites")) {
			String minSatellites = sharedPreferences.getString("key_min_satellites", "4");
			GPSSettings.MIN_SATELLITES = Integer.parseInt(minSatellites);
		}
		else if (key.equals("key_optimization_limit")) {
			String optimizationLimit = sharedPreferences.getString("key_optimization_limit", "4");
			GPSSettings.POS_OPTIMIZE_LIMIT = Integer.parseInt(optimizationLimit);
		}
		else if (key.equals("key_optimization_tolerance")) {
			String optimizationTolerance = sharedPreferences.getString("key_optimization_tolerance", "19.5");
			GPSSettings.DOUGLAS_PEUCKER_TOLARANCE = Double.parseDouble(optimizationTolerance);
		}
		else if (key.equals("key_marker_transparency")) {
			String markerTransparency = sharedPreferences.getString("key_marker_transparency", "64");
			GPSSettings.ACCURACY_MARKER_TRANSPARENCY = Integer.parseInt(markerTransparency);
		}
		else if (key.equals("key_usagetype")) {
			// 12/15/2012
			String usageType = sharedPreferences.getString("key_usagetype", "3");
			if (usageType.equals("0")) {
				GPSSettings.USAGE_TYPE = UsageType.AIRCRAFT;
				GPSSettings.MAP_CALC_ROUTE_MODE = "transit";
				GPSSettings.MIN_DISTANCE = 5.0f;
				GPSSettings.MIN_TIME = 5000;
			}
			else if (usageType.equals("1")) {
				GPSSettings.USAGE_TYPE = UsageType.WATERCRAFTS;
				GPSSettings.MAP_CALC_ROUTE_MODE = "transit";
				GPSSettings.MIN_DISTANCE = 2.5f;
				GPSSettings.MIN_TIME = 2500;
			}
			else if (usageType.equals("2")) {
				GPSSettings.USAGE_TYPE = UsageType.FOUR_WHEELERS;
				GPSSettings.MAP_CALC_ROUTE_MODE = "driving";
				GPSSettings.MIN_DISTANCE = 1.5f;
				GPSSettings.MIN_TIME = 2000;
			}
			else if (usageType.equals("3")) {
				GPSSettings.USAGE_TYPE = UsageType.TWO_WHEELERS;
				GPSSettings.MAP_CALC_ROUTE_MODE = "driving";
				GPSSettings.MIN_DISTANCE = 1.0f;
				GPSSettings.MIN_TIME = 2000;
			}
			else if (usageType.equals("4")) {
				GPSSettings.USAGE_TYPE = UsageType.WALKING_HIKE;
				GPSSettings.MAP_CALC_ROUTE_MODE = "walking";
				GPSSettings.MIN_DISTANCE = 3.0f;
				GPSSettings.MIN_TIME = 1000;
			}
			else if (usageType.equals("5")) {
				GPSSettings.USAGE_TYPE = UsageType.PEDESTRIAN;
				GPSSettings.MAP_CALC_ROUTE_MODE = "walking";
				GPSSettings.MIN_DISTANCE = 5.0f;
				GPSSettings.MIN_TIME = 1000;
			}
			else if (usageType.equals("6")) {
				GPSSettings.USAGE_TYPE = UsageType.RUNNER;
				GPSSettings.MAP_CALC_ROUTE_MODE = "running";
				GPSSettings.MIN_DISTANCE = 0.5f;
				GPSSettings.MIN_TIME = 100;
			}
			sharedPreferences.edit().putString("key_min_distance", ""+GPSSettings.MIN_DISTANCE);
			sharedPreferences.edit().putString("key_min_time", ""+GPSSettings.MIN_TIME);
		}
		else if (key.equals("key_altitude_required")) {
			boolean isAltitudeRequired = sharedPreferences.getBoolean("key_altitude_required", true);
			GPSSettings.IS_CRITERIA_ALTITUDE_REQUIRED = isAltitudeRequired;
		}
		else if (key.equals("key_bearing_required")) {
			boolean isBearingRequired = sharedPreferences.getBoolean("key_bearing_required", true);
			GPSSettings.IS_CRITERIA_BEARING_REQUIRED = isBearingRequired;
		}
		else if (key.equals("key_speed_required")) {
			boolean isSpeedRequired = sharedPreferences.getBoolean("key_speed_required", true);
			GPSSettings.IS_CRITERIA_SPEED_REQUIRED = isSpeedRequired;
		}
		else if (key.equals("key_accuracy_level")) {
			String accuracyLevel = sharedPreferences.getString("key_accuracy_level", "1");
			GPSSettings.CRITERIA_ACCURACY_LEVEL = Integer.parseInt(accuracyLevel);
		}
		else if (key.equals("key_power_level")) {
			String powerLevel = sharedPreferences.getString("key_power_level", "1");
			GPSSettings.CRITERIA_POWER_LEVEL = Integer.parseInt(powerLevel);
		}
		else if (key.equals("key_animate_to_position")) {
			boolean isAnimateToPosition = sharedPreferences.getBoolean("key_animate_to_position", false);
			GPSSettings.IS_ANIMATE_TO_POSITION = isAnimateToPosition;
			if (GPSSettings.IS_ANIMATE_TO_POSITION) {
				// 12/02/2012 switch off keep full track on view
				sharedPreferences.edit().putBoolean("key_keep_full_track_on_map", false);
				sharedPreferences.edit().putBoolean("key_automatic_zoom_level", false);
			}
		}
		else if (key.equals("key_automatic_zoom_level")) {
			boolean isSetAutomaticZoomLevel = sharedPreferences.getBoolean("key_automatic_zoom_level", false);
			GPSSettings.IS_SET_AUTOMATIC_ZOOM_LEVEL = isSetAutomaticZoomLevel;
			if (GPSSettings.IS_SET_AUTOMATIC_ZOOM_LEVEL) {
				// 12/02/2012 switch off keep full track on view
				sharedPreferences.edit().putBoolean("key_keep_full_track_on_map", false);
			}
		}
		else if (key.equals("key_show_accuracy_marker")) {
			boolean isShowAccuracyMarker = sharedPreferences.getBoolean("key_show_accuracy_marker", true);
			GPSSettings.IS_SHOW_ACCURACY_MARKER = isShowAccuracyMarker;
		}
		else if (key.equals("key_show_address")) {
			boolean isShowAddress = sharedPreferences.getBoolean("key_show_address", false);
			GPSSettings.IS_SHOW_ADDRESS_IF_TOUCH_TO_MAP = isShowAddress;
		}
		else if (key.equals("key_set_optimization")) {
			boolean isOptimizationActive = sharedPreferences.getBoolean("key_set_optimization", true);
			GPSSettings.IS_OPTIMIZATION_ACTIVE = isOptimizationActive;
		}
		else if (key.equals("key_remove_zero_speed_location_points")) {
			boolean isRemoveZeroSpeedPositions = sharedPreferences.getBoolean("key_remove_zero_speed_location_points", false);
			GPSSettings.IS_REMOVE_ZERO_SPEED_LOCATIONS_ACTIVE = isRemoveZeroSpeedPositions;
		}
		else if (key.equals("key_min_distance")) {
			String minDistance = sharedPreferences.getString("key_min_distance", "1.0");
			GPSSettings.MIN_DISTANCE = Float.parseFloat(minDistance);
		}
		else if (key.equals("key_min_time")) {
			String minTime = sharedPreferences.getString("key_min_time", "0");
			GPSSettings.MIN_TIME = Long.parseLong(minTime);
		}
		else if (key.equals("key_store_placemarks_at_start_pause_stop_only")) {
			boolean isStorePlacemarkStartPauseStopOnly = sharedPreferences.getBoolean("key_store_placemarks_at_start_pause_stop_only", false);
			GPSSettings.IS_STORE_PLACEMARKS_AT_START_PAUSE_STOP_ONLY = isStorePlacemarkStartPauseStopOnly;
		}
		else if (key.equals("key_switch_to_map_view_when_stop")) {
			boolean isSwitchToMapViewWhenStop = sharedPreferences.getBoolean("key_switch_to_map_view_when_stop", true);
			GPSSettings.IS_SWITCH_TO_MAP_TAB_WHEN_STOP_TRACKING = isSwitchToMapViewWhenStop;
		}
		else if (key.equals("key_keep_full_track_on_map")) {
			boolean isKeepFullTrackOnMapView = sharedPreferences.getBoolean("key_keep_full_track_on_map", true);
			GPSSettings.IS_KEEP_FULL_TRACK_ON_VIEW_OF_MAP = isKeepFullTrackOnMapView;
			if (GPSSettings.IS_KEEP_FULL_TRACK_ON_VIEW_OF_MAP) {
				GPSSettings.IS_ANIMATE_TO_POSITION = false;
				GPSSettings.IS_SET_AUTOMATIC_ZOOM_LEVEL = false;
				// switch off animate to position and automatic zoom level
				sharedPreferences.edit().putBoolean("key_animate_to_position", false);
				sharedPreferences.edit().putBoolean("key_automatic_zoom_level", false);
			}	
		}
		else if (key.equals("key_measurementsystem")) {
			String measurementSystem = sharedPreferences.getString("key_measurementsystem", "0");
			int ms = Integer.parseInt(measurementSystem);
			if (ms == 0)
				GPSSettings.MEASUREMENT_SYSTEM = MeasurementSystem.METRIC;
			else if (ms == 1)
				GPSSettings.MEASUREMENT_SYSTEM = MeasurementSystem.IMPERIAL;
			else if (ms == 2)
				GPSSettings.MEASUREMENT_SYSTEM = MeasurementSystem.ICAO;
		}
		else if (key.equals("key_use_offline_map")) {
			boolean isUseOfflineMap = sharedPreferences.getBoolean("key_use_offline_map", false);
			GPSSettings.IS_USE_OFFLINE_MAP = isUseOfflineMap;
		}
		else if (key.equals("key_fullscreen")) {
			boolean isFullScreenOn = sharedPreferences.getBoolean("key_fullscreen", true);
			GPSSettings.IS_FULLSCREEN_ON = isFullScreenOn;
		}
		else if (key.equals("key_calcroute_avoid_highways")) {
			// 05/22/2013
			boolean isAvoidHighways = sharedPreferences.getBoolean("key_calcroute_avoid_highways", true);
			GPSSettings.IS_MAP_CALC_ROUTE_AVOID_HIGHWAYS = isAvoidHighways;
		}
		else if (key.equals("key_calcroute_avoid_tolls")) {
			// 05/22/2013
			boolean isAvoidTolls = sharedPreferences.getBoolean("key_calcroute_avoid_tolls", true);
			GPSSettings.IS_MAP_CALC_ROUTE_AVOID_TOLLS = isAvoidTolls;
		}
		else if (key.equals("key_track_color")) {
			String trackColor = sharedPreferences.getString("key_track_color", "5");
			int colorValue = 0;
			if (trackColor.equals("0")) {
				colorValue = TrackColorKind.BLACK.getValue();
			}
			else if (trackColor.equals("1")) {
				colorValue = TrackColorKind.BLUE.getValue();
			}
			else if (trackColor.equals("2")) {
				colorValue = TrackColorKind.GREEN.getValue();
			}
			else if (trackColor.equals("3")) {
				colorValue = TrackColorKind.CYAN.getValue();
			}
			else if (trackColor.equals("4")) {
				colorValue = TrackColorKind.MAGENTA.getValue();
			}
			else if (trackColor.equals("5")) {
				colorValue = TrackColorKind.RED.getValue();
			}
			GPSSettings.KML_TRACK_COLOR = String.format("%08X", (0xFFFFFFFF & colorValue));
			// kml ff0000ff map ff0000
			colorValue =  colorValue << 8;
			colorValue =  colorValue >>> 8;
			//GPSSettings.MAP_TRACK_COLOR = Integer.toHexString(colorValue);
			GPSSettings.MAP_TRACK_COLOR = String.format("%06X", (0xFFFFFF & colorValue));		
		}
		else if (key.equals("key_track_thickness")) {
			String trackThickness = sharedPreferences.getString("key_track_thickness", "8");
			if (trackThickness.equals("4")) {
				GPSSettings.KML_TRACK_THICKNESS = TrackThicknessKind.NARROW.getValue();
				GPSSettings.MAP_TRACK_THICKNESS = GPSSettings.KML_TRACK_THICKNESS - 2;
				
			}
			else if (trackThickness.equals("8")) {
				GPSSettings.KML_TRACK_THICKNESS = TrackThicknessKind.MEDIUM.getValue();
				GPSSettings.MAP_TRACK_THICKNESS = GPSSettings.KML_TRACK_THICKNESS - 2;
			}
			else if (trackThickness.equals("12")) {
				GPSSettings.KML_TRACK_THICKNESS = TrackThicknessKind.THICK.getValue();
				GPSSettings.MAP_TRACK_THICKNESS = GPSSettings.KML_TRACK_THICKNESS - 2;
			}
		}
		else if (key.equals("key_marker_store_on_web")) {
			// 22/043/2014 #9 v1.02.6
			String timePeriod = sharedPreferences.getString("key_marker_store_on_web", "0");
			GPSSettings.STORELOCATION_EVERY_N_MINUTES = Integer.valueOf(timePeriod);
			GPSSettings.IS_STORELOCATION_ON_WEB_FUNCTIONS_ENABLED = (GPSSettings.STORELOCATION_EVERY_N_MINUTES > 0);
		}
		else if (key.equals("key_runners_distance")) {
			String distance = sharedPreferences.getString("key_runners_distance", "0");
			GPSSettings.RUNNER_DISTNACE = Integer.parseInt(distance);
			// reset the other
			sharedPreferences.edit().putString("key_runners_elapsedtime", "0");
			
		}
		else if (key.equals("key_runners_elapsedtime")) {
			String elapsedTime = sharedPreferences.getString("key_runners_elapsedtime", "0");
			GPSSettings.RUNNER_ELAPSED_TIME = Integer.parseInt(elapsedTime);
			// reset the other
			sharedPreferences.edit().putString("key_runners_distance", "0");
		}
		
		isChanged = true;
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
	}
	
	@Override
	protected void onStop() {
		// #5 10/07/2013
		if (isChanged) {
			Toast.makeText(GTLSettings.this, R.string.settings_take_effect, Toast.LENGTH_LONG).show();
		}	
		super.onStop();
	}
	
}
