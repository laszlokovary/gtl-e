package com.lkovari.mobile.apps.gtl.utils;

import java.lang.reflect.Field;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Build;

import com.lkovari.mobile.apps.gtl.Coordinate;


/**
 * 
 * @author lkovari
 *
 */
public class Utils {
    public static final String PREFS_NAME = "trsipr";
	public static final String LICENSE_PREFERENCE_TAG = "islicenseaccepted";
	public static final String LAST_USED_OFFLINE_PREFERENCE_TAG = "lastusedofflinemap";
	//#1/10/2014 #10 v1.02.7
	public static final String LOG_FILE_PATH_TAG = "logfilepath";
	
	/**
	 * 
	 * @param mps
	 * @return
	 */
	public static double meterpersec2kilometer(double mps) {
		return (mps * (60 * 60)) / 1000;
	}

	/**
	 * 
	 * @param value
	 * @param numOfFraction
	 * @return
	 */
    public static double roundTo(double value, int numOfFraction) {
        int mul = 10;
        int m = 10;
        for (int ix = 1; ix < numOfFraction; ix++)
            m = m * mul; 
        int truncatedToNumOfFraction = (int) (value * m);
        double vlu = ((double)truncatedToNumOfFraction / m);
        return vlu;
    }

    /**
     * 
     * @param pos
     * @return Coordinate
     */
    public static Coordinate convertposToCoordinate(double pos) {
    	Coordinate coord = new Coordinate();
    	int degree = 0;
    	double fraction = 0.0;
    	degree = (int) pos;
    	fraction = pos - (double) degree;
    	double minutes = fraction * 60;
    	coord.setDegree(degree);
    	coord.setMin(minutes);
    	return coord;
    }
    
    /**
     * 
     * @param milis
     * @return
     */
    public static String milis2TimeString(long milis) {
    	String timeAsText = "";
    	long miliseconds = milis;
    	
    	long ref_sec = 1000;
    	long ref_minute = 60 * ref_sec;
    	long ref_hour = ref_minute * 60;
    	long ref_day = ref_hour * 24;
    	long ref_week = ref_day * 7;
    	
    	if (miliseconds > (ref_week * 2)) {
    		timeAsText = "0";
    		return timeAsText;
    	}
    		
    	int weeks = 0;
    	int days = 0;
    	int hours = 0;
    	int minutes = 0;
    	int secs = 0;
    	
    	if (miliseconds > ref_week) {
    		// weeks
    		weeks = (int)(miliseconds / ref_week);
    		// remainder
    		miliseconds = milis - (weeks * ref_week);
    	}
    	
    	if (miliseconds > ref_day) {
    		days = (int)(miliseconds / ref_day);
    		miliseconds = miliseconds - (days * ref_day);
    	}
    	
    	if (miliseconds > ref_hour) {
    		hours = (int)(miliseconds / ref_hour);
    		miliseconds = miliseconds - (hours * ref_hour);
    	}
    	
    	if (miliseconds > ref_minute) {
    		minutes = (int)(miliseconds / ref_minute);
    		miliseconds = miliseconds - (minutes * ref_minute);
    	}
    	if (miliseconds > ref_sec) {
    		secs = (int)(miliseconds / ref_sec);
    		miliseconds = miliseconds - (secs * ref_sec);
    	}

    	if (weeks > 0) {
    		timeAsText = String.format("%dw %dd %dh %dm %ds", weeks, days, hours, minutes, secs);
    	}
    	else if (days > 0) {
    		timeAsText = String.format("%dd %dh %dm %ds", days, hours, minutes, secs);
    	}
    	else if (hours > 0) {
    		timeAsText = String.format("%dh %dm %ds", hours, minutes, secs);
    	}
    	else if (minutes > 0) {
    		timeAsText = String.format("%dm %ds", minutes, secs);
    	}
    	else if (secs > 0) {
    		timeAsText = String.format("%ds", secs);
    	}
    	return timeAsText;
    }
    
    

	/**
	 * 
	 * @param c
	 * @param string
	 * @return
	 */
	public static <T extends Enum<T>> T getEnumByString(Class<T> c, String string) {
	    if( c != null && string != null ) {
	        try {
	            return Enum.valueOf(c, string);
	        }
	        catch(IllegalArgumentException ex) {
	        	throw new RuntimeException("Can't get Enum by String with valueOf!");
	        }
	    }
	    return null;
	}
	
	/**
	 * #5 10/13/2013
	 * @param activity
	 * @return
	 */
	public static String extractVersionText(Activity activity) {
	    String versionName = "";
	    String versionCode = "";
	    PackageInfo packageInfo;
	    try {
	        packageInfo = activity.getPackageManager().getPackageInfo(activity.getPackageName(), 0);
	        versionName = "v " + packageInfo.versionName;
	        versionCode = " #" + packageInfo.versionCode;
	        
	    } catch (NameNotFoundException e) {
	    	versionName = "-";
	    	versionCode = "-";
	        e.printStackTrace();
	    }
	    return versionCode + " " + versionName;
	}	
	
	/**
	 * #6 11/26/2013
	 * @param activity
	 * @param prefName
	 * @return
	 */
	public static boolean readLicenseAccepted(Activity activity) {
		SharedPreferences settings = activity.getSharedPreferences(PREFS_NAME, 0);
        boolean isLicenseAccepted = settings.getBoolean(LICENSE_PREFERENCE_TAG, false);
		return isLicenseAccepted;
	}
	
	public static void writeLicenseAccepted(Activity activity, boolean isAccepted) {
		SharedPreferences settings = activity.getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(LICENSE_PREFERENCE_TAG, true);
        editor.commit();
	}
	
	/**
	 * #1/10/2014 #10 v1.02.7
	 * @param activity Activity
	 * @param path String - the pat which want to store
	 */
	public static void writeLogFilePath(Activity activity, String path) {
		SharedPreferences settings = activity.getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(LOG_FILE_PATH_TAG, path);
        editor.commit();
	}

	/**
	 * #1/10/2014 #10 v1.02.7
	 * @param activity
	 * @return String the stored path if exists else null
	 */
	public static String readLogFilePath(Activity activity) {
		SharedPreferences settings = activity.getSharedPreferences(PREFS_NAME, 0);
        String path = settings.getString(LOG_FILE_PATH_TAG, null);
		return path;		
	}
	
	/**
	 * #6 11/26/2013
	 * @param activity
	 * @return
	 */
	public static String readLastUsedOfflineMap(Activity activity) {
		SharedPreferences settings = activity.getSharedPreferences(PREFS_NAME, 0);
        String lastUsedOfflineMapName = settings.getString(LAST_USED_OFFLINE_PREFERENCE_TAG, null);
		return lastUsedOfflineMapName;
	}

	/**
	 * #6 11/26/2013
	 * @param activity
	 * @param offlineMapName
	 */
	public static void writeLastUsedOfflineMap(Activity activity, String offlineMapName) {
		SharedPreferences settings = activity.getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(LAST_USED_OFFLINE_PREFERENCE_TAG, offlineMapName);
        editor.commit();
	}
    
	 @SuppressLint("NewApi") 
	 public static String extractDeviceName(boolean withSerial) {
		String deviceName = "";
		String manufacturer = Build.MANUFACTURER;
		String model = Build.MODEL;
		if (model.startsWith(manufacturer)) {
			deviceName = deviceNameToCapitalize(model);
		} 
		else {
			deviceName = deviceNameToCapitalize(manufacturer) + " " + model;
		}
		if (withSerial) {
			if (android.os.Build.VERSION.SDK_INT >= 9) {
				deviceName += " (sn:" +Build.SERIAL+")";
			}	
		}
		return deviceName;
	 }


	 private static String deviceNameToCapitalize(String s) {
	   if (s == null || s.length() == 0) {
	     return "";
	   }
	   char first = s.charAt(0);
	   if (Character.isUpperCase(first)) {
	     return s;
	   } 
	   else {
	     return Character.toUpperCase(first) + s.substring(1);
	   }
	 } 

	 public static String extractAndroidVersion() {
		 String ver = null;
		 StringBuilder builder = new StringBuilder();
		 builder.append("Android ").append(Build.VERSION.RELEASE);

		 Field[] fields = Build.VERSION_CODES.class.getFields();
		 for (Field field : fields) {
		     String fieldName = field.getName();
		     int fieldValue = -1;

		     try {
		         fieldValue = field.getInt(new Object());
		     } 
		     catch (IllegalArgumentException e) {
		         e.printStackTrace();
		     } 
		     catch (IllegalAccessException e) {
		         e.printStackTrace();
		     } 
		     catch (NullPointerException e) {
		         e.printStackTrace();
		     }

		     if (fieldValue == Build.VERSION.SDK_INT) {
		         builder.append(" ");
		         builder.append(fieldName);
		         builder.append(" ");
		         builder.append("SDK#").append(fieldValue);
		     }
		 }

		 ver = builder.toString();
		 builder = null;
		 return ver;
	 }
	 
	 /**
	  * #1/10/2014 #10 v1.02.7
	  * @param activity Activity
	  * @param packageName String - desired package name
	  * @return boolean - true if installed else not
	  */
	 public static boolean isAppInstalled(Activity activity, String packageName) {
	     PackageManager packageManager = activity.getPackageManager();
	     boolean isInstalled = false;
	     try {
	    	 packageManager.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES);
	    	 isInstalled = true;
	     } catch (PackageManager.NameNotFoundException e) {
	    	 isInstalled = false;
	     }
	     return isInstalled;
	 }	 
	
	 /**
	  * #1/10/2014 #10 v1.02.7
	  * @param activity Activity
	  * @return boolean - true if Google Earth is installed else not
	  */
	 public static boolean isGoogleEarthInstalled(Activity activity) {
		 String packageName = "com.google.earth";
		 return isAppInstalled(activity, packageName);
	 }
	 
}
