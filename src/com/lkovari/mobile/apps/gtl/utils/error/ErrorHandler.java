package com.lkovari.mobile.apps.gtl.utils.error;

import java.lang.reflect.Field;
import java.util.Date;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;

import com.lkovari.mobile.apps.gtl.utils.logger.CommonLogger;
import com.lkovari.mobile.apps.gtl.utils.sdcard.SDCardManager;

/**
 * 
 * @author lkovari
 *
 */
public class ErrorHandler {

	private static String deviceNameToCapitalize(String s) {
		if (s == null || s.length() == 0) {
			return "";
		}
		char first = s.charAt(0);
		if (Character.isUpperCase(first)) {
			return s;
		} else {
			return Character.toUpperCase(first) + s.substring(1);
		}
	}
	
	@SuppressLint("NewApi") 
	public static String extractDeviceName(boolean withSerial) {
		String deviceName = "";
		String manufacturer = Build.MANUFACTURER;
		String model = Build.MODEL;
		if (model.startsWith(manufacturer)) {
			deviceName = deviceNameToCapitalize(model);
		} else {
			deviceName = deviceNameToCapitalize(manufacturer) + " " + model;
		}
		if (withSerial) {
			if (android.os.Build.VERSION.SDK_INT >= 9) {
				deviceName += " (sn:" + Build.SERIAL + ")";
			}
		}
		return deviceName;
	}
	
	public static String extractAndroidVersion() {
		String ver = null;
		StringBuilder builder = new StringBuilder();
		builder.append("Android ").append(Build.VERSION.RELEASE);

		Field[] fields = Build.VERSION_CODES.class.getFields();
		for (Field field : fields) {
			String fieldName = field.getName();
			int fieldValue = -1;

			try {
				fieldValue = field.getInt(new Object());
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (NullPointerException e) {
				e.printStackTrace();
			}

			if (fieldValue == Build.VERSION.SDK_INT) {
				builder.append(" ");
				builder.append(fieldName);
				builder.append(" ");
				builder.append("SDK#").append(fieldValue);
			}
		}

		ver = builder.toString();
		builder = null;
		return ver;
	}
	
	/**
	 * 
	 * @param e
	 * @return
	 * @throws Exception 
	 */
	public synchronized static void createErrorLog(Context context, Exception e) {
		StringBuffer errorLog = new StringBuffer();

		// device name and how many sensors are available
		errorLog.append("GTL on " + extractDeviceName(true) + " crashed\n\r ");
		// android version
		String androidVer = extractAndroidVersion();
		if (!androidVer.equals(""))
			errorLog.append(extractAndroidVersion() + " \n\r");
		errorLog.append("\n\r");
		errorLog.append("Stack Trace\n\r ");
		
		errorLog.append(e.getMessage() + "\r\n");
		StackTraceElement[] trace = e.getStackTrace();
		if (trace != null) {
			for (int ix = 0; ix < trace.length; ix++) {
				StackTraceElement ste = trace[ix];
				String line = ste.getFileName() + " " + ste.getClassName() + " " + ste.getMethodName() + " " + ste.getLineNumber()+ "\r\n"; 
				errorLog.append(line); 
			}
			Date time = new Date();
        	String timeStamp = CommonLogger.calculateFileNameStamp();
			String fileName = CommonLogger.BASE_FILE_NAME + "E-" + timeStamp + ".txt";
			if (context != null) {
				SDCardManager.createFileForStringBuffer(context, fileName, errorLog);
			}	
			errorLog = null;
		}
	}


}
