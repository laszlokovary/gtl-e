package com.lkovari.mobile.apps.gtl.utils;

/*-
 * 01/10/2012
 */
public enum MeasurementKind {
	LENGTH,
	SPEED;
}
