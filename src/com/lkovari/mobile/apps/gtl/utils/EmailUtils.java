package com.lkovari.mobile.apps.gtl.utils;

import com.lkovari.mobile.apps.gtl.R;
import com.lkovari.mobile.apps.gtl.settings.GPSSettings;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.URLSpan;
import android.widget.Toast;

/**
 * 18/06/2014 #9 v1.02.6
 * @author lkovari
 *
 */
public class EmailUtils {

	public static void sendTrackingPageInEmail(Activity activity, String recipient) {
	    String url = "http://www.eklsofttrade.com/gtl/gtlfind.php?phoneid="+GPSSettings.DEVICE_ID;
	    SpannableStringBuilder builder = new SpannableStringBuilder();
	    String welcome = activity.getString(R.string.followme_email_welcome);
	    String subject = activity.getString(R.string.followme_email_subject);
	    String mess = activity.getString(R.string.followme_email_message);
	    builder.append(welcome+"\n\n"+mess+"\n");
	    int start = builder.length();
	    builder.append(url);
	    int end = builder.length();
	    builder.setSpan(new URLSpan(url), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		
		Intent intent = new Intent(Intent.ACTION_SEND);
		intent.setType("message/rfc822");
		intent.putExtra(Intent.EXTRA_EMAIL  , new String[] {recipient});
		intent.putExtra(Intent.EXTRA_SUBJECT, subject);
		intent.putExtra(Intent.EXTRA_TEXT   , builder);
		try {
		    activity.startActivity(Intent.createChooser(intent, "Send mail..."));
		} 
		catch (ActivityNotFoundException ex) {
		    Toast.makeText(activity, R.string.followme_error_no_mail_client, Toast.LENGTH_SHORT).show();
		}
	}
	
	public static void sendEmailToTheDeveloper(Activity activity, String subject, String body) {
		Intent intent = new Intent(Intent.ACTION_SEND);
		intent.setType("message/rfc822");
		intent.putExtra(Intent.EXTRA_EMAIL  , new String[] {"info@eklsofttrade.com"});
		intent.putExtra(Intent.EXTRA_SUBJECT, subject);
		intent.putExtra(Intent.EXTRA_TEXT   , body);
		try {
		    activity.startActivity(Intent.createChooser(intent, "Send mail..."));
		} 
		catch (ActivityNotFoundException ex) {
		    Toast.makeText(activity, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
		}
	}
	
}
