package com.lkovari.mobile.apps.gtl.utils;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

import android.content.Context;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;

/**
 * 
 * @author lkovari
 * 23/03/2014 #9 v1.02.6
 * http://profi.co/getting-a-unique-id-signature-for-every-android-device/
 */
public class DeviceID {
	private static String ID = null;
	public static Context context;

	// return a cached unique ID for each device
	public static String getID(Context context) {
		DeviceID.context = context;
		// generate a new ID
		if (ID == null) {
			ID = generateID();
		}	
		return ID; 
	}

	// generate a unique ID for each device
	// use available schemes if possible / generate a random signature instead
	private static String generateID() {

		// use the ANDROID_ID constant, generated at the first device boot
		String deviceId = Secure.getString(context.getContentResolver(), Secure.ANDROID_ID);

		// in case known problems are occured
		if ("9774d56d682e549c".equals(deviceId) || deviceId == null) {

			// get a unique deviceID like IMEI for GSM or ESN for CDMA phones
			// don't forget:
			// <uses-permission
			// android:name="android.permission.READ_PHONE_STATE" />
			deviceId = ((TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId();

			// if nothing else works, generate a random number
			if (deviceId == null) {

				Random tmpRand = new Random();
				deviceId = String.valueOf(tmpRand.nextLong());
			}

		}

		// any value is hashed to have consistent format
		return getHash(deviceId);
	}

	// generates a SHA-1 hash for any string
	public static String getHash(String stringToHash) {

		MessageDigest digest = null;
		try {
			digest = MessageDigest.getInstance("SHA-1");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}

		byte[] result = null;

		try {
			result = digest.digest(stringToHash.getBytes("UTF-8"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		StringBuilder sb = new StringBuilder();

		for (byte b : result) {
			sb.append(String.format("%02X", b));
		}

		String messageDigest = sb.toString();
		return messageDigest;
	}
}
