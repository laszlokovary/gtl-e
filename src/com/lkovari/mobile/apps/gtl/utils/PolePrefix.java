package com.lkovari.mobile.apps.gtl.utils;

import com.lkovari.mobile.apps.gtl.R;

import android.content.Context;

/**
 * 01/02/2013
 * @author lkovari
 *
 */
public class PolePrefix {
	private static final int POLE_N0 = 0; 
	private static final int POLE_N1 = 360; 
	private static final int POLE_NW = 315; 
	private static final int POLE_W = 270; 
	private static final int POLE_SW = 225; 
	private static final int POLE_S = 180; 
	private static final int POLE_SE = 135; 
	private static final int POLE_E = 90; 
	private static final int POLE_NE = 45; 
	
	/**
	 * 01/02/2013
	 * @param context
	 * @param degree
	 * @return
	 */
	public static String determinatePolePrefixByDegree(Context context, int degree) {
		String pole = "";
		// is North
		if ((degree >=  (POLE_N1 - 22.5)) && (degree <= POLE_N1)) {
			pole = context.getResources().getString(R.string.compass_pole_N);
		}
		else if ((degree >= (POLE_NW - 22.5)) && (degree < (POLE_NW + 22.5))) {
			pole = context.getResources().getString(R.string.compass_pole_NW);
		}
		else if ((degree >= (POLE_W - 22.5)) && (degree < (POLE_W + 22.5))) {
			pole = context.getResources().getString(R.string.compass_pole_W);
		}
		else if ((degree >= (POLE_SW - 22.5)) && (degree < (POLE_SW + 22.5))) {
			pole = context.getResources().getString(R.string.compass_pole_SW);
		}
		else if ((degree >= (POLE_S - 22.5)) && (degree < (POLE_S + 22.5))) {
			pole = context.getResources().getString(R.string.compass_pole_S);
		}
		else if ((degree >= (POLE_SE - 22.5)) && (degree < (POLE_SE + 22.5))) {
			pole = context.getResources().getString(R.string.compass_pole_SE);
		}
		else if ((degree >= (POLE_E - 22.5)) && (degree < (POLE_E + 22.5))) {
			pole = context.getResources().getString(R.string.compass_pole_E);
		}
		else if ((degree >= (POLE_NE - 22.5)) && (degree < (POLE_NE + 22.5))) {
			pole = context.getResources().getString(R.string.compass_pole_NE);
		}
		else if ((degree >= 0) && (degree < (POLE_NE - 22.5))) {
			pole = context.getResources().getString(R.string.compass_pole_N);
		}
		
		return pole;
	}

}
