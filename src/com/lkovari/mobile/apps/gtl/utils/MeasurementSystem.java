package com.lkovari.mobile.apps.gtl.utils;

/**
 * 12/10/2012
 * @author lkovari
 *
 */
public enum MeasurementSystem {
	METRIC,
	IMPERIAL,
	ICAO;
}
