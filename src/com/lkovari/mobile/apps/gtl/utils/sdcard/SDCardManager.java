package com.lkovari.mobile.apps.gtl.utils.sdcard;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Level;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Environment;
import android.widget.Toast;

import com.lkovari.mobile.apps.gtl.R;
import com.lkovari.mobile.apps.gtl.settings.GPSSettings;
import com.lkovari.mobile.apps.gtl.utils.ConnectionUtils;
import com.lkovari.mobile.apps.gtl.utils.Utils;
import com.lkovari.mobile.apps.gtl.utils.logger.CommonLogger;

/**
 * 
 * @author lkovari
 *
 */
public class SDCardManager {
	private static String fileExt = null;
	private static final int TIMEOUT_CONNECTION = 5000;
	private static final int TIMEOUT_SOCKET = 30000;

	public static StringBuffer log = new StringBuffer();
	private static int cnt = 0;
	public static boolean isStoreDebugLog = false;
	
	public static void debug(String text) {
		if (isStoreDebugLog) {
			cnt++;
			log.append("> " + cnt + ". " + text + "\n\r");
		}
	}
	
	/**
	 * #5 10/06/2013
	 * @param context
	 * @param fileExt
	 */
	public static void copyfilesToRightFolder(Context context, String fileExt) {
		List<File> foundMapFiles = SDCardManager.findFiles(Environment.getExternalStorageDirectory().getPath() + "/" , fileExt);
		if (foundMapFiles.size() > 0) {
			String destDir = null;
			if (fileExt.equals(".map")) {
				destDir = Environment.getExternalStorageDirectory().getPath() + "/" + GPSSettings.GTL_MAPS_FOLDER + "/";
	    		Toast.makeText(context, R.string.warningr_copy_map_files, Toast.LENGTH_LONG).show();
			}	
			else {
				switch (GPSSettings.trackFormat) {
				case GPX : {
					break;
				}
				case GRM : {
					break;
				}
				case KML : {
					if (fileExt.equals(".kml")) {
						destDir = Environment.getExternalStorageDirectory().getPath() + "/" + GPSSettings.GTL_TRACKLOG_FOLDER + "/";
					}
				}
				}
	    		Toast.makeText(context, R.string.warningr_copy_tracklog_files, Toast.LENGTH_LONG).show();
			}
			for (File fl : foundMapFiles) {
				String sourceFile = fl.getAbsolutePath();
		        if (sourceFile.endsWith(fileExt)) {
		        	String[] fragments = sourceFile.split("/");
		        	String sourceFileName = fragments[fragments.length - 1];
			        File destFile = new File(destDir +  "/"  + sourceFileName);
			        try {
						SDCardManager.copyFile(fl, destFile);
						if (!fl.delete()) {
			        		Toast.makeText(context, R.string.error_cant_delete_map_files + "\n"+fl.getAbsolutePath(), Toast.LENGTH_LONG).show();
						}
					} catch (IOException e) {
		        		Toast.makeText(context, R.string.error_cant_copy_map_files_to_gtltracklogsmaps_folder+ "\n"+fl.getAbsolutePath(), Toast.LENGTH_LONG).show();
					}
		        }    
			}
		}
		
	}
	
	/**
	 * #6 11/26/2013
	 * @param context
	 * @param state
	 * @param isShowMessage
	 */
	public static void checkMediaState(Context context, String state, boolean isShowMessage) {
		String mess = null;
        if (Environment.MEDIA_MOUNTED.equals(state)) {
        	// nothing to do
        } 
        else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
    		if (isShowMessage)
    			mess =  context.getResources().getString(R.string.error_externalstorage_mounted_read_only);;
        }
        else if (Environment.MEDIA_REMOVED.equals(state)) {
    		if (isShowMessage)
    			mess = context.getResources().getString(R.string.error_externalstorage_not_present);
        } 
        else if (Environment.MEDIA_UNMOUNTABLE.equals(state)){
    		if (isShowMessage)
    			mess = context.getResources().getString(R.string.error_externalstorage_media_unmountable);
        }	    
        else if (Environment.MEDIA_BAD_REMOVAL.equals(state)){
        	mess = context.getResources().getString(R.string.error_externalstorage_media_bad_removal);
        }
        else if (Environment.MEDIA_NOFS.equals(state)){
        	mess = context.getResources().getString(R.string.error_externalstorage_nofs);
        }
        else if (Environment.MEDIA_UNMOUNTED.equals(state)){
        	mess = context.getResources().getString(R.string.error_externalstorage_media_unmounted);
        }
		if (isShowMessage && (mess != null))
			Toast.makeText(context, mess, Toast.LENGTH_LONG).show();
	}
	
	/**
	 * #5 10/06/2013
	 * @param context
	 * @param isShowMessage
	 * @return
	 */
	public static String checkExternalStorageAvailable(Context context, boolean isShowMessage) {
		String mess = null;
		if (context == null)
			isShowMessage = false;
        String state = Environment.getExternalStorageState();
        checkMediaState(context, state, isShowMessage);
	    return state;
	}	
	
	/**
	 * 01/10/2013 
	 * @param urlText
	 * @return
	 * @throws Exception
	public static File downloadFileToSDCard(ProgressDialog progressBar, String urlText, String folder) throws Exception {
		File file = null;
		// #5 10/01/2013 
		String state = checkExternalStorageAvailable(progressBar.getContext(), true);
		if (Environment.MEDIA_MOUNTED.equals(state)) {
		    File root = Environment.getExternalStorageDirectory();
		    if (root.canWrite()) {

		        String fileName = null;
		        if (urlText.endsWith(".map")) {
		        	
		        	String[] fragments = urlText.split("/");
		        	fileName = fragments[fragments.length - 1];
			        file = new File(root.getPath() + "/" + folder + "/" + fileName);

			        URL url = new URL(urlText);

			        //Open a connection to that URL.
			        URLConnection urlConnection = url.openConnection();

			        int fileSize = 0;
			        if (progressBar != null) {
				        urlConnection.connect();
				        fileSize = urlConnection.getContentLength();
			        }
			        
			        //this timeout affects how long it takes for the app to realize there's a connection problem
			        urlConnection.setReadTimeout(TIMEOUT_CONNECTION);
			        urlConnection.setConnectTimeout(TIMEOUT_SOCKET);


			        //Define InputStreams to read from the URLConnection.
			        // uses 3KB download buffer
			        InputStream is = urlConnection.getInputStream();
			        BufferedInputStream inStream = new BufferedInputStream(is, 1024 * 5);
			        FileOutputStream outStream = new FileOutputStream(file);
			        byte[] buff = new byte[5 * 1024];

			        if (progressBar != null) {
						progressBar.setProgress(0);
						progressBar.setMax(fileSize / 1014);		        
			        }
					
			        //Read bytes (and store them) until there is nothing more to read(-1)
			        int len;
			        while ((len = inStream.read(buff)) != -1)  {
			            outStream.write(buff,0,len);
				        if (progressBar != null) {
				        	progressBar.setProgress(len / 1024);
				        }	
			        }

			        //clean up
			        outStream.flush();
			        outStream.close();
			        inStream.close();
		        }
		    }
		}
		return file;
	}
	*/
	
	/**
	 * 12/30/2012
	 * @return
	 */
	public static boolean isMapFileExists() {
		boolean isMapFileExists = false;
		// #5 10/06/2013 
		String state = checkExternalStorageAvailable(null, false);
		if (state.equals(Environment.MEDIA_MOUNTED)) {
			// #5 10/06/2013
			List<File> foundMapFiles = SDCardManager.findFiles(Environment.getExternalStorageDirectory().getPath() + "/" + GPSSettings.GTL_MAPS_FOLDER + "/", ".map");
			if (foundMapFiles.size() > 0) {
				// first map file
				File mapFile = foundMapFiles.get(0);
				isMapFileExists = (mapFile != null) && mapFile.exists();
				mapFile = null;
			}
			foundMapFiles.clear();
			foundMapFiles = null;
		}
		return isMapFileExists;
	}
	
	/**
	 * 12/30/2012
	 * @param folderName
	 * @return
	 */
	public static boolean createFolderOnSDCard(String folderName) {
		boolean isSuccess = false;
		try {
			// #5 10/06/2013
			String state = checkExternalStorageAvailable(null, false);
			if (state.equals(Environment.MEDIA_MOUNTED)) {
			    File root = Environment.getExternalStorageDirectory();
			    if (root.canWrite()) {
					File folder = new File(root.getPath() + "/"  + folderName + "/");
					if (!folder.exists()) {
						isSuccess = folder.mkdirs();	
					}	
					else {
						isSuccess = true;
					}	
			    }
			}
		}
		catch (Exception e) {
			isSuccess = false;
		}
		return isSuccess;
	}
	
	/**
	 * #5 10/06/2013
	 * @param src
	 * @param dst
	 * @throws IOException
	 */
	public static void copyFile(File src, File dst) throws IOException {
    	BufferedInputStream buffInStream = null;
    	try {
    		buffInStream = new BufferedInputStream(new FileInputStream(src));
    	    BufferedOutputStream buffOutStream = null;
    	    try {
    	    	buffOutStream = new BufferedOutputStream(new FileOutputStream(dst, false));
    	        byte[] buffer = new byte[1024];
    	        int len;
    	        while ((len = buffInStream.read(buffer)) > 0) {
    	        	buffOutStream.write(buffer, 0, len);
    	        }
    	    }
    	    finally {
    	        if (buffOutStream != null) { 
    	        	buffOutStream.close();
    	        }	
    	    }
    	} finally {
    	    if (buffInStream != null) {
    	    	buffInStream.close();
    	    }	
    	}	    	
	}
	
	/**
	 * 12/29/2012
	 * @param directory
	 * @param ext
	 * @return
	 */
	public static List<File> findFiles(String directory, String ext) {
        List<File> foundFiles = new ArrayList<File>();
        fileExt = ext;
        File dir = new File(directory);
        File[] fileMatches = dir.listFiles(new FilenameFilter() {
          public boolean accept(File dir, String name)  {
             return name.endsWith(fileExt);
          }
        });
        // add to list
        if (fileMatches != null) {
            for (File f : fileMatches) {
                foundFiles.add(f);
            }
            // order
            Collections.sort(foundFiles, new Comparator<File>() {
                @Override
                public int compare(File lhs, File rhs) {
                    int res = lhs.getName().compareTo(rhs.getName());
                    return res;
                }
                
            });
        }
        return foundFiles;
	}

	/**
	 * 01/13/2013
	 * @param path
	 * @return
	 */
	public static List<File> findFile(String path) {
        List<File> foundFiles = new ArrayList<File>();
        File file = new File(path);
        if (file.exists()) {
        	foundFiles.add(file);
        }
        return foundFiles;
	}
	
	/**
	 * 
	 * @param fileName
	 * @return
	 */
	public static boolean isFileExists(String fileName) {
		boolean isFileExists = false;
		// #5 10/01/2013
		String state = checkExternalStorageAvailable(null,false);
		if (Environment.MEDIA_MOUNTED.equals(state)) {
		    File root = Environment.getExternalStorageDirectory();
		    if (root.canRead()) {
		        File file = new File(root, fileName);
		        isFileExists = file.exists();
		        file = null;
		    }
		}
	    return isFileExists;
	}
	
	/**
	 * 10/29/2012
	 * @param context
	 * @param fileName
	 * @param content
	 */
	public static String createFileForStringBuffer(Context context, String fileName, StringBuffer content) {
		// 11/25/2012
		String filePath = null;
		try {
			// #5 10/01/2013
			String state = checkExternalStorageAvailable(context, true);
			if (Environment.MEDIA_MOUNTED.equals(state)) {
				File root = Environment.getExternalStorageDirectory();
			    if (root.canWrite()) {
			    	// #5 10/06/2013
			        File file = null;
			    	File folderDir = new File(Environment.getExternalStorageDirectory() + "/" + GPSSettings.GTL_TRACKLOG_FOLDER);
			    	if (!folderDir.exists()) {
			    		if (folderDir.mkdir()) {
					        file = new File(folderDir.getAbsolutePath() + "/" + fileName);
			    		}
			    		else {
					        throw new Exception("Directory can\'t created " + folderDir.getAbsolutePath());
			    		}
			    	}
			    	else {
				        file = new File(folderDir.getAbsolutePath() + "/" + fileName);
			    	}
			        FileWriter fileWriter = new FileWriter(file);
			        BufferedWriter bufferWriter = new BufferedWriter(fileWriter);
			        bufferWriter.write(content.toString());
			        bufferWriter.flush();
			        bufferWriter.close();
					// 11/25/2012
			        filePath = file.getAbsolutePath();
			    }
				else {
					Toast.makeText(context, "ERROR! SDCard media is read only!", Toast.LENGTH_LONG).show();
				}
			}
			else {
				Toast.makeText(context, "ERROR! SDCard media is not mounted!", Toast.LENGTH_LONG).show();
			}
		} catch (Exception e) {
    		e.printStackTrace();
            CommonLogger.log(Level.SEVERE, e.getMessage());
			Toast.makeText(context, "ERROR in createFile " + e.getMessage(), Toast.LENGTH_LONG).show();
			// 11/25/2012
			filePath = null;
		}	
		// 11/25/2012
		return filePath;
	}

	/**
	 * #1/10/2014 #10 v1.02.7
	 * @param context Context
	 * @return String - absolute path
	 */
	public static String composeAbsolutePath(Context context) {
		String absolutePath = null;
		try {
			// #5 10/01/2013
			String state = checkExternalStorageAvailable(context, true);
			if (Environment.MEDIA_MOUNTED.equals(state)) {
				File root = Environment.getExternalStorageDirectory();
			    if (root.canWrite()) {
			    	File folderDir = new File(Environment.getExternalStorageDirectory() + "/" + GPSSettings.GTL_TRACKLOG_FOLDER);
			    	if (!folderDir.exists()) {
			    		if (folderDir.mkdir()) {
					        absolutePath = folderDir.getAbsolutePath();
			    		}
			    		else {
					        throw new Exception("Directory can\'t created " + folderDir.getAbsolutePath());
			    		}
			    	}
			    	else {
			    		absolutePath =  folderDir.getAbsolutePath();
			    	}
			    }
				else {
					Toast.makeText(context, "ERROR! SDCard media is read only!", Toast.LENGTH_LONG).show();
				}
			}
			else {
				Toast.makeText(context, "ERROR! SDCard media is not mounted!", Toast.LENGTH_LONG).show();
			}
		} catch (Exception e) {
    		e.printStackTrace();
            CommonLogger.log(Level.SEVERE, e.getMessage());
			Toast.makeText(context, "ERROR in createFile " + e.getMessage(), Toast.LENGTH_LONG).show();
		}	
		// 11/25/2012
		return absolutePath;
	}
	
	/**
	 * #1/10/2014 #10 v1.02.7
	 * @param context Contect
	 * @param path String - path of log files
	 * @param fileName String - log file name
	 * @param content StringBuffer - which want to store
	 * @return String - file path
	 */
	public static String createFileForStringBufferWithPath(Context context, String path, String fileName, StringBuffer content) {
		// 11/25/2012
		String filePath = null;
		try {
	    	File folderDir = new File(path);
	    	if (folderDir.exists()) {
	    		File file = new File(path + "/" + fileName);
		        FileWriter fileWriter = new FileWriter(file);
		        BufferedWriter bufferWriter = new BufferedWriter(fileWriter);
		        bufferWriter.write(content.toString());
		        bufferWriter.flush();
		        bufferWriter.close();
				// 11/25/2012
		        filePath = file.getAbsolutePath();
	    	}
		} catch (Exception e) {
    		e.printStackTrace();
            CommonLogger.log(Level.SEVERE, e.getMessage());
			Toast.makeText(context, "ERROR in createFile " + e.getMessage(), Toast.LENGTH_LONG).show();
			// 11/25/2012
			filePath = null;
		}	
		// 11/25/2012
		return filePath;
	}	
	/**
	 * 
	 * @param context
	 * @param fileName
	 * @param content
	 * @return
	 */
	public static String createFileForRoutePlan(Context context, String fileName, StringBuffer content) {
		String timeStamp = CommonLogger.calculateFileNameStamp();
		fileName += timeStamp + ".txt";
		return createFileForStringBuffer(context, fileName, content);
	}
	
	/**
	 * 10/29/2012
	 * @param context
	 * @param fileName
	 * @param content
	 */
	public static void createFileForString(Context context, String fileName, String content) {
		File storage = null;
		try {
			// #5 10/01/2013
			String state = checkExternalStorageAvailable(context, true);
			if (Environment.MEDIA_MOUNTED.equals(state)) {
				storage = Environment.getExternalStorageDirectory();
			    if (storage.canWrite()){
			        File file = new File(storage, fileName);
			        FileWriter fileWriter = new FileWriter(file);
			        BufferedWriter bufferWriter = new BufferedWriter(fileWriter);
			        bufferWriter.write(content);
			        bufferWriter.flush();
			        bufferWriter.close();
			    }
			}
			else {
				// #5 10/06/2013 try to use internal storage
				storage = context.getFilesDir();
				if (storage.canWrite()) {
			        File file = new File(storage, fileName);
			        FileWriter fileWriter = new FileWriter(file);
			        BufferedWriter bufferWriter = new BufferedWriter(fileWriter);
			        bufferWriter.write(content);
			        bufferWriter.flush();
			        bufferWriter.close();
				}
			}
		} catch (IOException e) {
    		e.printStackTrace();
            CommonLogger.log(Level.SEVERE, e.getMessage());
            if (context != null) {
            	Toast.makeText(context, "ERROR when createFile " + e.getMessage() + "\n" + storage.getAbsolutePath(), Toast.LENGTH_LONG).show();
            }	
		}		
	}

	/**
	 * #8 v1.02.502/15/2014
	 * @param context
	 * @param imei
	 * @return
	 */
	public static String saveImeiToFile(Context context, String imei) {
		String savePath = null;
		String fileName = CommonLogger.BASE_FILE_NAME + Build.MODEL +"_IMEI.txt";
		 StringBuffer text = new StringBuffer();
		 // device name and how many sensors are available
		 text.append(Utils.extractDeviceName(true) + "\n\r ");
		 // android version
		 String androidVer = Utils.extractAndroidVersion();
		 if (!androidVer.equals(""))
			 text.append(Utils.extractAndroidVersion() + " \n\r ");
		 text.append(" \n\r ");
		
		 try {
			String imeiMd5 = ConnectionUtils.md5(imei);
			 text.append("IMEI " + imei);
			 text.append(" \n\r ");
			 text.append("IMEI Md5 " + imeiMd5);
		 } 
		 catch (Exception e) {
			 text.append("IMEI Md5 -");
		 }
		 text.append(" \n\r ");
		if (context != null) {
			savePath = SDCardManager.createFileForStringBuffer(context, fileName, text);
			 if (savePath != null) {
				 Toast.makeText(context, "IME SAVEd to : "+savePath, Toast.LENGTH_LONG).show();
			 }
		}	
		text = null;
		return savePath;
	}

	/**
	 * #1/10/2014 #10 v1.02.7
	 * @param activity Activity
	 * @param ext String - file extension with dot
	 * @return List<String> - list of files which found with the specified ext
	 */
	public static List<String> loadLogFilesPath(Activity activity, String ext) {
		List<String> logFilePath = null;
		String storedPath = Utils.readLogFilePath(activity);
		if ((storedPath == null) || ((storedPath != null) && (storedPath.trim() == ""))) {
			// 03/07/2015 #10 try to compose path 
			storedPath = GPSSettings.GTL_TRACKLOG_FOLDER;
			storedPath = SDCardManager.composeAbsolutePath(activity.getApplicationContext());
    		if ((storedPath == null) || ((storedPath != null) && (storedPath.trim() == ""))) {
    			Utils.writeLogFilePath(activity, storedPath);
    			// re read it
    			storedPath = Utils.readLogFilePath(activity);
    		}
		}
		
		if (storedPath != null) {
			logFilePath = new ArrayList<String>();
			File dir = new File(storedPath);
			if (dir.exists()) {
				// 10/23/2015 #11 v1.02.8 just interested files based on TrackFormat
				File[] files = dir.listFiles(new FilenameFilter() {
				    @Override
				    public boolean accept(File dir, String name) {
				    	String extension = ".kml";
				    	switch (GPSSettings.trackFormat) {
				    	case KML : extension = ".kml"; break;
				    	case GPX : extension = ".gpx"; break;
				    	case GRM : extension = ".grm"; break;
				    	}
				        return name.endsWith(extension);
				    }
				});
				// 10/23/2015 #11 v1.02.8
				List<File> fileList = new ArrayList<File>(Arrays.asList(files));
				Collections.sort(fileList, new Comparator<File>() {
					@Override
					public int compare(File f1, File f2) {
						return Long.valueOf(f2.lastModified()).compareTo(f1.lastModified());					
					}
				});
				// move name into string array
				for (File f : fileList) {
//					if (f.getName().contains(ext)) {
						logFilePath.add(f.getName());
//					}
				}
				fileList.clear();
				fileList = null;
				files = null;
			}
		}
		else {
            Toast.makeText(activity.getApplicationContext(), R.string.error_no_stored_path_in_preference, Toast.LENGTH_LONG).show();
		}
		
		return logFilePath;
	}

	/**
	 * #1/10/2014 #10 v1.02.7
	 * @param activity Activity
	 * @param fileName String - file name
	 * @return String- 
	 * @throws Exception
	 */
	public static String extractKMLFolder(Activity activity) {
		String fullFilePath = null;
		String storedPath = Utils.readLogFilePath(activity);
		if ((storedPath != null) && (storedPath.trim() != "")) {
			fullFilePath = storedPath;
		}
		return fullFilePath;
	}
	
	
}
