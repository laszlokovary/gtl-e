package com.lkovari.mobile.apps.gtl.utils.mapsforge;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * 01/1062013
 * @author lkovari
 *
 */
public class MapsForgeDownloadManager {

	private static Map<String, String> mapsUrLByCountry = null;
	private static String baseUrl = "http://download.mapsforge.org/maps";
	private static MapsForgeDownloadManager instance = null;
	
	public MapsForgeDownloadManager() {
		if (mapsUrLByCountry == null) {
			mapsUrLByCountry = new HashMap<String, String>();

			// Asia
			mapsUrLByCountry.put("Azerbaijan", baseUrl + "/asia/azerbaijan.map");
			mapsUrLByCountry.put("China", baseUrl + "/asia/china.map");
			mapsUrLByCountry.put("Gaza", baseUrl + "/asia/gaza.map");
			mapsUrLByCountry.put("Gcc states", baseUrl + "/asia/gcc_states.map");
			mapsUrLByCountry.put("India", baseUrl + "/asia/india.map");
			mapsUrLByCountry.put("Indonesia", baseUrl + "/asia/indonesia.map");
			mapsUrLByCountry.put("Iran", baseUrl + "/asia/iran.map");
			mapsUrLByCountry.put("Iraq", baseUrl + "/asia/iraq.map");
			mapsUrLByCountry.put("Israel-Palestina", baseUrl + "/asia/israel_and_palestine.map");
			mapsUrLByCountry.put("Japan", baseUrl + "/asia/japan.map");
			mapsUrLByCountry.put("Kazahstan", baseUrl + "/asia/kazakhstan.map");
			mapsUrLByCountry.put("Kyrgyzstan", baseUrl + "/asia/kyrgyzstan.map");
			mapsUrLByCountry.put("Malaysia-Singapore-Brunei", baseUrl + "/asia/malaysia_singapore_brunei.map");
			mapsUrLByCountry.put("Mongolia", baseUrl + "/asia/mongolia.map");
			mapsUrLByCountry.put("Pakistan", baseUrl + "/asia/pakistan.map");
			mapsUrLByCountry.put("Philippines", baseUrl + "/asia/philippines.map");
			mapsUrLByCountry.put("Taiwan", baseUrl + "/asia/taiwan.map");
			mapsUrLByCountry.put("Turkmenistan", baseUrl + "/asia/turkmenistan.map");
			mapsUrLByCountry.put("Uzbegistan", baseUrl + "/asia/uzbekistan.map");
			mapsUrLByCountry.put("Vietnam", baseUrl + "/asia/vietnam.map");

			// Europe
			mapsUrLByCountry.put("EU Albania", "http://download.mapsforge.org/maps/europe/albania.map");
			mapsUrLByCountry.put("EU England", "http://download.mapsforge.org/maps/europe/great_britain/england.map");
			mapsUrLByCountry.put("EU Scotland", "http://download.mapsforge.org/maps/europe/great_britain/scotland.map");
			mapsUrLByCountry.put("EU Wales", "http://download.mapsforge.org/maps/europe/great_britain/wales.map");
			mapsUrLByCountry.put("EU Andorra", "http://download.mapsforge.org/maps/europe/andorra.map");
			mapsUrLByCountry.put("EU Austria", "http://download.mapsforge.org/maps/europe/austria.map");
			mapsUrLByCountry.put("EU Azores", "http://download.mapsforge.org/maps/europe/azores.map");
			mapsUrLByCountry.put("EU Belarus", "http://download.mapsforge.org/maps/europe/belarus.map");
			mapsUrLByCountry.put("EU Belgium", "http://download.mapsforge.org/maps/europe/belgium.map");
			mapsUrLByCountry.put("EU Bosnia-Herzegovina", "http://download.mapsforge.org/maps/europe/bosnia-herzegovina.map");
			mapsUrLByCountry.put("EU Bulgaria", "http://download.mapsforge.org/maps/europe/bulgaria.map");
			mapsUrLByCountry.put("EU Croatia", "http://download.mapsforge.org/maps/europe/croatia.map");
			mapsUrLByCountry.put("EU Cyprus", "http://download.mapsforge.org/maps/europe/cyprus.map");
			mapsUrLByCountry.put("EU Czech Republic", "http://download.mapsforge.org/maps/europe/czech_republic.map");
			mapsUrLByCountry.put("EU Denmark", "http://download.mapsforge.org/maps/europe/denmark.map");
			mapsUrLByCountry.put("EU Estonia", "http://download.mapsforge.org/maps/europe/estonia.map");
			mapsUrLByCountry.put("EU Faroe Island", "http://download.mapsforge.org/maps/europe/faroe_islands.map");
			mapsUrLByCountry.put("EU Finland", "http://download.mapsforge.org/maps/europe/finland.map");
			mapsUrLByCountry.put("EU Germany", "http://download.mapsforge.org/maps/europe/germany.map");
			mapsUrLByCountry.put("EU Greece", "http://download.mapsforge.org/maps/europe/greece.map");
			mapsUrLByCountry.put("EU Hungary", "http://download.mapsforge.org/maps/europe/hungary.map");
			mapsUrLByCountry.put("EU Iceland", "http://download.mapsforge.org/maps/europe/iceland.map");
			mapsUrLByCountry.put("EU Ireland", "http://download.mapsforge.org/maps/europe/ireland.map");
			mapsUrLByCountry.put("EU Isle of man", "http://download.mapsforge.org/maps/europe/isle_of_man.map");
			mapsUrLByCountry.put("EU Italy", "http://download.mapsforge.org/maps/europe/italy.map");
			mapsUrLByCountry.put("EU Kosovo", "http://download.mapsforge.org/maps/europe/kosovo.map");
			mapsUrLByCountry.put("EU Latvia", "http://download.mapsforge.org/maps/europe/latvia.map");
			mapsUrLByCountry.put("EU Liechtenstein", "http://download.mapsforge.org/maps/europe/liechtenstein.map");
			mapsUrLByCountry.put("EU Lithuania", "http://download.mapsforge.org/maps/europe/lithuania.map");
			mapsUrLByCountry.put("EU Luxembourg", "http://download.mapsforge.org/maps/europe/luxembourg.map");
			mapsUrLByCountry.put("EU Macedonia", "http://download.mapsforge.org/maps/europe/macedonia.map");
			mapsUrLByCountry.put("EU Madeira", "http://download.mapsforge.org/maps/europe/madeira.map");
			mapsUrLByCountry.put("EU Malta", "http://download.mapsforge.org/maps/europe/malta.map");
			mapsUrLByCountry.put("EU Moldova", "http://download.mapsforge.org/maps/europe/moldova.map");
			mapsUrLByCountry.put("EU Monaco", "http://download.mapsforge.org/maps/europe/monaco.map");
			mapsUrLByCountry.put("EU Montenegro", "http://download.mapsforge.org/maps/europe/montenegro.map");
			mapsUrLByCountry.put("EU Netherlands", "http://download.mapsforge.org/maps/europe/netherlands.map");
			mapsUrLByCountry.put("EU Norway", "http://download.mapsforge.org/maps/europe/norway.map");
			mapsUrLByCountry.put("EU Poland", "http://download.mapsforge.org/maps/europe/poland.map");
			mapsUrLByCountry.put("EU Portugal", "http://download.mapsforge.org/maps/europe/portugal.map");
			mapsUrLByCountry.put("EU Romania", "http://download.mapsforge.org/maps/europe/romania.map");
			mapsUrLByCountry.put("EU Russia", "http://download.mapsforge.org/maps/europe/russia.map");
			mapsUrLByCountry.put("EU Serbia", "http://download.mapsforge.org/maps/europe/serbia.map");
			mapsUrLByCountry.put("EU Slovakia", "http://download.mapsforge.org/maps/europe/slovakia.map");
			mapsUrLByCountry.put("EU Slovenia", "http://download.mapsforge.org/maps/europe/slovenia.map");
			mapsUrLByCountry.put("EU Spain", "http://download.mapsforge.org/maps/europe/spain.map");
			mapsUrLByCountry.put("EU Sweden", "http://download.mapsforge.org/maps/europe/sweden.map");
			mapsUrLByCountry.put("EU Switzerland", "http://download.mapsforge.org/maps/europe/switzerland.map");
			mapsUrLByCountry.put("EU Turkey", "http://download.mapsforge.org/maps/europe/turkey.map");
			mapsUrLByCountry.put("EU Ukraine", "http://download.mapsforge.org/maps/europe/ukraine.map");
			
			// North America US
			mapsUrLByCountry.put("US Alabama", "http://download.mapsforge.org/maps/north-america/us/alabama.map");
			mapsUrLByCountry.put("US Arizona", "http://download.mapsforge.org/maps/north-america/us/arizona.map");
			mapsUrLByCountry.put("US Arkansas", "http://download.mapsforge.org/maps/north-america/us/arkansas.map");
			mapsUrLByCountry.put("US California", "http://download.mapsforge.org/maps/north-america/us/california.map");
			mapsUrLByCountry.put("US Colorado", "http://download.mapsforge.org/maps/north-america/us/colorado.map");
			mapsUrLByCountry.put("US Delaware", "http://download.mapsforge.org/maps/north-america/us/delaware.map");
			mapsUrLByCountry.put("US Columbia", "http://download.mapsforge.org/maps/north-america/us/district-of-columbia.map");
			mapsUrLByCountry.put("US Florida", "http://download.mapsforge.org/maps/north-america/us/florida.map");
			mapsUrLByCountry.put("US Georgia", "http://download.mapsforge.org/maps/north-america/us/georgia.map");
			mapsUrLByCountry.put("US Hawaii", "http://download.mapsforge.org/maps/north-america/us/hawaii.map");
			mapsUrLByCountry.put("US Idaho", "http://download.mapsforge.org/maps/north-america/us/idaho.map");
			mapsUrLByCountry.put("US Ilinois", "http://download.mapsforge.org/maps/north-america/us/illinois.map");
			mapsUrLByCountry.put("US Indiana", "http://download.mapsforge.org/maps/north-america/us/indiana.map");
			mapsUrLByCountry.put("US Iowa", "http://download.mapsforge.org/maps/north-america/us/iowa.map");
			mapsUrLByCountry.put("US Kansas", "http://download.mapsforge.org/maps/north-america/us/kansas.map");
			mapsUrLByCountry.put("US Kentucky", "http://download.mapsforge.org/maps/north-america/us/kentucky.map");
			mapsUrLByCountry.put("US Louisiana", "http://download.mapsforge.org/maps/north-america/us/louisiana.map");
			mapsUrLByCountry.put("US Maine", "http://download.mapsforge.org/maps/north-america/us/maine.map");
			mapsUrLByCountry.put("US Maryland", "http://download.mapsforge.org/maps/north-america/us/maryland.map");
			mapsUrLByCountry.put("US Massachusetts", "http://download.mapsforge.org/maps/north-america/us/massachusetts.map");
			mapsUrLByCountry.put("US Michigan", "http://download.mapsforge.org/maps/north-america/us/michigan.map");
			mapsUrLByCountry.put("US Minnesota", "http://download.mapsforge.org/maps/north-america/us/minnesota.map");
			mapsUrLByCountry.put("US Mississippi", "http://download.mapsforge.org/maps/north-america/us/mississippi.map");
			mapsUrLByCountry.put("US Missouri", "http://download.mapsforge.org/maps/north-america/us/missouri.map");
			mapsUrLByCountry.put("US Montana", "http://download.mapsforge.org/maps/north-america/us/montana.map");
			mapsUrLByCountry.put("US Nebraska", "http://download.mapsforge.org/maps/north-america/us/nebraska.map");
			mapsUrLByCountry.put("US Nevada", "http://download.mapsforge.org/maps/north-america/us/nevada.map");
			mapsUrLByCountry.put("US New Hampshire", "http://download.mapsforge.org/maps/north-america/us/new-hampshire.map");
			mapsUrLByCountry.put("US New Jersey", "http://download.mapsforge.org/maps/north-america/us/new-jersey.map");
			mapsUrLByCountry.put("US New Mexico", "http://download.mapsforge.org/maps/north-america/us/new-mexico.map");
			mapsUrLByCountry.put("US New York", "http://download.mapsforge.org/maps/north-america/us/new-york.map");
			mapsUrLByCountry.put("US North Carolina", "http://download.mapsforge.org/maps/north-america/us/north-carolina.map");
			mapsUrLByCountry.put("US North Dakota", "http://download.mapsforge.org/maps/north-america/us/north-dakota.map");
			mapsUrLByCountry.put("US Ohio", "http://download.mapsforge.org/maps/north-america/us/ohio.map");
			mapsUrLByCountry.put("US Oklahoma", "http://download.mapsforge.org/maps/north-america/us/oklahoma.map");
			mapsUrLByCountry.put("US Oregon", "http://download.mapsforge.org/maps/north-america/us/oregon.map");
			mapsUrLByCountry.put("US Pensylvania", "http://download.mapsforge.org/maps/north-america/us/pennsylvania.map");
			mapsUrLByCountry.put("US Rhode Island", "http://download.mapsforge.org/maps/north-america/us/rhode-island.map");
			mapsUrLByCountry.put("US South Carolina", "http://download.mapsforge.org/maps/north-america/us/south-carolina.map");
			mapsUrLByCountry.put("US Sourh Dakota", "http://download.mapsforge.org/maps/north-america/us/south-dakota.map");
			mapsUrLByCountry.put("US Tennessee", "http://download.mapsforge.org/maps/north-america/us/tennessee.map");
			mapsUrLByCountry.put("US Texas", "http://download.mapsforge.org/maps/north-america/us/texas.map");
			mapsUrLByCountry.put("US Pacific", "http://download.mapsforge.org/maps/north-america/us/us-pacific.map");
			mapsUrLByCountry.put("US Utah", "http://download.mapsforge.org/maps/north-america/us/utah.map");
			mapsUrLByCountry.put("US Vermont", "http://download.mapsforge.org/maps/north-america/us/vermont.map");
			mapsUrLByCountry.put("US Virginia", "http://download.mapsforge.org/maps/north-america/us/virginia.map");
			mapsUrLByCountry.put("US Washington", "http://download.mapsforge.org/maps/north-america/us/washington.map");
			mapsUrLByCountry.put("US West Virginia", "http://download.mapsforge.org/maps/north-america/us/west-virginia.map");
			mapsUrLByCountry.put("US Visconsin", "http://download.mapsforge.org/maps/north-america/us/wisconsin.map");
			mapsUrLByCountry.put("US Wyoming", "http://download.mapsforge.org/maps/north-america/us/wyoming.map");

			// North America Canada
			mapsUrLByCountry.put("CA Alberta", "http://download.mapsforge.org/maps/north-america/canada/alberta.map");
			mapsUrLByCountry.put("CA British Columbia", "http://download.mapsforge.org/maps/north-america/canada/british-columbia.map");
			mapsUrLByCountry.put("CA Manitoba", "http://download.mapsforge.org/maps/north-america/canada/manitoba.map");
			mapsUrLByCountry.put("CA New Brunswick", "http://download.mapsforge.org/maps/north-america/canada/new-brunswick.map");
			mapsUrLByCountry.put("CA New Foundland Labrador", "http://download.mapsforge.org/maps/north-america/canada/newfoundland-and-labrador.map");
			mapsUrLByCountry.put("CA North-West Territories", "http://download.mapsforge.org/maps/north-america/canada/northwest-territories.map");
			mapsUrLByCountry.put("CA Nova Scotia", "http://download.mapsforge.org/maps/north-america/canada/nova-scotia.map");
			mapsUrLByCountry.put("CA Nunavut", "http://download.mapsforge.org/maps/north-america/canada/nunavut.map");
			mapsUrLByCountry.put("CA Ontario", "http://download.mapsforge.org/maps/north-america/canada/ontario.map");
			mapsUrLByCountry.put("CA Prince Edward Island", "http://download.mapsforge.org/maps/north-america/canada/prince-edward-island.map");
			mapsUrLByCountry.put("CA Quebec", "http://download.mapsforge.org/maps/north-america/canada/quebec.map");
			mapsUrLByCountry.put("CA Saskatchewan", "http://download.mapsforge.org/maps/north-america/canada/saskatchewan.map");
			mapsUrLByCountry.put("CA Yukon", "http://download.mapsforge.org/maps/north-america/canada/yukon.map");
			
			// South America
			mapsUrLByCountry.put("Argentina", "http://download.mapsforge.org/maps/south-america/argentina.map");
			mapsUrLByCountry.put("Bolivia", "http://download.mapsforge.org/maps/south-america/bolivia.map");
			mapsUrLByCountry.put("Brazil", "http://download.mapsforge.org/maps/south-america/brazil.map");
			mapsUrLByCountry.put("Chile", "http://download.mapsforge.org/maps/south-america/chile.map");
			mapsUrLByCountry.put("Columbia", "http://download.mapsforge.org/maps/south-america/colombia.map");
			mapsUrLByCountry.put("Ecuador", "http://download.mapsforge.org/maps/south-america/ecuador.map");
			
			// Africa
			mapsUrLByCountry.put("Africa", "http://download.mapsforge.org/maps/africa.map");
			
			// Australia
			mapsUrLByCountry.put("Australia Oceania", "http://download.mapsforge.org/maps/australia-oceania.map");
			
			// Central America
			mapsUrLByCountry.put("Central America", "http://download.mapsforge.org/maps/central-america.map");
		}	
	}
		
	public static MapsForgeDownloadManager getInstance() {
		if (instance == null) {
			instance = new MapsForgeDownloadManager();
		}	
		return instance;
	}
	
	
	public String key2Url(String key) {
		return (mapsUrLByCountry != null) ? mapsUrLByCountry.get(key) : null;
	}
	
	public Set<String> mapSet() {
		return mapsUrLByCountry.keySet();
	}
	
}
