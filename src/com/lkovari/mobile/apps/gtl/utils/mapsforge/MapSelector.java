package com.lkovari.mobile.apps.gtl.utils.mapsforge;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.lkovari.mobile.apps.gtl.Main;
import com.lkovari.mobile.apps.gtl.R;
import com.lkovari.mobile.apps.gtl.settings.GPSSettings;
import com.lkovari.mobile.apps.gtl.utils.Utils;
import com.lkovari.mobile.apps.gtl.utils.sdcard.SDCardManager;

/**
 * 01/11/2013
 * @author lkovari
 *
 */
public class MapSelector extends Activity {
	private static final int TIMEOUT_CONNECTION = 5000;
	private static final int TIMEOUT_SOCKET = 30000;
	// 02/10/2013 10kb
	private static int MAP_DOWNLOAD_BUFFER_SIZE = 100 * 1024;
	public static final int MAP_SELECTOR_ID_NUM = 1965020201;
	public static int MAP_FILE_SELECTED = 0;
	public static int MAP_SELECTOR_OK = Activity.RESULT_CANCELED;
	public static String MAP_SELECTOR_ID = MapSelector.class.getSimpleName();
	
	private ListView mapsListView;
	private ProgressDialog progressBar;
	private Handler progressBarHandler = new Handler();
	private String mapUrl;
	private boolean isDownloadSuccess = false;
	private int downloadedSize;
	private View viewWhenClick;
	private String selectedCountryOfMap;
	private boolean isDownloadMode = true;
	// #6 11/26/2013
	private Activity mainActivity = null;
	

    
	/**
	 * 02/10/2013
	 * @param isSelected
	 */
	private void setupResult(Boolean isSelected) {
		// 02/10/2013
		if (isSelected != null) {
			if (isSelected) {
				MAP_SELECTOR_OK = Activity.RESULT_OK;
				MAP_FILE_SELECTED = 1;
			}
			else {
				MAP_SELECTOR_OK = Activity.RESULT_CANCELED;
				MAP_FILE_SELECTED = 0;
			}
		}
		Intent resultIntent = new Intent();
		resultIntent.putExtra(MAP_SELECTOR_ID, MapSelector.MAP_FILE_SELECTED);
		setResult(MAP_SELECTOR_OK, resultIntent);		
	}
	
	public File downloadMapFileToSDCard(String urlText) throws Exception {
		File file = null;
        // #5 10/06/2013
		String state = SDCardManager.checkExternalStorageAvailable(getApplicationContext(), true);
        // #5 10/06/2013
		if (Environment.MEDIA_MOUNTED.equals(state)) {
		    File root = Environment.getExternalStorageDirectory();
		    if (root.canWrite()) {

		        String fileName = null;
		        if (urlText.endsWith(".map")) {
		        	
		        	String[] fragments = urlText.split("/");
		        	fileName = fragments[fragments.length - 1];
		        	// #5 10/06/2013
			        file = new File(root.getPath() + "/" + GPSSettings.GTL_MAPS_FOLDER + "/" + fileName);
			        // 02/10/2013
			        try {
				        URL url = new URL(urlText);

				        //Open a connection to that URL.
				        URLConnection urlConnection = url.openConnection();

				        int fileSize = 0;
				        if (progressBar != null) {
					        urlConnection.connect();
					        fileSize = urlConnection.getContentLength();
				        }
				        
				        //this timeout affects how long it takes for the app to realize there's a connection problem
				        urlConnection.setReadTimeout(TIMEOUT_CONNECTION);
				        urlConnection.setConnectTimeout(TIMEOUT_SOCKET);


				        //Define InputStreams to read from the URLConnection.
				        // uses 3KB download buffer
				        InputStream is = urlConnection.getInputStream();
				        BufferedInputStream inStream = new BufferedInputStream(is, MAP_DOWNLOAD_BUFFER_SIZE);
				        FileOutputStream outStream = new FileOutputStream(file);
				        byte[] buff = new byte[MAP_DOWNLOAD_BUFFER_SIZE];

				        if (progressBar != null) {
							progressBar.setProgress(0);
							progressBar.setMax(fileSize / 1014);		        
				        }
						
				        //Read bytes (and store them) until there is nothing more to read(-1)
				        int len;
				        downloadedSize = 0;
				        while ((len = inStream.read(buff)) != -1)  {
				            outStream.write(buff,0,len);
					        if (progressBar != null) {
					        	if (len > 0) {
					        		downloadedSize += len;
					        	}	
					            progressBarHandler.post(new Runnable() {
					            	public void run() {
					            		progressBar.setProgress((downloadedSize / 1024));
					            	}
					            });			        	
					        }	
				        }
				        //02/10/2013
				        MAP_FILE_SELECTED = 1;
				    	MAP_SELECTOR_OK = Activity.RESULT_OK;
				        //clean up
				        outStream.flush();
				        outStream.close();
				        inStream.close();
			        }
			        catch (Exception e) {
				        //02/10/2013
				    	MAP_FILE_SELECTED = 0;
				    	MAP_SELECTOR_OK = Activity.RESULT_CANCELED;
			        }
		        }
		    }
		    else {
		        //02/10/2013
		    	MAP_FILE_SELECTED = 0;
		    	MAP_SELECTOR_OK = Activity.RESULT_CANCELED;
		    }
		}
		// 01/12/2012
		progressBar.dismiss();
        //02/10/2013 null means use values in MAP_SELECTOR_OK and MAP_FILE_SELECTED
		setupResult(null);
		// return to previous activity
		finish();
		return file;
	}
	
	/**
	 * 08/14/2016 #17 v1.02.14 
	 */
    private void dismissProgressDialog() {
        if (progressBar != null && progressBar.isShowing()) {
        	progressBar.dismiss();
        }
    }	
	
	@Override
	protected void onDestroy() {
		// 08/14/2016 #17 v1.02.14 
		dismissProgressDialog();
		super.onDestroy();
	}
	

    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.mapselector);
		// 08/14/2016 #17 v1.02.14 
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		
		GPSSettings.DEFAUL_MAPSFORGE_MAP = null;
		// 01/13/2013 get the display or selection mode
		Long ind1 = getIntent().getExtras().getLong(Main.BUNDLE_CONTENT_TAG_DATA1);
		isDownloadMode = (ind1 == 1);

		mapsListView = (ListView) findViewById( R.id.mapsListView);
		
		// #6 11/26/2013
		this.mainActivity = this;
		
		// 01/13/2013
		String[] mapnameArray = null;
		boolean isMapNamesOk = false;
		if (isDownloadMode) {
			// download selected map mode
			mapnameArray = new String[MapsForgeDownloadManager.getInstance().mapSet().size()];
			int ix = 0;
			for (String mapName : MapsForgeDownloadManager.getInstance().mapSet()) {
				mapnameArray[ix] = mapName;
				ix++;
			}
			
			// sort array
			Arrays.sort(mapnameArray, new Comparator<String>() {
			    @Override
			    public int compare(String entry1, String entry2) {
			        return entry1.compareTo(entry2);
			    }
			});		
			
			isMapNamesOk = true;
		}
		else {
			// 01/13/2013 select available map mode
			TextView mapSelectorTitle = (TextView) findViewById(R.id.mapselector_titleTextView);
			String title = getResources().getString(R.string.mapselector_select_proper_map_to_use);
			mapSelectorTitle.setText(title);
			TextView mapSelectorSubtitle = (TextView) findViewById(R.id.mapselector_subtitleTextView);
			String subtitle = getResources().getString(R.string.mapselector_after_select_to_use);
			mapSelectorSubtitle.setText(subtitle);
			
	        // #5 10/06/2013
			String state = SDCardManager.checkExternalStorageAvailable(getApplicationContext(), true);
			if (Environment.MEDIA_MOUNTED.equals(state)) {
				List<File> foundMapFiles = SDCardManager.findFiles(Environment.getExternalStorageDirectory().getPath() + "/" + GPSSettings.GTL_MAPS_FOLDER + "/" , ".map");
				if (foundMapFiles.size() > 0) {
					if (foundMapFiles.size() > 1) {
						// more than one
						mapnameArray = new String[foundMapFiles.size()];
						int ix = 0;
						for (File file : foundMapFiles) {
							mapnameArray[ix] = file.getName();
							ix++;
						}
						isMapNamesOk = true;
					}
					else {
						// only one
						// #6 11/26/2013
						isMapNamesOk = true;
						selectedCountryOfMap = foundMapFiles.get(0).getName();
						// use selected map
						GPSSettings.DEFAUL_MAPSFORGE_MAP = selectedCountryOfMap;
						// #7 12/08/2013
						mapnameArray = new String[foundMapFiles.size()];
						mapnameArray[0] = selectedCountryOfMap;
						// # 11/26/2013
						Utils.writeLastUsedOfflineMap(mainActivity, selectedCountryOfMap);
					}
				}
				else {
					isMapNamesOk = false;
					Toast.makeText(this, R.string.error_offline_map_file_not_found , Toast.LENGTH_LONG).show();
				}
			}
			else {
				isMapNamesOk = false;
			}
		}

		if (isMapNamesOk) {
			//#7 12/08/2013
			if (mapnameArray != null) {
				// Binding resources Array to ListAdapter
				mapsListView.setAdapter(new ArrayAdapter<String>(this, R.layout.map_list_item, R.id.label, mapnameArray));
			}
		}
		mapsListView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> listView, View view, int position, long arg3) {
				Object o = listView.getItemAtPosition(position);
				if (o != null) {
					if (o instanceof String) {
						selectedCountryOfMap = (String)o;
						if (isDownloadMode) {
							mapUrl = MapsForgeDownloadManager.getInstance().key2Url(selectedCountryOfMap);
							try {
//								Toast.makeText(context, R.string.mapselector_download_started, Toast.LENGTH_LONG).show();

								viewWhenClick = view;							
								String mess = getApplicationContext().getString(R.string.mapselector_download_map) + " " + selectedCountryOfMap;
								String title = getApplicationContext().getString(R.string.mapselector_are_you_sure_to_download);
								String yes_btn = getApplicationContext().getString(R.string.button_yes_title);
								String no_btn = getApplicationContext().getString(R.string.button_no_title);
								Toast.makeText(getApplicationContext(), mess, Toast.LENGTH_LONG).show();
								//AlertDialog alertDialog = new AlertDialog.Builder(Main.this).create();
								AlertDialog.Builder alertDialog = new AlertDialog.Builder(view.getContext());
								alertDialog.setTitle(title);
								alertDialog.setMessage(mess);
								alertDialog.setPositiveButton(yes_btn, new DialogInterface.OnClickListener() {
						            public void onClick(DialogInterface dialog,int which) {
						            	//#7 12/08(2013
										String downloadingText = getApplicationContext().getString(R.string.mapselector_downloading);
										// 01/12/2012
										progressBar = new ProgressDialog(viewWhenClick.getContext());
										// 08/14/2016 #17 v1.02.14 
										progressBar.setCancelable(false);
										progressBar.setOnKeyListener(new DialogInterface.OnKeyListener() {
										    @Override
										    public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
										        if (keyCode == KeyEvent.KEYCODE_SEARCH && event.getRepeatCount() == 0) {
										            return true; // Pretend we processed it
										        }
										        return false; // Any other keys are still processed as normal
										    }
										});
										//#7 12/08/20113
										progressBar.setMessage(selectedCountryOfMap +  " " + downloadingText + "...");
										progressBar.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
										progressBar.setProgress(0);
										progressBar.show();		
										
										new Thread(new Runnable() {
											public void run() {
												try {
													// #5 10/06/2013
													downloadMapFileToSDCard(mapUrl);
													// extract url the map file
													String[] pathSplitted = mapUrl.split("/");
													// 02/10/2013
													String mapFileName = pathSplitted[pathSplitted.length - 1];
													// use selected map
													GPSSettings.DEFAUL_MAPSFORGE_MAP = mapFileName;
												} catch (Exception e) {
													isDownloadSuccess = false;
													// 02/10/2013
													setupResult(false);
													GPSSettings.DEFAUL_MAPSFORGE_MAP = null;
													e.printStackTrace();
												}
											}
										}).start();
										
										isDownloadSuccess = true;
						            }
						        });
						 
						        // Setting Negative "NO" Button
						        alertDialog.setNegativeButton(no_btn, new DialogInterface.OnClickListener() {
						            public void onClick(DialogInterface dialog,    int which) {
										// 02/10/2013
										setupResult(false);
						            }
						        });
						 			
								alertDialog.show();
								
								
							} catch (Exception e) {
								isDownloadSuccess = false;
								// 02/10/2013
								setupResult(false);
								Toast.makeText(view.getContext(), R.string.error_download_map, Toast.LENGTH_LONG).show();
							}
							finally {
								if (isDownloadSuccess) {
									Toast.makeText(view.getContext(), R.string.mapselector_download_finished, Toast.LENGTH_LONG).show();
								}
							}
						}
						else {
							// use selected map
							GPSSettings.DEFAUL_MAPSFORGE_MAP = selectedCountryOfMap;
							// # 11/26/2013
							Utils.writeLastUsedOfflineMap(mainActivity, selectedCountryOfMap);
					        //02/10/2013 true means yes selected
							setupResult(true);
							// return to previous activity
							finish();
						}
					}
				}
			}
		});
 		
	}
	
}
