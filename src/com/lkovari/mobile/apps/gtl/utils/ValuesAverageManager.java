package com.lkovari.mobile.apps.gtl.utils;

/**
 * 
 * @author lkovari
 *
 */
public class ValuesAverageManager {

	private	double sumDouble = 0;
	private	float sumFloat = 0;
	private int divider = 1;
	private int counter = 0;
	private double averageDouble = 0;
	private float averageFloat = 0;
	
	public ValuesAverageManager() {
		this(10);
	}
	
	public ValuesAverageManager(int divider) {
		this.divider = divider;
		this.counter = 0;
	}
	
	public void setDivider(int divider) {
		this.divider = divider;
	}
	
	public void addValueDouble(double value) {
		sumDouble += value;
		counter++;
	}

	public void addValueFloat(float value) {
		sumFloat += value;
		counter++;
	}
	
	public double getAverageDouble() {
		if (counter > divider) {
			counter = 0;
			averageDouble = sumDouble / divider;
		}
		return averageDouble;
	}
	
	public float getAverageFloat() {
		if (counter > divider) {
			counter = 0;
			averageFloat = sumFloat / divider;
		}
		return averageFloat;
	}
	
}
