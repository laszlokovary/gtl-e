package com.lkovari.mobile.apps.gtl.utils;


/**
 * 12/10/2012
 * @author lkovari
 *
 */
public class MeasurementManager {
	private static double STATUE_MILE_IN_METERS = 1609.347;
	private static double NAUTICAL_MILE_IN_METERS = 1852.000;
	private static double FOOT_IN_METERS = 0.3048;
	private static double METER_IN_FEET = 3.2808399;
	private static double METER_PER_SECOND_IN_KNOTS = 1.94384449;
	private static double NAUTICAL_MILE_IN_FEET = 6076.11549;
	
	/**
	 * 12/11/2012
	 * @param ms MeasurementSystem
	 * @param mk MeasurementKind
	 * @param value double
	 * @return double
	 */
	private static double convertvalueByMeasurementSystemAndKind(MeasurementSystem ms, MeasurementKind mk, double value, boolean isFootOnly) {
		double result = 0.0;
		double unitLimitInMeters = 1000;
		switch (ms) {
		case METRIC: {
			unitLimitInMeters = 1000; 
			switch (mk) {
			case LENGTH : {
				if (isFootOnly) {
					result = value;
				}
				else {
					if (value > unitLimitInMeters) {
						result = value / unitLimitInMeters;
					}	
					else {
						result = value;
					}	
				}
				break;
			}
			case SPEED : {
				// m/s to km
				result = (value * (60 * 60)) / 1000;
				break;
			}
			}
			break;
		}
		case IMPERIAL: {
			unitLimitInMeters = STATUE_MILE_IN_METERS; 
			switch (mk) {
			case LENGTH : {
				if (isFootOnly) {
					result = value * METER_IN_FEET;
				}
				else {
					if (value > unitLimitInMeters) {
						result = value / unitLimitInMeters;
					}	
					else {
						result = value * METER_IN_FEET;
					}	
				}
				break;
			}
			case SPEED : {
				result = (value * (60 * 60)) / unitLimitInMeters;
				break;
			}
			}
			break;
		}
		case ICAO : {
			unitLimitInMeters = NAUTICAL_MILE_IN_METERS; 
			switch (mk) {
			case LENGTH : {
				if (isFootOnly) {
					// convet m to Nm and to feet
					result = (value / unitLimitInMeters) * NAUTICAL_MILE_IN_FEET;
				}
				else {
					if (value > unitLimitInMeters) {
						result = value / unitLimitInMeters;
					}	
					else {
						result = value * METER_IN_FEET;
					}	
				}
				break;
			}
			case SPEED : {
				// m/s to knots
				result = value * METER_PER_SECOND_IN_KNOTS;
				break;
			}
			}
			break;
		}
		}
		return result;
	}
	

	/**
	 * 12/13/2012
	 * @param ms
	 * @param mk
	 * @param value
	 * @param isFootOnly
	 * @return
	 */
	public static String convertvalueByMeasurementSystemAndKindToText(MeasurementSystem ms, MeasurementKind mk, double value, boolean isFootOnly) {
		double result = convertvalueByMeasurementSystemAndKind(ms, mk, value, isFootOnly);
		return String.format("%.02f", result);
	}


	/**
	 * 12/13/2012
	 * @param ms
	 * @param mk
	 * @param value
	 * @param isFootOnly
	 * @return
	 */
	public static String selectUnitNameByMeasurementSystemAndKind(MeasurementSystem ms, MeasurementKind mk, double value, boolean isFootOnly) {
		String result = "";
		double unitLimitInMeters = 1000;
		switch (ms) {
		case METRIC: {
			unitLimitInMeters = 1000; 
			switch (mk) {
			case LENGTH : {
				if (isFootOnly) {
					result = "m";
				}
				else {
					if (value > unitLimitInMeters) {
						result ="Km";
					}	
					else {
						result = "m";
					}	
				}
				break;
			}
			case SPEED : {
				// m/s to km
				result = "Kph";
				break;
			}
			}
			break;
		}
		case IMPERIAL: {
			unitLimitInMeters = STATUE_MILE_IN_METERS; 
			switch (mk) {
			case LENGTH : {
				if (isFootOnly) {
					result = "Ft";
				}
				else {
					if (value > unitLimitInMeters) {
						result = "Mi";
					}	
					else {
						result = "Ft";
					}	
				}
				break;
			}
			case SPEED : {
				result = "Mph";
				break;
			}
			}
			break;
		}
		case ICAO : {
			unitLimitInMeters = NAUTICAL_MILE_IN_METERS; 
			switch (mk) {
			case LENGTH : {
				if (isFootOnly) {
					// convet m to Nm and to feet
					result = "Ft";
				}
				else {
					if (value > unitLimitInMeters) {
						result = "Nm";
					}	
					else {
						result = "Ft";
					}	
				}
				break;
			}
			case SPEED : {
				// m/s to knots
				result = "Kt";
				break;
			}
			}
			break;
		}
		}
		return result;
	}
	
	
	
}
