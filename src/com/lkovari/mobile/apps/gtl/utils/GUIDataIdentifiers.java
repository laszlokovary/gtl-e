package com.lkovari.mobile.apps.gtl.utils;

/**
 * 
 * @author lkovari
 *
 */
public class GUIDataIdentifiers {

	public static final String GPS_DATA_BROADCAST_LOCATION_IX = "broadcaslocationtix";
	public static final String GPS_DATA_BROADCAST_STATUS_IX = "broadcasstatusix";
	public static final String GPS_DATA_STATUS = "gpsstatus";
	public static final String GPS_DATA_SATELLITES = "maxsatellites";
	public static final String GPS_DATA_SATELLITESINFIX = "usedsatellitesinfix";
	public static final String GPS_DATA_SATELLITESAVGSNR = "satellitesavgsnr";	
	public static final String GPS_DATA_PROVIDER = "provider";
	public static final String GPS_DATA_ACCURACY = "accuracy";
	public static final String GPS_DATA_LONGITUDE = "longitude";
	public static final String GPS_DATA_LATITUDE = "latitude";
	// 07/09/2013 #122
	public static final String GPS_DATA_GLONASS_SATELLITES = "glonasssatellites";
	public static final String GPS_DATA_GLONASS_SATELLITES_INFIX = "glonasssatellitesinfix";
	// 19/07/2014 #9 v1.02.6
	public static final String GPS_DATA_BEIDOU_SATELLITES = "beidoussatellites";
	public static final String GPS_DATA_BEIDOU_SATELLITES_INFIX = "beidoussatellitesinfix";
	
	public static final String ROUTE_DATA_ELAPSED = "elapsed";
	public static final String ROUTE_DATA_ODOMETER = "odometer";
	public static final String ROUTE_DATA_SPEED = "speed";
	public static final String ROUTE_DATA_BEARING = "bearing";
	public static final String ROUTE_DATA_ALTITUDE = "altitude";
	// 01/02/2013
	public static final String ROUTE_DATA_AVGSPEED = "avgspeed";
	// #5 10/12/2013
	public static final String ROUTE_DATA_ELAPSED_IN_MOVE = "elapsedinmove";
	public static final String ROUTE_DATA_ELAPSED_IN_WAIT = "elapsedinwait";
	
}
