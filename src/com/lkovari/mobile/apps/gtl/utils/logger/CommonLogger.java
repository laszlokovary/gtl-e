package com.lkovari.mobile.apps.gtl.utils.logger;

import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import com.lkovari.mobile.apps.gtl.settings.GPSSettings;

import android.os.Environment;


/**
 * 
 * @author lkovari
 *
 */
public class CommonLogger {
	public static boolean IS_LOGGER_ENABLED = false;
	private static BlockingQueue<LogEntry> queue = new LinkedBlockingQueue<LogEntry>();
	private static LogConsumer consumer = new LogConsumer(queue);
	private static Thread loggerThread = new Thread(consumer);
	private static LogProducer producer = new LogProducer(queue);
	
    private static int LOG_FILE_SIZE_LIMIT = 100 * 1024;
	public static String BASE_FILE_NAME = "GTL-";
	public static String FILE_STAMP = null;

    private static FileHandler logHandler;  
    private static Logger logger;
    
	static {
		if (IS_LOGGER_ENABLED) {
			FILE_STAMP = calculateFileNameStamp();
			String logFileName = BASE_FILE_NAME + "L-" + FILE_STAMP + "_log.txt";
			logFileName = Environment.getExternalStorageDirectory() + "/" + GPSSettings.GTL_TRACKLOG_FOLDER + "/" + logFileName;
	        try {
				logHandler = new FileHandler(logFileName, LOG_FILE_SIZE_LIMIT, 1, true);
			} catch (IOException e) {
				e.printStackTrace();
			}
	        logHandler.setFormatter(new SimpleFormatter());
	        logger = Logger.getLogger("com.lkovari.mobile.apps");
	        logger.setLevel(Level.ALL);
	        logger.addHandler(logHandler);

			loggerThread.start();
		}
	}
	
	
	/**
	 * 
	 * @author lkovari
	 *
	 */
	public static class LogEntry {
		private final String message;
		private Level level;

		LogEntry(String msg) {
			this.level = Level.FINE;
			this.message = msg;
		}

		LogEntry(Level level, String msg) {
			this.level = level;
			this.message = msg;
		}
		
		public Level getLevel() {
			return level;
		}
		
		public String getMessage() {
			return message;
		}
	}

	/**
	 * 
	 * @author lkovari
	 *
	 */
	public static class LogProducer {
		private final BlockingQueue<LogEntry> queue;

		LogProducer(BlockingQueue<LogEntry> q) {
			queue = q;
		}

		public void log(Level level, String msg) throws Exception {
			queue.put(new LogEntry(level, msg));
		}
	}

	/**
	 * 
	 * @author lkovari
	 *
	 */
	public static class LogConsumer implements Runnable {
		private final BlockingQueue<LogEntry> queue;
		private boolean isRun = true;
		
		LogConsumer(BlockingQueue<LogEntry> q) {
			queue = q;
		}

		public void run() {
			try {
				while(isRun) {
					LogEntry entry = queue.take();
					if (entry != null)
						logger.log(entry.getLevel(), entry.getMessage());
				}
		    } 
			catch(InterruptedException e) {
		       e.printStackTrace();
		    }
		}
		
		public void setRun(boolean isRun) {
			this.isRun = isRun;
		}
	}

	/**
	 * 
	 * @return
	 */
    public static String calculateFileNameStamp() {
    	String fileStamp = null;
       	Calendar c = Calendar.getInstance();
    	if (FILE_STAMP == null) {
           	FILE_STAMP = "" + c.get(Calendar.YEAR) + "-" + c.get(Calendar.DAY_OF_YEAR) + "-" + c.get(Calendar.HOUR_OF_DAY) + c.get(Calendar.MINUTE) + c.get(Calendar.SECOND) + c.get(Calendar.MILLISECOND);
           	fileStamp = FILE_STAMP;
    	}
    	else {
    		fileStamp = "" + c.get(Calendar.YEAR) + "-" + c.get(Calendar.DAY_OF_YEAR) + "-" + c.get(Calendar.HOUR_OF_DAY) + c.get(Calendar.MINUTE) + c.get(Calendar.SECOND) + c.get(Calendar.MILLISECOND);
    	}
    	c = null;
       	return fileStamp;
    }
	
	/**
	 * 
	 * @param text
	 * @throws Exception
	 */
	public static void log(Level level, String text) {
		if (IS_LOGGER_ENABLED)
			try {
				StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
				StackTraceElement element = stackTrace[3];
				if (text != null)
					producer.log(level, element.toString() + "::" + text);
				else
					producer.log(level, element.toString());
				stackTrace = null;
			} catch (Exception e) {
				e.printStackTrace();
			}
	}

	public static void log(Level level) {
		if (IS_LOGGER_ENABLED)
			try {
				StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
				StackTraceElement element = stackTrace[3];
				producer.log(level, element.toString());
				stackTrace = null;
			} catch (Exception e) {
				e.printStackTrace();
			}
	}
	
	
	/**
	 * 
	 */
    public static void shutDown() {
    	if (IS_LOGGER_ENABLED) {
    		long cnt = 0;
    		while (!queue.isEmpty()) {
    			cnt++;
    		}
    		System.out.println("Queue wait Cnt #" + cnt);
    		logHandler.flush();
    		logHandler.close();
    		if (consumer != null) {
    			// stop
    			consumer.setRun(false);
    			consumer = null;
    		}	
    		if (loggerThread != null) {
        		loggerThread.interrupt();
    		}
    		if (producer != null)
    			producer = null;
    	}	
        logHandler = null;
        logger = null;
    }
	
}
