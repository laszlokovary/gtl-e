package com.lkovari.mobile.apps.gtl.map.mapsforge;


import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Level;

import org.mapsforge.android.maps.MapActivity;
import org.mapsforge.android.maps.MapView;
import org.mapsforge.android.maps.Projection;
import org.mapsforge.android.maps.mapgenerator.MapGenerator;
import org.mapsforge.android.maps.overlay.ArrayCircleOverlay;
import org.mapsforge.android.maps.overlay.ArrayWayOverlay;
import org.mapsforge.android.maps.overlay.OverlayCircle;
import org.mapsforge.android.maps.overlay.OverlayWay;
import org.mapsforge.core.BoundingBox;
import org.mapsforge.core.GeoPoint;
import org.mapsforge.core.MapPosition;
import org.mapsforge.core.MercatorProjection;
import org.mapsforge.core.Tile;
import org.mapsforge.map.reader.header.FileOpenResult;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Environment;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.Toast;

import com.lkovari.mobile.apps.gtl.Main;
import com.lkovari.mobile.apps.gtl.R;
import com.lkovari.mobile.apps.gtl.map.filter.DouglasPeuckerOptimizer;
import com.lkovari.mobile.apps.gtl.settings.GPSSettings;
import com.lkovari.mobile.apps.gtl.track.GPSCoordinate;
import com.lkovari.mobile.apps.gtl.utils.GUIDataIdentifiers;
import com.lkovari.mobile.apps.gtl.utils.logger.CommonLogger;
import com.lkovari.mobile.apps.gtl.utils.sdcard.SDCardManager;

/**
 * 12/28/2012
 * @author lkovari
 *
 */
public class MapTabMapForge extends MapActivity {
	private boolean isReceiversRegistered = false;
    private float lastAccuracy = 50;
    private float currentSpeed;
    private GeoPoint currentGeoPoint = null;
    private GeoPoint headGeoPoint = null;
    private GeoPoint startGeoPoint = null;
	private List<GeoPoint> routePoints = new ArrayList<GeoPoint>();
	private List<GeoPoint> optimizedRoutePoints = new ArrayList<GeoPoint>();
	private MapView mapView = null;
	private boolean isFirstLocation = false;
	// 12/29/2012
	private ArrayCircleOverlay circleOverlay = null;
	private int markerBorderColor = 0xff1414FC;
	private int markerFillColor = 0xff6666ff;
	// 12/30/2012
	private ArrayWayOverlay wayOverlay = null;
	private int posCnt = 0;
	private int minLat = Integer.MAX_VALUE;
	private int maxLat = Integer.MIN_VALUE;
	private int minLon = Integer.MAX_VALUE;
	private int maxLon = Integer.MIN_VALUE;
	private boolean isAllvalueCaptured = false;
	// 10/06/2013
	private String loadedMapFile = null;
	private boolean isSetupOverlays = false;
	// #8 08/03/2014
	private double ZOOM_CORRECTION = 1.72d;

	
	/**
	 *  12/30/2012
	 * @param collection
	 * @return
	 */
	private GeoPoint[][] routePointList2GeoPointArray(Collection<GeoPoint> collection) {
		if ((collection == null) || (collection.size() == 0)) {
	            return new GeoPoint[][] {};
        }
        GeoPoint[][] result = new GeoPoint[1][collection.size()];
        int ix1 = 0;
        int ix2 = 0;
        for (GeoPoint geoPoint : collection) {
            result[ix1][ix2++] = geoPoint;
        }
        return result;
    }	
	
	@Override
	protected void onStart() {
		if (loadedMapFile != null) {
			if (!loadedMapFile.endsWith(GPSSettings.DEFAUL_MAPSFORGE_MAP)) {
				loadMapFile();
			}
		}
		else {
			loadMapFile();
		}
		super.onStart();
	}

	/**
	 *  #5 10/06/2013
	 */
	private void loadMapFile() {
		// 01/13/2013
		List<File> foundMapFiles = null;
		// 01/13/2013
		if (GPSSettings.DEFAUL_MAPSFORGE_MAP != null) {
			Toast.makeText(this, "FILE " + GPSSettings.DEFAUL_MAPSFORGE_MAP, Toast.LENGTH_LONG);
			// #5 10/06/2013
			foundMapFiles = SDCardManager.findFile(Environment.getExternalStorageDirectory().getPath() + "/" + GPSSettings.GTL_MAPS_FOLDER + "/" + GPSSettings.DEFAUL_MAPSFORGE_MAP);
		}
		else {
			foundMapFiles = SDCardManager.findFiles(Environment.getExternalStorageDirectory().getPath() + "/" + GPSSettings.GTL_MAPS_FOLDER + "/" , ".map");
		}
		
		if (foundMapFiles.size() > 0) {
			// first map file
			File mapFile = foundMapFiles.get(0);		
			FileOpenResult fileOpenResult = mapView.setMapFile(mapFile);
			this.loadedMapFile = mapFile.getAbsolutePath();
			boolean isValidMap = fileOpenResult.isSuccess();
			if (isValidMap) {
				setContentView(mapView);
				if (!isSetupOverlays) {
					try {
				    	// 12/29/2012
				    	if (GPSSettings.IS_SHOW_ACCURACY_MARKER) {
					        Paint circlePaint = new Paint();
					        circlePaint.setAntiAlias(true);
					        circlePaint.setStrokeWidth(2.0f);
					        circlePaint.setColor(markerFillColor);
					        circlePaint.setStyle(Paint.Style.FILL_AND_STROKE);
					        circlePaint.setAlpha(GPSSettings.ACCURACY_MARKER_TRANSPARENCY);
							
					        Paint circleBorderPaint = new Paint();
					        circleBorderPaint.setAntiAlias(true);
					        circleBorderPaint.setStrokeWidth(2.0f);
					        circleBorderPaint.setColor(markerBorderColor);
					        circleBorderPaint.setStyle(Paint.Style.STROKE);
					        circleBorderPaint.setAlpha(255);
					        
					        circleOverlay = new ArrayCircleOverlay(circlePaint, circleBorderPaint);
					        mapView.getOverlays().add(circleOverlay);
				    	}

				    	// 12/30/2012
				        Paint routePaint = new Paint();
				        routePaint.setAntiAlias(true);
				        routePaint.setStrokeWidth(2.0f);
				        routePaint.setColor(Color.parseColor("#"+GPSSettings.MAP_TRACK_COLOR));
				        routePaint.setStyle(Paint.Style.FILL_AND_STROKE);
						
				        Paint routeBorderPaint = new Paint();
				        routeBorderPaint.setAntiAlias(true);
				        routeBorderPaint.setColor(Color.parseColor("#"+GPSSettings.MAP_TRACK_COLOR));
				        routeBorderPaint.setStyle(Paint.Style.STROKE);
				        routeBorderPaint.setStrokeWidth(5);
				    	wayOverlay = new ArrayWayOverlay(null, routeBorderPaint);
				    	mapView.getOverlays().add(wayOverlay);
					}
					finally {
						isSetupOverlays = true;
					}
				}
			}
			else {
				Toast.makeText(this, fileOpenResult.getErrorMessage(), Toast.LENGTH_LONG).show();
				mapView.setClickable(false);
//				finish();
			}
		}	
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
//		setContentView(R.layout.map_tab_mf);
		mapView = new MapView(this);
		mapView.setClickable(true);
		mapView.setBuiltInZoomControls(false);
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
    	if (GPSSettings.IS_SHOW_ADDRESS_IF_TOUCH_TO_MAP) {
    		
    	}
		return super.onTouchEvent(event);
	}
	
	/**
	 * 12/30/2012
	 */
	private void showAccuracyMarker() {
    	// 12/29/2012 setup circle
    	if (GPSSettings.IS_SHOW_ACCURACY_MARKER) {
            OverlayCircle centerOverlayCircle = new OverlayCircle();
            centerOverlayCircle.setCircleData(currentGeoPoint, 0.25f);
            
            OverlayCircle overlayCircle = new OverlayCircle();
            overlayCircle.setCircleData(currentGeoPoint, lastAccuracy);
            // 01/16/2013
            headGeoPoint = currentGeoPoint;
            if (circleOverlay != null) {
            	circleOverlay.clear();
            	circleOverlay.addCircle(centerOverlayCircle);
            	circleOverlay.addCircle(overlayCircle);
            	circleOverlay.requestRedraw();    	
            }
    	}
	}

	
	/**
	 * 12/30/2012
	 */
	private void showRoutePoints(List<GeoPoint> routePoints) {
        // 12/30/2012 show route
        GeoPoint[][] routePointsArray = routePointList2GeoPointArray(routePoints);
        OverlayWay overlayWay = new OverlayWay(routePointsArray);
        wayOverlay.clear();
        wayOverlay.addWay(overlayWay);
	}

	/**
	 * 12/30/2012
	 */
	private void moveToCenterOfView(GeoPoint geoPoint) {
    	mapView.getController().setCenter(geoPoint);
	}
	
	
    private void captureMaxPositionValues(GeoPoint geoPoint) {
    	int lat = (int) (geoPoint.getLatitude() * 1E6);
        int lon = (int) (geoPoint.getLongitude() * 1E6);
        maxLat = Math.max(lat, maxLat);
        minLat = Math.min(lat, minLat);
        maxLon = Math.max(lon, maxLon);
        minLon = Math.min(lon, minLon);
        
	    // check all value is captured
		if (!isAllvalueCaptured) {
			boolean isCaptured = (minLat != Integer.MAX_VALUE) && (maxLat != Integer.MIN_VALUE) && (minLon != Integer.MAX_VALUE) && (maxLon != Integer.MIN_VALUE);
		    if (isCaptured) {
		    	isAllvalueCaptured = true;
		    }
		}
    }
    
    /**
     * #98 12/31/2012 by hannes.janetzek@gmail.com	
     * @param latitude
     * @return
     */
    public double latitudeToY(double latitude) {
        double sinLatitude = Math.sin(latitude * (Math.PI / 180));
        return 0.5 - Math.log((1 + sinLatitude) / (1 - sinLatitude)) / (4 * Math.PI);
    }
    
    /**
     * #98 12/31/2012 by hannes.janetzek@gmail.com	
     * @param longitude
     * @return
     */
    public double longitudeToX(double longitude) {
        return (longitude + 180) / 360;
    }

    /**
     * #98 12/31/2012 by hannes.janetzek@gmail.com	
     * @param boundingBox
     * @return
     */
    public byte calculateZoomLevel(BoundingBox boundingBox) {
    	byte zoomLevel = 0;
    	
    	double dx = longitudeToX(boundingBox.getMaxLongitude()) - longitudeToX(boundingBox.getMinLongitude());
    	double dy = latitudeToY(boundingBox.getMinLatitude()) - latitudeToY(boundingBox.getMaxLatitude());
		/*
    	// #8 06/03/2014
    	MapPosition mapPosition = this.mapView.getMapPosition().getMapPosition();
    	double dx = MercatorProjection.longitudeToPixelX(boundingBox.getMaxLongitude(), mapPosition.zoomLevel) - MercatorProjection.longitudeToPixelX(boundingBox.getMinLongitude(), mapPosition.zoomLevel);
    	double dy = MercatorProjection.latitudeToPixelY(boundingBox.getMinLatitude(), mapPosition.zoomLevel) - MercatorProjection.latitudeToPixelY(boundingBox.getMaxLatitude(), mapPosition.zoomLevel);
    	*/
    	double log4 = Math.log(4);
    	double zx = -log4 * Math.log(dx) + (mapView.getWidth() / Tile.TILE_SIZE);
    	double zy = -log4 * Math.log(dy) + (mapView.getHeight() / Tile.TILE_SIZE);

    	
    	//zoomLevel = (byte) Math.floor(Math.min(zx, zy));
    	
    	// #8 08/03/2014
    	zoomLevel = (byte) Math.floor(Math.min(zx, zy) + ZOOM_CORRECTION);
    	// #8 23/02/2014 not over than the max zoom
    	zoomLevel = (byte) Math.min(zoomLevel, mapView.getMapZoomControls().getZoomLevelMax());
    	zoomLevel = (byte) Math.max(zoomLevel, mapView.getMapZoomControls().getZoomLevelMin());
    	return zoomLevel;
    }
    
    /**
     * 9/03/2014 #8 v1.02.5
     * @param boundingBox
     * @return
     */
	private byte zoomAndPanTo(BoundingBox boundingBox) {
		int width = mapView.getWidth();
		int heigth = mapView.getHeight();
		if (width <= 0 || heigth <= 0) {
			return -1;
		}
		
		int cntLat = (boundingBox.maxLatitudeE6 + boundingBox.minLatitudeE6) / 2;
		int cntLng = (boundingBox.maxLongitudeE6 + boundingBox.minLongitudeE6) / 2;

		mapView.setCenter(new GeoPoint(cntLat, cntLng));

		GeoPoint pointSouthWest = new GeoPoint(boundingBox.minLatitudeE6, boundingBox.minLongitudeE6);
		GeoPoint pointNorthEast = new GeoPoint(boundingBox.maxLatitudeE6, boundingBox.maxLongitudeE6);

		Projection projection = mapView.getProjection();
		Point pointSW = new Point();
		Point pointNE = new Point();
		byte maxLvl = (byte) mapView.getMapZoomControls().getZoomLevelMax();
		byte zoomLevel = 0;
		for (; zoomLevel < maxLvl;) {
			byte tmpZoomLevel = (byte) (zoomLevel + 1);
			projection.toPoint(pointSouthWest, pointSW, tmpZoomLevel);
			projection.toPoint(pointNorthEast, pointNE, tmpZoomLevel);
			if (pointNE.x - pointSW.x > width) {
				break;
			}
			if (pointSW.y - pointNE.y > heigth) {
				break;
			}
			zoomLevel = tmpZoomLevel;
		}
		return zoomLevel;
	}
    
    /**
     * #8 25/02/2014
     * @param speed
     * @return
     */

	private int calculateZoomLevelBySpeed(double speed) {
		// calculate max speed
		double dummyMaxSpeed = 250.0; 
		switch (GPSSettings.USAGE_TYPE) {
		case AIRCRAFT : {
			dummyMaxSpeed = 1700;
			break;
		}
		case FOUR_WHEELERS : {
			dummyMaxSpeed = 270;
			break;
		}
		case PEDESTRIAN : {
			dummyMaxSpeed = 80;
			break;
		}
		case TWO_WHEELERS : {
			dummyMaxSpeed = 320;
			break;
		}
		case WALKING_HIKE : {
			dummyMaxSpeed = 80;
			break;
		}
		case WATERCRAFTS : {
			dummyMaxSpeed = 100;
			break;
		}
		}
	    // closest zoom level is the max	
    	int zoomLevelMax = mapView.getMapZoomControls().getZoomLevelMax();
    	// farest
    	int zoomLevelMin = mapView.getMapZoomControls().getZoomLevelMin();
    	int zoomLevel = 0;
	    // if no speed data this is the default	
    	int defaultZoomLevel = zoomLevelMax; 
	    if (speed > 0) {
	        // theoretically max (dummy) / max zoom steps
	        double kmPerZoomLevel = dummyMaxSpeed / zoomLevelMax;
	        zoomLevel = (int) Math.round(speed / kmPerZoomLevel);
	        // check
	    	zoomLevel = Math.min(zoomLevel, mapView.getMapZoomControls().getZoomLevelMax());
	    	zoomLevel = Math.max(zoomLevel, mapView.getMapZoomControls().getZoomLevelMin());
	        // reverse it, more fast then more far
	        zoomLevel = zoomLevelMax - zoomLevel;
	        zoomLevel += zoomLevelMin;
	    }
	    else { 
	    	zoomLevel = defaultZoomLevel;
	    }	
	    return zoomLevel;
	}    
	
    /**
     * #98 12/31/2012
     */
    private void spanToTrackAreaAndAnimateToCenterOfIt() {
    	BoundingBox boundingBox = new BoundingBox(minLat, minLon, maxLat, maxLon);
    	mapView.getController().setCenter(boundingBox.getCenterPoint());
//    	byte zoomLevel = calculateZoomLevel(boundingBox);
    	byte zoomLevel = zoomAndPanTo(boundingBox);
    	// #8 25/02/2014 correction
    	zoomLevel = (byte) Math.min(zoomLevel, mapView.getMapZoomControls().getZoomLevelMax());
    	zoomLevel = (byte) Math.max(zoomLevel, mapView.getMapZoomControls().getZoomLevelMin());
    	mapView.getController().setZoom(zoomLevel);
    	boundingBox = null;
    }
    
	
	public void showLocationOnMap(double longitude, double latitude, float speed) {
		int lat = (int) (latitude * 1E6);
		int lon = (int) (longitude * 1E6);
        currentGeoPoint = new GeoPoint(lat, lon);
        routePoints.add(currentGeoPoint);
        optimizedRoutePoints.add(currentGeoPoint);
        // when start show the start position
        if (isFirstLocation) {
        	isFirstLocation = false;
        	//12/07/2012
        	startGeoPoint = currentGeoPoint;
        }
        
		if (GPSSettings.IS_KEEP_FULL_TRACK_ON_VIEW_OF_MAP)
			captureMaxPositionValues(currentGeoPoint);
        
		posCnt++;
		// is need to optimize every POS_OPTIMIZE_LIMIT
		if ((posCnt % GPSSettings.POS_OPTIMIZE_LIMIT) == 0) {
			posCnt = 0;
			List<GeoPoint> reduced = DouglasPeuckerOptimizer.reductionGeoPointMF(optimizedRoutePoints, GPSSettings.DOUGLAS_PEUCKER_TOLARANCE);
//			List<LocPts> reduced = SGOptimizer.simplifyLine2D((float)GPSSettings.DOUGLAS_PEUCKER_TOLARANCE, routePoints);
			if (optimizedRoutePoints.size() != reduced.size()) {
//				System.out.println("Douglas Peucker " + MapTab.this.routePoints.size() + " : " + reduced.size());
				optimizedRoutePoints.clear();
				optimizedRoutePoints.addAll(reduced);
			}
			/* 11/28/2012
			else {
				reduced.clear();
				reduced = null;
			}
			*/
			reduced.clear();
			reduced = null;
		}
    	

		if (GPSSettings.IS_ANIMATE_TO_POSITION) {
			moveToCenterOfView(currentGeoPoint);
			// #8 25/02/2014
			if (GPSSettings.IS_SET_AUTOMATIC_ZOOM_LEVEL) {
				if (currentSpeed > -1) {
					int zoom = calculateZoomLevelBySpeed(currentSpeed);
					mapView.getController().setZoom(zoom);
				}
			}
		}
		else if (GPSSettings.IS_KEEP_FULL_TRACK_ON_VIEW_OF_MAP) {
			if (isAllvalueCaptured) {
				spanToTrackAreaAndAnimateToCenterOfIt();
//				fitToBoundingBox();
			}	
		}
		
    	// 12/29/2012
    	showAccuracyMarker();

    	// 12/30/2012
    	showRoutePoints(optimizedRoutePoints);
        
       	mapView.invalidate();
	}
	
	
	private final BroadcastReceiver intentReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
	    	double longitude = 0;
	    	double latitude = 0;
		    // Handle reciever
		    String mAction = intent.getAction();
		    if (mAction.equals(Main.BROADCAST_ACTION_GPS_LOCATION_DATA_CHANGED)) {
		    	Bundle bundle = intent.getExtras();
		    	longitude = bundle.getDouble(GUIDataIdentifiers.GPS_DATA_LONGITUDE);
		    	latitude = bundle.getDouble(GUIDataIdentifiers.GPS_DATA_LATITUDE);
			    currentSpeed = bundle.getFloat(GUIDataIdentifiers.ROUTE_DATA_SPEED);
			    lastAccuracy = bundle.getFloat(GUIDataIdentifiers.GPS_DATA_ACCURACY);
		    	// show values
			    showLocationOnMap(longitude, latitude, currentSpeed);
		    }	
		}
		
	};
	
	/**
	 * #5 11/23/2013
	 */
	private void clearMinMaxRectOfRouteValues() {
		this.minLat = Integer.MAX_VALUE;
		this.maxLat = Integer.MIN_VALUE;
		this.minLon = Integer.MAX_VALUE;
		this.maxLon = Integer.MIN_VALUE;
	}
	
	/**
	 * 12/30/2012
	 */
	public void clearRoutePoints() {
		// 8/12/2016 #16 v1.02.13 
		if (wayOverlay != null)
			wayOverlay.clear();
		if (optimizedRoutePoints != null)
			optimizedRoutePoints.clear();
		if (routePoints != null) {
			// #5 11/2362013
			routePoints.clear();
		}
		clearMinMaxRectOfRouteValues();
	}

	/**
	 * 01/16/2013
	 */
	public void redrawRoutePoints(List<GPSCoordinate> coordinates, boolean isClearRoutePoints) {
		if (isClearRoutePoints) {
			// #5 11/23/2013
			clearRoutePoints();
		}
		// #5 11/23/2013 add all coordinates
		for (GPSCoordinate gpsCoordinate : coordinates) {
			GeoPoint geoPoint = new GeoPoint(gpsCoordinate.getLocation().getLatitude(), gpsCoordinate.getLocation().getLongitude());
			captureMaxPositionValues(geoPoint);
			routePoints.add(geoPoint);
			
		}
		List<GeoPoint> reduced = DouglasPeuckerOptimizer.reductionGeoPointMF(routePoints, GPSSettings.DOUGLAS_PEUCKER_TOLARANCE);
		optimizedRoutePoints.clear();
		optimizedRoutePoints.addAll(reduced);
		reduced.clear();
		reduced = null;
		// #5 10/24/2013
		showRoutePoints(optimizedRoutePoints);
	}
	
	/**
	 * 12/30/2012
	 */
	public void clearMarker() {
		if (circleOverlay != null)
			circleOverlay.clear();
	}
	

	/**
	 * 12/30/2012
	 */
	public void clearMap() {
		clearRoutePoints();
		clearMarker();
	}
	
	/**
	 *  08/23/2013
	 */
	public void moveToCurrentLocation() {
		// 01/05/2013
		if (currentGeoPoint != null) {
			moveToCenterOfView(currentGeoPoint);				
			showAccuracyMarker();
		}
		else {
			// 01/05/2013
        	Toast.makeText(this, R.string.error_position_is_not_yes, Toast.LENGTH_LONG).show();
		}
	}
	
	@Override
	protected void onResume() {
	  try {
		  CommonLogger.log(Level.INFO);
		  // Register Sync Recievers
		  IntentFilter intentToReceiveFilter = new IntentFilter();
		  intentToReceiveFilter.addAction(Main.BROADCAST_ACTION_GPS_LOCATION_DATA_CHANGED);
//		  this.registerReceiver(intentReceiver, intentToReceiveFilter, null, handler);
		  this.registerReceiver(intentReceiver, intentToReceiveFilter);
		  isReceiversRegistered = true;
		  // 03/12/2016 #15 v1.02.11
		  if (GPSSettings.IS_LOGGING_STARTED) {
			  clearMap();
			  redrawRoutePoints(GPSSettings.gpsCoordinates, true);
			  moveToCurrentLocation();
		  }
	  }
	  catch (Exception e) {
		e.printStackTrace();
        CommonLogger.log(Level.SEVERE, e.getMessage());
		Toast.makeText(getApplicationContext(), "ERROR in MAPTab.onResume " + e.getMessage(), Toast.LENGTH_LONG).show();
	  }
	  super.onResume();
	}
	
	@Override
	public void onPause() {
	  try {
		  CommonLogger.log(Level.INFO);
		// Make sure you unregister your receivers when you pause your activity
		if(isReceiversRegistered) {
			unregisterReceiver(intentReceiver);
			isReceiversRegistered = false;
		}
	  }
	  catch (Exception e) {
		e.printStackTrace();
        CommonLogger.log(Level.SEVERE, e.getMessage());
		Toast.makeText(getApplicationContext(), "ERROR in MAPTab.onPause " + e.getMessage(), Toast.LENGTH_LONG).show();
	  }
	  super.onPause();
	}


	/**
	 * 01/15/2013
	 */
    @Override
    public void onBackPressed() {
    	this.getParent().onBackPressed();
    }

    public String getLoadedMapFile() {
		return loadedMapFile;
	}
    
    public void setLoadedMapFile(String loadedMapFile) {
		this.loadedMapFile = loadedMapFile;
	}

}
