package com.lkovari.mobile.apps.gtl.map.filter;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

public class SGOptimizer {
	   
 //adapted from http://geometryalgorithms.com/Archive/algorithm_0205/algorithm_0205.htm
 //original copyright message follows
 
 // Copyright 2002, softSurfer (www.softsurfer.com)
 // This code may be freely used and modified for any purpose
 // providing that this copyright notice is included with it.
 // SoftSurfer makes no warranty for this code, and cannot be held
 // liable for any real or imagined damage resulting from its use.
 // Users of this code must verify correctness for their application.
         
 /**
  * This method will reduce a 2D complex polyline.
  * @param tol the tolerance of the reduction algorithm. Higher numbers will simplify the line more.
  * @param V the array of Tuple2fs to be simplified
  * @return an array of Tuple2f representing the simplified polyline
  */
 public static List<LocPts> simplifyLine2D(float tol, List<LocPts> points) {
     
     int n = points.size();
     
     int i, k, m, pv;
     float tol2 = tol*tol;
     
     List<LocPts> vt = new ArrayList<LocPts>();
     
     int [] mk = new int[n];
     
     Vector sV = new Vector();
     
     for (int b = 0; b < n; b++){
         mk[b] = 0;
     }
     
     //STAGE 1 simple vertex reduction
     vt.set(0, points.get(0));
     
     for (i = k =1, pv = 0; i < n; i++){
         if (points.get(i).distanceSquared(points.get(pv)) < tol2)
             continue;
         vt.set(k++, points.get(i));
         pv = i;
     }
     
     if (pv < n-1)
    	 vt.set(k++, points.get(n-1));
     
     //STAGE 2 Douglas-Peucker polyline simplify
     //mark the first and last vertices
     mk[0] = mk[k-1] = 1;
     simplifyDP2D(tol, vt, 0, k-1, mk);
     
     //copy marked vertices to output
     for (i=m=0; i<k; i++) {
         if (mk[i] == 1)
             sV.add(vt.get(i));
     }
     
     List<LocPts> out = new ArrayList<LocPts>();
     out.addAll(sV);
     sV = null;
     return out;
     
 }
 
 private static void simplifyDP2D(float tol, List<LocPts> v, int j, int k, int [] mk) {
     
     if (k <= j + 1) 
     	return;  
     
     int maxi = j;
     float maxd2 = 0;
     float tol2 = tol*tol;
     //Seg S = new Seg(v[j], v[k]);
     
     LocPts u = v.get(k).minus(v.get(j));
     float cu = u.dot(u);
     
     LocPts w;
     LocPts Pb;
     float b, cw, dv2;
     
     for (int i=j+1; i < k; i++ ){
         w = v.get(i).minus(v.get(j));
         cw = w.dot(u);
         if (cw <= 0)
             dv2 = v.get(i).distanceSquared(v.get(j));
         else if (cu <= cw)
             dv2 = v.get(i).distanceSquared(v.get(k));
         else{
             b = cw/cu;
             Pb= v.get(j).minus(u.times(-b));
             dv2 = v.get(i).distanceSquared(Pb);
             
         }
         
         if (dv2 <= maxd2)
             continue;
         maxi = i;
         maxd2 = dv2;
     }
     if (maxd2 > tol2){
         mk[maxi] = 1;
         simplifyDP2D(tol,v,j,maxi,mk);
         simplifyDP2D(tol,v,maxi,k,mk);
         
     }
     return;
     
 }
 
}
