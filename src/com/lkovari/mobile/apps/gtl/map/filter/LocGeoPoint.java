package com.lkovari.mobile.apps.gtl.map.filter;

import com.google.android.maps.GeoPoint;

public class LocGeoPoint extends GeoPoint implements LocPts {

	public LocGeoPoint(int latitudeE6, int longitudeE6) {
		super(latitudeE6, longitudeE6);
	}

	@Override
	public float distanceSquared(LocPts otherLocPts) {
        return (getLatitudeE6() - ((LocGeoPoint)otherLocPts).getLatitudeE6()) * (getLatitudeE6() - ((LocGeoPoint)otherLocPts).getLatitudeE6()) + (getLongitudeE6() - ((LocGeoPoint)otherLocPts).getLongitudeE6()) * (getLongitudeE6() - ((LocGeoPoint)otherLocPts).getLongitudeE6());        
	}

	@Override
	public float length() {
        return Math.round(Math.sqrt(getLatitudeE6() * getLatitudeE6() + getLongitudeE6() * getLongitudeE6()));        
	}

	@Override
	public LocPts minus(LocPts locPts) {
    	return new LocGeoPoint(getLatitudeE6() - ((LocGeoPoint)locPts).getLatitudeE6(), getLongitudeE6() - ((LocGeoPoint)locPts).getLongitudeE6());
	}

	@Override
	public LocPts plus(LocPts locPts) {
    	return new LocGeoPoint(getLatitudeE6() + ((LocGeoPoint)locPts).getLatitudeE6(), getLongitudeE6() + ((LocGeoPoint)locPts).getLongitudeE6());
	}

	@Override
	public float dot(LocPts locPts) {
    	return (getLatitudeE6() * ((LocGeoPoint)locPts).getLatitudeE6()) + (getLongitudeE6() * ((LocGeoPoint)locPts).getLongitudeE6());  
	}

	@Override
	public LocPts times(float a) {
    	int lat = Math.round(this.getLatitudeE6() * a);
    	int lon = Math.round(this.getLongitudeE6() * a);
		return new LocGeoPoint(lat,  lon);
	}

}
