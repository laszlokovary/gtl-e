package com.lkovari.mobile.apps.gtl.map.filter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.google.android.maps.GeoPoint;
import com.lkovari.mobile.apps.gtl.track.GPSCoordinate;

/**
 * 
 * @author lkovari
 * http://www.codeproject.com/Articles/18936/A-C-Implementation-of-Douglas-Peucker-Line-Approxi
 * http://www.codeproject.com/Articles/1711/A-C-implementation-of-Douglas-Peucker-Line-Approxi
 * http://vault42.org/path-optimization-using-the-douglas-peucker-line-approximation-algorithm/
 */
public class DouglasPeuckerOptimizer {

	/**
	 * 
	 * @param points
	 * @param tolerance
	 * @return
	 */
    public static List<GPSCoordinate> reductionGPSCoordinate(List<GPSCoordinate> points, double tolerance) {
        if (points == null || points.size() < 3)
            return points;

        int firstPoint = 0;
        int lastPoint = points.size() - 1;
        List<Integer> pointIndexsToKeep = new ArrayList<Integer>();

        //Add the first and last index to the keepers
        pointIndexsToKeep.add(firstPoint);
        pointIndexsToKeep.add(lastPoint);


        //The first and the last point can not be the same
        while (points.get(firstPoint).equals(points.get(lastPoint))) {
            lastPoint--;
        }

        reductionGPSCoordinate(points, firstPoint, lastPoint, tolerance, pointIndexsToKeep);

        List<GPSCoordinate> returnPoints = new ArrayList<GPSCoordinate>();
        Collections.sort(pointIndexsToKeep);
        for (Integer index : pointIndexsToKeep) {
            returnPoints.add(points.get(index));
        }
        return returnPoints;
    }
    
    /**
     * 
     * @param points
     * @param firstPoint
     * @param lastPoint
     * @param tolerance
     * @param pointIndexsToKeep
     */
    private static void reductionGPSCoordinate(List<GPSCoordinate> points, int firstPoint, int lastPoint, double tolerance, List<Integer> pointIndexsToKeep)  {
        double maxDistance = 0;
        int indexFarthest = 0;

        for (Integer index = firstPoint; index < lastPoint; index++) {
//        	GPSCoordinate gpsCoordinate = points.get(index);
//        	boolean isSpeedZero = (gpsCoordinate.getLocation().getSpeed() == 0);
            double distance = perpendicularDistanceGPSCoordinate(points.get(firstPoint), points.get(lastPoint), points.get(index));
            if (distance > maxDistance) {
                maxDistance = distance;
                indexFarthest = index;
            }
        }

        if ((maxDistance > tolerance) && (indexFarthest != 0))  {
            //Add the largest point that exceeds the tolerance
            pointIndexsToKeep.add(indexFarthest);

            reductionGPSCoordinate(points, firstPoint, indexFarthest, tolerance, pointIndexsToKeep);
            reductionGPSCoordinate(points, indexFarthest, lastPoint, tolerance, pointIndexsToKeep);
        }
    }
    
	/**
	 * 
	 * @param point1
	 * @param point2
	 * @param point
	 * @return
	 */
    public static double perpendicularDistanceGPSCoordinate(GPSCoordinate point1, GPSCoordinate point2, GPSCoordinate point) {
    	double point1X = point1.getLatitudeE6();
    	double point1Y = point1.getLongitudeE6();
    	double point2X = point2.getLatitudeE6();
    	double point2Y = point2.getLongitudeE6();
    	double pointX = point.getLatitudeE6();
    	double pointY = point.getLongitudeE6();
    	
        double area = Math.abs(0.5 * (point1X * point2Y + point2X * pointY + pointX * point1Y - point2X * point1Y - pointX * point2Y - point1X * pointY));
        double bottom = Math.sqrt(Math.pow(point1X - point2X, 2) + Math.pow(point1Y - point2Y, 2));
        double height = area / bottom * 2;

        return height;
    }
    
    
	/**
	 * Uses the Douglas Peucker algorithim to reduce the number of points.
	 * @param points - points
	 * @param tolerance - the tolerance 
	 * @return
	 */
    public static List<GeoPoint> reductionGeoPoint(List<GeoPoint> points, double tolerance) {

        if (points == null || points.size() < 3)
            return points;

        int firstPoint = 0;
        int lastPoint = points.size() - 1;
        List<Integer> pointIndexsToKeep = new ArrayList<Integer>();

        //Add the first and last index to the keepers
        pointIndexsToKeep.add(firstPoint);
        pointIndexsToKeep.add(lastPoint);


        //The first and the last point can not be the same
        while (points.get(firstPoint).equals(points.get(lastPoint))) {
            lastPoint--;
        }

        reductionGeoPoint(points, firstPoint, lastPoint, tolerance, pointIndexsToKeep);

        List<GeoPoint> returnPoints = new ArrayList<GeoPoint>();
        Collections.sort(pointIndexsToKeep);
        for (Integer index : pointIndexsToKeep) {
            returnPoints.add(points.get(index));
        }
        return returnPoints;
    }

    /**
     * 
     * @param points - the points
     * @param firstPoint - the first point
     * @param lastPoint - the last point
     * @param tolerance - tolerance
     * @param pointIndexsToKeep - the point indexes to keep
     */
    private static void reductionGeoPoint(List<GeoPoint> points, int firstPoint, int lastPoint, double tolerance, List<Integer> pointIndexsToKeep)  {
        double maxDistance = 0;
        int indexFarthest = 0;

        for (Integer index = firstPoint; index < lastPoint; index++) {
            double distance = perpendicularDistanceGeoPoint(points.get(firstPoint), points.get(lastPoint), points.get(index));
            if (distance > maxDistance) {
                maxDistance = distance;
                indexFarthest = index;
            }
        }

        if ((maxDistance > tolerance) && (indexFarthest != 0))  {
            //Add the largest point that exceeds the tolerance
            pointIndexsToKeep.add(indexFarthest);

            reductionGeoPoint(points, firstPoint, indexFarthest, tolerance, pointIndexsToKeep);
            reductionGeoPoint(points, indexFarthest, lastPoint, tolerance, pointIndexsToKeep);
        }
    }
    
    /**
     * The distance of a point from a line made from point1 and point2.
     * @param point1 - point 1
     * @param point2 - point 2
     * @param point -  the p
     * @return
     */
    public static double perpendicularDistanceGeoPoint(GeoPoint point1, GeoPoint point2, GeoPoint point) {
        //Area = |(1/2)(x1y2 + x2y3 + x3y1 - x2y1 - x3y2 - x1y3)|   *Area of triangle
        //Base = √((x1-x2)²+(x1-x2)²)                               *Base of Triangle*
        //Area = .5*Base*H                                          *Solve for height
        //Height = Area/.5/Base

    	double point1X = point1.getLatitudeE6();
    	double point1Y = point1.getLongitudeE6();
    	double point2X = point2.getLatitudeE6();
    	double point2Y = point2.getLongitudeE6();
    	double pointX = point.getLatitudeE6();
    	double pointY = point.getLongitudeE6();
    	
        double area = Math.abs(0.5 * (point1X * point2Y + point2X * pointY + pointX * point1Y - point2X * point1Y - pointX * point2Y - point1X * pointY));
        double bottom = Math.sqrt(Math.pow(point1X - point2X, 2) + Math.pow(point1Y - point2Y, 2));
        double height = area / bottom * 2;

        return height;

        //Another option
        //Double A = Point.X - Point1.X;
        //Double B = Point.Y - Point1.Y;
        //Double C = Point2.X - Point1.X;
        //Double D = Point2.Y - Point1.Y;

        //Double dot = A * C + B * D;
        //Double len_sq = C * C + D * D;
        //Double param = dot / len_sq;

        //Double xx, yy;

        //if (param < 0)
        //{
        //    xx = Point1.X;
        //    yy = Point1.Y;
        //}
        //else if (param > 1)
        //{
        //    xx = Point2.X;
        //    yy = Point2.Y;
        //}
        //else
        //{
        //    xx = Point1.X + param * C;
        //    yy = Point1.Y + param * D;
        //}

        //Double d = DistanceBetweenOn2DPlane(Point, new Point(xx, yy));

    }
    
    
    public static List<org.mapsforge.core.GeoPoint> reductionGeoPointMF(List<org.mapsforge.core.GeoPoint> points, double tolerance) {

        if (points == null || points.size() < 3)
            return points;

        int firstPoint = 0;
        int lastPoint = points.size() - 1;
        List<Integer> pointIndexsToKeep = new ArrayList<Integer>();

        //Add the first and last index to the keepers
        pointIndexsToKeep.add(firstPoint);
        pointIndexsToKeep.add(lastPoint);


        //The first and the last point can not be the same
        while (points.get(firstPoint).equals(points.get(lastPoint))) {
            lastPoint--;
        }

        reductionGeoPointMF(points, firstPoint, lastPoint, tolerance, pointIndexsToKeep);

        List<org.mapsforge.core.GeoPoint> returnPoints = new ArrayList<org.mapsforge.core.GeoPoint>();
        Collections.sort(pointIndexsToKeep);
        for (Integer index : pointIndexsToKeep) {
            returnPoints.add(points.get(index));
        }
        return returnPoints;
    }

    /**
     * 
     * @param points - the points
     * @param firstPoint - the first point
     * @param lastPoint - the last point
     * @param tolerance - tolerance
     * @param pointIndexsToKeep - the point indexes to keep
     */
    private static void reductionGeoPointMF(List<org.mapsforge.core.GeoPoint> points, int firstPoint, int lastPoint, double tolerance, List<Integer> pointIndexsToKeep)  {
        double maxDistance = 0;
        int indexFarthest = 0;

        for (Integer index = firstPoint; index < lastPoint; index++) {
            double distance = perpendicularDistanceGeoPointMF(points.get(firstPoint), points.get(lastPoint), points.get(index));
            if (distance > maxDistance) {
                maxDistance = distance;
                indexFarthest = index;
            }
        }

        if ((maxDistance > tolerance) && (indexFarthest != 0))  {
            //Add the largest point that exceeds the tolerance
            pointIndexsToKeep.add(indexFarthest);

            reductionGeoPointMF(points, firstPoint, indexFarthest, tolerance, pointIndexsToKeep);
            reductionGeoPointMF(points, indexFarthest, lastPoint, tolerance, pointIndexsToKeep);
        }
    }
    
    /**
     * The distance of a point from a line made from point1 and point2.
     * @param point1 - point 1
     * @param point2 - point 2
     * @param point -  the p
     * @return
     */
    public static double perpendicularDistanceGeoPointMF(org.mapsforge.core.GeoPoint point1, org.mapsforge.core.GeoPoint point2, org.mapsforge.core.GeoPoint point) {
        //Area = |(1/2)(x1y2 + x2y3 + x3y1 - x2y1 - x3y2 - x1y3)|   *Area of triangle
        //Base = √((x1-x2)²+(x1-x2)²)                               *Base of Triangle*
        //Area = .5*Base*H                                          *Solve for height
        //Height = Area/.5/Base

    	double point1X = point1.getLatitude() * 1E6;
    	double point1Y = point1.getLongitude() * 1E6;
    	double point2X = point2.getLatitude() * 1E6;
    	double point2Y = point2.getLongitude() * 1E6;
    	double pointX = point.getLatitude() * 1E6;
    	double pointY = point.getLongitude() * 1E6;
    	
        double area = Math.abs(0.5 * (point1X * point2Y + point2X * pointY + pointX * point1Y - point2X * point1Y - pointX * point2Y - point1X * pointY));
        double bottom = Math.sqrt(Math.pow(point1X - point2X, 2) + Math.pow(point1Y - point2Y, 2));
        double height = area / bottom * 2;

        return height;

        //Another option
        //Double A = Point.X - Point1.X;
        //Double B = Point.Y - Point1.Y;
        //Double C = Point2.X - Point1.X;
        //Double D = Point2.Y - Point1.Y;

        //Double dot = A * C + B * D;
        //Double len_sq = C * C + D * D;
        //Double param = dot / len_sq;

        //Double xx, yy;

        //if (param < 0)
        //{
        //    xx = Point1.X;
        //    yy = Point1.Y;
        //}
        //else if (param > 1)
        //{
        //    xx = Point2.X;
        //    yy = Point2.Y;
        //}
        //else
        //{
        //    xx = Point1.X + param * C;
        //    yy = Point1.Y + param * D;
        //}

        //Double d = DistanceBetweenOn2DPlane(Point, new Point(xx, yy));

    }
    
}
