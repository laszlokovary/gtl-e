package com.lkovari.mobile.apps.gtl.map.filter;

public class Tuple3f {
    
    public float x=0,y=0,z=0;
    
    public Tuple3f(){
    }
    
    public float getAxis(int axis){
        switch (axis){
            case 0: return x;
            case 1: return y;
            case 2: return z;
            
        }
        return -1;
    }
    public void setAxis(int axis,float val){
       
        switch (axis){
            case 0: x = val;
                break;
            case 1: y = val;
                break;
            case 2: z = val;
                break;
    
        }

    }
    
    /** Creates a new instance of Tuple3f */
    public Tuple3f(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }
    
    public float dot(Tuple3f a){
        return (x*a.x) + (y*a.y) + (z*a.z);
    }
    
    /*  Subtracts two tuples and return the result in a new Tuple 
     *
     */
    public Tuple3f minus(Tuple3f a){
        return new Tuple3f(x-a.x,y-a.y,z-a.z);
    }
    /*  Subtracts two tuples and return the result in a new Tuple 
     *
     */
    public Tuple3f plus(Tuple3f a){
        return new Tuple3f(x+a.x,y+a.y,z+a.z);
    }
    
    public Tuple3f times(float a){
        return new Tuple3f(x*a,y*a,z*a);
    }
    
    public void divideEquals(float a){
       
       x/=a;
       y/=a;
       z/=a;
    }
    public float distanceSquared(Tuple3f other){
        return (x-other.x)*(x-other.x) + (y-other.y)*(y-other.y) + (z-other.z)*(z-other.z);        
    }
    public float distance(Tuple3f other){
        return (float)Math.sqrt((x-other.x)*(x-other.x) + (y-other.y)*(y-other.y) + (z-other.z)*(z-other.z));        
    }
    
    /* Normalizes in place.
     *
     */
    public void normalize(){
        float length = 1.f/getLength();
        x *= length;
        y *= length;
        z *= length;
    }
    
    public float getLength(){
        return (float)Math.sqrt(x*x + y*y + z*z);
    }
    public float getLengthSquared(){
        return (x*x + y*y + z*z);
    }
    
    public void plusEquals(Tuple3f rhs){
        x += rhs.x;
        y += rhs.y;
        z += rhs.z;
    }
    public void minusEquals(Tuple3f rhs){
        x -= rhs.x;
        y -= rhs.y;
        z -= rhs.z;
    }
    
    public void timesEquals(float rhs){
        x *= rhs;
        y *= rhs;
        z *= rhs;
    }    
    
    public void cross(Tuple3f v2, Tuple3f dest){
	dest.x = (y * v2.z) - (z * v2.y);
	dest.y = (z * v2.x) - (x * v2.z);
	dest.z = (x * v2.y) - (y * v2.x);
    }
    public Tuple3f cross(Tuple3f v2){
	return new Tuple3f((y * v2.z) - (z * v2.y),
            (z * v2.x) - (x * v2.z),
            (x * v2.y) - (y * v2.x));
    }
    
    public boolean greaterThan(Tuple3f rhs){
     	if (x>=rhs.x && y>=rhs.y && z >= rhs.z){
		return true;
	}
	else{
		return false;
	}   
    }
    public boolean lessThan(Tuple3f rhs){
 	if (x<=rhs.x && y<= rhs.y && z <= rhs.z){
		return true;
	}
	else{
		return false;
	}   
    }
    
    public Tuple3f getCopy(){
        return new Tuple3f(x,y,z);
    }
    
    public Tuple3f reflect(Tuple3f normal){
    
        return this.minus(normal.times(2*this.dot(normal)));
    
    }
    public String toString(){
        return "x:" + x + " y:" + y + " z:" + z;
    }
    
    /*
     *Rotate this vector around another vector in place
     */
    public void rotate(Tuple3f axis, float angle){
        
        //yeah, I don't like rotation matrices
        //but I loooooooooooooove 3D trig!
        Tuple3f ret = this.minus(axis.times(axis.dot(this)).times((float)Math.cos(angle)));
        ret.plusEquals(this.cross(axis).times((float)Math.sin(angle)));
        ret.plusEquals(axis.times(axis.dot(this)));
        ret.normalize();
        x = ret.x;
        y = ret.y;
        z = ret.z;
        
    }
    /*
     *Rotate this vector around another vector in place
     */
    public void rotateZ(float theta){
        
        float tx = (float)(x*Math.cos(theta) + y*Math.sin(theta));
        y = (float)(-x*Math.sin(theta) + y*Math.cos(theta));
        x = tx;
        
    }
    
    }
