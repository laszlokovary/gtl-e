package com.lkovari.mobile.apps.gtl.map.filter;

import com.google.android.maps.GeoPoint;

public class Tuple2f extends GeoPoint {
    
    /**
     * Creates a new instance of Tuple2f
     * @param x 
     * @param y 
     */
    public Tuple2f(int x, int y) {
    	super(x, y);
    }
    
    
    
    /**
     * Finds the squared distance between two Tuples. This is useful when comparing
     * distances because it avoids a square root.
     * @param other 
     * @return squared distance
     */
    public float distanceSquared(Tuple2f other){
        return (getLatitudeE6() - other.getLatitudeE6()) * (getLatitudeE6() - other.getLatitudeE6()) + (getLongitudeE6() - other.getLongitudeE6()) * (getLongitudeE6() - other.getLongitudeE6());        
    }

    
    public float length(){
        return (float)Math.sqrt(getLatitudeE6() * getLatitudeE6() + getLongitudeE6() * getLongitudeE6());        
    }    
    
    /**
     * Subtract two tuples and return the value in a new Tuple
     * @param a 

     */
    public Tuple2f minus(Tuple2f a){
    	return new Tuple2f(getLatitudeE6() - a.getLatitudeE6(), getLongitudeE6() - a.getLongitudeE6());
    }
    /**
     * Add two tuples and return the value in a new Tuple
     * @param a 

     */
    public Tuple2f plus( Tuple2f a){
    	return new Tuple2f(getLatitudeE6() + a.getLatitudeE6(), getLongitudeE6() + a.getLongitudeE6());
    }  
    
    
    /**
     * Returns the dot product of two Tuples.  Generally, this can b interpreted as the
     * angle between them.
     * @param a compare to this Tuple
     * @return the dot product
     */
    public float dot( Tuple2f a){
    	return (getLatitudeE6() * a.getLatitudeE6()) + (getLongitudeE6() * a.getLongitudeE6());  
    }
  
    public Tuple2f times(float a){
    	int mx = Math.round(getLatitudeE6() * a);
    	int my = Math.round(getLongitudeE6() * a);
    	return new  Tuple2f(mx , my);
    }    
  }
