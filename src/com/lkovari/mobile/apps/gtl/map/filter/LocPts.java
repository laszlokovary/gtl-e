package com.lkovari.mobile.apps.gtl.map.filter;

/**
 * 
 * @author lkovari
 *
 */
public interface LocPts {
	public float distanceSquared(LocPts otherLocPts);
	public float length();
	public LocPts minus(LocPts locPts);
	public LocPts plus(LocPts locPts);
	public float dot(LocPts locPts);
	public LocPts times(float a);
}
