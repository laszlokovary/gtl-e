package com.lkovari.mobile.apps.gtl.map;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.lkovari.mobile.apps.gtl.R;
import com.lkovari.mobile.apps.gtl.utils.sdcard.SDCardManager;

/**
 * #1/10/2014 #10 v1.02.7
 * @author lkovari
 *
 */
public class LogSelector extends Activity {
	private ListView logsListView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.tracklogselector);
		
		logsListView = (ListView) findViewById( R.id.logListView);
	
		TextView mapSelectorTitle = (TextView) findViewById(R.id.logselector_titleTextView);
		String title = getResources().getString(R.string.logselector_select_desired_log);
		mapSelectorTitle.setText(title);
		TextView mapSelectorSubtitle = (TextView) findViewById(R.id.logselector_subtitleTextView);
		String subtitle = getResources().getString(R.string.logselector_show_in_ge);
		mapSelectorSubtitle.setText(subtitle);
		
		List<String> kmlFileList = SDCardManager.loadLogFilesPath(LogSelector.this, ".kml");
        // #01/01/2015 #10 v1.02.7
        if ((kmlFileList != null) && (kmlFileList.size() > 0)) {
        	//# 01/01/2015 #10 v1.02.7
        	/* removed 10/23/2015 #12 v1.02.9
        	Collections.sort(kmlFileList, new Comparator<String>() {
        		@Override
        		public int compare(String lhs, String rhs) {
        			return rhs.compareTo(lhs);
        		}
        	});
        	*/
            String[] lognameArray = null;
            lognameArray = new String[kmlFileList.size()];
            int ix = 0;
            for (String logName : kmlFileList) {
                lognameArray[ix] = logName;
                ix++;
            }
            // Binding resources Array to ListAdapter
            logsListView.setAdapter(new ArrayAdapter<String>(this, R.layout.map_list_item, R.id.label, lognameArray));

            logsListView.setOnItemClickListener(new OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> listView, View view, int position, long arg3) {
                    Object o = listView.getItemAtPosition(position);
                    if (o != null) {
                        if (o instanceof String) {
                            String logFileName = (String)o;
                            ShowKMLInGoogleEarth.showKML(LogSelector.this, logFileName);
                        }
                    }
                }
            });
        }
        else {
            Toast.makeText(this, R.string.error_no_stored_routes_found, Toast.LENGTH_LONG).show();
        }
	}
}
