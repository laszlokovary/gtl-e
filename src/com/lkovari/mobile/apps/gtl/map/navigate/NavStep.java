package com.lkovari.mobile.apps.gtl.map.navigate;

import java.util.List;

import com.google.android.maps.GeoPoint;

/**
 * #8 v1.02.5 03/08/2014 
 * @author lkovari
 *
 */
public class NavStep {
	private NavDistance navDistance;
	private NavDuration navDuration;
	private NavPos endLocation;
	private String htmlInstruction;
	private String maneuver;
	private List<GeoPoint> polyLine;
	private NavPos startLocation;
	private String travelMode;
	
	public NavDistance getNavDistance() {
		return navDistance;
	}
	
	public NavDuration getNavDuration() {
		return navDuration;
	}
	
	public NavPos getStartLocation() {
		return startLocation;
	}
	
	public NavPos getEndLocation() {
		return endLocation;
	}
	
	public String getHtmlInstruction() {
		return htmlInstruction;
	}
	
	public String getManeuver() {
		return maneuver;
	}
	
	public String getTravelMode() {
		return travelMode;
	}
	
	public List<GeoPoint> getPolyLine() {
		return polyLine;
	}
	
	public void setNavDistance(NavDistance navDistance) {
		this.navDistance = navDistance;
	}
	
	public void setNavDuration(NavDuration navDuration) {
		this.navDuration = navDuration;
	}
	
	public void setStartLocation(NavPos startLocation) {
		this.startLocation = startLocation;
	}
	
	public void setEndLocation(NavPos endLocation) {
		this.endLocation = endLocation;
	}
	
	public void setHtmlInstruction(String htmlInstruction) {
		this.htmlInstruction = htmlInstruction;
	}
	
	public void setManeuver(String maneuver) {
		this.maneuver = maneuver;
	}
	
	public void setTravelMode(String travelMode) {
		this.travelMode = travelMode;
	}
	
	public void setPolyLine(List<GeoPoint> polyLine) {
		this.polyLine = polyLine;
	}
}
