package com.lkovari.mobile.apps.gtl.map.navigate;

import java.util.List;

import com.google.android.maps.GeoPoint;

/**
 * #8 v1.02.5 03/08/2014 
 * @author lkovari
 *
 */
public class NavRoute {
	private NavBounds navBounds;
	private String copyright;
	private List<NavLeg> navLegs;
	private List<GeoPoint> overlayPolyline;
	
	public NavBounds getNavBounds() {
		return navBounds;
	}
	
	public String getCopyright() {
		return copyright;
	}
	
	public List<NavLeg> getNavLegs() {
		return navLegs;
	}
	
	public List<GeoPoint> getOverlayPolyline() {
		return overlayPolyline;
	}
	
	public void setNavBounds(NavBounds navBounds) {
		this.navBounds = navBounds;
	}
	
	public void setCopyright(String copyright) {
		this.copyright = copyright;
	}
	
	public void setNavLegs(List<NavLeg> navLegs) {
		this.navLegs = navLegs;
	}
	
	public void setOverlayPolyline(List<GeoPoint> overlayPolyline) {
		this.overlayPolyline = overlayPolyline;
	}
}
