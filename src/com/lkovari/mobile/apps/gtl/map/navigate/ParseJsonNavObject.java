package com.lkovari.mobile.apps.gtl.map.navigate;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xml.sax.DTDHandler;
import org.xml.sax.DocumentHandler;
import org.xml.sax.EntityResolver;
import org.xml.sax.ErrorHandler;
import org.xml.sax.InputSource;
import org.xml.sax.Parser;
import org.xml.sax.SAXException;

import com.google.android.maps.GeoPoint;

/**
 * #8 v1.02.5 03/08/2014 
 * @author lkovari
 *
 */
public class ParseJsonNavObject extends XMLParser implements Parser{

	protected ParseJsonNavObject(String feedUrl) {
		super(feedUrl);
	}

	public List<NavRoute> parseJSonObject(JSONObject jsonObject) throws JSONException {
		List<NavRoute> navRoutes = null;
		String status = jsonObject.getString("status").toString();	
		if (status.equalsIgnoreCase("OK")) {
			navRoutes = new ArrayList<NavRoute>();	
			JSONArray routesArray = jsonObject.getJSONArray("routes");
			if ((routesArray != null) && (routesArray.length() > 0)) {
				// Grab the first route
				JSONObject route = routesArray.getJSONObject(0);
				if (route != null) {
					NavRoute navRoute = new NavRoute();
					JSONObject bounds = route.getJSONObject("bounds");
					if (bounds != null) {
						NavBounds navBounds = new NavBounds();
						JSONObject northEast = bounds.getJSONObject("northeast");
						if (northEast != null) {
							double lat = Double.parseDouble((String) northEast.get("lat"));
							double lng = Double.parseDouble((String) northEast.get("lng"));
							GeoPoint neGp = new GeoPoint((int)(lat * 1E6), (int)(lng * 1E6));
							navBounds.setNorthEast(neGp);
						}
						JSONObject southWest = bounds.getJSONObject("southwest");
						if (northEast != null) {
							double lat = Double.parseDouble((String) southWest.get("lat"));
							double lng = Double.parseDouble((String) southWest.get("lng"));
							GeoPoint seGp = new GeoPoint((int)(lat * 1E6), (int)(lng * 1E6));
							navBounds.setSouthWest(seGp);
						}
						navRoute.setNavBounds(navBounds);
					}
					String copyright = route.getString("copyrights");
					if (copyright != null) {
						navRoute.setCopyright(copyright);
					}	
					
					// Take all legs from the route
					JSONArray legs = route.getJSONArray("legs");
					
				}
				
//				navRoutes.add(navRoute);
			}
			
			// Grab the first route
			JSONObject route = routesArray.getJSONObject(0);				
			// Take all legs from the route
			JSONArray legs = route.getJSONArray("legs");
			// Grab first leg
			JSONObject leg = legs.getJSONObject(0);
			JSONObject distanceObject = leg.getJSONObject("distance");
			String distance = distanceObject.getString("text");				
			JSONObject durationObject = leg.getJSONObject("duration");
			String duration = durationObject.getString("text");			
			JSONObject overviewPolylines = route.getJSONObject("overview_polyline");
			String encodedString = overviewPolylines.getString("points");			
			
		}
		return navRoutes;
	}

	@Override
	public void parse(InputSource source) throws SAXException, IOException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void parse(String systemId) throws SAXException, IOException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setDTDHandler(DTDHandler handler) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setDocumentHandler(DocumentHandler handler) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEntityResolver(EntityResolver resolver) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setErrorHandler(ErrorHandler handler) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setLocale(Locale locale) throws SAXException {
		// TODO Auto-generated method stub
		
	}
	
	
}
