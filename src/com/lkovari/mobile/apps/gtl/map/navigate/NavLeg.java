package com.lkovari.mobile.apps.gtl.map.navigate;

import java.util.List;

/**
 * #8 v1.02.5 03/08/2014 
 * @author lkovari
 *
 */
public class NavLeg {
	private NavDistance navDistance;
	private NavDuration navDuration;
	private NavPos startAddress;
	private NavPos endAddress;
	private List<NavStep> navSteps;

	public NavDistance getNavDistance() {
		return navDistance;
	}
	
	public NavDuration getNavDuration() {
		return navDuration;
	}
	
	public NavPos getStartAddress() {
		return startAddress;
	}
	
	public NavPos getEndAddress() {
		return endAddress;
	}
	
	public List<NavStep> getNavSteps() {
		return navSteps;
	}
	
	public void setNavDistance(NavDistance navDistance) {
		this.navDistance = navDistance;
	}
	
	public void setNavDuration(NavDuration navDuration) {
		this.navDuration = navDuration;
	}
	
	public void setStartAddress(NavPos startAddress) {
		this.startAddress = startAddress;
	}
	
	public void setEndAddress(NavPos endAddress) {
		this.endAddress = endAddress;
	}
	
	public void setNavSteps(List<NavStep> navSteps) {
		this.navSteps = navSteps;
	}
}
