package com.lkovari.mobile.apps.gtl.map.navigate;

/**
 * #8 v1.02.5 03/08/2014 
 * @author lkovari
 *
 */
public class NavPos {
	private String address;
	private double lat;
	private double lng;
	
	public NavPos(String addr, double lat, double lng) {
		this.address = addr;
		this.lat = lat;
		this.lng = lng;
	}
	
	public String getAddress() {
		return address;
	}
	
	public double getLat() {
		return lat;
	}
	
	public double getLng() {
		return lng;
	}
}
