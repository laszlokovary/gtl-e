package com.lkovari.mobile.apps.gtl.map.navigate;

/**
 * #8 v1.02.5 03/08/2014 
 * @author lkovari
 *
 */
public class NavDuration {
	private String duration;
	private int minutes;
	
	public NavDuration(String duration, int minutes) {
		this.duration = duration;
		this.minutes = minutes;
	}
	
	public String getDuration() {
		return duration;
	}
	
	public int getMinutes() {
		return minutes;
	}
}