package com.lkovari.mobile.apps.gtl.map.navigate;

import com.google.android.maps.GeoPoint;

/**
 * #8 v1.02.5 03/08/2014 
 * @author lkovari
 *
 */
public class NavBounds {
	private GeoPoint northEast;
	private GeoPoint southWest;
	
	public GeoPoint getNorthEast() {
		return northEast;
	}
	
	public GeoPoint getSouthWest() {
		return southWest;
	}
	
	public void setNorthEast(GeoPoint northEast) {
		this.northEast = northEast;
	}
	
	public void setSouthWest(GeoPoint southWest) {
		this.southWest = southWest;
	}
}
