package com.lkovari.mobile.apps.gtl.map.navigate;

/**
 * #8 v1.02.5 03/08/2014 
 * @author lkovari
 *
 */
public class NavDistance {
	private String distance;
	private int meters;
	
	public NavDistance(String distance, int meters) {
		this.distance = distance;
		this.meters = meters;
	}
	
	public String getDistance() {
		return distance;
	}
	
	public int getMeters() {
		return meters;
	}
}
