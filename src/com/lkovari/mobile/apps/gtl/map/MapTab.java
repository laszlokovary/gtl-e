package com.lkovari.mobile.apps.gtl.map;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.Toast;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.ItemizedOverlay;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
import com.google.android.maps.OverlayItem;
import com.google.android.maps.Projection;
import com.lkovari.mobile.apps.gtl.Main;
import com.lkovari.mobile.apps.gtl.R;
import com.lkovari.mobile.apps.gtl.map.filter.DouglasPeuckerOptimizer;
import com.lkovari.mobile.apps.gtl.settings.GPSSettings;
import com.lkovari.mobile.apps.gtl.track.GPSCoordinate;
import com.lkovari.mobile.apps.gtl.utils.GUIDataIdentifiers;
import com.lkovari.mobile.apps.gtl.utils.error.ErrorHandler;
import com.lkovari.mobile.apps.gtl.utils.logger.CommonLogger;
import com.lkovari.mobile.apps.gtl.utils.sdcard.SDCardManager;



/**
 * 
 * @author lkovari
 *
 */
public class MapTab extends MapActivity {
	private boolean isReceiversRegistered = false;
    private MapView mapView;
    private MapController mapController;
    private GeoPoint currentGeoPoint = null;
	private List<GeoPoint> routePoints = new ArrayList<GeoPoint>();
	private MarkerOverlay markerOverlay;
	private RouteOverlay routeOverlay;
    private AccuracyOverlay accuracyOverlay;
    // 18/05/2013
    private RoutePathOverlay routePathOverlay;
    private boolean isRoutePathOverlayAdded = false;
    
    private Projection projection;  
    private int maxZoomLevel = -1;
    private int zoomLevel = -1;
    private boolean isMarkPressed = false;
//    private Canvas originClearCanvas = null;
    private boolean isFirstLocation = true;
    private int posCnt = 0;
    private float lastAccuracy;
    private boolean needToShowAccuracyMarker = false;
    private float currentSpeed;
    private boolean isSaveView = false;
    private Bitmap viewBitmap = null;
    
	// Define a handler and a broadcast receiver
	private final Handler handler = new Handler();
	// DEBUGGING
	private StringBuffer log = new StringBuffer();
	private int cnt = 0;
	private boolean isStoreDebugLog = false;
	
    // 12/05/2012
	private GeoPoint startGeoPoint = null;
	private GeoPoint finishGeoPoint = null;
	// 12/06/2012
	private FlagOverlay flagsOverlay = null; 
	// 04/27/2013
	private String targetAddressAsText;
	private Address targetAddressAsAddress = null;
	private Context context = null;
	//#8 v1.02.5 03/08/2014 
	private PlanRouteAsyncTask planRouteAsyncTask = null;
	private StringBuffer navBuffer = new StringBuffer();
	
	/**
	 * #8 v1.02.5 03/08/2014
	 * @author lkovari
	 *
	 */
	private class PlanRouteResult {
		private List<GeoPoint> routePoints = null;
		private String distance = null;
		private String duration = null;
		private Exception exception = null;
		private String status = null;
		
		public void setDistance(String distance) {
			this.distance = distance;
		}
		
		public void setDuration(String duration) {
			this.duration = duration;
		}
		
		public String getDistance() {
			return distance;
		}
		
		public String getDuration() {
			return duration;
		}
		
		public String getStatus() {
			return status;
		}
		
		
		public void setRoutePoints(List<GeoPoint> routePoints) {
			this.routePoints = routePoints;
		}
		
		public void setException(Exception exception) {
			this.exception = exception;
		}
		
		public List<GeoPoint> getRoutePoints() {
			return routePoints;
		}

		public Exception getException() {
			return exception;
		}
		
		public void setStatus(String status) {
			this.status = status;
		}
	}
	
	/**
	 * #8 v1.02.5 03/08/2014 
	 * @author lkovari
	 *
	 */
	private class PlanRouteAsyncTask extends AsyncTask<String, Void, PlanRouteResult> {

		/**
		 * #04/28/2013
		 * @param encoded
		 * @return
		 */
		private List<GeoPoint> decodePoly(String encoded) {
		    List<GeoPoint> poly = new ArrayList<GeoPoint>();
		    int index = 0, len = encoded.length();
		    int lat = 0, lng = 0;

		    while (index < len) {
		        int b, shift = 0, result = 0;
		        do {
		            b = encoded.charAt(index++) - 63;
		            result |= (b & 0x1f) << shift;
		            shift += 5;
		        } while (b >= 0x20);
		        int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
		        lat += dlat;

		        shift = 0;
		        result = 0;
		        do {
		            b = encoded.charAt(index++) - 63;
		            result |= (b & 0x1f) << shift;
		            shift += 5;
		        } while (b >= 0x20);
		        int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
		        lng += dlng;

		        GeoPoint geoPoint = new GeoPoint((int) (( (double) lat / 1E5) * 1E6), (int) (((double) lng / 1E5) * 1E6));
//		        LatLng p = new     LatLng((int) (( (double) lat / 1E5) * 1E6), (int) (((double) lng / 1E5) * 1E6));
		        poly.add(geoPoint);
		    }

		    return poly;
		}	

		@Override
		protected PlanRouteResult doInBackground(String... params) {
			List<GeoPoint> pointToDraw = null;
			PlanRouteResult planRouteResult = null;
			
			String url = (String)params[0];
			if (url != null) {
				planRouteResult = new PlanRouteResult();
				navBuffer.append("PLANNING ROUTE\n");
				Date today = new Date();
				navBuffer.append(today.toString() + "\n");
				navBuffer.append("\n");
				navBuffer.append("URL " + url + "\n");
				navBuffer.append("\n");
				HttpPost httppost = new HttpPost(url);
				HttpClient httpClient = new DefaultHttpClient();
				HttpResponse response;
				try {
					response = httpClient.execute(httppost);
					HttpEntity entity = response.getEntity();
					InputStream is = null;
					is = entity.getContent();
					BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
					StringBuilder sb = new StringBuilder();
					sb.append(reader.readLine() + "\n");
					String line = "0";
					while ((line = reader.readLine()) != null) {
					    sb.append(line + "\n");
					}
					is.close();
					reader.close();
					String result = sb.toString();
					navBuffer.append(sb.toString() + "\n");
					navBuffer.append("\n");
					JSONObject jsonObject = new JSONObject(result);
					String status = jsonObject.getString("status").toString();
					planRouteResult.setStatus(status);
					if (status.equalsIgnoreCase("OK")) {
						JSONArray routesArray = jsonObject.getJSONArray("routes");
						// Grab the first route
						JSONObject route = routesArray.getJSONObject(0);				
						// Take all legs from the route
						JSONArray legs = route.getJSONArray("legs");
						// Grab first leg
						JSONObject leg = legs.getJSONObject(0);
						JSONObject distanceObject = leg.getJSONObject("distance");
						String distance = distanceObject.getString("text");				
						JSONObject durationObject = leg.getJSONObject("duration");
						String duration = durationObject.getString("text");			
						JSONObject overviewPolylines = route.getJSONObject("overview_polyline");
						String encodedString = overviewPolylines.getString("points");
						pointToDraw = decodePoly(encodedString);
						navBuffer.append("Distance" + distance+ " Duration " + duration + "\n");
						navBuffer.append("Route coordinates\n");
						for (GeoPoint gp : pointToDraw) {
							double lat = gp.getLatitudeE6() / 1E6;
							double lng = gp.getLongitudeE6() / 1E6;
							navBuffer.append("Lat " + lat + " Lng " + lng + "\n");
						}
						navBuffer.append("\n");
						SDCardManager.createFileForRoutePlan(getApplicationContext(), "GTL-Route", navBuffer);
						navBuffer.delete(0, navBuffer.length());
						if (duration != null)
							planRouteResult.setDuration(duration);
						if (distance != null)
							planRouteResult.setDistance(distance);
						if ((pointToDraw != null) && (pointToDraw.size() > 0))
							planRouteResult.setRoutePoints(pointToDraw);
					}
				} 
				catch (ClientProtocolException e) {
					planRouteResult.setException(e);
				} 
				catch (IOException e) {
					planRouteResult.setException(e);
				} 
				catch (JSONException e) {
					planRouteResult.setException(e);
				}
			}
			
			return planRouteResult;
		}

		@Override
		protected void onPostExecute(PlanRouteResult planRouteResult) {
			if ((planRouteResult.getStatus().equalsIgnoreCase("ok")) && (planRouteResult.getRoutePoints() != null) && (planRouteResult.getException() == null)) {
				String mess = String.format("Distance %s Duration %s", planRouteResult.getDistance(), planRouteResult.getDuration());
				Toast.makeText(getApplicationContext(), mess, Toast.LENGTH_LONG).show();
				//Add line
				if (!isRoutePathOverlayAdded) {
					routePathOverlay = new RoutePathOverlay(planRouteResult.getRoutePoints());
					mapView.getOverlays().add(routePathOverlay);
					mapView.invalidate();
					isRoutePathOverlayAdded = true;
				}
				else {
					routePathOverlay.clear();
					routePathOverlay.getPathPoints().addAll(planRouteResult.getRoutePoints());
				}
			}
			else {
				if (planRouteResult.getException() != null) {
					Toast.makeText(getApplicationContext(), planRouteResult.getStatus() + "\n" + planRouteResult.getException().getMessage(), Toast.LENGTH_LONG).show();
					ErrorHandler.createErrorLog(getApplicationContext(), planRouteResult.getException());
				}	
			}
		}

		@Override
		protected void onPreExecute() {

		}

		@Override
		protected void onProgressUpdate(Void... values) {

		}
	}
	
	class GTLMapView extends MapView {

		public GTLMapView(Context context, String apiKey) {
			super(context, apiKey);
		}

		public GTLMapView(Context context, AttributeSet attrs, int defStyle) {
			super(context, attrs, defStyle);
		}
		
		public GTLMapView(Context context, AttributeSet attrs) {
			super(context, attrs);
		}
		

		@Override
		public void invalidateDrawable(Drawable drawable) {
			super.invalidateDrawable(drawable);
			if (drawable.getTransparentRegion() != null) {
				if (drawable.getTransparentRegion().getBounds() != null) {
					Rect region = drawable.getTransparentRegion().getBounds();
				}
			}
		}
		
	}
	
	private void debug(String text) {
		if (isStoreDebugLog) {
			cnt++;
			log.append("> " + cnt + ". " + text);
		}
	}
			
	/**
	 * #6 11/26/2013
	 * @param gp
	 */
	private void setupStartGeoPoint(GeoPoint gp) {
		if (this.startGeoPoint == null) {
			this.startGeoPoint = gp;
		}
	}
	
	/**
	 * 
	 * @author lkovari
	 *
	 */
	public class AccuracyOverlay extends Overlay {
		private GeoPoint accuracyGeoPoint;
		private boolean isNeedClearCanvas = false;
		// 12/26/2012
		private int markerBorderColor = 0xff1414FC;
		private int markerFillColor = 0xff6666ff;
		
        public void clearCanvas(Canvas canvas) {
            canvas.drawColor(Color.WHITE);
        }
		
	    private void drawAccuracyCircle(Canvas canvas, MapView mapView, GeoPoint geoPoint) {
	    	  // convert point to pixels
	        Point screenPoint = new Point();
	        mapView.getProjection().toPixels(geoPoint, screenPoint);

	        Paint circlePaint = new Paint();
	        circlePaint.setAntiAlias(true);
	        circlePaint.setStrokeWidth(2.0f);
	        circlePaint.setColor(markerFillColor);
	        circlePaint.setStyle(Paint.Style.FILL_AND_STROKE);
	        circlePaint.setAlpha(GPSSettings.ACCURACY_MARKER_TRANSPARENCY);
	        // 12/26/2012
	        Paint circleBorderPaint = new Paint();
	        circleBorderPaint.setAntiAlias(true);
	        circleBorderPaint.setStrokeWidth(2.0f);
	        circleBorderPaint.setColor(markerBorderColor);
	        circleBorderPaint.setStyle(Paint.Style.STROKE);
	        circleBorderPaint.setAlpha(255);
	        double lat = geoPoint.getLatitudeE6();
	        int radius = (int) mapView.getProjection().metersToEquatorPixels(lastAccuracy);
	        canvas.drawCircle(screenPoint.x, screenPoint.y, radius, circlePaint);
	        // 12/26/2012
	        canvas.drawCircle(screenPoint.x, screenPoint.y, radius, circleBorderPaint);
	    }
	    
		public int metersToRadius(double latitude) {
			return (int) (mapView.getProjection().metersToEquatorPixels(lastAccuracy) * (1/ Math.cos(Math.toRadians(latitude))));
		}	    
        
		public void draw(Canvas canvas, MapView mapView, boolean shadow) {
			if (GPSSettings.IS_SHOW_ACCURACY_MARKER) {
				if (!shadow && getLastLocation() != null) {
					accuracyGeoPoint = getLastLocation();
				}
				
				if (accuracyGeoPoint != null) {
					if (GPSSettings.IS_SHOW_ACCURACY_MARKER && (lastAccuracy > 0)) {
						//drawAccuracyCircle(canvas, mapView, getLastLocation());
						drawAccuracyCircle(canvas, mapView, accuracyGeoPoint);
					}
				}
				if (isNeedClearCanvas) {
					boolean wasOn = GPSSettings.IS_SHOW_ACCURACY_MARKER;
					try {
						try {
							if (wasOn)
								GPSSettings.IS_SHOW_ACCURACY_MARKER = false;
							accuracyGeoPoint = null;
							canvas.drawColor(Color.WHITE);
						}
						finally {
							if (wasOn) {
								GPSSettings.IS_SHOW_ACCURACY_MARKER = true;
							}	
						}
					}
					finally {
						isNeedClearCanvas = false;
					}
				}
			}
		}
		
		public AccuracyOverlay() {
			super();
		}
		
		
		public void clearMarks() {
			this.isNeedClearCanvas = true;
		}

		public GeoPoint getAccuracyGeoPoint() {
			return accuracyGeoPoint;
		}
	}
	
	
	/**
	 * 
	 * @author lkovari
	 *
	 */
	public class MarkerOverlay extends Overlay {
		private GeoPoint headGeoPoint;
		private GeoPoint markerGeoPoint;
		private boolean isNeedClearCanvas = false;
		
        public void clearCanvas(Canvas canvas) {
            canvas.drawColor(Color.WHITE);
        }
		
        /**
         * 12/01/2012
         * @param flag
         * @param pos
         * @return
         */
        private Point calculateMarkerPosition(Bitmap flag, Point pos) {
        	Point pt = pos;
        	pt.y = pos.y - flag.getHeight();
        	return pt;
        }

        private Point calculateHeadPosition(Bitmap flag, Point pos) {
        	Point pt = pos;
        	pt.y = pos.y - (flag.getHeight() / 2);
        	pt.x = pos.x - (flag.getWidth() / 2);
        	return pt;
        }
        
		public void draw(Canvas canvas, MapView mapView, boolean shadow) {
			super.draw(canvas, mapView, shadow);
			
			if (isMarkPressed && !shadow && getLastLocation() != null) {
				isMarkPressed = false;
				markerGeoPoint = getLastLocation();
			}
			
			// #5 10/06/2013 fix NPE
			if ((accuracyOverlay != null) && (accuracyOverlay.getAccuracyGeoPoint() != null)) {
				// 12/01/2012
				headGeoPoint = accuracyOverlay.getAccuracyGeoPoint();
			}

			if (headGeoPoint != null) {
				Bitmap headFlag = BitmapFactory.decodeResource(MapTab.this.getResources(), R.drawable.head_blue_16x16);
				Point headPoint = mapView.getProjection().toPixels(headGeoPoint, null);
				Point pt = calculateHeadPosition(headFlag, headPoint);
				canvas.drawBitmap(headFlag, pt.x, pt.y, new Paint());
			}
			
			if (markerGeoPoint != null) {
				Bitmap yellowFlag = BitmapFactory.decodeResource(MapTab.this.getResources(), R.drawable.flag_yellow_16x16);
				Point yellowFlagPoint = mapView.getProjection().toPixels(markerGeoPoint, null);
				Point pt = calculateMarkerPosition(yellowFlag, yellowFlagPoint);
				canvas.drawBitmap(yellowFlag, pt.x, pt.y, new Paint());
			}
			
			if (isNeedClearCanvas) {
				try {
					canvas.drawColor(Color.WHITE);
					headGeoPoint = null;
					markerGeoPoint = null;
					isMarkPressed = false;
				}
				finally {
					isNeedClearCanvas = false;
				}
			}
		}
		
		public MarkerOverlay() {
			super();
		}
		
		
		public void clearMarks() {
			this.isNeedClearCanvas = true;
			
		}
		
	}
	
	/**
	 * 12/06/2012
	 * @author lkovari
	 *
	 */
	public class FlagOverlay extends ItemizedOverlay<OverlayItem> {
		private ArrayList<OverlayItem> mapOverlays = new ArrayList<OverlayItem>();
		   
		private Context context;
		   
		public FlagOverlay(Drawable defaultMarker) {
			super(boundCenterBottom(defaultMarker));
		}
		   
		public FlagOverlay(Drawable defaultMarker, Context context) {
			this(defaultMarker);
			this.context = context;
			// 12/27/2012
			populate();
		}

		@Override
		protected OverlayItem createItem(int i) {
			// 12/27/2012
			return (mapOverlays != null) ? mapOverlays.get(i) : null;
		}

		@Override
		public int size() {
			// 12/27/2012
			return (mapOverlays != null) ? mapOverlays.size() : 0;
		}


		@Override
		protected boolean onTap(int index) {
			// 12/27/2012
			if (mapOverlays != null) {
				OverlayItem overlayItem = mapOverlays.get(index);
				if (overlayItem != null) {
					/*
					int ix = 0;
					for (OverlayItem oi : mapOverlays) {
						debug("OI #"+ ix + " " +oi.getTitle() + ": " + oi.getSnippet()+" LA "+ oi.getPoint().getLatitudeE6() + " LO "+ oi.getPoint().getLongitudeE6() + "\n");
						ix++;
					}
					*/
					Toast.makeText(MapTab.this, overlayItem.getTitle() + ": " + overlayItem.getSnippet(), Toast.LENGTH_LONG).show();			
				}
			}
			return true;
		}
		
		/*
	    @Override
	    public boolean onTap (final GeoPoint p, final MapView mapView) {
	    	return super.onTap(p, mapView);
	    }		
		*/
		
        public void addOverlayItem(OverlayItem overlayItem) {
			// 12/27/2012
        	if (mapOverlays != null) {
            	if (mapOverlays.size() < 2) {
                    mapOverlays.add(overlayItem);
                    populate();
            	}
        	}
        }


        /**
         * 
         * @param lat
         * @param lon
         * @param title
         * @param text
         */
        public void addOverlayItem(int lat, int lon, String title, String text) {
        	GeoPoint point = new GeoPoint(lat, lon);
            OverlayItem overlayItem = new OverlayItem(point, title, text);
            addOverlayItem(overlayItem);    
        }
		   
		public void clearOverlayItems() {
			// 12/27/2012
        	if (mapOverlays != null) 
        		mapOverlays.clear();
		}
	}

	/**
	 * #04/28/2013
	 * @author lkovari
	 *
	 */
	public class RoutePathOverlay extends Overlay {

	    private final List<GeoPoint> pathPoints;
		private boolean isNeedClearCanvas = false;
		private boolean isEnableDrawStartEnd = false;
		private Paint paint = new Paint();

	    public RoutePathOverlay(List<GeoPoint> points) {
	    	this(points, Color.GREEN, true);
	    }

	    public RoutePathOverlay(List<GeoPoint> points, int pathColor, boolean isEnableDrawStartEnd) {
	    	this.pathPoints = points;
	        this.isEnableDrawStartEnd = isEnableDrawStartEnd;
			this.paint.setStyle(Paint.Style.STROKE);
			this.paint.setColor(Color.parseColor("#"+GPSSettings.MAP_NAV_COLOR));
			this.paint.setAntiAlias(true);
			this.paint.setStrokeWidth(GPSSettings.MAP_TRACK_THICKNESS);	        
	    }


	    private void drawOval(Canvas canvas, Paint paint, Point point) {
            Paint ovalPaint = new Paint(paint);
            ovalPaint.setStyle(Paint.Style.FILL_AND_STROKE);
            ovalPaint.setStrokeWidth(2);
            int _radius = 8;
            RectF oval = new RectF(point.x - _radius, point.y - _radius, point.x + _radius, point.y + _radius);
            canvas.drawOval(oval, ovalPaint);               
	    }

	    private void showRoutePoints(Canvas canvas) {
			for (int ix = 0; ix < pathPoints.size() - 1; ix++) {
				Point pointFrom = mapView.getProjection().toPixels(pathPoints.get(ix), null);
				Point pointTo = mapView.getProjection().toPixels(pathPoints.get(ix + 1), null);
				canvas.drawLine(pointFrom.x, pointFrom.y, pointTo.x, pointTo.y, paint);
			}
			mapView.invalidate();
	    }
	    
	    public boolean draw(Canvas canvas, MapView mapView, boolean shadow, long when) {
	    	Projection projection = mapView.getProjection();
	        if ((shadow == false) && (this.pathPoints != null) && (this.pathPoints.size() > 0)) {
	        	Point startPoint = null;
	        	Point endPoint = null;
	            Path path = new Path();
	            //We are creating the path
	            for (int i = 0; i < this.pathPoints.size(); i++) {
	            	GeoPoint gPointA = this.pathPoints.get(i);
	            	Point pointA = new Point();
	            	projection.toPixels(gPointA, pointA);
	            	//This is the start point
	            	if (i == 0) { 
	            		startPoint = pointA;
	            		path.moveTo(pointA.x, pointA.y);
	            	} 
	            	else {
	            		//This is the end point
	            		if (i == this.pathPoints.size() - 1)
	            			endPoint = pointA;
	                   	path.lineTo(pointA.x, pointA.y);
	                }
	            }

	                
                if (isEnableDrawStartEnd) {

                	if (startPoint != null) {
                		drawOval(canvas, paint, startPoint);
                    }

                	if (endPoint != null) {
                    	drawOval(canvas, paint, endPoint);
                    }
                	
                }
                if (!path.isEmpty())
                	canvas.drawPath(path, paint);
                
            }
			if (isNeedClearCanvas) {
				try {
					canvas.drawColor(Color.WHITE);
				}
				finally {
					isNeedClearCanvas = false;
				}
			}
			showRoutePoints(canvas);
            return super.draw(canvas, mapView, shadow, when);
    	}

	    public boolean isEnableDrawStartEnd() {
			return isEnableDrawStartEnd;
		}
	    
	    public void setEnableDrawStartEnd(boolean isEnableDrawStartEnd) {
			this.isEnableDrawStartEnd = isEnableDrawStartEnd;
		}
	    
	    public void clear() {
	    	if (this.pathPoints != null)
	    		this.pathPoints.clear();
			isNeedClearCanvas = true;;
	    }
	    
	    public List<GeoPoint> getPathPoints() {
			return pathPoints;
		}
	    
	}	
	
	/**
	 * 
	 * @author lkovari
	 *
	 */
	public class RouteOverlay extends Overlay {
		private List<GeoPoint> optimizedRoutePoints = new ArrayList<GeoPoint>();
		private Paint paint = new Paint();
		private Context context;
		private GeoPoint prevLocation = null;
		//12/02/2012
		private int minLat = Integer.MAX_VALUE;
		private int maxLat = Integer.MIN_VALUE;
		private int minLon = Integer.MAX_VALUE;
		private int maxLon = Integer.MIN_VALUE;
		private boolean isAllvalueCaptured = false;
		
		public RouteOverlay(Context context) {
			this.context = context;
			this.paint.setStyle(Paint.Style.STROKE);
			this.paint.setColor(Color.parseColor("#"+GPSSettings.MAP_TRACK_COLOR));
			this.paint.setAntiAlias(true);
			this.paint.setStrokeWidth(GPSSettings.MAP_TRACK_THICKNESS);
		}
		
		
	    public void clearGeoPoints() {
	    	// 08/18/2012
			if (this.optimizedRoutePoints != null)
				this.optimizedRoutePoints.clear();
	    	// 11/28/2012
	    	prevLocation = null;
			minLat = Integer.MAX_VALUE;
		    maxLat = Integer.MIN_VALUE;
		    minLon = Integer.MAX_VALUE;
		    maxLon = Integer.MIN_VALUE;
		    isAllvalueCaptured = false;
	    }
	    
	    private void showRoutePoints(Canvas canvas) {
			for (int ix = 0; ix < optimizedRoutePoints.size() - 1; ix++) {
				Point pointFrom = mapView.getProjection().toPixels(optimizedRoutePoints.get(ix), null);
				Point pointTo = mapView.getProjection().toPixels(optimizedRoutePoints.get(ix + 1), null);
				canvas.drawLine(pointFrom.x, pointFrom.y, pointTo.x, pointTo.y, paint);
			}
			mapView.invalidate();
	    }
		
	    
	    /**
	     * 12/05/2012 EXAMPLE
	     * @param canvas
	     * @param mapv
	     * @param shadow
	     */
	    public void draw1(Canvas canvas, MapView mapv, boolean shadow){
	        super.draw(canvas, mapv, shadow);

	        Paint   mPaint = new Paint();
	        mPaint.setDither(true);
	        mPaint.setColor(Color.RED);
	        mPaint.setStyle(Paint.Style.FILL_AND_STROKE);
	        mPaint.setStrokeJoin(Paint.Join.ROUND);
	        mPaint.setStrokeCap(Paint.Cap.ROUND);
	        mPaint.setStrokeWidth(2);

	        GeoPoint gP1 = new GeoPoint(19240000,-99120000);
	        GeoPoint gP2 = new GeoPoint(37423157, -122085008);

	        Point p1 = new Point();
	        Point p2 = new Point();
	        Path path = new Path();

	        projection.toPixels(gP1, p1);
	        projection.toPixels(gP2, p2);

	        path.moveTo(p2.x, p2.y);
	        path.lineTo(p1.x,p1.y);

	        canvas.drawPath(path, mPaint);
	    }	    
	    
	    /**
	     * 12/02/2012
	     * @param geoPoint
	     */
	    public void captureMaxPositionValues(GeoPoint geoPoint) {
	    	int lat = geoPoint.getLatitudeE6();
	        int lon = geoPoint.getLongitudeE6();
	        maxLat = Math.max(lat, maxLat);
	        minLat = Math.min(lat, minLat);
	        maxLon = Math.max(lon, maxLon);
	        minLon = Math.min(lon, minLon);
	          
		    // check all value is captured
			if (!isAllvalueCaptured) {
				boolean isCaptured = (minLat != Integer.MAX_VALUE) && (maxLat != Integer.MIN_VALUE) && (minLon != Integer.MAX_VALUE) && (maxLon != Integer.MIN_VALUE);
			    if (isCaptured) {
			    	isAllvalueCaptured = true;
			    }
			}
	    }

	    /**
	     * 12/02/2012
	     */
	    private void spanToTrackAreaAndAnimateToMiddleOfIt() {
		    //1.1 to have some padding on the edges.
			mapController.zoomToSpan((int) (Math.abs(maxLat - minLat) * 1.1), (int)(Math.abs(maxLon - minLon) * 1.1)); 
			//so the center is positioned just in the middle
			mapController.animateTo(new GeoPoint( (maxLat + minLat)/2,(maxLon + minLon)/2 ));
	    }

	    /**
	     * 11/30/2012
	     */
	    public void animateToMiddleAndSetupZoomOfRoute() {
	    	if (optimizedRoutePoints.size() > 2) {
				int minLat = Integer.MAX_VALUE;
			    int maxLat = Integer.MIN_VALUE;
			    int minLon = Integer.MAX_VALUE;
			    int maxLon = Integer.MIN_VALUE;
			    boolean isValuesCaptured = false;
			    for (GeoPoint item : optimizedRoutePoints) { 
			    	int lat = item.getLatitudeE6();
			    	int lon = item.getLongitudeE6();
			        maxLat = Math.max(lat, maxLat);
			        minLat = Math.min(lat, minLat);
			        maxLon = Math.max(lon, maxLon);
			        minLon = Math.min(lon, minLon);
					if (!isAllvalueCaptured) {
						boolean isCaptured = (minLat != Integer.MAX_VALUE) && (maxLat != Integer.MIN_VALUE) && (minLon != Integer.MAX_VALUE) && (maxLon != Integer.MIN_VALUE);
						if (isCaptured) {
							isValuesCaptured = true;
					    }
					}
			    }
			    
			    if (isValuesCaptured) {
				    //1.1 to have some padding on the edges.
					mapController.zoomToSpan((int) (Math.abs(maxLat - minLat) * 1.05), (int)(Math.abs(maxLon - minLon) * 1.05)); 
					//so the center is positioned just in the middle
					mapController.animateTo(new GeoPoint( (maxLat + minLat)/2,(maxLon + minLon)/2 ));
			    }
			    mapView.invalidate();
	    	}
	    }
	    
	    /**
	     * #5 10/24/2013
	     */
	    public void captureMaxMinCoordinatesOfRoutePoints() {
	    	for (GeoPoint gp : optimizedRoutePoints) {
	    		captureMaxPositionValues(gp);
	    	}
	    }
	    
		@Override
		public void draw(Canvas canvas, MapView mapView, boolean shadow) {
			if (isSaveView) {
				viewBitmap =  Bitmap.createBitmap(mapView.getRootView().getWidth(), mapView.getRootView().getHeight(), Bitmap.Config.ARGB_8888);
				
			}
			GeoPoint loc = getLastLocation();
			if (!shadow && loc != null) {
				if (prevLocation != null) {
					if (!loc.equals(prevLocation)) {
						prevLocation = loc;
						optimizedRoutePoints.add(loc);
						// 12/07/2012
						if (GPSSettings.IS_ENABLE_ROUTEOVERLAY_TO_STORE_START_AND_FINISH_GP) {
							finishGeoPoint = loc;
						}
						
						// 12/02/2012 capture max position values
						if (GPSSettings.IS_KEEP_FULL_TRACK_ON_VIEW_OF_MAP)
							captureMaxPositionValues(loc);
						
						posCnt++;
						// is need to optimize every POS_OPTIMIZE_LIMIT
						if ((posCnt % GPSSettings.POS_OPTIMIZE_LIMIT) == 0) {
							posCnt = 0;
							List<GeoPoint> reduced = DouglasPeuckerOptimizer.reductionGeoPoint(optimizedRoutePoints, GPSSettings.DOUGLAS_PEUCKER_TOLARANCE);
//							List<LocPts> reduced = SGOptimizer.simplifyLine2D((float)GPSSettings.DOUGLAS_PEUCKER_TOLARANCE, routePoints);
							if (optimizedRoutePoints.size() != reduced.size()) {
//								System.out.println("Douglas Peucker " + MapTab.this.routePoints.size() + " : " + reduced.size());
								optimizedRoutePoints.clear();
								optimizedRoutePoints.addAll(reduced);
							}
							/* 11/28/2012
							else {
								reduced.clear();
								reduced = null;
							}
							*/
							reduced.clear();
							reduced = null;
						}

						if (GPSSettings.IS_ANIMATE_TO_POSITION) {
							mapController.animateTo(loc);
							if (GPSSettings.IS_SET_AUTOMATIC_ZOOM_LEVEL) {
								if (currentSpeed > -1) {
									int zoom = calculateZoomLevelBySpeed(currentSpeed);
									mapController.setZoom(zoom);
								}
							}
						}
						else if (GPSSettings.IS_KEEP_FULL_TRACK_ON_VIEW_OF_MAP) {
							// 12/02/2012
							spanToTrackAreaAndAnimateToMiddleOfIt();							
						}
					}
				}
				else {
					// 12/07/2012
					if (GPSSettings.IS_ENABLE_ROUTEOVERLAY_TO_STORE_START_AND_FINISH_GP) {
						// #6 11/27/2013
						setupStartGeoPoint(loc);
					}
					setupStartRoutePointFlag();
					prevLocation = loc;
					optimizedRoutePoints.add(loc);
				}
			}
			showRoutePoints(canvas);
		}
		

		
		
        @Override
        public boolean onTouchEvent(MotionEvent event, MapView mapView) {
        	boolean ret = false;
        	if (GPSSettings.IS_SHOW_ADDRESS_IF_TOUCH_TO_MAP) {
                if (event.getAction() == 1) {                
                    GeoPoint p = mapView.getProjection().fromPixels((int) event.getX(), (int) event.getY());
     
                    Geocoder geoCoder = new Geocoder(context, Locale.getDefault());
                    try {
                        List<Address> addresses = geoCoder.getFromLocation(
                            p.getLatitudeE6()  / 1E6, 
                            p.getLongitudeE6() / 1E6, 1);
     
                        String add = "";
                        if (addresses.size() > 0) {
                            for (int i=0; i<addresses.get(0).getMaxAddressLineIndex(); i++)
                               add += addresses.get(0).getAddressLine(i) + "\n";
                        }
     
                        Toast.makeText(context, add, Toast.LENGTH_SHORT).show();
                        ret = true;
                    }
                    catch (IOException e) {                
                        e.printStackTrace();
                        CommonLogger.log(Level.SEVERE, e.getMessage());
                    }   
                }
        	}
        	return ret;
        }
        
        public List<GeoPoint> getOptimizedRoutePoints() {
			return optimizedRoutePoints;
		}
	}
	
	
	private final BroadcastReceiver intentReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
	    	double longitude = 0;
	    	double latitude = 0;
		    // Handle reciever
		    String mAction = intent.getAction();
		    if (mAction.equals(Main.BROADCAST_ACTION_GPS_LOCATION_DATA_CHANGED)) {
		    	Bundle bundle = intent.getExtras();
		    	longitude = bundle.getDouble(GUIDataIdentifiers.GPS_DATA_LONGITUDE);
		    	latitude = bundle.getDouble(GUIDataIdentifiers.GPS_DATA_LATITUDE);
			    currentSpeed = bundle.getFloat(GUIDataIdentifiers.ROUTE_DATA_SPEED);
			    lastAccuracy = bundle.getFloat(GUIDataIdentifiers.GPS_DATA_ACCURACY);
		    	// show values
			    showLocationOnMap(longitude, latitude, currentSpeed);
		    }	
		}
		
	};

	/**
	 * 12/06/2012
	 * @param geoPoint
	 * @return
	 */
	private String extractAddress(GeoPoint geoPoint) {
		String address = null;
        Geocoder geoCoder = new Geocoder(this.getApplicationContext(), Locale.getDefault());
        try {
            List<Address> addresses = geoCoder.getFromLocation(geoPoint.getLatitudeE6()  / 1E6, geoPoint.getLongitudeE6() / 1E6, 1);
            if (addresses.size() > 0) {
                address = "";
                for (int i=0; i<addresses.get(0).getMaxAddressLineIndex(); i++)
                	address += addresses.get(0).getAddressLine(i) + "\n";
            }
        }
        catch (IOException e) {                
            e.printStackTrace();
            CommonLogger.log(Level.SEVERE, e.getMessage());
        }   
        return address;
	}
	
    public void setupStartRoutePointFlag() {
    	// 12/06/2012
    	if (startGeoPoint != null) {
    		String address = extractAddress(startGeoPoint);
    		String text = "";
    		if ((address != null) && (!address.equals(""))) 
    			text = address;
            flagsOverlay.addOverlayItem(startGeoPoint.getLatitudeE6(), startGeoPoint.getLongitudeE6(), "Start", text);
    	}
    }
    
    public void setupFinishRoutePointFlag() {
    	// 12/06/2012
    	if (finishGeoPoint != null) {
    		String address = extractAddress(finishGeoPoint);
    		String text = "";
    		if ((address != null) && (!address.equals(""))) 
    			text = address;
            flagsOverlay.addOverlayItem(finishGeoPoint.getLatitudeE6(), finishGeoPoint.getLongitudeE6(), "Finish", text);
    	}
    }
	
	
	/**
	 * 11/28/2012
	 */
	private void initializeOverlays() {
		// Create marker overlay
		markerOverlay = new MarkerOverlay();
		mapView.getOverlays().add(markerOverlay);

		//Create route overlay
		routeOverlay = new RouteOverlay(this.getApplicationContext());
		mapView.getOverlays().add(routeOverlay);        
    
		//Create accuracy overlay 
		accuracyOverlay = new AccuracyOverlay();
		mapView.getOverlays().add(accuracyOverlay);        
		
		// flags overlay
        Drawable startDrawable = MapTab.this.getResources().getDrawable(R.drawable.flag_green_16x16);
        flagsOverlay = new FlagOverlay(startDrawable, this.getApplicationContext());
        mapView.getOverlays().add(flagsOverlay);
        
	}
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.map_tab);
		// fix orientation
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);		
		mapView = (MapView) findViewById(R.id.mapview);
	    mapView.setBuiltInZoomControls(true);        
		mapView.setClickable(true);
        maxZoomLevel = mapView.getMaxZoomLevel();
        zoomLevel = maxZoomLevel + 1;
        mapController = mapView.getController();
        mapController.setZoom(zoomLevel);

        initializeOverlays();
        
        // 05/19/2013
        context = getApplicationContext();

        // 12/05/2012
        mapView.invalidate();
    }
	
    
    
    @Override
    protected void onDestroy() {
		markerOverlay = null;
		flagsOverlay = null;
		routeOverlay = null;
		accuracyOverlay = null;
	    mapView = null;
	    mapController = null;
	    currentGeoPoint = null;
	    if (routePoints != null) {
	    	routePoints.clear();
	    	routePoints = null;
	    }	
    	super.onDestroy();
    }

    
	public void setupZoomForFullRoute() {
		routeOverlay.animateToMiddleAndSetupZoomOfRoute();
	}
    
	public void showStartAndEndFlags(boolean isNeedToRefresh) {
		// 12/07/2012
		GPSSettings.IS_ENABLE_ROUTEOVERLAY_TO_STORE_START_AND_FINISH_GP = false;
		setupStartRoutePointFlag();
		setupFinishRoutePointFlag();
		if (isNeedToRefresh)
			mapView.postInvalidate();
	}
	
	/**
	 * (log(40 075 / 1m) / log(2)) + 1 = 16.2904149
	 * @param speed
	 * @return
	 */
	private int calculateZoomLevelBySpeed(float speed) {
		// calculate max speed
		double dummyMaxSpeed = 250.0; 
		switch (GPSSettings.USAGE_TYPE) {
		case AIRCRAFT : {
			dummyMaxSpeed = 1700;
			break;
		}
		case FOUR_WHEELERS : {
			dummyMaxSpeed = 270;
			break;
		}
		case PEDESTRIAN : {
			dummyMaxSpeed = 80;
			break;
		}
		case TWO_WHEELERS : {
			dummyMaxSpeed = 320;
			break;
		}
		case WALKING_HIKE : {
			dummyMaxSpeed = 80;
			break;
		}
		case WATERCRAFTS : {
			dummyMaxSpeed = 100;
			break;
		}
		}
	    // closest zoom level is the max	
	    int zoomLevelMax = mapView.getMaxZoomLevel();
	    // if no speed data this is the default	
	    int defaultZoomLevel = zoomLevelMax;	
	    if (speed > 0) {
	        // 300km theoretically max / max zoom steps
	        double kmPerZoomLevel = dummyMaxSpeed / zoomLevelMax;
	        zoomLevel = (int) Math.round(speed / kmPerZoomLevel);
	        // reverse it, more fast then more far
	        zoomLevel = zoomLevelMax - zoomLevel;
	    }
	    else { 
	    	zoomLevel = defaultZoomLevel;
	    }	
	    return zoomLevel;
	}

	
	/**
	 * 
	 * @param distance
	 * @return
	 */
	public int calculateZoomLevelByDistance (double distance){
		// closest
	    int zoom = mapView.getMaxZoomLevel();
	    // equator 40075004
	    double E = 40075;
	    zoom = (int) Math.round(Math.log(E/distance)/Math.log(2)+1);
	    // to avoid exceptions
	    zoom = Math.min(zoom, mapView.getMaxZoomLevel());
	    zoom = Math.max(zoom, 1);
	    return zoom;
	}
	
	/**
	 * 
	 * @param degree
	 * @return
	 */
	public static double degree2Radian(double degree) {
		return (degree * Math.PI / 180.0);
	}
	
	
	/**
	 * Haversine formula
	 * http://www.movable-type.co.uk/scripts/latlong.html
	 * @param lon1
	 * @param lat1
	 * @param lon2
	 * @param lat2
	 * @return
	 */
	public static double distanceBetweenTwoLocation(double lon1, double lat1, double lon2, double lat2) {
		double distance = 0;
		double r = 6371; 
		double dLat = degree2Radian(lat2 - lat1);
		double dLon = degree2Radian(lon2 - lon1);
		double lat_1 = degree2Radian(lat1);
		double lat_2 = degree2Radian(lon1);

		double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(lat_1) * Math.cos(lat_2); 
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		
		distance = r * c;
		return distance;
	}
	
	/**
	 * 11/28/2012
	 * @param coordinates
	 */
	public void redrawRoute(List<GPSCoordinate> coordinates, boolean isCaptureMaxPoints) {
		debug("coordinates.size() " + coordinates.size());
		if (routePoints.size() < 1) {
			debug("routePoints.size() < 1");
			GPSCoordinate lastCoordinate = null;
			// add coordinate points
			for (GPSCoordinate coordinate : coordinates) {
				Float speed = null;
				// 26/1/2014 #8 v1.02.5
				if ((coordinate != null) && (coordinate.getLocation() != null)) {
					if (coordinate.getLocation().hasSpeed()) {
						speed = coordinate.getLocation().getSpeed();
					}	
					double lon = coordinate.getLocation().getLongitude();
					double lat = coordinate.getLocation().getLatitude();
					showLocationOnMap(lon, lat, speed);
					lastCoordinate = coordinate;
				}
			}
	    	//12/07/2012
			if (lastCoordinate != null) {
				int lat = (int) (lastCoordinate.getLocation().getLatitude() * 1E6);
				int lon = (int) (lastCoordinate.getLocation().getLongitude() * 1E6);
				finishGeoPoint = new GeoPoint(lat, lon);
			}
		}
		// optimize route
		List<GeoPoint> reduced = DouglasPeuckerOptimizer.reductionGeoPoint(routePoints, GPSSettings.DOUGLAS_PEUCKER_TOLARANCE);
		if (reduced.size() > 0) {
			debug("reduced.size() > 0 " + reduced.size());
			if (routeOverlay.getOptimizedRoutePoints().size() < 1) {
				debug("routeOverlay.getOptimizedRoutePoints().size() < 1");
				routeOverlay.getOptimizedRoutePoints().addAll(reduced);
			}
			else {
				routeOverlay.getOptimizedRoutePoints().clear();
				routeOverlay.getOptimizedRoutePoints().addAll(reduced);
			}
			// #5 11/23/2013
			if (isCaptureMaxPoints) {
				routeOverlay.captureMaxMinCoordinatesOfRoutePoints();
			}
		}
		else {
			debug("reduced.size() < 1 " + reduced.size());
			if (routeOverlay.getOptimizedRoutePoints().size() < 1) {
				debug("routeOverlay.getOptimizedRoutePoints().size() < 1");
				routeOverlay.getOptimizedRoutePoints().addAll(routePoints);
				// #5 10/24/2013
				if (isCaptureMaxPoints) {
					routeOverlay.captureMaxMinCoordinatesOfRoutePoints();
				}
			}
		}
		mapView.invalidate();
		mapView.postInvalidate();
	}
	
	public void showLocationOnMap(double longitude, double latitude, Float speed) {
		int lat = (int) (latitude * 1E6);
		int lon = (int) (longitude * 1E6);
        currentGeoPoint = new GeoPoint(lat, lon);
        routePoints.add(currentGeoPoint);
        // when start show the start position
        if (isFirstLocation) {
        	isFirstLocation = false;
        	//12/07/2012
			// #6 11/27/2013
			setupStartGeoPoint(currentGeoPoint);
        	mapController.animateTo(currentGeoPoint);
        }
	}

	
	public List<GeoPoint> getRoutePoints() {
		return routePoints;
	}
	
	public GeoPoint getLastLocation() {
		return ((routePoints != null) && (routePoints.size() > 0)) ? routePoints.get(routePoints.size() - 1) : null;
	}
	
	/*
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.mapmenu, menu);
		return true;
	}
	*/
	
	/**
	 * 11/28/2012
	 */
	public void clear(boolean isNeedToRefresh, boolean isCelarRoutePoints, boolean isClearStastAndStop) {
		GPSSettings.IS_ENABLE_ROUTEOVERLAY_TO_STORE_START_AND_FINISH_GP = true;
		currentGeoPoint = null;

		if (isCelarRoutePoints) {
			if (routePoints != null)
				routePoints.clear();
		}
		
		if (routeOverlay != null)
			routeOverlay.clearGeoPoints();
		
		if (markerOverlay != null) 
			markerOverlay.clearMarks();

    	// 12/02/2012
		if (isClearStastAndStop) {
			startGeoPoint = null;
			finishGeoPoint = null;
			//#6 11/27/2013
			if (flagsOverlay != null) 
				flagsOverlay.clearOverlayItems();
		}
		
		
		if (accuracyOverlay != null)
			accuracyOverlay.clearMarks();

		// 05/19/2013
		if (routePathOverlay != null) 
			routePathOverlay.clear();
		
		isFirstLocation = true;
		if (isNeedToRefresh)
			mapView.invalidate();
		
	}

	/**
	 * 11/28/2012
	 */
	public void animateToLastLocation() {
		try {
			needToShowAccuracyMarker = true;
			GeoPoint lastLoc = getLastLocation();
			if (lastLoc != null) {
				mapController.animateTo(lastLoc);
			} else {
				// my location
				mapController.setCenter(new GeoPoint((int)(47.44721885 * 1E6), (int)(19.19528895 * 1E6)));
			}
		}
		finally {
			needToShowAccuracyMarker = false;
		}
	}


	/**
	 * #04/27/2013
	 * @param address
	 */
	public void animateToAddRess(Address address) {
		try {
			needToShowAccuracyMarker = true;
			int lat = (int) (address.getLatitude() * 1E6);
			int lon = (int) (address.getLongitude() * 1E6);
			GeoPoint addressLoc = new GeoPoint(lat, lon);
			if (addressLoc != null) {
				mapController.animateTo(addressLoc);
			} else {
				// my location
				mapController.setCenter(new GeoPoint((int)(47.44721885 * 1E6), (int)(19.19528895 * 1E6)));
			}
		}
		finally {
			needToShowAccuracyMarker = false;
		}
	}
	
	/**
	 * #04/28/2013
	 * @param startLat
	 * @param startLon
	 * @param endLat
	 * @param endLon
	 * @throws Exception
	 */
	public void drawRouteToTarget(double startLat, double startLon, double endLat, double endLon, String mode, boolean isAvoidHighways, boolean isAvoidTolls) throws Exception {
		String avoids= "";
		// is exist one of parameters?
		if (isAvoidHighways || isAvoidTolls)
			avoids += "&avoid=";
		
		// is need to avoid tolls?
		if (isAvoidTolls) {
			// avoid tols
			avoids += "tolls";
			// is need to avoid highways?
			if (isAvoidHighways) {
				// avoid highways
				avoids += URLEncoder.encode("|", "UTF-8") + "highways";
			}	
		}
		else {
			// is need to avoid highways?
			if (isAvoidHighways) {
				// avoid highways
				avoids += "highways";
			}	
		}
		
		if (mode != null)
			mode = "&mode=" + mode;
		else
			mode = "";
		
		// access to service
		String url = "http://maps.googleapis.com/maps/api/directions/json?origin="+startLat+","+startLon+"&destination="+endLat+","+endLon+"&sensor=false" + avoids + mode;
//		Toast.makeText(context, ">"+url+"<", Toast.LENGTH_LONG).show();
		//#8 v1.02.5 03/08/2014 
		if (planRouteAsyncTask != null) {
			planRouteAsyncTask = null;
		}
		planRouteAsyncTask = new PlanRouteAsyncTask();
		planRouteAsyncTask.execute(url);
	}
	
	/**
	 * 05/20/2013
	 * @param address
	 * @return
	 */
	public String extractAddressLine(Address address) {
		return address.getFeatureName() + " " + address.getLocality() + " " + address.getAdminArea() + " " + address.getCountryName();
	}
	
	
	@Override
	protected void onResume() {
	  try {
		  CommonLogger.log(Level.INFO);
		  // Register Sync Recievers
		  IntentFilter intentToReceiveFilter = new IntentFilter();
		  intentToReceiveFilter.addAction(Main.BROADCAST_ACTION_GPS_LOCATION_DATA_CHANGED);
//		  this.registerReceiver(intentReceiver, intentToReceiveFilter, null, handler);
		  this.registerReceiver(intentReceiver, intentToReceiveFilter);
		  isReceiversRegistered = true;
		  // 03/12/2016 #15 v1.02.11
		  if (GPSSettings.IS_LOGGING_STARTED) {
			  clear(true, true, false);
			  redrawRoute(GPSSettings.gpsCoordinates, false);
			  setupZoomForFullRoute();
		  }
		  
	  }
	  catch (Exception e) {
		e.printStackTrace();
        CommonLogger.log(Level.SEVERE, e.getMessage());
		Toast.makeText(getApplicationContext(), "ERROR in MAPTab.onResume " + e.getMessage(), Toast.LENGTH_LONG).show();
	  }
	  super.onResume();
	}
	
	@Override
	public void onPause() {
	  try {
		  CommonLogger.log(Level.INFO);
		// Make sure you unregister your receivers when you pause your activity
		if(isReceiversRegistered) {
			unregisterReceiver(intentReceiver);
			isReceiversRegistered = false;
		}
	  }
	  catch (Exception e) {
		e.printStackTrace();
        CommonLogger.log(Level.SEVERE, e.getMessage());
		Toast.makeText(getApplicationContext(), "ERROR in MAPTab.onPause " + e.getMessage(), Toast.LENGTH_LONG).show();
	  }
	  super.onPause();
	}
    
    
    @Override
    protected boolean isRouteDisplayed() {
        return true;
    }
    
    
    public void setNeedToShowAccuracyMarker(boolean needToShowAccuracyMarker) {
		this.needToShowAccuracyMarker = needToShowAccuracyMarker;
	}
    
    public boolean isNeedToShowAccuracyMarker() {
		return needToShowAccuracyMarker;
	}
 
	/**
	 * 01/15/2013
	 */
    @Override
    public void onBackPressed() {
    	this.getParent().onBackPressed();
    }

    public boolean isMarkPressed() {
		return isMarkPressed;
	}
    
    public void setMarkPressed(boolean isMarkPressed) {
		this.isMarkPressed = isMarkPressed;
	}
    
    public Address getTargetAddressAsAddress() {
		return targetAddressAsAddress;
	}
    
    /**
     * #5 10/1262013
     */
    public void refresh() {
    	mapView.invalidate();
    }
}
