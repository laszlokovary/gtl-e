package com.lkovari.mobile.apps.gtl.map;

import java.io.File;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.widget.Toast;

import com.lkovari.mobile.apps.gtl.R;
import com.lkovari.mobile.apps.gtl.utils.error.ErrorHandler;
import com.lkovari.mobile.apps.gtl.utils.sdcard.SDCardManager;

/**
 * # 1/10/2014 #10 v1.02.7
 * @author lkovari
 *
 */
public class ShowKMLInGoogleEarth {
    // #01/01/2015 #10 v1.02.7
    private static String GOOGLE_EARTH_PACKAGE_ACTIVITY = "com.google.earth.EarthActivity";
    private static String GOOGLE_EARTH_PACKAGE = "com.google.earth";
    

    /**
     * // #01/01/2015 #10 v1.02.7
     * @param packagename
     * @param context
     * @return
     */
    private static String extractPackageInstalled(String packagename, Context context) {
    	String earthPackageName = null;
        PackageManager packageManager = context.getPackageManager();
        List<ApplicationInfo> applicationInfoList = packageManager.getInstalledApplications(PackageManager.GET_META_DATA);
        for (ApplicationInfo appInfo : applicationInfoList) {
        	String packageName = appInfo.packageName.toUpperCase(); 
        	if (packageName.contains("EARTH")) {
                earthPackageName = appInfo.packageName;
                break;
        	}
        }
        return earthPackageName;
    }

    /**
     * // #01/01/2015 #10 v1.02.7
     * @param packageName String name of package
     * @param activity Activity the activity of app
     * @return boolean true if app enabled else false
     */
    private static boolean isAppEnabled(String packageName, Activity activity) {
        ApplicationInfo applicationInfo = null;
        boolean isEnabled = false;
        try {
            applicationInfo = activity.getPackageManager().getApplicationInfo(packageName, 0);
            isEnabled = applicationInfo.enabled;
        }
        catch(NameNotFoundException e) {
            isEnabled = false;
        }
        finally {
            applicationInfo = null;
        }
        return isEnabled;
    }

	public static void showKML(Activity activity, String kmlFileName) {
		String kmlFolder = SDCardManager.extractKMLFolder(activity);
		File file = new File(kmlFolder + "/" + kmlFileName);
		if (file.exists()) {
            String earthPackageName = extractPackageInstalled(GOOGLE_EARTH_PACKAGE, activity.getApplicationContext());
            if (earthPackageName != null) {
                if (isAppEnabled(earthPackageName, activity)) {
                    try {
                        Intent intent = new Intent();
                        intent.setAction(android.content.Intent.ACTION_VIEW);
                        intent.setDataAndType(Uri.fromFile(file), "application/vnd.google-earth.kml+xml");
                        intent.setClassName(GOOGLE_EARTH_PACKAGE, GOOGLE_EARTH_PACKAGE_ACTIVITY);
                        activity.startActivity(intent);
                    }
                    catch (Exception e) {
                        ErrorHandler.createErrorLog(activity.getApplicationContext(), e);
                        Toast.makeText(activity.getApplicationContext(), R.string.error_cannot_start_googleearth, Toast.LENGTH_LONG).show();
                    }
                }
                else {
                    Toast.makeText(activity.getApplicationContext(), R.string.error_googleearth_disabled, Toast.LENGTH_LONG).show();
                }
            }
            else {
                Toast.makeText(activity.getApplicationContext(), R.string.error_googleearth_not_installed, Toast.LENGTH_LONG).show();
            }
		}
	}
}
