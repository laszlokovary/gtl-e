package com.lkovari.mobile.apps.gtl;


import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.lkovari.mobile.apps.gtl.utils.Utils;

/**
 * 
 * @author lkovari
 *
 */
public class AppMain extends Activity  {
    private Activity mainActivity = null;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.appmain);
		// fix orientation
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);		
		
        // Restore preferences
		// #6 11/26/2013
		this.mainActivity = this;
        boolean isLicenseAccepted = Utils.readLicenseAccepted(this);

        if (isLicenseAccepted) {
            Intent myIntent = new Intent(this, Main.class);
            // code 1965 means this is main need to exit
            startActivityForResult(myIntent, 1965);
        }
        else {
            final Button acceptbutton = (Button) findViewById(R.id.accept_btn);
            //#4 08/27/2013
            acceptbutton.setTextColor(getResources().getColor(R.color.ForestGreen));
            acceptbutton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                	// write preference
                	Utils.writeLicenseAccepted(mainActivity, true);
                    Intent myIntent = new Intent(v.getContext(), Main.class);
                    startActivityForResult(myIntent, 1965);
                }
            });

            final Button refuseButton = (Button) findViewById(R.id.refuse_btn);
            refuseButton.setTextColor(Color.RED);
            refuseButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    finish();
                }
            });
        }
        /*
        boolean is3gEnable = ConnectionUtils.isMobileNetworkDataEnabled(this);
        boolean is3gdataConnectionAvailable = ConnectionUtils.isMobile3GDataConnectionAvailable(this);
        System.out.println("3G isMobileNetworkDataEnabled " + is3gEnable + " isMobile3GDataConnectionAvailable " + is3gdataConnectionAvailable);
        */
    }
    
	@Override
	protected void onPause() {
		super.onPause();
	}

	@Override
	protected void onRestart() {
		super.onRestart();
	}

	@Override
	protected void onStart() {
		super.onStart();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}
	
    @Override
    protected void onResume() {
        super.onResume();
    }
    
    @Override
    protected void onStop() {
        super.onStop();
    }    
    

    @Override 
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1965) {
            if (resultCode == Activity.RESULT_OK) {
                boolean isExit = data.getExtras().getBoolean("IS_EXIT");    	          
                if (isExit)
                	finish();
    	    }
    	}
    }
}