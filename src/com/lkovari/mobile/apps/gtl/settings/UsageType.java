package com.lkovari.mobile.apps.gtl.settings;

/**
 * 
 * @author lkovari
 *
 */
public enum UsageType {
	AIRCRAFT,
	WATERCRAFTS,
	FOUR_WHEELERS,
	TWO_WHEELERS,
	WALKING_HIKE,
	PEDESTRIAN,
	RUNNER;
}
