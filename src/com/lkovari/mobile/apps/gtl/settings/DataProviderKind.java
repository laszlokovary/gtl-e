package com.lkovari.mobile.apps.gtl.settings;

/**
 * 25/1/2014 #8 v1.02.5
 * @author lkovari
 *
 */
public enum DataProviderKind {
	BEST_PROVIDER,
	GPS_PROVIDER,
	NETWORK_PROVIDER;
}
