package com.lkovari.mobile.apps.gtl.settings;

import android.graphics.Color;

/**
 * 26/1/2014 #8 v1.02.5
 * @author lkovari
 *
 */
public enum TrackColorKind {
	BLACK(Color.BLACK),
	BLUE(Color.BLUE),
	GREEN(Color.GREEN),
	CYAN(Color.CYAN),
	MAGENTA(Color.MAGENTA),
	RED(Color.RED);
	
	private int value;
	
	private TrackColorKind(int value) {
		this.value = value;
	}
	
	public int getValue() {
		return value;
	}
}
