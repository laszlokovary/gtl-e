package com.lkovari.mobile.apps.gtl.settings;

import java.util.List;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Criteria;
import android.preference.PreferenceManager;

import com.lkovari.mobile.apps.gtl.track.GPSCoordinate;
import com.lkovari.mobile.apps.gtl.track.TrackFormat;
import com.lkovari.mobile.apps.gtl.utils.MeasurementSystem;

/**
 * 
 * @author lkovari
 *
 */
public class GPSSettings {
	// Enable or disable real time position sending
	public static final boolean IS_ENABLE_REALTIME_POSITION_SENDING = true;
	// Usage type default is Motorcycle
	public static UsageType USAGE_TYPE = UsageType.TWO_WHEELERS;
	// Measurement system
	public static MeasurementSystem MEASUREMENT_SYSTEM = MeasurementSystem.METRIC;
	//track log format KML
	public static TrackFormat trackFormat = TrackFormat.KML;
	// true if logging started
	public static boolean IS_LOGGING_STARTED = false;
	// min distance in meters
	public static float MIN_DISTANCE = 2.5f;
	// min time in milisecs
	public static long MIN_TIME = 500;
	// track color
	public static String KML_TRACK_COLOR = "ff0000ff";
	public static String MAP_TRACK_COLOR = "ff0000";
	public static String MAP_NAV_COLOR = "a9d02f";
	// 1/03/2014 #8 v1.02.5
	public static int KML_TRACK_THICKNESS = 8;
	public static int MAP_TRACK_THICKNESS = 6;
	// # of positions on map where need to optimize 
    public static int POS_OPTIMIZE_LIMIT = 4;
    public static double DOUGLAS_PEUCKER_TOLARANCE = 19.5;
    public static int MIN_ACCURACY = 30;
    public static int MIN_SATELLITES = 4;
    public static int CRITERIA_ACCURACY_LEVEL = Criteria.ACCURACY_FINE;
    public static int CRITERIA_POWER_LEVEL = Criteria.POWER_LOW;
    public static boolean IS_SHOW_ACCURACY_MARKER = true;
	public static int ACCURACY_MARKER_TRANSPARENCY = 64;
	public static boolean IS_ANIMATE_TO_POSITION = false;
	public static boolean IS_SET_AUTOMATIC_ZOOM_LEVEL = false;
	public static boolean IS_SHOW_ADDRESS_IF_TOUCH_TO_MAP = false;
	public static boolean IS_CRITERIA_SPEED_REQUIRED = true;
	public static boolean IS_CRITERIA_ALTITUDE_REQUIRED = true;
	public static boolean IS_CRITERIA_BEARING_REQUIRED = true;
	public static boolean IS_OPTIMIZATION_ACTIVE = true;
	public static boolean IS_REMOVE_ZERO_SPEED_LOCATIONS_ACTIVE = false;
	//11/19/2012
	public static boolean IS_STORE_PLACEMARKS_AT_START_PAUSE_STOP_ONLY = false;
	// 11/25/2012
	public static boolean IS_SWITCH_TO_MAP_TAB_WHEN_STOP_TRACKING = true;
	//12/02/2012
	public static boolean IS_KEEP_FULL_TRACK_ON_VIEW_OF_MAP = true;
	// 12/07/2012
	public static boolean IS_ENABLE_ROUTEOVERLAY_TO_STORE_START_AND_FINISH_GP = true;
	// 12/28/2012
	public static String DEFAULT_GTL_FOLDER = "gtl";
	// 01/13/2013 set to null
	public static String DEFAUL_MAPSFORGE_MAP = null;
	// 12/29/2012
	public static boolean IS_USE_OFFLINE_MAP = false;
	// 01/15/2013
	public static int BATTERY_LEVEL_WHEN_SAVE_TRACKLOG_AND_FINISH_TRACKING = 25;
	// 01/27=2013
	public static boolean IS_FULLSCREEN_ON = true;
	// 05/20/2013
	public static boolean IS_MAP_CALC_ROUTE_AVOID_HIGHWAYS = false;
	public static boolean IS_MAP_CALC_ROUTE_AVOID_TOLLS = true;
	// driving, walking, bicycling, transit
	public static String MAP_CALC_ROUTE_MODE = "driving";
	//#07/11/2013 #122
	public static boolean IS_FIRST_FIX = false;
	//#5 10/06/2013
	public static String GTL_TRACKLOG_FOLDER = "gtltracklogs";
	public static String GTL_MAPS_FOLDER = GTL_TRACKLOG_FOLDER + "//maps";
	// #5 10/12/2013
	public static String PHONE_IMEI = null;
	public static boolean IS_EXTRA_FUNCTIONS_ENABLED = false;
	public static String VERSION = "#8 v1.02.5";
	// 25/1/2014 #8 v1.02.5
	public static DataProviderKind DATA_PROVIDER_KIND = DataProviderKind.BEST_PROVIDER;
	// 22/3/2014 #9 v1.02.6
	public static int STORELOCATION_EVERY_N_MINUTES = 0;
	// 22/04/2014 #9 v1.02.6
	public static boolean IS_STORELOCATION_ON_WEB_FUNCTIONS_ENABLED = (STORELOCATION_EVERY_N_MINUTES > 0);
	public static String DEVICE_ID = "EKL36205583720"; 
	// #13 1.02.10 2015.12.02.		
//	public static boolean isAmbientTemreratureExists = false;

	// 03/12/2016 #15 v1.02.11 
	public static int RUNNER_DISTNACE = 3000;
	public static int RUNNER_ELAPSED_TIME = 19;
	
	//  03/12/2016 #15 v1.02.11 
	public static List<GPSCoordinate> gpsCoordinates = null;	
	
	
	public static void loadPreferences(Context context) {
		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
		String minAccuracy = sharedPreferences.getString("key_min_accuracy", "30");
		GPSSettings.MIN_ACCURACY = Integer.parseInt(minAccuracy);

		String minSatellites = sharedPreferences.getString("key_min_satellites", "4");
		GPSSettings.MIN_SATELLITES = Integer.parseInt(minSatellites);
		
		String optimizationLimit = sharedPreferences.getString("key_optimization_limit", "4");
		GPSSettings.POS_OPTIMIZE_LIMIT = Integer.parseInt(optimizationLimit);

		String optimizationTolerance = sharedPreferences.getString("key_optimization_tolerance", "19.5");
		GPSSettings.DOUGLAS_PEUCKER_TOLARANCE = Double.parseDouble(optimizationTolerance);

		String markerTransparency = sharedPreferences.getString("key_marker_transparency", "64");
		GPSSettings.ACCURACY_MARKER_TRANSPARENCY = Integer.parseInt(markerTransparency);

		// 12/15/2012
		String usageType = sharedPreferences.getString("key_usagetype", "3");
		if (usageType.equals("0")) {
			GPSSettings.USAGE_TYPE = UsageType.AIRCRAFT;
			GPSSettings.MAP_CALC_ROUTE_MODE = "transit";
		}
		else if (usageType.equals("1")) {
			GPSSettings.USAGE_TYPE = UsageType.WATERCRAFTS;
			GPSSettings.MAP_CALC_ROUTE_MODE = "transit";
		}
		else if (usageType.equals("2")) {
			GPSSettings.USAGE_TYPE = UsageType.FOUR_WHEELERS;
			GPSSettings.MAP_CALC_ROUTE_MODE = "driving";
		}
		else if (usageType.equals("3")) {
			GPSSettings.USAGE_TYPE = UsageType.TWO_WHEELERS;
			GPSSettings.MAP_CALC_ROUTE_MODE = "driving";
		}
		else if (usageType.equals("4")) {
			GPSSettings.USAGE_TYPE = UsageType.WALKING_HIKE;
			GPSSettings.MAP_CALC_ROUTE_MODE = "walking";
		}
		else if (usageType.equals("5")) {
			GPSSettings.USAGE_TYPE = UsageType.PEDESTRIAN;
			GPSSettings.MAP_CALC_ROUTE_MODE = "walking";
		}
		else if (usageType.equals("6")) {
			GPSSettings.USAGE_TYPE = UsageType.RUNNER;
			GPSSettings.MAP_CALC_ROUTE_MODE = "running";
		}
		
		// 25/1/2014 #8 v1.02.5
		String dataProvider = sharedPreferences.getString("key_dataprovider", "0");
		if (dataProvider.equals("0")) {
			GPSSettings.DATA_PROVIDER_KIND = DATA_PROVIDER_KIND.BEST_PROVIDER;
		}
		else if (dataProvider.equals("1")) {
			GPSSettings.DATA_PROVIDER_KIND = DATA_PROVIDER_KIND.GPS_PROVIDER;
		}
		else if (dataProvider.equals("2")) {
			GPSSettings.DATA_PROVIDER_KIND = DATA_PROVIDER_KIND.NETWORK_PROVIDER;
		}
		
		boolean isAltitudeRequired = sharedPreferences.getBoolean("key_altitude_required", true);
		GPSSettings.IS_CRITERIA_ALTITUDE_REQUIRED = isAltitudeRequired;

		boolean isBearingRequired = sharedPreferences.getBoolean("key_bearing_required", true);
		GPSSettings.IS_CRITERIA_BEARING_REQUIRED = isBearingRequired;

		boolean isSpeedRequired = sharedPreferences.getBoolean("key_speed_required", true);
		GPSSettings.IS_CRITERIA_SPEED_REQUIRED = isSpeedRequired;

		String accuracyLevel = sharedPreferences.getString("key_accuracy_level", "1");
		GPSSettings.CRITERIA_ACCURACY_LEVEL = Integer.parseInt(accuracyLevel);

		String powerLevel = sharedPreferences.getString("key_power_level", "1");
		GPSSettings.CRITERIA_POWER_LEVEL = Integer.parseInt(powerLevel);
		
		boolean isAnimateToPosition = sharedPreferences.getBoolean("key_animate_to_position", false);
		GPSSettings.IS_ANIMATE_TO_POSITION = isAnimateToPosition;
		if (GPSSettings.IS_ANIMATE_TO_POSITION) {
			GPSSettings.IS_SET_AUTOMATIC_ZOOM_LEVEL = false;
			GPSSettings.IS_KEEP_FULL_TRACK_ON_VIEW_OF_MAP = false;
		}
		boolean isSetAutomaticZoomLevel = sharedPreferences.getBoolean("key_automatic_zoom_level", false);
		GPSSettings.IS_SET_AUTOMATIC_ZOOM_LEVEL = isSetAutomaticZoomLevel;

		boolean isShowAccuracyMarker = sharedPreferences.getBoolean("key_show_accuracy_marker", true);
		GPSSettings.IS_SHOW_ACCURACY_MARKER = isShowAccuracyMarker;
		
		boolean isShowAddress = sharedPreferences.getBoolean("key_show_address", false);
		GPSSettings.IS_SHOW_ADDRESS_IF_TOUCH_TO_MAP = isShowAddress;
		
		boolean isOptimizationActive = sharedPreferences.getBoolean("key_set_optimization", true);
		GPSSettings.IS_OPTIMIZATION_ACTIVE = isOptimizationActive;
		
		boolean isRemoveZeroSpeedPositions = sharedPreferences.getBoolean("key_remove_zero_speed_location_points", false);
		GPSSettings.IS_REMOVE_ZERO_SPEED_LOCATIONS_ACTIVE = isRemoveZeroSpeedPositions;
		
		String minDistance = sharedPreferences.getString("key_min_distance", "1.0");
		GPSSettings.MIN_DISTANCE = Float.parseFloat(minDistance);
		
		String minTime = sharedPreferences.getString("key_min_time", "0");
		GPSSettings.MIN_TIME = Long.parseLong(minTime);
		
		boolean isStorePlacemarkStartPauseStopOnly = sharedPreferences.getBoolean("key_store_placemarks_at_start_pause_stop_only", false);
		GPSSettings.IS_STORE_PLACEMARKS_AT_START_PAUSE_STOP_ONLY = isStorePlacemarkStartPauseStopOnly;
		
		boolean isSwitchToMapViewWhenStop = sharedPreferences.getBoolean("key_switch_to_map_view_when_stop", true);
		GPSSettings.IS_SWITCH_TO_MAP_TAB_WHEN_STOP_TRACKING = isSwitchToMapViewWhenStop;
		//12/02/2012
		boolean isKeepFullTrackOnMapView = sharedPreferences.getBoolean("key_keep_full_track_on_map", true);
		GPSSettings.IS_KEEP_FULL_TRACK_ON_VIEW_OF_MAP = isKeepFullTrackOnMapView;
		if (GPSSettings.IS_KEEP_FULL_TRACK_ON_VIEW_OF_MAP) {
			GPSSettings.IS_ANIMATE_TO_POSITION = false;
			GPSSettings.IS_SET_AUTOMATIC_ZOOM_LEVEL = false;
		}	

		String measurementSystem = sharedPreferences.getString("key_measurementsystem", "0");
		int ms = Integer.parseInt(measurementSystem);
		if (ms == 0)
			GPSSettings.MEASUREMENT_SYSTEM = MeasurementSystem.METRIC;
		else if (ms == 1)
			GPSSettings.MEASUREMENT_SYSTEM = MeasurementSystem.IMPERIAL;
		else if (ms == 2)
			GPSSettings.MEASUREMENT_SYSTEM = MeasurementSystem.ICAO;
		
		boolean isUseOfflineMap = sharedPreferences.getBoolean("key_use_offline_map", false);
		GPSSettings.IS_USE_OFFLINE_MAP = isUseOfflineMap;
		// 01/27/2013
		boolean isFullScreenOn = sharedPreferences.getBoolean("key_fullscreen", true);
		GPSSettings.IS_FULLSCREEN_ON = isFullScreenOn;
		// 05/22/2013
		boolean isAvoidHighways = sharedPreferences.getBoolean("key_calcroute_avoid_highways", true);
		GPSSettings.IS_MAP_CALC_ROUTE_AVOID_HIGHWAYS = isAvoidHighways;
		// 05/22/2013
		boolean isAvoidTolls = sharedPreferences.getBoolean("key_calcroute_avoid_tolls", true);
		GPSSettings.IS_MAP_CALC_ROUTE_AVOID_TOLLS = isAvoidTolls;

		String trackColor = sharedPreferences.getString("key_track_color", "5");
		int colorValue = 0;
		if (trackColor.equals("0")) {
			colorValue = TrackColorKind.BLACK.getValue();
		}
		else if (trackColor.equals("1")) {
			colorValue = TrackColorKind.BLUE.getValue();
		}
		else if (trackColor.equals("2")) {
			colorValue = TrackColorKind.GREEN.getValue();
		}
		else if (trackColor.equals("3")) {
			colorValue = TrackColorKind.CYAN.getValue();
		}
		else if (trackColor.equals("4")) {
			colorValue = TrackColorKind.MAGENTA.getValue();
		}
		else if (trackColor.equals("5")) {
			colorValue = TrackColorKind.RED.getValue();
		}
		GPSSettings.KML_TRACK_COLOR = String.format("%08X", (0xFFFFFFFF & colorValue));
		// kml ff0000ff map ff0000
		colorValue = colorValue << 8;
		colorValue =  colorValue >>> 8;
		//GPSSettings.MAP_TRACK_COLOR = Integer.toHexString(colorValue);
		GPSSettings.MAP_TRACK_COLOR = String.format("%06X", (0xFFFFFF & colorValue));
		
		String trackThickness = sharedPreferences.getString("key_track_thickness", "8");
		if (trackThickness.equals("4")) {
			GPSSettings.KML_TRACK_THICKNESS = TrackThicknessKind.NARROW.getValue();
			GPSSettings.MAP_TRACK_THICKNESS = GPSSettings.KML_TRACK_THICKNESS - 2;
			
		}
		else if (trackThickness.equals("8")) {
			GPSSettings.KML_TRACK_THICKNESS = TrackThicknessKind.MEDIUM.getValue();
			GPSSettings.MAP_TRACK_THICKNESS = GPSSettings.KML_TRACK_THICKNESS - 2;
		}
		else if (trackThickness.equals("12")) {
			GPSSettings.KML_TRACK_THICKNESS = TrackThicknessKind.THICK.getValue();
			GPSSettings.MAP_TRACK_THICKNESS = GPSSettings.KML_TRACK_THICKNESS - 2;
		}
		
		// 22/043/2014 #9 v1.02.6
		String timePeriod = sharedPreferences.getString("key_marker_store_on_web", "0");
		GPSSettings.STORELOCATION_EVERY_N_MINUTES = Integer.valueOf(timePeriod);
		GPSSettings.IS_STORELOCATION_ON_WEB_FUNCTIONS_ENABLED = (GPSSettings.STORELOCATION_EVERY_N_MINUTES > 0);
	
		String distance = sharedPreferences.getString("key_runners_distance", "0");
		GPSSettings.RUNNER_DISTNACE = Integer.parseInt(distance);
		String elapsedTime = sharedPreferences.getString("key_runners_elapsedtime", "0");
		GPSSettings.RUNNER_ELAPSED_TIME = Integer.parseInt(elapsedTime);
	}
}
