package com.lkovari.mobile.apps.gtl.settings;

/**
 * 15/03/2014 #9 v1.02.6
 * @author lkovari
 *
 */
public enum TrackThicknessKind {
	NARROW(4),
	MEDIUM(8),
	THICK(12);
	
	private int value;
	
	private TrackThicknessKind(int value) {
		this.value = value;
	}
	
	public int getValue() {
		return value;
	}
}
