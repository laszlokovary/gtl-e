package com.lkovari.mobile.apps.gtl.maxvalues;



/**
 * 
 * @author lkovari
 *
 */
public class MaxSpeedValue extends MaxValue {
	private double maxSpeed = Double.MIN_VALUE;
	
	public MaxSpeedValue() {
		super();
	}
	
	public double getMaxSpeed() {
		return maxSpeed;
	}
	
	
	public void captureMaxSpeedValues(double spd, double lon, double lat) {
		super.captureLocation(lon, lat);
		if (maxSpeed < spd) {
			maxSpeed = spd;
		}
	}
	
}

