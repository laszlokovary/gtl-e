package com.lkovari.mobile.apps.gtl.maxvalues;



/**
 * 
 * @author lkovari
 *
 */
public class MaxValue {
	private double longitude;
	private double latitude;
	
	public MaxValue() {
	}

	public double getLatitude() {
		return latitude;
	}
	
	public double getLongitude() {
		return longitude;
	}
	
	public void captureLocation(double lon, double lat) {
		this.longitude = lon;
		this.latitude = lat;
	}
}

