package com.lkovari.mobile.apps.gtl.maxvalues;




/**
 * 
 * @author lkovari
 *
 */
public class MaxOrientationValue extends MaxValue {
	private double maxLeftAngle = Double.MIN_VALUE;
	private double maxRightAngle = Double.MIN_VALUE;

	public MaxOrientationValue() {
		super();
	}	
	
	public double getMaxLeftAngle() {
		return maxLeftAngle;
	}
	
	public double getMaxRightAngle() {
		return maxRightAngle;
	}
	
	
	public void captureMaxOrientationValues(double left, double right, double lon, double lat) {
		super.captureLocation(lon, lat);
		if (maxLeftAngle < left) {
			maxLeftAngle = left;
		}
		if (maxRightAngle < right) {
			maxRightAngle = right;
		}
	}

}
