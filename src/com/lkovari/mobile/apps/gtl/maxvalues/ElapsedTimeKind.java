package com.lkovari.mobile.apps.gtl.maxvalues;

/**
 * #5 10/12/2013
 * @author lkovari
 *
 */
public enum ElapsedTimeKind {
	IN_MOVE,
	IN_WAIT;
}
