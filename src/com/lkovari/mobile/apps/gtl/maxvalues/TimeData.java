package com.lkovari.mobile.apps.gtl.maxvalues;

/**
 * #5 10/13/2013
 * @author lkovari
 *
 */
public class TimeData {
    private long elapsed;
    private long mils;
    private float speed;
    private ElapsedTimeKind elapsedTimeKind;
    
    public TimeData(long elapsed, long mils, float speed) {
        this.elapsed = elapsed;
        this.mils = mils;
        this.speed = speed;
        this.elapsedTimeKind = (speed > 0) ? ElapsedTimeKind.IN_MOVE :ElapsedTimeKind.IN_WAIT;
    }

    
    
    @Override
    public boolean equals(Object obj) {
        TimeData other = null;
        if (obj instanceof TimeData) {
            other = (TimeData)obj;
        }
        if (other == null)
            return false;
        
        boolean isEquals1 = (this.getElapsedTimeKind().equals(other.getElapsedTimeKind()));
        boolean isEquals2 = (this.getElapsed() == other.getElapsed());
        boolean isEquals3 = (this.getMils() == other.getMils());
        boolean isEquals4 = (this.getSpeed() == other.getSpeed());
        
        return isEquals1 && isEquals2 && isEquals3 && isEquals4;
    }
    
    public long getElapsed() {
        return elapsed;
    }
    
    public long getMils() {
        return mils;
    }
    
    public float getSpeed() {
        return speed;
    }
    
    public boolean isInMove() {
        return this.speed > 0;
    }
    
    public boolean isInWait() {
        return this.speed < 0;
    }
    
    public ElapsedTimeKind getElapsedTimeKind() {
        return this.elapsedTimeKind;
    }
}
