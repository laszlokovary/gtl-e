package com.lkovari.mobile.apps.gtl.maxvalues;



/**
 * 
 * @author lkovari
 *
 */
public class MaxAccelerationValue extends MaxValue {
	private double maxAccelerationX = Double.MIN_VALUE; 
	private double maxAccelerationY = Double.MIN_VALUE; 
	private double maxAccelerationZ = Double.MIN_VALUE;
	
	public MaxAccelerationValue() {
		super();
	}
	
	public double getMaxAccelerationX() {
		return maxAccelerationX;
	}
	
	public double getMaxAccelerationY() {
		return maxAccelerationY;
	}
	
	public double getMaxAccelerationZ() {
		return maxAccelerationZ;
	}
	

	public void captureMaxAccelerationValues(double x, double y, double z, double lon, double lat) {
		super.captureLocation(lon, lat);
		if (maxAccelerationX < x) {
			maxAccelerationX = x;
		}
		if (maxAccelerationY < y) {
			maxAccelerationY = y;
		}
		if (maxAccelerationZ < z) {
			maxAccelerationZ = z;
		}
	}
}

