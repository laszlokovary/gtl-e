package com.lkovari.mobile.apps.gtl.maxvalues;


/**
 * 
 * @author lkovari
 *
 */
public class MaxAltitudeValue extends MaxValue {
	private double maxAltitude = Double.MIN_VALUE;

	public MaxAltitudeValue() {
		super();
	}
	
	public double getMaxAltitude() {
		return maxAltitude;
	}
	
	public void captureMaxAltitude(double alt, double lon, double lat) {
		super.captureLocation(lon, lat);
		if (maxAltitude < alt) {
			maxAltitude = alt;
		}
	}
	
}

