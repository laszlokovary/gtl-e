package com.lkovari.mobile.apps.gtl.compass;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.util.AttributeSet;
import android.view.View;

import com.lkovari.mobile.apps.gtl.utils.PolePrefix;


public class CompassView extends View {
	private float direction;
	private Paint circlePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
	private Paint circleBorderPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
	private Paint northArrowPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
	private Paint northArrowBorderPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
	private Paint degreePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
	private Paint paintSouth = new Paint(Paint.ANTI_ALIAS_FLAG);
	private Paint rectPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private Path  northArrowPath = new Path();
    private Path  southArrowPath = new Path();
	private boolean isSimpleNorthSouthLine = true;
	private Context context = null;
	private boolean isFirstDraw = true;
	private Point center = new Point();
	private int length;
	private int width;
	private int radius;
	private Paint diamondPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
	private Paint diamondNorthPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
	private Paint diamondSouthPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
	private Paint centerPaint = new Paint(Paint.ANTI_ALIAS_FLAG);

	private boolean isShowCenter = true;
	
	// DEBUGGING
	private StringBuffer log = new StringBuffer();
	private int cnt = 0;
	private boolean isStoreDebugLog = false;

	private void debug(String text) {
		if (isStoreDebugLog) {
			cnt++;
			log.append("> " + cnt + ". " + text + "\n");
		}
	}
	
	
	public CompassView(Context context) {
		super(context);
		this.context = context;
		init();
	}

	public CompassView(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.context = context;
		init();
	}

	public CompassView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		this.context = context;
		init();
	}
	
	
	private float calculateRadius() {
		int cx = getMeasuredWidth() / 2;
		int cy = getMeasuredHeight() / 2;

		Point origo = new Point(cx, cy);
		
		float radiusCompass;
		
		if (cx > cy) {
			radiusCompass = (float) (cy * 0.9);
		}
		else {
			radiusCompass = (float) (cx * 0.9);
		}
		return radiusCompass;
	}
	
	private void init() {
		circlePaint.setStyle(Paint.Style.FILL);
		circlePaint.setStrokeWidth(2);
		circlePaint.setColor(Color.LTGRAY);
		
		circleBorderPaint.setStyle(Paint.Style.STROKE);
		circleBorderPaint.setStrokeWidth(10);
		circleBorderPaint.setColor(Color.BLUE);
		
		northArrowPaint.setStyle(Paint.Style.FILL);
		northArrowPaint.setStrokeWidth(2);
		northArrowPaint.setColor(Color.BLACK);
		
		northArrowBorderPaint.setStyle(Paint.Style.STROKE);
		northArrowBorderPaint.setStrokeWidth(8);
		northArrowBorderPaint.setColor(Color.BLUE);
		
		
		paintSouth.setStyle(Paint.Style.STROKE);
		paintSouth.setStrokeWidth(4);
		paintSouth.setColor(Color.BLUE);

		rectPaint.setStyle(Paint.Style.FILL);
		rectPaint.setColor(Color.DKGRAY);

		
		degreePaint.setStyle(Paint.Style.STROKE);
		degreePaint.setStrokeWidth(2);
		degreePaint.setColor(Color.WHITE);
		degreePaint.setTextSize(30);
		
        
		diamondPaint.setStyle(Paint.Style.STROKE);
		diamondPaint.setStrokeWidth(2);
		diamondPaint.setColor(Color.BLUE);
		

		diamondNorthPaint.setStyle(Paint.Style.FILL);
		diamondNorthPaint.setStrokeWidth(2);
		diamondNorthPaint.setColor(Color.RED);

		diamondSouthPaint.setStyle(Paint.Style.FILL);
		diamondSouthPaint.setStrokeWidth(2);
		diamondSouthPaint.setColor(Color.BLUE);
		
	}

	public void initializeArrow() {
		int arrowSize = Math.round(calculateRadius());		
		
		arrowSize = (arrowSize * 2) - 5;
		
		int arrowM = (arrowSize / 100) * 70;
		int arrowW = (arrowSize / 100) * 32;

		/*
        northArrowPath.moveTo(0, -170);
        northArrowPath.lineTo(-55, 170);
        northArrowPath.lineTo(0, 120);
        northArrowPath.lineTo(55, 170);
        */
        northArrowPath.moveTo(0, -arrowSize);
        northArrowPath.lineTo(-arrowW, arrowSize);
        northArrowPath.lineTo(0, arrowM);
        northArrowPath.lineTo(arrowW, arrowSize);
		
        northArrowPath.close();
	}
	
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		setMeasuredDimension(MeasureSpec.getSize(widthMeasureSpec), MeasureSpec.getSize(heightMeasureSpec));
	}
	
	/**
	 * 01/01/2013
	 * @param cx
	 * @param cy
	 * @param sideA
	 * @param sideB
	 * @param isNorth
	 * @return
	 */
	public static Path constructTriangle(int cx, int cy, int sideA, int sideB, boolean isNorth) {
		int sideC = (int) Math.sqrt((sideA * sideA) + (sideB * sideB));
		
		Point p1 = null;
		Point p2 = null;
		Point p3 = null;
	 
		if (isNorth) {
			p1 = new Point(cx - sideA, cy);
			p2 = new Point(cx, cy - sideB);
			p3 = new Point(cx + sideA, cy);
		}
		else {
			p1 = new Point(cx - sideA, cy);
			p2 = new Point(cx, cy + sideB);
			p3 = new Point(cx + sideA, cy);
		}
	 
		Path path = new Path();
		path.moveTo(p1.x, p1.y);
		path.lineTo(p2.x, p2.y);
		path.lineTo(p3.x, p3.y);
		path.lineTo(p1.x, p1.y);
	 
		return path;
	}
	
    private void calculateCenter() {
        this.center.x = getMeasuredWidth() / 2;
        this.center.y = getMeasuredHeight() / 2;
    }

    private void calculateLength() {
        this.length = (int) (calculateRadius() - 10);
    }

    private void calculateWidth() {
        this.width = this.length / 5;
    }
    
    private int xPolarToRectangular(int centerX, double dist, double angle) {
        return centerX + (int) Math.round(dist * Math.sin(angle));
    }
    
    private int yPolarToRectangular(int centerY, double dist, double angle) {
        return centerY - (int) Math.round(dist * Math.cos(angle));
    }

    private double radian2Degree(double r) {
        return ((180.0 / Math.PI) * r);
    }

    private double degree2Radian(double d) {
        return ((Math.PI / 180.0) * d);
    }
	
	
    private void drawCenter(Canvas c) {
    	centerPaint.setColor(Color.WHITE);
    	centerPaint.setStyle(Paint.Style.FILL);
        // draw center
        int r = width / 5;
        c.drawCircle(center.x,  center.y, r, centerPaint);
    	centerPaint.setColor(Color.BLACK);
    	centerPaint.setStyle(Paint.Style.STROKE);
        c.drawCircle(center.x,  center.y, r, centerPaint);
    }
	

    private void drawDiamond(Canvas canvas, int length, double angle) {
        double angleInRad = degree2Radian(-angle);
        
        // calculated points
        int x1 = xPolarToRectangular(this.center.x, -length, angleInRad);
        int y1 =  yPolarToRectangular(this.center.y, -length, angleInRad);

        int x2 = xPolarToRectangular(this.center.x, width, angleInRad + Math.PI / 2);
        int y2 =  yPolarToRectangular(this.center.y, width, angleInRad + Math.PI / 2);
        
        int x3 = xPolarToRectangular(this.center.x, length, angleInRad);
        int y3 = yPolarToRectangular(this.center.y, length, angleInRad);
        
        int x4 = xPolarToRectangular(this.center.x, width, angleInRad - Math.PI / 2);
        int y4 = yPolarToRectangular(this.center.y, width, angleInRad - Math.PI / 2);
        
        // north
        northArrowPath.reset();
        northArrowPath.moveTo(x3, y3);        
        northArrowPath.lineTo(x2, y2);
        northArrowPath.lineTo(x4, y4);
        northArrowPath.lineTo(x3, y3);
        canvas.drawPath(northArrowPath, this.diamondNorthPaint);
        
        // south
        southArrowPath.reset();
        southArrowPath.moveTo(x1, y1);
        southArrowPath.lineTo(x2, y2);
        southArrowPath.lineTo(x4, y4);
        southArrowPath.lineTo(x1, y1);
        canvas.drawPath(southArrowPath, this.diamondSouthPaint);
        
        // middle slider of diamond
        canvas.drawLine(x2, y2, x4, y4, this.diamondPaint);

        if (this.isShowCenter) {
        	drawCenter(canvas);
        }
        
    }
    

	@Override
	protected void onDraw(Canvas canvas) {
		float radiusCompass = calculateRadius();
        calculateCenter();
        double degree = (360 - direction);
		canvas.drawRect(0, 0, getMeasuredWidth(), getMeasuredHeight(), rectPaint);
		String pole = PolePrefix.determinatePolePrefixByDegree(context, (int) degree);
        canvas.drawText(" " + pole + " " + String.format("%.0f�", degree), 20, 40, degreePaint);

		canvas.drawCircle(center.x, center.y, radiusCompass, circlePaint);
		canvas.drawCircle(center.x, center.y, radiusCompass, circleBorderPaint);
        
		if (isSimpleNorthSouthLine) {

	        this.radius = (int) calculateRadius();
	        calculateLength();
	        calculateWidth();
	        
	        drawDiamond(canvas, this.length, this.direction);
		}
		else {
			if (isFirstDraw) {
				isFirstDraw = false;
				int arrowSize = Math.round(radiusCompass);		
				
				arrowSize = (int) (arrowSize * 0.85);
				
				int arrowM = (arrowSize / 100) * 70;
				int arrowW = (arrowSize / 100) * 45;

		        northArrowPath.moveTo(0, -arrowSize);
		        northArrowPath.lineTo(-arrowW, arrowSize);
		        northArrowPath.lineTo(0, arrowM);
		        northArrowPath.lineTo(arrowW, arrowSize);
				
		        northArrowPath.close();
			}
			
			
	        canvas.translate(center.x, center.y);
	        canvas.rotate(-direction);
	        canvas.drawPath(northArrowPath, northArrowPaint);
	        canvas.drawPath(northArrowPath, northArrowBorderPaint);
		}
	}
	
	
	
	public void updateDirection(float dir) {
		direction = dir;
		invalidate();
	}

}
